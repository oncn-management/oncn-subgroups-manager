package net.styleru.ikomarov.domain_layer.mapping.remote_to_local;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 19.03.17.
 */

public class RemoteToLocalPrincipalObjectMapper implements Function<PrincipalObjectRemoteEntity, PrincipalObjectLocalEntity> {

    @Override
    public PrincipalObjectLocalEntity apply(PrincipalObjectRemoteEntity entity) throws Exception {
        return PrincipalObjectLocalEntity.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getEmail(),
                entity.getDepartment(),
                entity.getPrincipalType().value()
        );
    }
}
