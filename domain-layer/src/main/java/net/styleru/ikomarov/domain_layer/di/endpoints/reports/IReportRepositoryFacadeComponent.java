package net.styleru.ikomarov.domain_layer.di.endpoints.reports;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 18.04.17.
 */

public interface IReportRepositoryFacadeComponent {

    @NonNull
    ReportRepositoryFacadeModule facade();
}
