package net.styleru.ikomarov.presentation_layer.view.sg_permission_wizard;

import android.app.Fragment;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import net.styleru.ikomarov.domain_layer.use_cases.permissions.SGPermissionsBatchCreateUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.students.StudentsGetUseCase;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.adapter.students.SimpleStudentsAdapter;
import net.styleru.ikomarov.presentation_layer.application.ISingletonProvider;
import net.styleru.ikomarov.presentation_layer.presenter.sg_permission_wizard.SGPermissionWizardPresenter;
import net.styleru.ikomarov.presentation_layer.utils.pagination.PaginationListener;
import net.styleru.ikomarov.presentation_layer.utils.pagination.State;
import net.styleru.ikomarov.presentation_layer.utils.ui.ParticipantsChipFactory;
import net.styleru.ikomarov.presentation_layer.view.base.PresenterFragment;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.drawer.IDrawerView;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;
import net.styleru.ikomarov.presentation_layer.view_holder.students_list.SimpleStudentsViewHolder;

import org.apmem.tools.layouts.FlowLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 05.04.17.
 */

public class SGPermissionWizardFragment extends PresenterFragment<ISGPermissionWizardView, SGPermissionWizardPresenter> implements ISGPermissionWizardView, SearchView.OnQueryTextListener, SimpleStudentsViewHolder.InteractionCallbacks, PaginationListener.Callback {

    private RecyclerView list;

    private FlowLayout chipsContainer;

    private SearchView studentsSearchView;

    private AppCompatImageView syncSuccessIndicator;

    private AppCompatImageView syncFailIndicator;

    private ProgressBar syncProgressIndicator;

    private AppCompatButton syncWithServerButton;

    private AppBarLayout appBarLayout;

    private ViewGroup root;

    private SimpleStudentsAdapter adapter;

    private ParticipantsChipFactory chipsFactory;

    private PaginationListener paginationListener;

    private FragmentState state;

    private List<String> pickedStudents = new ArrayList<>();

    public static Fragment newInstance(String notebookId, String sectionGroupId, String sectionGroupName, ArrayList<String> ignoreItems) {
        Fragment fragment = new SGPermissionWizardFragment();

        Bundle args = new Bundle();
        args.putString(FragmentState.KEY_NOTEBOOK_ID, notebookId);
        args.putString(FragmentState.KEY_SECTION_GROUP_ID, sectionGroupId);
        args.putString(FragmentState.KEY_SECTION_GROUP_NAME, sectionGroupName);
        args.putStringArrayList(FragmentState.KEY_IGNORE_ITEMS, ignoreItems);

        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.state = new FragmentState(getArguments(), savedInstanceState);
        super.onCreate(savedInstanceState);
        chipsFactory = new ParticipantsChipFactory(getActivity());
    }

    @Override
    public void addStudents(List<StudentViewObject> students) {
        adapter.add(students);
    }

    @Override
    public void applySyncState(SyncState syncState) {
        switch(syncState) {
            case SYNC_PROGRESS: {
                TransitionManager.beginDelayedTransition(root);
                syncWithServerButton.setVisibility(View.INVISIBLE);
                TransitionManager.beginDelayedTransition(root);
                syncProgressIndicator.setVisibility(View.VISIBLE);
                break;
            }
            case SYNC_SUCCESS: {
                TransitionManager.beginDelayedTransition(root);
                syncProgressIndicator.setVisibility(View.GONE);
                TransitionManager.beginDelayedTransition(root);
                syncSuccessIndicator.setVisibility(View.VISIBLE);
                chipsContainer.removeAllViews();
                adapter.addItemsToIgnore(pickedStudents.toArray(new String[pickedStudents.size()]));
                pickedStudents.clear();
                new Handler().postDelayed(() -> applySyncState(ISGPermissionWizardView.SyncState.DEFAULT), 2500);
                break;
            }
            case SYNC_FAILURE: {
                TransitionManager.beginDelayedTransition(root);
                syncProgressIndicator.setVisibility(View.INVISIBLE);
                syncFailIndicator.setVisibility(View.VISIBLE);
                new Handler().postDelayed(() -> applySyncState(ISGPermissionWizardView.SyncState.DEFAULT), 2500);
                break;
            }
            case SYNC_DELAY: {
                TransitionManager.beginDelayedTransition(root);
                syncProgressIndicator.setVisibility(View.INVISIBLE);
                showError(R.string.notification_operation_delayed);
                new Handler().postDelayed(() -> applySyncState(ISGPermissionWizardView.SyncState.DEFAULT), 2500);
                break;
            }
            case DEFAULT: {
                TransitionManager.beginDelayedTransition(root);
                if(syncFailIndicator.getVisibility() == View.VISIBLE) {
                    syncFailIndicator.setVisibility(View.INVISIBLE);
                }
                if(syncSuccessIndicator.getVisibility() == View.VISIBLE) {
                    syncSuccessIndicator.setVisibility(View.INVISIBLE);
                }
                TransitionManager.beginDelayedTransition(root);
                syncWithServerButton.setVisibility(View.VISIBLE);
                break;
            }
        }
    }

    @Override
    public void showError(@StringRes int error) {
        Snackbar errorSnack = Snackbar.make(getView(), getResources().getString(error), Snackbar.LENGTH_INDEFINITE);
        errorSnack.setAction(R.string.action_ok, (view) -> errorSnack.dismiss());
        errorSnack.setActionTextColor(getResources().getColor(R.color.colorAccent));
        errorSnack.show();
    }

    @Override
    public void showConnectionFailedIndicator() {
        showError(R.string.error_connection_lost);
    }

    @Override
    public void showOperationDelayedDialog() {
        showError(R.string.notification_operation_delayed);
        new Handler().postDelayed(
                () -> {
                    if(getFragmentManager() != null) {
                        getFragmentManager().popBackStack();
                    }
                },
                2000
        );
    }

    @Override
    public void showNoCachedDataPlug() {
        showError(R.string.notification_no_cached_data);
    }

    @Override
    public void showNoDataPlug() {
        showError(R.string.notification_no_data);
    }

    @Override
    public void showNoPermissionsDialog() {
        showError(R.string.notification_no_permissions);
    }

    @Override
    public void showAuthorizationDialog() {
        ((IDrawerView) getActivity()).showAuthenticationActivity();
    }

    @Override
    protected void bindUserInterface(View view) {
        list = (RecyclerView) view.findViewById(R.id.fragment_sg_permission_wizard_recycler_view);
        chipsContainer = (FlowLayout) view.findViewById(R.id.fragment_sg_permission_wizard_participant_chips_container);
        studentsSearchView = (SearchView) view.findViewById(R.id.fragment_sg_permission_wizard_participants_search_view);
        syncWithServerButton = (AppCompatButton) view.findViewById(R.id.fragment_sg_permission_wizard_sync_with_server);
        syncSuccessIndicator = (AppCompatImageView) view.findViewById(R.id.fragment_sg_permission_wizard_sync_successful_indicator);
        syncFailIndicator = (AppCompatImageView) view.findViewById(R.id.fragment_sg_permission_wizard_sync_failed_indicator);
        syncProgressIndicator = (ProgressBar) view.findViewById(R.id.fragment_sg_permission_wizard_sync_with_server_progress_bar);
        appBarLayout = (AppBarLayout) view.findViewById(R.id.fragment_sg_permission_wizard_appbar_layout);
        root = (ViewGroup) view.findViewById(R.id.fragment_sg_permission_wizard_root);

        syncProgressIndicator.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimaryLight), PorterDuff.Mode.SRC_IN);

        adapter = new SimpleStudentsAdapter();
        adapter.addItemsToIgnore(state.getIgnoreItems().toArray(new String[state.getIgnoreItems().size()]));
        adapter.registerCallbacks(this);

        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        list.setAdapter(adapter);

        paginationListener = new PaginationListener(this);

        studentsSearchView.setOnQueryTextListener(this);

        syncWithServerButton.setOnClickListener(this::onSyncWithServerButtonClicked);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(state.getSectionGroupName());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(R.string.title_adding_permissions);
        list.setOnScrollListener(paginationListener);
        if(adapter.getItemCount() == 0) {
            getPresenter().loadStudents(studentsSearchView.getQuery().toString(), 0, paginationListener.getLimit());
        }
    }

    @Override
    public void onPause() {
        list.setOnScrollListener(null);
        super.onPause();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //do nothing, search is in real-time
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.clear();
        getPresenter().loadStudents(newText, 0, paginationListener.getLimit());
        return false;
    }

    @Override
    public void onStudentClicked(int position, StudentViewObject item) {
        chipsContainer.addView(chipsFactory.create(chipsContainer, item.getName(), v -> {
            adapter.removeItemsFromIgnore(item.getId());
            pickedStudents.remove(item.getId());
            adapter.add(item);
        }), new FlowLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        adapter.addItemsToIgnore(item.getId());
        adapter.remove(item.getId());
        pickedStudents.add(item.getId());
    }

    @Override
    public void onListStateChanged(State state) {
        getPresenter().loadStudents(studentsSearchView.getQuery().toString(), adapter.getItemCount(), paginationListener.getLimit());
    }

    @Override
    protected RetainContainer.Factory<SGPermissionWizardPresenter> providePresenterFactory() {
        ISingletonProvider provider = (ISingletonProvider) getActivity().getApplication();
        return new SGPermissionWizardPresenter.Factory(
                new StudentsGetUseCase(
                        provider.studentsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ),
                new SGPermissionsBatchCreateUseCase(
                        new SGPermissionsBatchCreateBroadcastFactory(getActivity().getApplicationContext(), state.getSectionGroupName()),
                        provider.jobEnvironment().provideJobManager(),
                        provider.sgPermissionsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ),
                provider.errorTranslation().provideOneNoteErrorCodeTranslator(),
                provider.logging().provideLoggingManager(),
                state.getNotebookId(),
                state.getSectionGroupId()
        );
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_sg_permission_wizard;
    }

    @Override
    public ISGPermissionWizardView getViewInterface() {
        return this;
    }

    private void onSyncWithServerButtonClicked(View view) {
        if(!pickedStudents.isEmpty()) {
            applySyncState(SyncState.SYNC_PROGRESS);
            getPresenter().createPermissions(pickedStudents);
        } else {
            showError(R.string.notification_no_changes_performed);
        }
    }

    private static class FragmentState {

        private static final String PREFIX = FragmentState.class.getCanonicalName() + ".";

        private static final String KEY_NOTEBOOK_ID = PREFIX + "NOTEBOOK_ID";
        private static final String KEY_SECTION_GROUP_ID = PREFIX + "SECTION_GROUP_ID";
        private static final String KEY_SECTION_GROUP_NAME = PREFIX + "NOTEBOOK_NAME";
        private static final String KEY_IGNORE_ITEMS = PREFIX + "IGNORE_ITEMS";
        private static final String KEY_LIST_STATE = PREFIX + "LIST_STATE";
        private static final String KEY_LIST_ITEMS = PREFIX + "LIST_ITEMS";

        private String notebookId;
        private String sectionGroupId;
        private String sectionGroupName;
        private ArrayList<String> ignoreItems;

        public FragmentState(Bundle arguments, Bundle savedInstanceState) {
            if(arguments != null && arguments.containsKey(KEY_NOTEBOOK_ID) && arguments.containsKey(KEY_SECTION_GROUP_ID) && arguments.containsKey(KEY_SECTION_GROUP_NAME) && arguments.containsKey(KEY_IGNORE_ITEMS)) {
                this.notebookId = arguments.getString(KEY_NOTEBOOK_ID);
                this.sectionGroupId = arguments.getString(KEY_SECTION_GROUP_ID);
                this.sectionGroupName = arguments.getString(KEY_SECTION_GROUP_NAME);
                this.ignoreItems = arguments.getStringArrayList(KEY_IGNORE_ITEMS);
            } else if(savedInstanceState != null && savedInstanceState.containsKey(KEY_NOTEBOOK_ID) && savedInstanceState.containsKey(KEY_SECTION_GROUP_ID) && savedInstanceState.containsKey(KEY_SECTION_GROUP_NAME) && savedInstanceState.containsKey(KEY_IGNORE_ITEMS)) {
                this.notebookId = savedInstanceState.getString(KEY_NOTEBOOK_ID);
                this.sectionGroupId = savedInstanceState.getString(KEY_SECTION_GROUP_ID);
                this.sectionGroupName = savedInstanceState.getString(KEY_SECTION_GROUP_NAME);
                this.ignoreItems = savedInstanceState.getStringArrayList(KEY_IGNORE_ITEMS);
            }
        }

        public void saveState(Bundle outState) {
            outState.putString(KEY_NOTEBOOK_ID, notebookId);
            outState.putString(KEY_SECTION_GROUP_ID, sectionGroupId);
            outState.putString(KEY_SECTION_GROUP_NAME, sectionGroupName);
            outState.putStringArrayList(KEY_IGNORE_ITEMS, ignoreItems);
        }

        public String getNotebookId() {
            return this.notebookId;
        }

        public String getSectionGroupId() {
            return this.sectionGroupId;
        }

        public String getSectionGroupName() {
            return this.sectionGroupName;
        }

        public List<String> getIgnoreItems() {
            return this.ignoreItems;
        }
    }
}
