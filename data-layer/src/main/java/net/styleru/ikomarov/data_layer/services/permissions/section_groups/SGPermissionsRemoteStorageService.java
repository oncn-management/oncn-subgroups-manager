package net.styleru.ikomarov.data_layer.services.permissions.section_groups;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;
import net.styleru.ikomarov.data_layer.services.base.NetworkService;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by i_komarov on 21.03.17.
 */

public class SGPermissionsRemoteStorageService extends NetworkService<ISGPermissionsRemoteStorageServiceActual> implements ISGPermissionsRemoteStorageService {

    private SGPermissionsRemoteStorageService(OkHttpClient client) {
        super(Endpoints.ONE_NOTE_BASE_URL, client);
    }

    @Override
    public Observable<Response<DataResponse<PermissionRemoteEntity>>> create(String userSpacePath, String sectionGroupId, JsonObject permissionJson) {
        return getService().create(userSpacePath, sectionGroupId, permissionJson)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<PermissionRemoteEntity>>> read(String userSpacePath, String sectionGroupId) {
        return getService().read(userSpacePath, sectionGroupId)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<PermissionRemoteEntity>>> read(String userSpacePath, String sectionGroupId, String userId) {
        return getService().read(userSpacePath, sectionGroupId, "userId eq \'" + userId + "\'")
                .compose(handleError());
    }


    @Override
    public Observable<Response<DataResponse<Object>>> delete(String userSpacePath, String sectionGroupId, String permissionId) {
        return getService().delete(userSpacePath, sectionGroupId, permissionId)
                .compose(handleError());
    }

    @Override
    protected ISGPermissionsRemoteStorageServiceActual createService(Retrofit retrofit) {
        return retrofit.create(ISGPermissionsRemoteStorageServiceActual.class);
    }

    @Override
    protected Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(PermissionRemoteEntity.class, new PermissionRemoteEntity.Deserializer())
                .registerTypeAdapter(PermissionRemoteEntity.class, new PermissionRemoteEntity.Serializer())
                .create();
    }

    public static final class Factory {

        private Factory() {

        }

        public static ISGPermissionsRemoteStorageService create(OkHttpClient client) {
            return new SGPermissionsRemoteStorageService(client);
        }
    }
}
