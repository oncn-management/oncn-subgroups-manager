package net.styleru.ikomarov.domain_layer.di.level_3.session;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.manager.session.ISessionManager;
import net.styleru.ikomarov.data_layer.manager.session.SessionManager;
import net.styleru.ikomarov.domain_layer.di.level_1.AppComponent;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;

/**
 * Created by i_komarov on 13.03.17.
 */

public class SessionModule {

    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent component;

    @NonNull
    private final IAppComponent appComponent;

    @Nullable
    private volatile ISessionManager sessionManager;

    public SessionModule(@NonNull IAppComponent appComponent, @NonNull IEnvironmentComponent component) {
        this.appComponent = appComponent;
        this.component = component;
    }

    @NonNull
    public ISessionManager provideSessionManager() {
        ISessionManager localInstance = sessionManager;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = sessionManager;
                if(localInstance == null) {
                    localInstance = sessionManager = new SessionManager(appComponent.app().provideContext(), component.network().provideNetworkManager());
                }
            }
        }

        return localInstance;
    }
}
