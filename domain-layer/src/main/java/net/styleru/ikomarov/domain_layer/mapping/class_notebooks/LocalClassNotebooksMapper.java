package net.styleru.ikomarov.domain_layer.mapping.class_notebooks;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class LocalClassNotebooksMapper implements Function<List<ClassNotebookLocalEntity>, List<ClassNotebookDTO>> {

    @Override
    public List<ClassNotebookDTO> apply(List<ClassNotebookLocalEntity> classNotebookLocalEntities) throws Exception {
        return Observable.fromIterable(classNotebookLocalEntities)
                .map(new LocalClassNotebookMapper())
                .toList()
                .blockingGet();
    }
}
