package net.styleru.ikomarov.domain_layer.dto;

import java.util.Date;

/**
 * Created by i_komarov on 20.02.17.
 */

public class SectionDTO {

    private String id;

    private String name;

    private String createdBy;
    private Date createdTime;

    private String lastModifiedBy;
    private Date lastModifiedTime;

    public static SectionDTO newInstance(String id, String name, String createdBy, Date createdTime, String lastModifiedBy, Date lastModifiedTime) {
        SectionDTO object = new SectionDTO();

        object.id = id;
        object.name = name;
        object.createdBy = createdBy;
        object.createdTime = createdTime;
        object.lastModifiedBy = lastModifiedBy;
        object.lastModifiedTime = lastModifiedTime;

        return object;
    }

    private SectionDTO() {

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }
}
