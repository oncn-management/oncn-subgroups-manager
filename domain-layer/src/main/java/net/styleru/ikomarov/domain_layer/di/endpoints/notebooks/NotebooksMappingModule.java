package net.styleru.ikomarov.domain_layer.di.endpoints.notebooks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ClassNotebookRemoteEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.mapping.remote.notebooks.ClassNotebooksRemoteEntityMapping;

/**
 * Created by i_komarov on 13.03.17.
 */

public class NotebooksMappingModule {

    private final Object lock = new Object();

    @Nullable
    private volatile IRemoteMapping<ClassNotebookRemoteEntity> mapping;

    @NonNull
    public IRemoteMapping<ClassNotebookRemoteEntity> provideMapping() {
        IRemoteMapping<ClassNotebookRemoteEntity> localInstance = mapping;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = mapping;
                if(localInstance == null) {
                    localInstance = mapping = new ClassNotebooksRemoteEntityMapping();
                }
            }
        }

        return localInstance;
    }
}
