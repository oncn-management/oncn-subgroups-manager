package net.styleru.ikomarov.data_layer.services.class_notebooks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ClassNotebookRemoteEntity;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;
import net.styleru.ikomarov.data_layer.services.base.NetworkService;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by i_komarov on 26.02.17.
 */

public class ClassNotebooksRemoteStorageService extends NetworkService<IClassNotebooksRemoteStorageService> implements IClassNotebooksRemoteStorageService {

    private ClassNotebooksRemoteStorageService(OkHttpClient client) {
        super(Endpoints.ONE_NOTE_BASE_URL, client);
    }

    @Override
    public Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> create(String userId, JsonObject notebook) {
        return getService().create(userId, notebook)
                .compose(this.handleError());
    }

    @Override
    public Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> read(String userId, String notebookId) {
        return getService().read(userId, notebookId)
                .compose(this.handleError());
    }

    @Override
    public Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> read(String userId, int offset, int limit) {
        return getService().read(userId, offset, limit)
                .compose(this.handleError());
    }

    @Override
    public Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> read(String userId, int offset, int limit, String orderQuery) {
        return getService().read(userId, offset, limit, orderQuery)
                .compose(this.handleError());
    }

    @Override
    public Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> read(String userId, int offset, int limit, String orderQuery, String filterQuery) {
        return getService().read(userId, offset, limit, orderQuery, filterQuery)
                .compose(this.handleError());
    }

    @Override
    public Observable<Response<DataResponse<PrincipalObjectRemoteEntity>>> updateAddTeacher(String userId, String notebookId, JsonObject teacher) {
        return getService().updateAddTeacher(userId, notebookId, teacher)
                .compose(this.handleError());
    }

    @Override
    public Observable<Response<DataResponse<PrincipalObjectRemoteEntity>>> updateAddStudent(String userId, String notebookId, JsonObject student) {
        return getService().updateAddStudent(userId, notebookId, student)
                .compose(this.handleError());
    }

    @Override
    public Observable<Response<DataResponse<Object>>> updateRemoveTeacher(String userId, String notebookId, String teacherId) {
        return getService().updateRemoveTeacher(userId, notebookId, teacherId)
                .compose(this.<Response<DataResponse<Object>>>handleError());
    }

    @Override
    public Observable<Response<DataResponse<Object>>> updateRemoveStudent(String userId, String notebookId, String studentId) {
        return getService().updateRemoveStudent(userId, notebookId, studentId)
                .compose(this.handleError());
    }

    @Override
    public Observable<Response<DataResponse<Object>>> updateAddSections(String userId, String notebookId, ClassNotebookRemoteEntity.SectionsInsertRequestBody sections) {
        return getService().updateAddSections(userId, notebookId, sections)
                .compose(this.handleError());
    }

    @Override
    public Observable<Response<DataResponse<Object>>> delete(String userId, String notebookId) {
        return getService().delete(userId, notebookId)
                .compose(this.handleError());
    }

    @Override
    protected IClassNotebooksRemoteStorageService createService(Retrofit retrofit) {
        return retrofit.create(IClassNotebooksRemoteStorageService.class);
    }

    @Override
    protected Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(DataResponse.class, new DataResponse.Deserializer())
                .registerTypeAdapter(ClassNotebookRemoteEntity.class, new ClassNotebookRemoteEntity.Deserializer())
                .registerTypeAdapter(ClassNotebookRemoteEntity.class, new ClassNotebookRemoteEntity.Serializer())
                .setDateFormat(DateTimeContract.API_DATE_FORMAT)
                .create();
    }

    public static final class Factory {

        private Factory() {

        }

        public static IClassNotebooksRemoteStorageService create(OkHttpClient client) {
            return new ClassNotebooksRemoteStorageService(client);
        }
    }
}
