package net.styleru.ikomarov.domain_layer.dto;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.base.ReportSubgroupDTO;

/**
 * Created by i_komarov on 17.04.17.
 */

public class PermissionReportSubgroupDTO extends ReportSubgroupDTO {

    @NonNull
    private final String permission;

    public PermissionReportSubgroupDTO(@NonNull String name, @NonNull String permission) {
        super(name);
        this.permission = permission;
    }

    @NonNull
    public String getPermission() {
        return permission;
    }
}
