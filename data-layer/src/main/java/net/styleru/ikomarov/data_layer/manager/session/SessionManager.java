package net.styleru.ikomarov.data_layer.manager.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import net.styleru.ikomarov.data_layer.BuildConfig;
import net.styleru.ikomarov.data_layer.contracts.network.NetworkStatus;
import net.styleru.ikomarov.data_layer.contracts.session.SessionStatus;
import net.styleru.ikomarov.data_layer.entities.session.AzureToken;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.network.INetworkManager;
import net.styleru.ikomarov.data_layer.services.session.ISessionService;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by i_komarov on 26.02.17.
 */

public class SessionManager implements ISessionManager {

     private static final class Config {

         static final String PREFIX = SessionManager.class.getCanonicalName() + ".";

         static final String STORAGE_NAME = PREFIX + "STORAGE";

         static final String KEY_REFRESH_TOKEN = PREFIX + "REFRESH_TOKEN";

         static final String KEY_ONENOTE_TOKEN = PREFIX + "ONENOTE_TOKEN";

         static final String KEY_GRAPH_TOKEN = PREFIX + "GRAPH_TOKEN";
    }

    private final Object lock = new Object();

    private final INetworkManager networkManager;

    private final ISessionService service;

    private final Context context;

    private volatile boolean isSessionActive = false;
    private volatile boolean isAuthenticating = false;

    private Map<String, IAuthenticationCallbacks> sessionCallbacks;

    private volatile String refreshToken;
    private volatile String oneNoteToken;
    private volatile String graphToken;

    public SessionManager(Context context, INetworkManager networkManager) {
        this.networkManager = networkManager;
        this.service = new AuthorizationClientFactory().get().create(ISessionService.class);
        this.context = context;
        this.sessionCallbacks = new HashMap<>();

        this.postStorageAction((preferences) -> {
            this.refreshToken = preferences.getString(Config.KEY_REFRESH_TOKEN, null);
            this.oneNoteToken = preferences.getString(Config.KEY_ONENOTE_TOKEN, null);
            this.graphToken = preferences.getString(Config.KEY_GRAPH_TOKEN, null);
            this.isSessionActive = this.refreshToken != null;
        });
    }

    public SessionManager(Context context, INetworkManager networkManager, String refreshToken) {
        this.networkManager = networkManager;
        this.service = new AuthorizationClientFactory().get().create(ISessionService.class);
        this.context = context;
        this.sessionCallbacks = new HashMap<>();
        this.refreshToken = refreshToken;
        this.isSessionActive = this.refreshToken != null;
    }

    @Override
    public Observable<SessionStatus> getCurrentSessionStatus() {
        return Observable.just(isAuthenticating ? SessionStatus.AUTHENTICATING :
                isSessionActive ? SessionStatus.ACTIVE : SessionStatus.INACTIVE);
    }

    @Override
    public Observable<String> getSessionToken(final String resourceType) {
        if(resourceType.equals(RESOURCE_GRAPH) && graphToken != null) {
            return Observable.just(graphToken);
        } else if(resourceType.equals(RESOURCE_ONENOTE) && oneNoteToken != null) {
            return Observable.just(oneNoteToken);
        } else if(refreshToken != null) {
            return refreshAndGetSessionToken(resourceType);
        } else {
            publishDeauthentication();
            return Observable.error(new ExceptionBundle(ExceptionBundle.Reason.NO_SESSION));
        }
    }

    @Override
    public String blockingGetSessionToken(String resourceType) throws ExceptionBundle {
        return getSessionToken(resourceType)
                .blockingFirst();
    }

    @Override
    public Observable<String> refreshAndGetSessionToken(final String resourceType) {
        return Observable.create(new SessionTokenRefreshObservableOnSubscribe(service, BuildConfig.AZURE_AD_API_KEY, BuildConfig.AZURE_AD_REDIRECT_URI, refreshToken, resourceType))
                .doOnNext((token) -> {
                    if(resourceType.equals(RESOURCE_GRAPH)) {
                        graphToken = token.getAccessToken();
                        postStorageAction((preferences) -> {
                            preferences.edit().putString(Config.KEY_GRAPH_TOKEN, graphToken).apply();
                        });
                    } else if(resourceType.equals(RESOURCE_ONENOTE)) {
                        oneNoteToken = token.getAccessToken();
                        postStorageAction((preferences) -> {
                            preferences.edit().putString(Config.KEY_ONENOTE_TOKEN, oneNoteToken).apply();
                        });
                    }
                })
                .map(AzureToken::getAccessToken);
    }

    @Override
    public String blockingRefreshAndGetSessionToken(String resourceType) {
        return refreshAndGetSessionToken(resourceType)
                .blockingFirst();
    }

    @Override
    public void registerCallback(String key, IAuthenticationCallbacks callbacks) {
        synchronized (lock) {
            this.sessionCallbacks.put(key, callbacks);
        }
    }

    @Override
    public void unregisterCallback(String key) {
        synchronized (lock) {
            this.sessionCallbacks.remove(key);
        }
    }

    @Override
    public void deauthenticate() {
        publishDeauthentication();
        isSessionActive = false;
        oneNoteToken = null;
        graphToken = null;
    }

    @Override
    public void authenticate(final String code) throws ExceptionBundle {
        if(!isSessionActive && !isAuthenticating) {
            if (networkManager.getCurrentNetworkStatus() != NetworkStatus.DISCONNECTED) {
                isAuthenticating = true;
                Observable.create(new SessionTokenTradeObservableOnSubscribe(service, BuildConfig.AZURE_AD_API_KEY, BuildConfig.AZURE_AD_REDIRECT_URI, RESOURCE_ONENOTE, code))
                        .doOnNext((token) -> {
                            refreshToken = token.getRefreshToken();
                            oneNoteToken = token.getAccessToken();
                            //publishAuthenticationSuccess();
                            postStorageAction((preferences) -> {
                                preferences.edit().putString(Config.KEY_REFRESH_TOKEN, refreshToken)
                                        .putString(Config.KEY_ONENOTE_TOKEN, oneNoteToken)
                                        .apply();
                            });
                        })
                        .doOnError((thr) -> {
                            ExceptionBundle wrappedException;
                            if (thr instanceof ExceptionBundle) {
                                wrappedException = (ExceptionBundle) thr;
                            } else {
                                wrappedException = new ExceptionBundle(ExceptionBundle.Reason.INTERNAL_DATA);
                                wrappedException.addThrowable(ExceptionBundle.InternalErrorContract.KEY_THROWABLE, thr);
                            }

                            publishAuthenticationFailure(wrappedException);
                        })
                        .subscribe(token -> publishAuthenticationSuccess());
            } else {
                throw new ExceptionBundle(ExceptionBundle.Reason.NETWORK_UNAVAILABLE);
            }
        }
    }

    private void publishAuthenticationFailure(ExceptionBundle error) {
        isAuthenticating = false;
        isSessionActive = false;
        synchronized (lock) {
            for(IAuthenticationCallbacks callback : sessionCallbacks.values()) {
                callback.onAuthenticationFailed(error);
            }
        }
    }

    private void publishAuthenticationSuccess() {
        isAuthenticating = false;
        isSessionActive = true;
        synchronized (lock) {
            for(IAuthenticationCallbacks callback : sessionCallbacks.values()) {
                callback.onAuthenticated();
            }
        }
    }

    private void publishDeauthentication() {
        postStorageAction((preferences) -> {
            preferences.edit().remove(Config.KEY_REFRESH_TOKEN)
                    .remove(Config.KEY_GRAPH_TOKEN)
                    .remove(Config.KEY_ONENOTE_TOKEN)
                    .commit();
        });
        isAuthenticating = false;
        isSessionActive = false;
        synchronized (lock) {
            for(IAuthenticationCallbacks callback : sessionCallbacks.values()) {
                callback.onDeauthenticated();
            }
        }
    }

    private void postStorageAction(StorageAction action) {
        SharedPreferences preferences;
        if(null != (preferences = context.getSharedPreferences(Config.STORAGE_NAME, Context.MODE_PRIVATE))) {
            action.apply(preferences);
        }
    }

    public interface IAuthenticationCallbacks {

        void onAuthenticated();

        void onDeauthenticated();

        void onAuthenticationFailed(ExceptionBundle error);
    }

    private interface StorageAction {

        void apply(SharedPreferences preferences);
    }

    private static final class AuthorizationClientFactory {

        private static final int READ_TIMEOUT = 60;
        private static final int WRITE_TIMEOUT = 15;
        private static final int CONNECT_TIMEOUT = 15;

        private static final Object lock = new Object();

        private static volatile Retrofit retrofit;

        private AuthorizationClientFactory() {

        }

        public Retrofit get() {
            Retrofit localInstance = retrofit;
            if(localInstance == null) {
                synchronized (lock) {
                    localInstance = retrofit;
                    if(localInstance == null) {
                        localInstance = retrofit = buildRetrofit();
                    }
                }
            }

            return localInstance;
        }

        @NonNull
        private Retrofit buildRetrofit() {
            return new Retrofit.Builder()
                    .baseUrl(ISessionService.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(buildClient())
                    .build();
        }

        @NonNull
        private OkHttpClient buildClient() {
            return new OkHttpClient.Builder()
                    .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                    .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                    .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                    .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                    .build();
        }
    }
}
