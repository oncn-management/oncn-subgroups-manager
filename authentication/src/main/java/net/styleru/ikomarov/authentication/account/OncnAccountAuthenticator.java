package net.styleru.ikomarov.authentication.account;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;

import net.styleru.ikomarov.authentication.BuildConfig;
import net.styleru.ikomarov.authentication.dto.entities.AzureToken;
import net.styleru.ikomarov.authentication.login.LoginActivity;
import net.styleru.ikomarov.authentication.http.RetrofitFactory;
import net.styleru.ikomarov.authentication.web_interfaces.OAuth2;

/**
 * Created by i_komarov on 18.02.17.
 */

public class OncnAccountAuthenticator extends AbstractAccountAuthenticator {

    private static final String TAG = "OncnAccountAuthenticator";

    private static final String KEY_ACCESS_TOKEN_TTL = TAG + ".ACCESS_TOKEN_TTL";

    private final Context context;

    private OAuth2 oauth;

    private static void log(String msg) {
        Log.d(TAG, msg);
    }

    public OncnAccountAuthenticator(Context context) {
        super(context);
        this.context = context;
        this.oauth = RetrofitFactory.buildRetrofit().create(OAuth2.class);
    }

    @Override
    public Bundle editProperties(AccountAuthenticatorResponse response, String accountType) {
        return null;
    }

    @Override
    public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType, String[] requiredFeatures, Bundle options) throws NetworkErrorException {
        final Intent intent = new Intent(context, LoginActivity.class);

        intent.putExtra(LoginActivity.KEY_TOKEN_TYPE, accountType);
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);

        final Bundle bundle = new Bundle();

        if (options != null) {
            bundle.putAll(options);
        }

        bundle.putParcelable(AccountManager.KEY_INTENT, intent);

        return bundle;
    }

    @Override
    public Bundle confirmCredentials(AccountAuthenticatorResponse response, Account account, Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        String resource = "";

        if(authTokenType.equals(OncnAccount.TOKEN_GRAPH)) {
            resource = OncnAccount.RESOURCE_GRAPH;
        } else if(authTokenType.equals(OncnAccount.TOKEN_ONE_NOTE)) {
            resource = OncnAccount.RESOURCE_ONENOTE;
        }

        final Bundle result = new Bundle();

        String authToken = null;

        AccountManager am = AccountManager.get(context);
        authToken = am.peekAuthToken(account, authTokenType);

        if(TextUtils.isEmpty(authToken)) {
            String refreshToken = am.peekAuthToken(account, OncnAccount.TOKEN_REFRESH);
            if(!TextUtils.isEmpty(refreshToken)) {
                log("Refresh token was not null, requested token was null; loading requested token by refresh token");
                AzureToken token = null;

                try {
                    token = oauth.refreshToken(OAuth2.GRANT_TYPE_REFRESH_TOKEN, BuildConfig.AZURE_AD_API_KEY, BuildConfig.AZURE_AD_REDIRECT_URI, refreshToken, resource).execute().body();
                } catch(Exception e) {
                    e.printStackTrace();
                }
                log("Refresh token was not null, requested token was null; new requested token: " + token.getAccessToken());
                am.setAuthToken(account, authTokenType, token.getAccessToken());
                am.setAuthToken(account, OncnAccount.TOKEN_REFRESH, token.getRefreshToken());
                result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
                result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
                result.putString(AccountManager.KEY_AUTHTOKEN, token.getAccessToken());
            } else {
                log("Both refresh token and requested token were null; creating an authenticator activity");
                final Intent intent = new Intent(context, LoginActivity.class);
                intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
                intent.putExtra(LoginActivity.KEY_TOKEN_TYPE, authTokenType);
                result.putParcelable(AccountManager.KEY_INTENT, intent);
            }
        } else {
            log("Requested token was not null; was returned");
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
        }

        return result;
    }

    @Override
    public String getAuthTokenLabel(String authTokenType) {
        return null;
    }

    @Override
    public Bundle updateCredentials(AccountAuthenticatorResponse response, Account account, String authTokenType, Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle hasFeatures(AccountAuthenticatorResponse response, Account account, String[] features) throws NetworkErrorException {
        return null;
    }
}
