package net.styleru.ikomarov.presentation_layer.view.section_groups_list;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.UUID;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupViewObject implements Parcelable {

    private String id;
    private Date lastModified;
    private String name;
    private boolean isSynchronized;

    public SectionGroupViewObject(String id, Date lastModified, String name) {
        this.id = id;
        this.lastModified = lastModified;
        this.name = name;
        this.isSynchronized = true;
    }

    public SectionGroupViewObject(UUID uuid, Date lastModified, String name) {
        this.id = uuid.toString();
        this.lastModified = lastModified;
        this.name = name;
        this.isSynchronized = false;
    }

    private SectionGroupViewObject(Parcel in) {
        id = in.readString();
        long lastModifiedTime = in.readLong();
        lastModified = lastModifiedTime != -1L ? new Date(lastModifiedTime) : null;
        name = in.readString();
        isSynchronized = in.readByte() != 0;
    }

    public String getId() {
        return id;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public String getName() {
        return name;
    }

    public boolean isSynchronized() {
        return isSynchronized;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeLong(lastModified != null ? lastModified.getTime() : -1);
        dest.writeString(name);
        dest.writeByte((byte) (isSynchronized ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SectionGroupViewObject> CREATOR = new Creator<SectionGroupViewObject>() {
        @Override
        public SectionGroupViewObject createFromParcel(Parcel in) {
            return new SectionGroupViewObject(in);
        }

        @Override
        public SectionGroupViewObject[] newArray(int size) {
            return new SectionGroupViewObject[size];
        }
    };
}
