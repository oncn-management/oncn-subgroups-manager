package net.styleru.ikomarov.domain_layer.di.endpoints.students;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.repository.students.IStudentsRepository;
import net.styleru.ikomarov.domain_layer.repository.students.StudentsRepository;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsRepositoryModule {

    private final Object lock = new Object();

    @NonNull
    private final IStudentsEnvironmentComponent component;

    @Nullable
    private volatile IStudentsRepository repository;

    public StudentsRepositoryModule(@NonNull IStudentsEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public IStudentsRepository provideRepository() {
        IStudentsRepository localInstance = this.repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = StudentsRepository.Factory.create(
                            component.localService().provideLocalService(),
                            component.remoteService().provideRemoteService(),
                            component.cacheComponent().strategy().provideCacheStrategy(),
                            component.mapping().provideMapping()
                    );
                }
            }
        }

        return localInstance;
    }
}
