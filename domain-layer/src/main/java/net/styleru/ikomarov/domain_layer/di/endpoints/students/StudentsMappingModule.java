package net.styleru.ikomarov.domain_layer.di.endpoints.students;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.mapping.remote.notebooks.PrincipalObjectsRemoteEntityMapping;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsMappingModule {

    private final Object lock = new Object();

    @Nullable
    private volatile IRemoteMapping<PrincipalObjectRemoteEntity> mapping;

    @NonNull
    public IRemoteMapping<PrincipalObjectRemoteEntity> provideMapping() {
        IRemoteMapping<PrincipalObjectRemoteEntity> localInstance = mapping;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = mapping;
                if(localInstance == null) {
                    localInstance = mapping = new PrincipalObjectsRemoteEntityMapping();
                }
            }
        }

        return localInstance;
    }
}
