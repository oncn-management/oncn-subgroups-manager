package net.styleru.ikomarov.data_layer.services.permissions.section_groups;

import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 21.03.17.
 */

public interface ISGPermissionsLocalStorageService {

    Observable<PutResult> create(PermissionLocalEntity entity);

    Observable<List<PermissionLocalEntity>> read(String sectionGroupId);

    Observable<List<PermissionLocalEntity>> read(String sectionGroupId, String userId);

    Observable<DeleteResult> delete(String sectionGroupId, String permissionId);
}
