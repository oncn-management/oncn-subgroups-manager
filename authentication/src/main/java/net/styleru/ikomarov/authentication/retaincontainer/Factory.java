package net.styleru.ikomarov.authentication.retaincontainer;

/**
 * Created by i_komarov on 18.02.17.
 */

public interface Factory<T> {

    T create();
}
