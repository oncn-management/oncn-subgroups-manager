package net.styleru.ikomarov.data_layer.entities.remote.notebooks;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static net.styleru.ikomarov.data_layer.mapping.remote.notebooks.ClassNotebooksRemoteEntityMapping.*;

/**
 * Created by i_komarov on 14.02.17.
 */

public class ClassNotebookRemoteEntity {

    @SerializedName(FIELD_CREATED_BY)
    private String createdBy;
    @SerializedName(FIELD_CREATED_TIME)
    private Date createdTime;
    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_IS_DEFAULT)
    private Boolean isDefault;
    @SerializedName(FIELD_IS_SHARED)
    private Boolean isShared;
    @SerializedName(FIELD_LAST_MODIFIED_BY)
    private String lastModifiedBy;
    @SerializedName(FIELD_LAST_MODIFIED_TIME)
    private Date lastModifiedTime;
    @SerializedName(FIELD_LINKS)
    private NotebookLinksRemoteEntity links;
    @SerializedName(FIELD_NAME)
    private String name;
    @SerializedName(FIELD_SECTION_GROUPS_URL)
    private String sectionGroupsUrl;
    @SerializedName(FIELD_SECTIONS_URL)
    private String sectionsUrl;
    @SerializedName(FIELD_SELF)
    private String self;
    @SerializedName(FIELD_USER_ROLE)
    private UserRole role;
    @SerializedName(FIELD_HAS_TEACHER_ONLY_SECTION_GROUP)
    private Boolean hasTeacherOnlySectionGroup;
    @SerializedName(FIELD_IS_COLLABORATION_SPACE_LOCKED)
    private Boolean isCollaborationSpaceLocked;


    private ClassNotebookRemoteEntity() {

    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public String getId() {
        return id;
    }

    public Boolean isDefault() {
        return isDefault;
    }

    public Boolean isShared() {
        return isShared;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    public NotebookLinksRemoteEntity getLinks() {
        return links;
    }

    public String getName() {
        return name;
    }

    public String getSectionGroupsUrl() {
        return sectionGroupsUrl;
    }

    public String getSectionsUrl() {
        return sectionsUrl;
    }

    public String getSelf() {
        return self;
    }

    public UserRole getRole() {
        return role;
    }

    public Boolean hasTeacherOnlySectionGroup() {
        return hasTeacherOnlySectionGroup;
    }

    public Boolean getCollaborationSpaceLocked() {
        return isCollaborationSpaceLocked;
    }

    @Override
    public String toString() {
        return "ClassNotebookRemoteEntity{" +
                "createdBy='" + createdBy + '\'' +
                ", createdTime=" + createdTime +
                ", id='" + id + '\'' +
                ", isDefault=" + isDefault +
                ", isShared=" + isShared +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", lastModifiedTime=" + lastModifiedTime +
                ", links=" + links +
                ", name='" + name + '\'' +
                ", sectionGroupsUrl='" + sectionGroupsUrl + '\'' +
                ", sectionsUrl='" + sectionsUrl + '\'' +
                ", self='" + self + '\'' +
                ", role=" + role +
                ", hasTeacherOnlySectionGroup=" + hasTeacherOnlySectionGroup +
                ", isCollaborationSpaceLocked=" + isCollaborationSpaceLocked +
                '}';
    }

    public static class SectionsInsertRequestBody {

        @SerializedName(FIELD_SECTION_IDS)
        private List<String> sectionIds;

        SectionsInsertRequestBody(List<String> sectionIds) {
            this.sectionIds = sectionIds;
        }

        public List<String> getSectionIds() {
            return this.sectionIds;
        }

        @Override
        public String toString() {
            return "SectionsInsertRequestBody{" +
                    "sectionIds=" + sectionIds +
                    '}';
        }
    }

    public static class Deserializer implements JsonDeserializer<ClassNotebookRemoteEntity> {

        @Override
        public ClassNotebookRemoteEntity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject root = json.getAsJsonObject();

            Gson gson = new GsonBuilder().create();

            ClassNotebookRemoteEntity entity = new ClassNotebookRemoteEntity();

            JsonElement createdBy = root.get(FIELD_CREATED_BY);
            if(!createdBy.isJsonNull()) {
                entity.createdBy = createdBy.getAsString();
            }
            JsonElement lastModifiedBy = root.get(FIELD_LAST_MODIFIED_BY);
            if(!lastModifiedBy.isJsonNull()) {
                entity.lastModifiedBy = lastModifiedBy.getAsString();
            }
            JsonElement id = root.get(FIELD_ID);
            if(!id.isJsonNull()) {
                entity.id = id.getAsString();
            }
            JsonElement isDefault = root.get(FIELD_IS_DEFAULT);
            if(!isDefault.isJsonNull()) {
                entity.isDefault = isDefault.getAsBoolean();
            }
            JsonElement isShared = root.get(FIELD_IS_SHARED);
            if(!isShared.isJsonNull()) {
                entity.isShared = isShared.getAsBoolean();
            }
            JsonElement name = root.get(FIELD_NAME);
            if(!name.isJsonNull()) {
                entity.name = name.getAsString();
            }
            JsonElement sectionGroupsUrl = root.get(FIELD_SECTION_GROUPS_URL);
            if(!sectionGroupsUrl.isJsonNull()) {
                entity.sectionGroupsUrl = sectionGroupsUrl.getAsString();
            }
            JsonElement sectionsUrl = root.get(FIELD_SECTIONS_URL);
            if(!sectionsUrl.isJsonNull()) {
                entity.sectionsUrl = sectionsUrl.getAsString();
            }
            JsonElement self = root.get(FIELD_SELF);
            if(!self.isJsonNull()) {
                entity.self = self.getAsString();
            }
            JsonElement hasTeacherOnlySectionGroup = root.get(FIELD_HAS_TEACHER_ONLY_SECTION_GROUP);
            if(!hasTeacherOnlySectionGroup.isJsonNull()) {
                entity.hasTeacherOnlySectionGroup = hasTeacherOnlySectionGroup.getAsBoolean();
            }
            JsonElement isCollaborationSpaceLocked = root.get(FIELD_IS_COLLABORATION_SPACE_LOCKED);
            if(!isCollaborationSpaceLocked.isJsonNull()) {
                entity.isCollaborationSpaceLocked = isCollaborationSpaceLocked.getAsBoolean();
            }
            JsonElement role = root.get(FIELD_USER_ROLE);
            if(!role.isJsonNull()) {
                entity.role = UserRole.forValue(role.getAsString());
            } else {
                entity.role = UserRole.NONE;
            }
            JsonElement links = root.get(FIELD_LINKS);
            if(!links.isJsonNull()) {
                entity.links = gson.fromJson(links, NotebookLinksRemoteEntity.class);
            }

            try {
                entity.createdTime = new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).parse(root.get(FIELD_CREATED_TIME).getAsString());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            try {
                entity.lastModifiedTime = new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).parse(root.get(FIELD_LAST_MODIFIED_TIME).getAsString());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return entity;
        }
    }

    public static class Serializer implements JsonSerializer<ClassNotebookRemoteEntity> {

        @Override
        public JsonElement serialize(ClassNotebookRemoteEntity src, Type typeOfSrc, JsonSerializationContext context) {

            JsonObject json = new JsonObject();

            json.addProperty(FIELD_NAME, src.name);
            json.addProperty(FIELD_HAS_TEACHER_ONLY_SECTION_GROUP, src.hasTeacherOnlySectionGroup);

            return json;
        }
    }
}
