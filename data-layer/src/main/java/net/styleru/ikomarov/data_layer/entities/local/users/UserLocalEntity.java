package net.styleru.ikomarov.data_layer.entities.local.users;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.ikomarov.data_layer.mapping.local.users.UsersLocalEntityMapping;

import static net.styleru.ikomarov.data_layer.mapping.local.users.UsersLocalEntityMapping.*;

/**
 * Created by i_komarov on 25.02.17.
 */

@StorIOSQLiteType(table = UsersLocalEntityMapping.TABLE)
public class UserLocalEntity {

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ID, key = true)
    String id;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_DISPLAY_NAME, ignoreNull = true)
    String displayName;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_GIVEN_NAME, ignoreNull = true)
    String givenName;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_JOB_TITLE, ignoreNull = true)
    String jobTitle;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_MAIL, ignoreNull = true)
    String mail;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_MOBILE_PHONE, ignoreNull = true)
    String mobilePhone;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_OFFICE_LOCATION, ignoreNull = true)
    String officeLocation;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_PREFERRED_LANGUAGE, ignoreNull = true)
    String preferredLanguage;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_LAST_NAME, ignoreNull = true)
    String lastName;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_PRINCIPAL_NAME, ignoreNull = true)
    String principalName;

    public static UserLocalEntity newInstance(String id, String displayName, String givenName, String jobTitle, String mail, String mobilePhone, String officeLocation, String preferredLanguage, String lastName, String principalName) {
        UserLocalEntity entity = new UserLocalEntity();

        entity.id = id;
        entity.displayName = displayName;
        entity.givenName = givenName;
        entity.jobTitle = jobTitle;
        entity.mail = mail;
        entity.mobilePhone = mobilePhone;
        entity.officeLocation = officeLocation;
        entity.preferredLanguage = preferredLanguage;
        entity.lastName = lastName;
        entity.principalName = principalName;

        return entity;
    }

    UserLocalEntity() {

    }

    @Nullable
    public String getId() {
        return id;
    }

    @Nullable
    public String getDisplayName() {
        return displayName;
    }

    @Nullable
    public String getGivenName() {
        return givenName;
    }

    @Nullable
    public String getJobTitle() {
        return jobTitle;
    }

    @NonNull
    public String getMail() {
        return mail;
    }

    @Nullable
    public String getMobilePhone() {
        return mobilePhone;
    }

    @Nullable
    public String getOfficeLocation() {
        return officeLocation;
    }

    @Nullable
    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    @Nullable
    public String getLastName() {
        return lastName;
    }

    @NonNull
    public String getPrincipalName() {
        return principalName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserLocalEntity that = (UserLocalEntity) o;

        if (!id.equals(that.id)) return false;
        if (displayName != null ? !displayName.equals(that.displayName) : that.displayName != null)
            return false;
        if (givenName != null ? !givenName.equals(that.givenName) : that.givenName != null)
            return false;
        if (jobTitle != null ? !jobTitle.equals(that.jobTitle) : that.jobTitle != null)
            return false;
        if (!mail.equals(that.mail)) return false;
        if (mobilePhone != null ? !mobilePhone.equals(that.mobilePhone) : that.mobilePhone != null)
            return false;
        if (officeLocation != null ? !officeLocation.equals(that.officeLocation) : that.officeLocation != null)
            return false;
        if (preferredLanguage != null ? !preferredLanguage.equals(that.preferredLanguage) : that.preferredLanguage != null)
            return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null)
            return false;
        return principalName.equals(that.principalName);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (displayName != null ? displayName.hashCode() : 0);
        result = 31 * result + (givenName != null ? givenName.hashCode() : 0);
        result = 31 * result + (jobTitle != null ? jobTitle.hashCode() : 0);
        result = 31 * result + mail.hashCode();
        result = 31 * result + (mobilePhone != null ? mobilePhone.hashCode() : 0);
        result = 31 * result + (officeLocation != null ? officeLocation.hashCode() : 0);
        result = 31 * result + (preferredLanguage != null ? preferredLanguage.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + principalName.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "UserLocalEntity{" +
                "id='" + id + '\'' +
                ", displayName='" + displayName + '\'' +
                ", givenName='" + givenName + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", mail='" + mail + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", officeLocation='" + officeLocation + '\'' +
                ", preferredLanguage='" + preferredLanguage + '\'' +
                ", lastName='" + lastName + '\'' +
                ", principalName='" + principalName + '\'' +
                '}';
    }
}
