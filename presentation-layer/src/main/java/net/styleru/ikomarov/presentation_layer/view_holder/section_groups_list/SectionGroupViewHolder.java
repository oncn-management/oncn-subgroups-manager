package net.styleru.ikomarov.presentation_layer.view_holder.section_groups_list;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.view.section_groups_list.SectionGroupViewObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener, View.OnClickListener {

    private AppCompatTextView sectionsCountHolder;
    private AppCompatTextView lastUpdateHolder;
    private AppCompatTextView titleHolder;
    private AppCompatImageButton actionOptionsHolder;

    private PopupMenu menu;

    private SimpleDateFormat contractDateTimeFormat = new SimpleDateFormat(DateTimeContract.VIEW_DATE_FORMAT, Locale.getDefault());

    private int position;
    private SectionGroupViewObject item;
    private InteractionCallbacks callbacks;

    public SectionGroupViewHolder(View itemView) {
        super(itemView);

        sectionsCountHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_subgroup_sections_count);
        lastUpdateHolder    = (AppCompatTextView) itemView.findViewById(R.id.list_item_subgroup_last_update);
        titleHolder         = (AppCompatTextView) itemView.findViewById(R.id.list_item_subgroup_title);
        actionOptionsHolder = (AppCompatImageButton) itemView.findViewById(R.id.list_item_subgroup_options_button);

        menu = new PopupMenu(itemView.getContext(), actionOptionsHolder);
        menu.getMenuInflater().inflate(R.menu.menu_list_item_subgroup, menu.getMenu());

        actionOptionsHolder.setOnClickListener((view) -> {
            prepareOptionsMenu(menu.getMenu());
            menu.show();
        });
        menu.setOnMenuItemClickListener(this);
        itemView.setOnClickListener(this);
    }

    public void bind(int position, SectionGroupViewObject item, InteractionCallbacks callbacks) {
        this.position = position;
        this.item = item;
        this.callbacks = callbacks;

        lastUpdateHolder.setText(parseDate(item.getLastModified()));
        titleHolder.setText(item.getName());
    }

    @Override
    public void onClick(View v) {
        postClick(position, item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch(menuItem.getItemId()) {
            case R.id.action_delete: {
                postAction(position, item, menuItem);
                return true;
            }
            case R.id.action_edit_permissions: {
                postAction(position, item, menuItem);
                return true;
            }
            case R.id.action_changes_list: {
                postAction(position, item, menuItem);
                return true;
            }
            default: {
                return false;
            }
        }
    }

    private String parseDate(Date date) {
        return contractDateTimeFormat.format(date);
    }

    private void postClick(int position, SectionGroupViewObject item) {
        if(callbacks != null) {
            callbacks.onSectionGroupClicked(position, item);
        }
    }

    private void postAction(int position, SectionGroupViewObject item, MenuItem menuItem) {
        if(callbacks != null) {
            callbacks.onSectionGroupAction(position, item, menuItem);
        }
    }

    private void prepareOptionsMenu(Menu menu) {
        //TODO: complete according to real ViewObject conditions
    }

    public interface InteractionCallbacks {

        void onSectionGroupClicked(int position, SectionGroupViewObject item);

        void onSectionGroupAction(int position, SectionGroupViewObject item, MenuItem menuItem);
    }
}
