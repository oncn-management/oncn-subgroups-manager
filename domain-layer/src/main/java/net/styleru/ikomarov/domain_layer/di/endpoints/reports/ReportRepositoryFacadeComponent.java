package net.styleru.ikomarov.domain_layer.di.endpoints.reports;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.endpoints.notebooks.INotebooksComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.permissions.ISGPermissionsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.section_groups.ISectionGroupsComponent;

/**
 * Created by i_komarov on 18.04.17.
 */

public class ReportRepositoryFacadeComponent implements IReportRepositoryFacadeComponent {

    @NonNull
    private final ReportRepositoryFacadeModule facade;

    public ReportRepositoryFacadeComponent(@NonNull INotebookReportRepositoryComponent environment,
                                           @NonNull INotebooksComponent notebooks,
                                           @NonNull ISectionGroupsComponent sectionGroups,
                                           @NonNull ISGPermissionsComponent permissions) {

        this.facade = new ReportRepositoryFacadeModule(environment, notebooks, sectionGroups, permissions);
    }

    @NonNull
    @Override
    public ReportRepositoryFacadeModule facade() {
        return facade;
    }
}
