package net.styleru.ikomarov.domain_layer.mapping.remote_to_local;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ClassNotebookRemoteEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class RemoteToLocalClassNotebooksMapping implements Function<List<ClassNotebookRemoteEntity>, List<ClassNotebookLocalEntity>> {

    @Override
    public List<ClassNotebookLocalEntity> apply(List<ClassNotebookRemoteEntity> classNotebookRemoteEntities) throws Exception {
        return Observable.fromIterable(classNotebookRemoteEntities)
                .map(new RemoteToLocalClassNotebookMapping())
                .toList()
                .blockingGet();
    }
}
