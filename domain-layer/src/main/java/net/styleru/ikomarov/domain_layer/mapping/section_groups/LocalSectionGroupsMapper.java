package net.styleru.ikomarov.domain_layer.mapping.section_groups;

import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 06.03.17.
 */

public class LocalSectionGroupsMapper implements Function<List<SectionGroupLocalEntity>, List<SectionGroupDTO>> {

    @Override
    public List<SectionGroupDTO> apply(List<SectionGroupLocalEntity> sectionGroupLocalEntities) throws Exception {
        return Observable.fromIterable(sectionGroupLocalEntities)
                .map(new LocalSectionGroupMapper())
                .toList()
                .blockingGet();
    }
}
