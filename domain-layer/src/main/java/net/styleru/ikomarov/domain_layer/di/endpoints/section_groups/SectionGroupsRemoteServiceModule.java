package net.styleru.ikomarov.domain_layer.di.endpoints.section_groups;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.SectionGroupsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.SectionGroupsRemoteStorageService;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsRemoteServiceModule {

    private final Object lock = new Object();

    @NonNull
    private final IClientComponent clientComponent;

    @Nullable
    private volatile ISectionGroupsRemoteStorageService localService;

    public SectionGroupsRemoteServiceModule(@NonNull IClientComponent clientComponent) {
        this.clientComponent = clientComponent;
    }

    @NonNull
    public ISectionGroupsRemoteStorageService provideRemoteService() {
        ISectionGroupsRemoteStorageService localInstance = localService;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = localService;
                if(localInstance == null) {
                    localInstance = localService = SectionGroupsRemoteStorageService.Factory.create(clientComponent.clients().provideOneNoteHttpClient());
                }
            }
        }

        return localInstance;
    }
}
