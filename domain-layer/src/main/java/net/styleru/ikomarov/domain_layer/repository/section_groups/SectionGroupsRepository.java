package net.styleru.ikomarov.domain_layer.repository.section_groups;

import net.styleru.ikomarov.data_layer.common.Tuple;
import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.section_groups.SectionGroupRemoteEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.utils.IClaster;
import net.styleru.ikomarov.data_layer.services.utils.IMemCache;
import net.styleru.ikomarov.domain_layer.cache.ICacheStrategy;
import net.styleru.ikomarov.domain_layer.contracts.policies.CachePolicy;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.mapping.remote_to_local.RemoteToLocalSectionGroupMapper;
import net.styleru.ikomarov.domain_layer.mapping.section_groups.LocalSectionGroupMapper;
import net.styleru.ikomarov.domain_layer.mapping.section_groups.LocalSectionGroupsMapper;
import net.styleru.ikomarov.domain_layer.mapping.section_groups.RemoteSectionGroupsMapper;
import net.styleru.ikomarov.domain_layer.repository.base.AbstractRepository;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.repository.utils.ODataUtils;
import net.styleru.ikomarov.domain_layer.utils.RxUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_PARTIAL;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsRepository extends AbstractRepository implements ISectionGroupsRepository {

    private static final String SELF = "me";

    private final ISectionGroupsLocalStorageService localService;
    private final ISectionGroupsRemoteStorageService remoteService;
    private final IClaster<IMemCache> cacheClaster;
    private final ICacheStrategy strategy;
    private final IRemoteMapping<SectionGroupRemoteEntity> mapping;

    public SectionGroupsRepository(ISectionGroupsLocalStorageService localService,
                                   ISectionGroupsRemoteStorageService remoteService,
                                   IClaster<IMemCache> cacheClaster,
                                   ICacheStrategy strategy,
                                   IRemoteMapping<SectionGroupRemoteEntity> mapping) {

        super();
        this.localService = localService;
        this.remoteService = remoteService;
        this.cacheClaster = cacheClaster;
        this.strategy = strategy;
        this.mapping = mapping;
    }

    @Override
    public Observable<SectionGroupDTO> get(String id) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(id, shouldThrowOnNoCachedResourcesFound(policy)) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(policy, id) : Observable.empty()
        );
    }

    @Override
    public Observable<CreateOperationResult> create(UUID operationId, String notebookId, SectionGroupDTO.Builder builder) {
        return this.create(operationId, notebookId, builder, false);
    }

    @Override
    public Observable<List<SectionGroupDTO>> fetch(String notebookId, Date ifModifiedSince) {
        return fetch(notebookId, ifModifiedSince, false);
    }

    @Override
    public Observable<List<SectionGroupDTO>> getList(String notebookId) {
        return getList(notebookId, false);
    }

    @Override
    public Observable<List<SectionGroupDTO>> getList(String notebookId, int offset, int limit) {
        return getList(notebookId, offset, limit, false);
    }

    @Override
    public Observable<List<SectionGroupDTO>> getList(String notebookId, int offset, int limit, String nameSubstring) {
        return getList(notebookId, offset, limit, nameSubstring, false);
    }

    @Override
    public Observable<CreateOperationResult> createNested(UUID operationId, String sectionGroupId, SectionGroupDTO.Builder builder) {
        return this.create(operationId, sectionGroupId, builder, true);
    }

    @Override
    public Observable<List<SectionGroupDTO>> fetchNested(String sectionGroupId, Date ifModifiedSince) {
        return fetch(sectionGroupId, ifModifiedSince, true);
    }

    @Override
    public Observable<List<SectionGroupDTO>> getListNested(String sectionGroupId, int offset, int limit) {
        return getList(sectionGroupId, offset, limit, true);
    }

    @Override
    public Observable<List<SectionGroupDTO>> getListNested(String sectionGroupId, int offset, int limit, String nameSubstring) {
        return getList(sectionGroupId, offset, limit, nameSubstring, true);
    }

    @Override
    public void release() {

    }

    private Observable<CreateOperationResult> create(UUID operationId, String parentId, SectionGroupDTO.Builder builder, boolean isNested) {
        CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            //send an operation to the delayed operations stack
            //TODO: implement the operations stack
            return Observable.just(CreateOperationResult.delayedResult(operationId));
        }

        return performCache(isNested ? remoteService.createNested(SELF, parentId, builder.build()) : remoteService.create(SELF, parentId, builder.build()), HTTP_CREATED)
                .concatMap(Observable::fromIterable)
                .map(data -> CreateOperationResult.successfulResult(operationId, data.getId()));
    }

    private Observable<List<SectionGroupDTO>> fetch(String sectionGroupId, Date ifModifiedSince, boolean nested) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(sectionGroupId, ifModifiedSince, nested) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(sectionGroupId, ifModifiedSince, nested) : Observable.empty()
        );
    }

    private Observable<List<SectionGroupDTO>> getList(String parentId, boolean nested) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy == CachePolicy.CACHE_ONLY ? tryReadFromCache(parentId, true, nested) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(CachePolicy.FORCE_NETWORK, parentId, nested) : Observable.empty()
        );
    }

    private Observable<List<SectionGroupDTO>> getList(String parentId, int offset, int limit, boolean nested) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(parentId, offset, limit, shouldThrowOnNoCachedResourcesFound(policy), nested) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(policy, parentId, offset, limit, nested) : Observable.empty()
        );
    }

    private Observable<List<SectionGroupDTO>> getList(String parentId, int offset, int limit, String nameSubstring, boolean nested) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(parentId, offset, limit, nameSubstring, shouldThrowOnNoCachedResourcesFound(policy), nested) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(policy, parentId, offset, limit, nameSubstring, nested) : Observable.empty()
        );
    }

    private Observable<SectionGroupDTO> tryReadFromCache(String id, boolean shouldThrowOnNoResourcesFound) {
        return entityStamp(id).flatMap(
                wrapper -> wrapper.isNull() ?
                        raiseNoResourcesException(shouldThrowOnNoResourcesFound) :
                        localService.read(id).map(new LocalSectionGroupMapper())
        );
    }

    private Observable<List<SectionGroupDTO>> tryReadFromCache(String parentId, Date ifModifiedSince, boolean nested) {
        return nested ? localService.readNested(parentId, ifModifiedSince).map(new LocalSectionGroupsMapper()) : localService.read(parentId, ifModifiedSince).map(new LocalSectionGroupsMapper());
    }

    private Observable<List<SectionGroupDTO>> tryReadFromCache(String parentId, boolean shouldThrowOnNoResourcesFound, boolean nested) {
        return entitiesCount(parentId).flatMap(
                size -> size == 0 ?
                        raiseNoResourcesException(shouldThrowOnNoResourcesFound) :
                        nested ? localService.readAllNested(parentId).map(new LocalSectionGroupsMapper()) : localService.readAll(parentId).map(new LocalSectionGroupsMapper())
        );
    }

    private Observable<List<SectionGroupDTO>> tryReadFromCache(String parentId, int offset, int limit, boolean shouldThrowOnNoResourcesFound, boolean nested) {
        return entitiesCount(parentId).flatMap(
                size -> size == 0 ?
                        raiseNoResourcesException(shouldThrowOnNoResourcesFound) :
                        nested ? localService.readNested(parentId, offset, limit).map(new LocalSectionGroupsMapper()) : localService.read(parentId, offset, limit).map(new LocalSectionGroupsMapper())
        );
    }

    private Observable<List<SectionGroupDTO>> tryReadFromCache(String parentId, int offset, int limit, String nameSubstring, boolean shouldThrowOnNoResourcesFound, boolean nested) {
        return entitiesCount(parentId).flatMap(
                size -> size == 0 ?
                        raiseNoResourcesException(shouldThrowOnNoResourcesFound) :
                        nested ? localService.readNested(parentId, offset, limit, nameSubstring).map(new LocalSectionGroupsMapper()) : localService.read(parentId, offset, limit, nameSubstring).map(new LocalSectionGroupsMapper())
        );
    }

    private Observable<SectionGroupDTO> readUpdatesFromNetwork(CachePolicy policy, String id) {
        Date lastModified = cacheClaster.lastModified(id);
        return performCache(lastModified != null && policy != CachePolicy.FORCE_NETWORK ?
                        remoteService.read(SELF, id, ODataUtils.modifiedSince(lastModified)) :
                        remoteService.read(SELF, id),
                HTTP_OK
        ).flatMap(Observable::fromIterable);
    }

    private Observable<List<SectionGroupDTO>> readUpdatesFromNetwork(String parentId, Date ifModifiedSince, boolean nested) {
        return performCache(nested ? remoteService.readNested(SELF, parentId, ODataUtils.orderByLastModified(), ODataUtils.modifiedSince(ifModifiedSince)) : remoteService.read(SELF, parentId, ODataUtils.orderByLastModified(), ODataUtils.modifiedSince(ifModifiedSince)),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<SectionGroupDTO>> readUpdatesFromNetwork(CachePolicy policy, String parentId, boolean nested) {
        Date lastModified = cacheClaster.get(parentId).lastModified();
        return performCache(lastModified != null && policy != CachePolicy.FORCE_NETWORK ?
                        (nested ? remoteService.readNested(SELF, parentId, ODataUtils.orderByLastModified(), ODataUtils.modifiedSince(lastModified)) : remoteService.read(SELF, parentId, ODataUtils.orderByLastModified(), ODataUtils.modifiedSince(lastModified))) :
                        (nested ? remoteService.readNested(SELF, parentId, ODataUtils.orderByLastModified()) : remoteService.read(SELF, parentId, ODataUtils.orderByLastModified())),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<SectionGroupDTO>> readUpdatesFromNetwork(CachePolicy policy, String parentId, int offset, int limit, boolean nested) {
        Date lastModified = cacheClaster.get(parentId).lastModified();
        return performCache(lastModified != null && policy != CachePolicy.FORCE_NETWORK ?
                        (nested ? remoteService.readNested(SELF, parentId, offset, limit, ODataUtils.orderByLastModified(), ODataUtils.modifiedSince(lastModified)) : remoteService.read(SELF, parentId, offset, limit, ODataUtils.orderByLastModified(), ODataUtils.modifiedSince(lastModified))) :
                        (nested ? remoteService.readNested(SELF, parentId, offset, limit, ODataUtils.orderByLastModified()) : remoteService.read(SELF, parentId, offset, limit, ODataUtils.orderByLastModified())),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<SectionGroupDTO>> readUpdatesFromNetwork(CachePolicy policy, String parentId, int offset, int limit, String nameSubstring, boolean nested) {
        Date lastModified = cacheClaster.get(parentId).lastModified();
        return performCache(lastModified != null && policy != CachePolicy.FORCE_NETWORK ?
                        (nested ? remoteService.readNested(SELF, parentId, offset, limit, ODataUtils.orderByName(), ODataUtils.and(ODataUtils.byNameSubstring(nameSubstring), ODataUtils.modifiedSince(lastModified))) : remoteService.read(SELF, parentId, offset, limit, ODataUtils.orderByName(), ODataUtils.and(ODataUtils.byNameSubstring(nameSubstring), ODataUtils.modifiedSince(lastModified)))) :
                        (nested ? remoteService.readNested(SELF, parentId, offset, limit, ODataUtils.orderByName(), ODataUtils.byNameSubstring(nameSubstring)) : remoteService.read(SELF, parentId, offset, limit, ODataUtils.orderByName(), ODataUtils.byNameSubstring(nameSubstring))),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<SectionGroupDTO>> performCache(Observable<Response<DataResponse<SectionGroupRemoteEntity>>> fromNetwork, Integer... successCodes) {
        return fromNetwork.flatMap(verifyResponse(SectionGroupRemoteEntity.class, successCodes))
                .doOnNext(response -> addToCache(response, mapping))
                .flatMap(response -> Observable.just(response.getBody(mapping))
                        .map(new RemoteSectionGroupsMapper()));
    }

    private void addToCache(DataResponse<SectionGroupRemoteEntity> data, IRemoteMapping<SectionGroupRemoteEntity> mapping) {
        Observable.fromIterable(data.getBody(mapping))
                .map(new RemoteToLocalSectionGroupMapper())
                .flatMap(local -> localService.create(local).map(putResult -> new Tuple<>(local.getNotebookId(), local.getId(), local.getLastModifiedTime())))
                .filter(tuple -> tuple.getFirst() != null)
                .subscribe(tuple -> cacheClaster.get(tuple.getFirst()).onModified(tuple.getSecond(), tuple.getThird()));
    }

    private Observable<RxUtils.NullSafe<Date>> entityStamp(String id) {
        return Observable.just(new RxUtils.NullSafe<>(cacheClaster.lastModified(id)))
                .flatMap((wrapper) -> wrapper.isNull() ?
                        Observable.defer(() -> Observable.just(0L).delay(50L, TimeUnit.MILLISECONDS).flatMap(val -> Observable.just(new RxUtils.NullSafe<>(cacheClaster.lastModified(id))))) :
                        Observable.just(wrapper)
                );
    }

    private Observable<Long> entitiesCount(String nodeId) {
        IMemCache node = cacheClaster.get(nodeId);
        return Observable.just(node.size())
                .delay(20, TimeUnit.MILLISECONDS)
                .repeatUntil(() -> !node.isCold())
                .flatMap(nodeSize -> Observable.just(node.size()));
    }

    public static final class Factory {

        private Factory() {

        }

        public static ISectionGroupsRepository create(ISectionGroupsLocalStorageService localService,
                                                      ISectionGroupsRemoteStorageService remoteService,
                                                      IClaster<IMemCache> cacheClaster,
                                                      ICacheStrategy strategy,
                                                      IRemoteMapping<SectionGroupRemoteEntity> mapping) {

            return new SectionGroupsRepository(localService, remoteService, cacheClaster, strategy, mapping);
        }
    }
}
