package net.styleru.ikomarov.domain_layer.mapping.class_notebooks;

import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ClassNotebookRemoteEntity;

import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.03.17.
 */

public class RemoteClassNotebookMapper implements Function<ClassNotebookRemoteEntity, ClassNotebookDTO> {

    @Nullable
    @Override
    public ClassNotebookDTO apply(ClassNotebookRemoteEntity entity) {
        return ClassNotebookDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getLinks().getWebUrl().getHref(),
                entity.getLinks().getClientUrl().getHref(),
                entity.getCreatedBy(),
                entity.getCreatedTime(),
                entity.getLastModifiedBy(),
                entity.getLastModifiedTime(),
                entity.getRole() == null ? UserRole.NONE : entity.getRole(),
                entity.isDefault(),
                entity.isShared(),
                entity.hasTeacherOnlySectionGroup(),
                entity.getCollaborationSpaceLocked()
        );
    }
}
