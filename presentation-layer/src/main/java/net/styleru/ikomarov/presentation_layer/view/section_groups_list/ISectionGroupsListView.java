package net.styleru.ikomarov.presentation_layer.view.section_groups_list;

import android.support.annotation.StringRes;

import net.styleru.ikomarov.presentation_layer.view.base.IView;

import java.util.List;

/**
 * Created by i_komarov on 15.03.17.
 */

public interface ISectionGroupsListView extends IView {

    void addSectionGroups(List<SectionGroupViewObject> items);

    void showError(@StringRes int error);

    void showConnectionFailedIndicator();

    void showOperationDelayedDialog();

    void showNoCachedDataPlug();

    void showNoDataPlug();

    void showNoPermissionsDialog();

    void showAuthorizationDialog();
}
