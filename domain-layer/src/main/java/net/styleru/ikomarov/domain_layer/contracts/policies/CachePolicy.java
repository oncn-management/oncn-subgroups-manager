package net.styleru.ikomarov.domain_layer.contracts.policies;

/**
 * Created by i_komarov on 08.03.17.
 */

public enum  CachePolicy {
    CACHE_ONLY    (0x000000),
    CHECK_UPDATES (0x000001),
    FORCE_NETWORK (0x000002);

    private final int code;

    public static CachePolicy getDefault() {
        return CHECK_UPDATES;
    }

    CachePolicy(int code) {
        this.code = code;
    }
}
