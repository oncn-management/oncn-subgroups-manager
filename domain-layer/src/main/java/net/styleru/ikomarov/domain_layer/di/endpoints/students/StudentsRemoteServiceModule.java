package net.styleru.ikomarov.domain_layer.di.endpoints.students;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.SectionGroupsRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.students.IStudentsRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.students.StudentsRemoteStorageService;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsRemoteServiceModule {

    private final Object lock = new Object();

    @NonNull
    private final IClientComponent clientComponent;

    @Nullable
    private volatile IStudentsRemoteStorageService localService;

    public StudentsRemoteServiceModule(@NonNull IClientComponent clientComponent) {
        this.clientComponent = clientComponent;
    }

    @NonNull
    public IStudentsRemoteStorageService provideRemoteService() {
        IStudentsRemoteStorageService localInstance = localService;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = localService;
                if(localInstance == null) {
                    localInstance = localService = StudentsRemoteStorageService.Factory.create(clientComponent.clients().provideOneNoteHttpClient());
                }
            }
        }

        return localInstance;
    }
}
