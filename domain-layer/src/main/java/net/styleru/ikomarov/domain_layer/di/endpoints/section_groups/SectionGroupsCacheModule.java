package net.styleru.ikomarov.domain_layer.di.endpoints.section_groups;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.utils.IClaster;
import net.styleru.ikomarov.data_layer.services.utils.IMemCache;
import net.styleru.ikomarov.data_layer.services.utils.SectionGroupsCacheClaster;
import net.styleru.ikomarov.data_layer.services.utils.SectionGroupsRecordsCache;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsCacheModule {

    private final Object lock = new Object();

    @NonNull
    private final ISectionGroupsEnvironmentComponent environment;

    @Nullable
    private volatile SectionGroupsRecordsCache cache;

    @NonNull
    private volatile SectionGroupsCacheClaster claster;

    public SectionGroupsCacheModule(@NonNull ISectionGroupsEnvironmentComponent environment) {
        this.environment = environment;
    }

    @NonNull
    public IMemCache provideSectionGroupsCache() {
        IMemCache localInstance = cache;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = cache;
                if(localInstance == null) {
                    localInstance = cache = new SectionGroupsRecordsCache("", environment.localService().provideLocalService());
                }
            }
        }

        return localInstance;
    }

    @NonNull
    public IClaster<IMemCache> provideSectionGroupsCacheClaster() {
        IClaster<IMemCache> localInstance = claster;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = claster;
                if(localInstance == null) {
                    localInstance = claster = new SectionGroupsCacheClaster(environment.localService().provideLocalService());
                }
            }
        }

        return localInstance;
    }
}
