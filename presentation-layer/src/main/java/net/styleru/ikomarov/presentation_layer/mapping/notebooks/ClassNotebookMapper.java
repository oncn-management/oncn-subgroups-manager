package net.styleru.ikomarov.presentation_layer.mapping.notebooks;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;
import net.styleru.ikomarov.presentation_layer.view.notebooks_list.NotebookViewObject;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class ClassNotebookMapper implements Function<ClassNotebookDTO, NotebookViewObject> {

    @Override
    public NotebookViewObject apply(ClassNotebookDTO dto) throws ExceptionBundle {
        return new NotebookViewObject(
                dto.getId(),
                dto.getBrowserUrl(),
                dto.getClientUrl(),
                dto.getLastModifiedTime(),
                dto.getName(),
                dto.getHasTeacherOnlySectionGroup(),
                dto.getCollaborationSpaceLocked()
        );
    }
}
