package net.styleru.ikomarov.data_layer.manager.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import net.styleru.ikomarov.data_layer.contracts.network.NetworkStatus;
import net.styleru.ikomarov.data_layer.exceptions.internal.UnsupportedSystemServiceException;

import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.disposables.Disposable;

/**
 * Created by i_komarov on 25.02.17.
 */

public class NetworkManager implements INetworkManager {

    private Context context;

    private IntentFilter filter;

    public NetworkManager(Context context) {
        this.context = context.getApplicationContext();
        this.filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    @Override
    public Observable<NetworkStatus> getNetworkStatusChangeEventsObservable() {
        return createNetworkStatusChangeEventsObservable();
    }

    @Override
    public Observable<NetworkStatus> getCurrentNetworkStatusObservable() {
        return Observable.just(getStatus(context));
    }

    @Override
    public NetworkStatus getCurrentNetworkStatus() {
        return getStatus(context);
    }

    private Observable<NetworkStatus> createNetworkStatusChangeEventsObservable() {
        return Observable.create(new ObservableOnSubscribe<NetworkStatus>() {
            @Override
            public void subscribe(final ObservableEmitter<NetworkStatus> emitter) throws Exception {

                final BroadcastReceiver networkInfoReceiver = new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if(!emitter.isDisposed()) {
                            try {
                                emitter.onNext(getStatus(context));
                            } catch(Exception e) {
                                emitter.onError(e);
                            }
                        }
                    }
                };

                emitter.setDisposable(new Disposable() {

                    private AtomicBoolean isDisposed = new AtomicBoolean(false);

                    @Override
                    public void dispose() {
                        isDisposed.set(true);
                        context.unregisterReceiver(networkInfoReceiver);
                    }

                    @Override
                    public boolean isDisposed() {
                        return isDisposed.get();
                    }
                });

                context.registerReceiver(networkInfoReceiver, filter);
            }
        });
    }

    private NetworkStatus getStatus(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            Log.d("NetworkManager", "connectivity status: " + info + (info != null ? info.getTypeName() : ""));
            if(info != null) {
                if(info.getType() == ConnectivityManager.TYPE_WIFI) {
                    return NetworkStatus.CONNECTED_WIFI;
                } else if(info.getType() == ConnectivityManager.TYPE_MOBILE) {
                    return NetworkStatus.CONNECTED_MOBILE;
                } else {
                    return NetworkStatus.DISCONNECTED;
                }
            } else {
                return NetworkStatus.DISCONNECTED;
            }
        } else {
            throw new UnsupportedSystemServiceException(ConnectivityManager.class.getSimpleName());
        }
    }
}
