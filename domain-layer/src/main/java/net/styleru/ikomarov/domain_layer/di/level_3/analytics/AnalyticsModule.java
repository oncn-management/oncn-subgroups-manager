package net.styleru.ikomarov.domain_layer.di.level_3.analytics;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.analytics.ScreenAnalyticsManager;
import net.styleru.ikomarov.domain_layer.analytics.IScreenAnalyticsManager;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;

/**
 * Created by i_komarov on 11.05.17.
 */

public class AnalyticsModule {

    @NonNull
    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent component;

    @Nullable
    private volatile IScreenAnalyticsManager proxy;

    public AnalyticsModule(@NonNull IEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public IScreenAnalyticsManager provideScreenAnalytics() {
        IScreenAnalyticsManager localInstance = proxy;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = proxy;
                if(localInstance == null) {
                    localInstance = proxy = new ScreenAnalyticsManager(component.logging().provideLoggingManager());
                }
            }
        }

        return localInstance;
    }
}
