package net.styleru.ikomarov.presentation_layer.view.sg_permissions_list;

import android.app.Fragment;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.transition.TransitionManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import net.styleru.ikomarov.domain_layer.use_cases.permissions.SGPermissionsBatchUpdateUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.permissions.SGPermissionsGetUseCase;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.adapter.sg_permissions_list.SGPermissionsAdapter;
import net.styleru.ikomarov.presentation_layer.application.ISingletonProvider;
import net.styleru.ikomarov.presentation_layer.presenter.sg_permissions_list.SGPermissionsListPresenter;
import net.styleru.ikomarov.presentation_layer.view.base.PresenterFragment;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.drawer.IDrawerView;
import net.styleru.ikomarov.presentation_layer.view.sg_permission_wizard.SGPermissionWizardFragment;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 24.03.17.
 */

public class SGPermissionsListFragment extends PresenterFragment<ISGPermissionsListView, SGPermissionsListPresenter> implements ISGPermissionsListView {

    private ViewGroup root;

    private RecyclerView permissionsList;

    private AppCompatButton syncWithServerButton;

    private AppCompatImageView syncSuccessIndicator;

    private AppCompatImageView syncFailedIndicator;

    private ProgressBar syncProgressIndicator;

    private FloatingActionButton participantsAddButton;

    private SGPermissionsAdapter adapter;

    private FragmentState state;

    public static Fragment newInstance(String notebookId, String sectionGroupId, String sectionGroupName) {
        Fragment fragment = new SGPermissionsListFragment();
        Bundle args = new Bundle();
        args.putString(FragmentState.KEY_NOTEBOOK_ID, notebookId);
        args.putString(FragmentState.KEY_SECTION_GROUP_ID, sectionGroupId);
        args.putString(FragmentState.KEY_SECTION_GROUP_NAME, sectionGroupName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.state = new FragmentState(getArguments(), savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void addPermissions(List<SGPermissionViewObject> items) {
        adapter.addItems(items);
    }

    @Override
    public void applySyncState(SyncState state) {
        switch(state) {
            case SYNC_PROGRESS: {
                TransitionManager.beginDelayedTransition(root);
                syncWithServerButton.setVisibility(View.INVISIBLE);

                TransitionManager.beginDelayedTransition(root);
                syncProgressIndicator.setVisibility(View.VISIBLE);

                break;
            }
            case SYNC_SUCCESS: {
                adapter.resetChanges();

                TransitionManager.beginDelayedTransition(root);
                syncProgressIndicator.setVisibility(View.GONE);

                TransitionManager.beginDelayedTransition(root);
                syncSuccessIndicator.setVisibility(View.VISIBLE);

                new Handler().postDelayed(() -> applySyncState(SyncState.DEFAULT), 2500);

                break;
            }
            case SYNC_FAILURE: {
                TransitionManager.beginDelayedTransition(root);

                syncProgressIndicator.setVisibility(View.INVISIBLE);
                syncFailedIndicator.setVisibility(View.VISIBLE);

                new Handler().postDelayed(() -> applySyncState(SyncState.DEFAULT), 2500);

                break;
            }
            case SYNC_DELAY: {
                TransitionManager.beginDelayedTransition(root);

                syncProgressIndicator.setVisibility(View.INVISIBLE);
                showError(R.string.notification_operation_delayed);

                new Handler().postDelayed(() -> applySyncState(SyncState.DEFAULT), 2500);

                break;
            }
            case DEFAULT: {
                TransitionManager.beginDelayedTransition(root);

                if(syncFailedIndicator.getVisibility() == View.VISIBLE) {
                    syncFailedIndicator.setVisibility(View.INVISIBLE);
                }
                if(syncSuccessIndicator.getVisibility() == View.VISIBLE) {
                    syncSuccessIndicator.setVisibility(View.INVISIBLE);
                }

                TransitionManager.beginDelayedTransition(root);

                syncWithServerButton.setVisibility(View.VISIBLE);

                break;
            }
        }
    }

    @Override
    public void showError(@StringRes int error) {
        Snackbar errorSnack = Snackbar.make(getView(), getResources().getString(error), Snackbar.LENGTH_INDEFINITE);
        errorSnack.setAction(R.string.action_ok, (view) -> errorSnack.dismiss());
        errorSnack.setActionTextColor(getResources().getColor(R.color.colorAccent));
        errorSnack.show();
    }

    @Override
    public void showBatchUpdateConflictNotification() {
        showError(R.string.notification_batch_update_conflict);
    }

    @Override
    public void showConnectionFailedIndicator() {
        showError(R.string.error_connection_lost);
    }

    @Override
    public void showOperationDelayedDialog() {
        showError(R.string.notification_operation_delayed);
    }

    @Override
    public void showNoCachedDataPlug() {
        showError(R.string.notification_no_cached_data);
    }

    @Override
    public void showNoDataPlug() {
        showError(R.string.notification_no_data);
    }

    @Override
    public void showNoPermissionsDialog() {
        showError(R.string.notification_no_permissions);
    }

    @Override
    public void showAuthorizationDialog() {
        ((IDrawerView) getActivity()).showAuthenticationActivity();
    }

    @Override
    protected void bindUserInterface(View view) {
        root = (ViewGroup) view.findViewById(R.id.fragment_sg_permissions_list_root);
        permissionsList = (RecyclerView) view.findViewById(R.id.fragment_sg_permissions_list_recycler_view);
        syncWithServerButton = (AppCompatButton) view.findViewById(R.id.fragment_sg_permissions_list_sync_with_server);
        syncSuccessIndicator = (AppCompatImageView) view.findViewById(R.id.fragment_sg_permissions_list_sync_successful_indicator);
        syncFailedIndicator = (AppCompatImageView) view.findViewById(R.id.fragment_sg_permissions_list_sync_failed_indicator);
        syncProgressIndicator = (ProgressBar) view.findViewById(R.id.fragment_sg_permissions_list_sync_with_server_progress_bar);
        participantsAddButton = (FloatingActionButton) view.findViewById(R.id.fragment_sg_permissions_list_participants_add_button);

        syncProgressIndicator.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorPrimaryLight), PorterDuff.Mode.SRC_IN);

        permissionsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new SGPermissionsAdapter();
        permissionsList.setAdapter(adapter);

        syncWithServerButton.setOnClickListener(this::onSyncWithServerButtonClicked);
        participantsAddButton.setOnClickListener(this::onParticipantsAddButtonClicked);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        state.saveState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(state.getSectionGroupName());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(R.string.title_manage_permissions);
        getPresenter().loadPermissions();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected RetainContainer.Factory<SGPermissionsListPresenter> providePresenterFactory() {
        ISingletonProvider provider = (ISingletonProvider) getActivity().getApplication();
        return new SGPermissionsListPresenter.Factory(
                new SGPermissionsGetUseCase(
                        provider.sgPermissionsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ),
                new SGPermissionsBatchUpdateUseCase(
                        provider.sgPermissionsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ),
                provider.errorTranslation().provideOneNoteErrorCodeTranslator(),
                provider.logging().provideLoggingManager(),
                state.getSectionGroupId()
        );
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_sg_permissions_list;
    }

    @Override
    public ISGPermissionsListView getViewInterface() {
        return this;
    }

    private void onSyncWithServerButtonClicked(View view) {
        List<SGPermissionsBatchUpdateUseCase.Permission> changes;
        if(0 != (changes =  adapter.getPermissionChanges()).size()) {
            getPresenter().batchUpdatePermissions(changes);
        } else {
            showError(R.string.notification_no_changes_performed);
        }
    }

    private void onParticipantsAddButtonClicked(View view) {
        getFragmentManager().beginTransaction()
                .replace(
                        R.id.activity_drawer_fragment_container,
                        SGPermissionWizardFragment.newInstance(
                                state.getNotebookId(),
                                state.getSectionGroupId(),
                                state.getSectionGroupName(),
                                adapter.getIds()
                        )
                )
                .addToBackStack(null)
                .commit();
    }

    private static class FragmentState {

        private static final String PREFIX = FragmentState.class.getCanonicalName() + ".";

        private static final String KEY_NOTEBOOK_ID = PREFIX + "NOTEBOOK_ID";
        private static final String KEY_SECTION_GROUP_ID = PREFIX + "SECTION_GROUP_ID";
        private static final String KEY_SECTION_GROUP_NAME = PREFIX + "SECTION_GROUP_NAME";
        private static final String KEY_LIST_STATE = PREFIX + "LIST_STATE";
        private static final String KEY_LIST_ITEMS = PREFIX + "LIST_ITEMS";

        private String notebookId;
        private String sectionGroupId;
        private String sectionGroupName;

        public FragmentState(Bundle arguments, Bundle savedInstanceState) {
            if(arguments != null && arguments.containsKey(KEY_NOTEBOOK_ID) && arguments.containsKey(KEY_SECTION_GROUP_ID) && arguments.containsKey(KEY_SECTION_GROUP_NAME)) {
                this.notebookId = arguments.getString(KEY_NOTEBOOK_ID);
                this.sectionGroupId = arguments.getString(KEY_SECTION_GROUP_ID);
                this.sectionGroupName = arguments.getString(KEY_SECTION_GROUP_NAME);
            } else if(savedInstanceState != null && savedInstanceState.containsKey(KEY_NOTEBOOK_ID) && savedInstanceState.containsKey(KEY_SECTION_GROUP_ID) && savedInstanceState.containsKey(KEY_SECTION_GROUP_NAME) && savedInstanceState.containsKey(KEY_NOTEBOOK_ID)) {
                this.notebookId = savedInstanceState.getString(KEY_NOTEBOOK_ID);
                this.sectionGroupId = savedInstanceState.getString(KEY_SECTION_GROUP_ID);
                this.sectionGroupName = savedInstanceState.getString(KEY_SECTION_GROUP_NAME);
            }
        }

        public void saveState(Bundle outState) {
            outState.putString(KEY_NOTEBOOK_ID, notebookId);
            outState.putString(KEY_SECTION_GROUP_ID, sectionGroupId);
            outState.putString(KEY_SECTION_GROUP_NAME, sectionGroupName);
        }

        public String getNotebookId() {
            return this.notebookId;
        }

        public String getSectionGroupId() {
            return this.sectionGroupId;
        }

        public String getSectionGroupName() {
            return this.sectionGroupName;
        }
    }
}
