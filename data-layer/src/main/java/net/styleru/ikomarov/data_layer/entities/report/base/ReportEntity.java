package net.styleru.ikomarov.data_layer.entities.report.base;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 17.04.17.
 */

public class ReportEntity<S extends ReportSubgroupEntity, G extends ReportGroupEntity<S>> {

    @NonNull
    private final String name;

    @NonNull
    private final List<G> groups;

    public ReportEntity(@NonNull String name, @NonNull List<G> groups) {
        this.name = name;
        this.groups = groups;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public List<G> getGroups() {
        return groups;
    }

    @Override
    public String toString() {
        return "ReportEntity{" +
                "name='" + name + '\'' +
                ", groups=" + groups +
                '}';
    }

    public static final class Builder<S extends ReportSubgroupEntity, G extends ReportGroupEntity<S>> {

        private String name;
        private List<G> groups;

        public Builder(String name) {
            this.name = name;
            this.groups = new ArrayList<>();
        }

        public Builder<S, G> addGroup(G group) {
            this.groups.add(group);
            return this;
        }

        public ReportEntity<S, G> build() {
            return new ReportEntity<>(name, groups);
        }
    }
}
