package net.styleru.ikomarov.presentation_layer.view.sg_permissions_list;

import android.support.annotation.StringRes;

import net.styleru.ikomarov.presentation_layer.view.base.IView;

import java.util.List;

/**
 * Created by i_komarov on 02.04.17.
 */

public interface ISGPermissionsListView extends IView {

    void addPermissions(List<SGPermissionViewObject> items);

    void applySyncState(SyncState state);

    void showError(@StringRes int error);

    void showBatchUpdateConflictNotification();

    void showConnectionFailedIndicator();

    void showOperationDelayedDialog();

    void showNoCachedDataPlug();

    void showNoDataPlug();

    void showNoPermissionsDialog();

    void showAuthorizationDialog();

    enum SyncState {
        SYNC_PROGRESS,
        SYNC_SUCCESS,
        SYNC_FAILURE,
        SYNC_DELAY,
        DEFAULT
    }
}
