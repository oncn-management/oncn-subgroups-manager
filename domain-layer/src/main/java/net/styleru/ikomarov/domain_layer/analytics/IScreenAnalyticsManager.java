package net.styleru.ikomarov.domain_layer.analytics;

/**
 * Created by i_komarov on 11.05.17.
 */

public interface IScreenAnalyticsManager {

    void onScreenEvent(String screen);

    void onScreenEvent(String screen, String resourceId);
}
