package net.styleru.ikomarov.data_layer.contracts.permissions;

import android.util.SparseArray;

/**
 * Created by i_komarov on 21.03.17.
 */

public enum PermissionCategory {
    NOTEBOOK      (0x000000),
    SECTION_GROUP (0x000001),
    SECTION       (0x000002);

    private static final SparseArray<PermissionCategory> typesMap;

    static {
        typesMap = new SparseArray<>(PermissionCategory.values().length);
        for(PermissionCategory type : PermissionCategory.values()) {
            typesMap.put(type.code, type);
        }
    }

    private final int code;

    public static PermissionCategory forValue(int code) {
        return typesMap.get(code);
    }

    PermissionCategory(int code) {
        this.code = code;
    }

    public int code() {
        return this.code;
    }
}
