package net.styleru.ikomarov.data_layer.entities.local.notebooks;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import static net.styleru.ikomarov.data_layer.mapping.local.notebooks.StudentSectionsLocalEntityMapping.*;

/**
 * Created by i_komarov on 28.02.17.
 */

@StorIOSQLiteType(table = TABLE)
public class StudentSectionLocalEntity {

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ID, key = true)
    String id;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_PARENT_ID)
    String parentId;

    public static StudentSectionLocalEntity newInstance(String id, String parentId) {
        StudentSectionLocalEntity entity = new StudentSectionLocalEntity();

        entity.id = id;
        entity.parentId = parentId;

        return entity;
    }

    StudentSectionLocalEntity() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getParentId() {
        return parentId;
    }

    public void setParentId(@NonNull String parentId) {
        this.parentId = parentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StudentSectionLocalEntity that = (StudentSectionLocalEntity) o;

        if (!id.equals(that.id)) return false;
        return parentId.equals(that.parentId);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + parentId.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "StudentSectionLocalEntity{" +
                "id='" + id + '\'' +
                ", parentId='" + parentId + '\'' +
                '}';
    }
}
