package net.styleru.ikomarov.presentation_layer.adapter.sg_permissions_list;

import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.domain_layer.use_cases.permissions.SGPermissionsBatchUpdateUseCase;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.view.sg_permissions_list.SGPermissionViewObject;
import net.styleru.ikomarov.presentation_layer.view_holder.sg_permissions_list.SGPermissionViewHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by i_komarov on 24.03.17.
 */

public class SGPermissionsAdapter extends RecyclerView.Adapter<SGPermissionViewHolder> implements SGPermissionViewHolder.IPermissionChangeListener {

    private Map<String, SGPermissionViewObject> itemMap;
    private SortedList<SGPermissionViewObject> items;

    private List<SGPermissionsBatchUpdateUseCase.Permission> changes;

    public SGPermissionsAdapter() {
        this.itemMap = new HashMap<>();
        this.changes = new ArrayList<>();
        this.items = new SortedList<>(SGPermissionViewObject.class, new SortedListAdapterCallback<SGPermissionViewObject>(this) {
            @Override
            public int compare(SGPermissionViewObject o1, SGPermissionViewObject o2) {
                return o1.getName().compareTo(o2.getName());
            }

            @Override
            public boolean areContentsTheSame(SGPermissionViewObject oldItem, SGPermissionViewObject newItem) {
                return  oldItem.getId().equals(newItem.getId()) &&
                        oldItem.getUserId().equals(newItem.getUserId());
            }

            @Override
            public boolean areItemsTheSame(SGPermissionViewObject item1, SGPermissionViewObject item2) {
                return  item1.getName().equals(item2.getName()) &&
                        item1.getRole() == item2.getRole();
            }
        });
    }

    @Override
    public SGPermissionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SGPermissionViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_permission, parent, false));
    }

    @Override
    public void onBindViewHolder(SGPermissionViewHolder holder, int position) {
        holder.bind(position, items.get(position), this);
    }

    @Override
    public int getItemCount() {
        return this.items.size();
    }

    @Override
    public void onPermissionAssigned(String permissionId, String userId, UserRole oldRole, UserRole newRole) {
        if(oldRole != newRole) {
            //update changes map
            this.changes.add(new SGPermissionsBatchUpdateUseCase.Permission(permissionId, userId, newRole));
            //update the particular item
            final SGPermissionViewObject affectedItem = this.itemMap.get(userId);
            affectedItem.setRole(newRole);
            //reset the updated item
            this.itemMap.put(userId, affectedItem);
            this.items.updateItemAt(this.items.indexOf(affectedItem), affectedItem);
        }
    }

    public List<SGPermissionsBatchUpdateUseCase.Permission> getPermissionChanges() {
        return this.changes;
    }

    public void resetChanges() {
        this.changes.clear();

        //perform a cleanup of items with UserRole.NONE role
        List<SGPermissionViewObject> permissionsToDelete = new ArrayList<>();

        for(int i = 0; i < this.items.size(); i++) {
            SGPermissionViewObject permission;
            if(UserRole.NONE == (permission = this.items.get(i)).getRole()) {
                permissionsToDelete.add(permission);
            }
        }

        this.items.beginBatchedUpdates();

        for(SGPermissionViewObject permissionToDelete : permissionsToDelete) {
            this.itemMap.remove(permissionToDelete.getUserId());
            this.items.remove(permissionToDelete);
        }

        this.items.endBatchedUpdates();
    }

    public void addItems(List<SGPermissionViewObject> items) {
        this.items.beginBatchedUpdates();
        for(SGPermissionViewObject item : items) {
            final SGPermissionViewObject existing = itemMap.get(item.getId());
            if(existing != null) {
                //remove if presents
                this.items.remove(existing);
            }

            //update the id-item map
            this.itemMap.put(item.getUserId(), item);
            //update the actual list
            this.items.add(item);
        }

        this.items.endBatchedUpdates();
    }

    public void removeItem(String userId) {
        final SGPermissionViewObject item = this.itemMap.get(userId);
        this.itemMap.remove(userId);
        this.items.remove(item);
    }

    public ArrayList<String> getIds() {
        return new ArrayList<>(Arrays.asList(itemMap.keySet().toArray(new String[itemMap.size()])));
    }
}
