package net.styleru.ikomarov.domain_layer.di.endpoints.reports;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 18.04.17.
 */

public class NotebooksReportRepositoryComponent implements INotebookReportRepositoryComponent {

    @NonNull
    private final NotebooksReportsRepositoryModule notebooks;

    public NotebooksReportRepositoryComponent(@NonNull IReportsEnvironmentComponent component) {
        this.notebooks = new NotebooksReportsRepositoryModule(component);
    }

    @NonNull
    @Override
    public NotebooksReportsRepositoryModule notebooks() {
        return notebooks;
    }
}
