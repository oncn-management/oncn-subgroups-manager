package net.styleru.ikomarov.domain_layer.repository.base;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;

import java.util.UUID;

/**
 * Created by i_komarov on 22.03.17.
 */

public class DeleteOperationResult {

    @NonNull
    private UUID operationId;

    @NonNull
    private OperationStatus status;

    public static DeleteOperationResult successfulResult(UUID operationId) {
        return new DeleteOperationResult(operationId, OperationStatus.SUCCESS);
    }

    public static DeleteOperationResult delayedResult(UUID operationId) {
        return new DeleteOperationResult(operationId, OperationStatus.DELAYED);
    }

    private DeleteOperationResult(@NonNull UUID operationId, @NonNull OperationStatus status) {
        this.operationId = operationId;
        this.status = status;
    }

    @NonNull
    public UUID getOperationId() {
        return operationId;
    }

    @NonNull
    public OperationStatus getStatus() {
        return status;
    }
}
