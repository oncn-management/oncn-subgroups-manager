package net.styleru.ikomarov.domain_layer.repository.base;

/**
 * Created by i_komarov on 09.03.17.
 */

public interface IRepository {

    void release();
}
