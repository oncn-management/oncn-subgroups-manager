package net.styleru.ikomarov.domain_layer.mapping.class_notebooks;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntity;

import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;

import java.text.SimpleDateFormat;

import java.util.Locale;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.03.17.
 */

public class LocalClassNotebookMapper implements Function<ClassNotebookLocalEntity, ClassNotebookDTO> {

    private SimpleDateFormat contractDateFormat;

    public LocalClassNotebookMapper() {
        this.contractDateFormat = new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault());
    }

    @Override
    public ClassNotebookDTO apply(ClassNotebookLocalEntity entity) throws Exception {

        return ClassNotebookDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getWebUrl(),
                entity.getClientUrl(),
                entity.getCreatedBy(),
                contractDateFormat.parse(entity.getCreatedTime()),
                entity.getLastModifiedBy(),
                contractDateFormat.parse(entity.getLastModifiedTime()),
                UserRole.forValue(entity.getUserRole()),
                entity.getDefault(),
                entity.getShared(),
                entity.getHasTeacherOnlySectionGroup(),
                entity.getIsCollaborationSpaceLocked()
        );
    }
}
