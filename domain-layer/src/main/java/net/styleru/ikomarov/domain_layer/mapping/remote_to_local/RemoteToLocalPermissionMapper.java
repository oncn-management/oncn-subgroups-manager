package net.styleru.ikomarov.domain_layer.mapping.remote_to_local;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.permissions.PermissionCategory;
import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 22.03.17.
 */

public class RemoteToLocalPermissionMapper implements Function<PermissionRemoteEntity, PermissionLocalEntity> {

    @NonNull
    private final PermissionCategory hostType;

    @NonNull
    private final String hostId;

    public RemoteToLocalPermissionMapper(@NonNull PermissionCategory hostType, @NonNull String hostId) {
        this.hostType = hostType;
        this.hostId = hostId;
    }

    @Override
    public PermissionLocalEntity apply(PermissionRemoteEntity entity) throws Exception {
        return PermissionLocalEntity.newInstance(
                entity.getId(),
                hostId,
                hostType.code(),
                entity.getName(),
                entity.getSelf(),
                entity.getUserId(),
                entity.getUserRole().value()
        );
    }
}
