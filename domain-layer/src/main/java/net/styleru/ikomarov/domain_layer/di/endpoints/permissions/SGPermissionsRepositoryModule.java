package net.styleru.ikomarov.domain_layer.di.endpoints.permissions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.repository.permissions.ISGPermissionsRepository;
import net.styleru.ikomarov.domain_layer.repository.permissions.SGPermissionsRepository;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SGPermissionsRepositoryModule {

    private final Object lock = new Object();

    @NonNull
    private final ISGPermissionsEnvironmentComponent component;

    @Nullable
    private volatile ISGPermissionsRepository repository;

    public SGPermissionsRepositoryModule(@NonNull ISGPermissionsEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public ISGPermissionsRepository provideRepository() {
        ISGPermissionsRepository localInstance = this.repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = SGPermissionsRepository.Factory.create(
                            component.localService().provideLocalService(),
                            component.remoteService().provideRemoteService(),
                            component.cacheComponent().strategy().provideCacheStrategy(),
                            component.mapping().provideMapping()
                    );
                }
            }
        }

        return localInstance;
    }
}
