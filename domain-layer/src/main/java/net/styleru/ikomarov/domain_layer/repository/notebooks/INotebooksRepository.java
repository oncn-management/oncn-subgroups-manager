package net.styleru.ikomarov.domain_layer.repository.notebooks;

import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;
import net.styleru.ikomarov.domain_layer.repository.base.IRepository;

import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 09.03.17.
 */

public interface INotebooksRepository extends IRepository {

    Observable<OperationStatus> create(UUID operationId, ClassNotebookDTO.Builder builder);

    Observable<ClassNotebookDTO> get(String id);

    Observable<List<ClassNotebookDTO>> getList(int offset, int limit);

    Observable<List<ClassNotebookDTO>> getList(int offset, int limit, String nameSubstring);

    Observable<OperationStatus> delete(UUID operationId, String id);
}
