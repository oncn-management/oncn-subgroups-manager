package net.styleru.ikomarov.domain_layer.di.endpoints.reports;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.di.endpoints.notebooks.INotebooksComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.permissions.ISGPermissionsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.section_groups.ISectionGroupsComponent;
import net.styleru.ikomarov.domain_layer.facades.repository.IReportRepositoryFacade;
import net.styleru.ikomarov.domain_layer.facades.repository.ReportRepositoryFacade;

/**
 * Created by i_komarov on 18.04.17.
 */

public class ReportRepositoryFacadeModule {

    private final Object lock = new Object();

    @NonNull
    private final INotebookReportRepositoryComponent environment;

    @NonNull
    private final INotebooksComponent notebooks;

    @NonNull
    private final ISectionGroupsComponent sectionGroups;

    @NonNull
    private final ISGPermissionsComponent permissions;

    @Nullable
    private volatile IReportRepositoryFacade reports;

    public ReportRepositoryFacadeModule(@NonNull INotebookReportRepositoryComponent environment,
                                        @NonNull INotebooksComponent notebooks,
                                        @NonNull ISectionGroupsComponent sectionGroups,
                                        @NonNull ISGPermissionsComponent permissions) {
        this.environment = environment;
        this.notebooks = notebooks;
        this.sectionGroups = sectionGroups;
        this.permissions = permissions;
    }

    @NonNull
    public IReportRepositoryFacade provideReportRepositoryFacade() {
        IReportRepositoryFacade localInstance = reports;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = reports;
                if(localInstance == null) {
                    localInstance = reports = new ReportRepositoryFacade(
                            notebooks.repository().provideRepository(),
                            sectionGroups.repository().provideRepository(),
                            permissions.repository().provideRepository(),
                            environment.notebooks().provideNotebooksRepository()
                    );
                }
            }
        }

        return localInstance;
    }
}
