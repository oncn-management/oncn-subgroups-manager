package net.styleru.ikomarov.data_layer.contracts.principal_object;

/**
 * Created by i_komarov on 18.02.17.
 */

class PrincipalObjectContract {

    static final String TYPE_GROUP = "Group";
    static final String TYPE_PERSON = "Person";

    private PrincipalObjectContract() {
        throw new IllegalStateException("No instances please");
    }
}
