package net.styleru.ikomarov.data_layer.services.users;

import android.database.Cursor;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;

import net.styleru.ikomarov.data_layer.entities.local.users.UserWithBusinessPhonesLocalEntity;
import net.styleru.ikomarov.data_layer.mapping.local.users.CompleteUserLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.users.UsersBusinessPhonesLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.users.UsersLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.users.queries.UserQueries;
import net.styleru.ikomarov.data_layer.services.base.DatabaseService;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by i_komarov on 01.03.17.
 */

public class UsersLocalStorageService extends DatabaseService implements IUsersLocalStorageService {

    private UsersLocalStorageService(StorIOSQLite sqlite) {
        super(sqlite);
    }

    @Override
    public Observable<PutResult> create(final UserWithBusinessPhonesLocalEntity entity) {
        return wrapTransactionSynchronousOff(new Producer<PutResult>() {
            @Override
            public PutResult produce() throws Exception {
                PutResult result = getSQLite().put()
                        .object(entity.getUser())
                        .prepare()
                        .executeAsBlocking();
                getSQLite().put()
                        .objects(entity.getBusinessPhones())
                        .prepare()
                        .executeAsBlocking();

                return result;
            }
        });
    }

    @Override
    public Observable<UserWithBusinessPhonesLocalEntity> read(final String id) {
        return wrap(new Producer<UserWithBusinessPhonesLocalEntity>() {
            @Override
            public UserWithBusinessPhonesLocalEntity produce() throws Exception {
                Cursor cursor = getSQLite().get()
                        .cursor()
                        .withQuery(RawQuery.builder().query(UserQueries.query(id)).build())
                        .prepare()
                        .executeAsBlocking();

                try {
                    return new CompleteUserLocalEntityMapping().fromCursor(cursor);
                } finally {
                    cursor.close();
                }
            }
        });
    }

    @Override
    public Observable<List<UserWithBusinessPhonesLocalEntity>> read(final int offset, final int limit) {
        return wrap(new Producer<List<UserWithBusinessPhonesLocalEntity>>() {
            @Override
            public List<UserWithBusinessPhonesLocalEntity> produce() throws Exception {
                Cursor cursor = getSQLite().get()
                        .cursor()
                        .withQuery(RawQuery.builder().query(UserQueries.query(offset, limit)).build())
                        .prepare()
                        .executeAsBlocking();

                try {
                    return new CompleteUserLocalEntityMapping().listFromCursor(cursor);
                } finally {
                    cursor.close();
                }
            }
        });
    }

    @Override
    public Observable<List<UserWithBusinessPhonesLocalEntity>> read(final int offset, final int limit, final String displayNameFilter) {
        return wrap(new Producer<List<UserWithBusinessPhonesLocalEntity>>() {
            @Override
            public List<UserWithBusinessPhonesLocalEntity> produce() throws Exception {
                Cursor cursor = getSQLite().get()
                        .cursor()
                        .withQuery(RawQuery.builder().query(UserQueries.query(offset, limit, displayNameFilter)).build())
                        .prepare()
                        .executeAsBlocking();

                try {
                    return new CompleteUserLocalEntityMapping().listFromCursor(cursor);
                } finally {
                    cursor.close();
                }
            }
        });
    }

    @Override
    public Observable<DeleteResult> delete(final String id) {
        return wrapTransactionSynchronousOff(new Producer<DeleteResult>() {
            @Override
            public DeleteResult produce() throws Exception {
                getSQLite().delete()
                        .byQuery(
                                DeleteQuery.builder().table(UsersBusinessPhonesLocalEntityMapping.TABLE)
                                        .where(UsersBusinessPhonesLocalEntityMapping.COLUMN_USER_ID + " = " + "\"" + id + "\"")
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking();
                return getSQLite().delete()
                        .byQuery(
                                DeleteQuery.builder().table(UsersLocalEntityMapping.TABLE)
                                        .where(UsersLocalEntityMapping.COLUMN_ID + " = " + "\"" + id + "\"")
                                        .build()
                        )
                        .prepare()
                        .executeAsBlocking();
            }
        });
    }

    private <T> Observable<T> wrap(final UsersLocalStorageService.Producer<T> producer) {
        return Observable.create(new ObservableOnSubscribe<T>() {
            @Override
            public void subscribe(final ObservableEmitter<T> e) throws Exception {
                if(!e.isDisposed()) {
                    try {
                        T data = producer.produce();
                        e.onNext(data);
                    } catch(Exception thr) {
                        e.onError(parseException(thr));
                    }
                }
            }
        });
    }

    private <T> Observable<T> wrapTransactionSynchronousOff(final Producer<T> producer) {
        return Observable.create(
                new ObservableOnSubscribe<T>() {
                    @Override
                    public void subscribe(final ObservableEmitter<T> e) throws Exception {
                        if(!e.isDisposed()) {
                            try {
                                getSQLite().lowLevel().executeSQL(RawQuery.builder().query("PRAGMA synchronous=OFF").build());
                                getSQLite().lowLevel().beginTransaction();

                                T data = producer.produce();

                                getSQLite().lowLevel().setTransactionSuccessful();
                                getSQLite().lowLevel().endTransaction();
                                e.onNext(data);
                            } catch(Exception thr) {
                                getSQLite().lowLevel().endTransaction();
                                e.onError(parseException(thr));
                            } finally {
                                getSQLite().lowLevel().executeSQL(RawQuery.builder().query("PRAGMA synchronous=NORMAL").build());
                            }
                        }
                    }
                }
        );
    }

    private interface Producer<T> {

        T produce() throws Exception;
    }

    public static class Factory {

        public static IUsersLocalStorageService create(StorIOSQLite sqlite) {
            return new UsersLocalStorageService(sqlite);
        }
    }
}
