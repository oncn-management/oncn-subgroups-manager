package net.styleru.ikomarov.domain_layer.di.level_4.session_consumer;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;

/**
 * Created by i_komarov on 13.03.17.
 */

public class ClientComponent implements IClientComponent {

    @NonNull
    private final ClientModule client;

    public ClientComponent(@NonNull ISessionComponent parent) {
        this.client = new ClientModule(parent);
    }

    @NonNull
    @Override
    public ClientModule clients() {
        return client;
    }
}
