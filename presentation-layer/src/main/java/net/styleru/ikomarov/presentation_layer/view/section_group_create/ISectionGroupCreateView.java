package net.styleru.ikomarov.presentation_layer.view.section_group_create;

import android.support.annotation.StringRes;

import net.styleru.ikomarov.presentation_layer.view.base.IView;

/**
 * Created by i_komarov on 21.03.17.
 */

public interface ISectionGroupCreateView extends IView {

    void onSectionGroupCreated(String sectionGroupId);

    void showNotification(@StringRes int notification);

    void showError(@StringRes int error);

    void showConnectionFailedIndicator();

    void showOperationDelayedDialog();

    void showNoCachedDataPlug();

    void showNoDataPlug();

    void showNoPermissionsDialog();

    void showAuthorizationDialog();
}
