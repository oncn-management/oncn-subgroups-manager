package net.styleru.ikomarov.presentation_layer.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import android.graphics.BitmapFactory;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import net.styleru.ikomarov.presentation_layer.BuildConfig;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.view.drawer.DrawerActivity;

/**
 * Created by i_komarov on 20.04.17.
 */

public class LocalNotificationsReceiver extends BroadcastReceiver {

    @NonNull
    private static final String PREFIX = LocalNotificationsReceiver.class.getCanonicalName() + ".";

    @NonNull
    public static final String KEY_NOTIFICATION_TYPE = PREFIX + "TYPE";

    @NonNull
    public static final String KEY_NOTIFICATION_TEXT = PREFIX + "TEXT";

    @NonNull
    public static final String KEY_NOTIFICATION_EXTRAS = PREFIX + "EXTRAS";

    @NonNull
    public static final String KEY_NOTIFICATION_UUID = PREFIX + "UUID";

    @NonNull
    public static final String KEY_RESOURCE_ID = PREFIX + "RESOURCE_ID";

    @NonNull
    public static final String KEY_NOTIFICATION_CLEANUP = PREFIX + "CLEANUP";

    public static final int NOTIFICATION_SECTION_GROUP = 0;
    public static final int NOTIFICATION_SECTION_GROUP_NESTED = 1;
    public static final int NOTIFICATION_PERMISSIONS_SECTION_GROUP = 2;
    public static final int NOTIFICATION_STUDENTS = 3;
    public static final int NOTIFICATION_TEACHERS = 4;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!intent.getAction().equals(BuildConfig.LOCAL_NOTIFICATIONS_ACTION)) {
            return;
        }

        Bundle extras;
        if(intent != null && (extras = intent.getExtras()) != null) {
            int notificationType = extras.getInt(KEY_NOTIFICATION_TYPE);
            NotificationManager notificationManager = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                    .setContentTitle(context.getResources().getString(R.string.title_operation_success))
                    .setContentText(extras.getString(KEY_NOTIFICATION_TEXT))
                    .setAutoCancel(true)
                    .setLights(context.getResources().getColor(R.color.colorAccent), 100, 1900)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setDefaults(NotificationCompat.DEFAULT_SOUND | NotificationCompat.DEFAULT_VIBRATE);

            Intent clicksIntent = new Intent(context, DrawerActivity.class);

            Bundle data = new Bundle();
            if(extras.containsKey(KEY_NOTIFICATION_EXTRAS)) {
                data.putBundle(KEY_NOTIFICATION_EXTRAS, extras.getBundle(KEY_NOTIFICATION_EXTRAS));
            }
            data.putInt(KEY_NOTIFICATION_TYPE, notificationType);
            data.putString(KEY_NOTIFICATION_UUID, extras.getString(KEY_NOTIFICATION_UUID));
            data.putString(KEY_RESOURCE_ID, extras.getString(KEY_RESOURCE_ID));

            clicksIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent resultPendingIntent = PendingIntent.getActivity(context, notificationType, clicksIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            notificationManager.notify(notificationType, builder.build());

            if(isOrderedBroadcast()) {
                abortBroadcast();
            }
        }
    }
}
