package net.styleru.ikomarov.data_layer.entities.report.permissions;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.report.base.ReportSubgroupEntity;

/**
 * Created by i_komarov on 13.04.17.
 */

public class PermissionReportSubgroupEntity extends ReportSubgroupEntity {

    @NonNull
    private final String permission;

    public PermissionReportSubgroupEntity(@NonNull String reportName, @NonNull String permission) {
        super(reportName);
        this.permission = permission;
    }

    @NonNull
    public String getPermission() {
        return permission;
    }

    @Override
    public String toString() {
        return "PermissionReportSubgroupEntity{" +
                "permission='" + permission + '\'' +
                '}';
    }
}
