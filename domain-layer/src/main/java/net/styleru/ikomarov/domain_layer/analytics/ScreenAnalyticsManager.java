package net.styleru.ikomarov.domain_layer.analytics;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import net.styleru.ikomarov.data_layer.BuildConfig;
import net.styleru.ikomarov.data_layer.manager.events.IEventsLogger;

/**
 * Created by i_komarov on 11.05.17.
 */

public class ScreenAnalyticsManager implements IScreenAnalyticsManager {

    private static final String TAG_SCREEN = "SCREEN_PICK";

    private static final String KEY_SCREEN = "SCREEN_NAME";

    private static final String KEY_RESOURCE_ID = "RESOURCE_ID";

    @NonNull
    private final IEventsLogger logger;

    public ScreenAnalyticsManager(@NonNull IEventsLogger logger) {
        Log.d("ScreenAnalyticsManager", "build config injected correctly? " +
                (
                        BuildConfig.AZURE_AD_API_KEY == null || BuildConfig.AZURE_AD_API_KEY.equals("") ?
                        "no" :
                        "yes"
                )
        );
        this.logger = logger;
    }

    @Override
    public void onScreenEvent(String screen) {
        Log.d("ScreenAnalyticsManager", "onScreenEvent(" + screen + ")");
        Bundle bundle = new Bundle();
        bundle.putString(KEY_SCREEN, screen);
        logger.logEvent(TAG_SCREEN, bundle);
    }

    @Override
    public void onScreenEvent(String screen, String resourceId) {
        Log.d("ScreenAnalyticsManager", "onScreenEvent(" + screen + "," + resourceId + ")");
        Bundle bundle = new Bundle();
        bundle.putString(KEY_SCREEN, screen);
        bundle.putString(KEY_RESOURCE_ID, resourceId);
        logger.logEvent(TAG_SCREEN, bundle);
    }
}
