package net.styleru.ikomarov.domain_layer.use_cases.students;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.repository.students.IStudentsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 09.04.17.
 */

public class StudentDeleteUseCase extends UseCase {

    @NonNull
    private IStudentsRepository repository;

    @NonNull
    private Scheduler executeScheduler;

    @NonNull
    private Scheduler postScheduler;

    public StudentDeleteUseCase(@NonNull IStudentsRepository repository,
                              @NonNull Scheduler executeScheduler,
                              @NonNull Scheduler postScheduler) {

        super();
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public UUID execute(Callbacks callbacks, String notebookId, String studentId) {
        UUID operationId;
        execute(operationId = UUID.randomUUID(), repository.delete(operationId, notebookId, studentId), callbacks);
        return operationId;
    }

    private void execute(UUID operationId, Observable<OperationStatus> upstream, Callbacks callbacks) {
        upstream.subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createStudentDeleteActionObserver(operationId, callbacks));
    }

    private <T> DisposableObserver<OperationStatus> createStudentDeleteActionObserver(final UUID operationId, final Callbacks callbacks) {
        DisposableObserver<OperationStatus> studentDeleteActionObserver = new DisposableObserver<OperationStatus>() {
            @Override
            public void onNext(OperationStatus value) {
                switch(value) {
                    case SUCCESS: {
                        callbacks.onStudentDeleted(operationId);
                        break;
                    }
                    case FAILURE: {
                        callbacks.onStudentDeletionFailed(operationId, new ExceptionBundle(ExceptionBundle.Reason.INTERNAL_DOMAIN));
                        break;
                    }
                    case DELAYED: {
                        callbacks.onStudentDeletionDelayed(operationId);
                        break;
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                callbacks.onStudentDeletionFailed(operationId, e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(studentDeleteActionObserver);

        return studentDeleteActionObserver;
    }

    public interface Callbacks {

        void onStudentDeleted(UUID operationId);

        void onStudentDeletionDelayed(UUID operationId);

        void onStudentDeletionFailed(UUID operationId, ExceptionBundle error);
    }
}
