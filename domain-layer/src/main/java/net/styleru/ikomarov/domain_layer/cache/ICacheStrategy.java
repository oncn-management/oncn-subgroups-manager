package net.styleru.ikomarov.domain_layer.cache;

import net.styleru.ikomarov.domain_layer.contracts.policies.CachePolicy;

/**
 * Created by i_komarov on 08.03.17.
 */

public interface ICacheStrategy {

    CachePolicy currentPolicy();
}
