package net.styleru.ikomarov.domain_layer.use_cases.permissions;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;
import net.styleru.ikomarov.domain_layer.repository.permissions.ISGPermissionsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 02.04.17.
 */

public class SGPermissionsGetUseCase extends UseCase {

    @NonNull
    private final ISGPermissionsRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public SGPermissionsGetUseCase(@NonNull ISGPermissionsRepository repository,
                                   @NonNull Scheduler executeScheduler,
                                   @NonNull Scheduler postScheduler) {

        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<List<PermissionDTO>, List<T>> mapper, Callbacks<T> callbacks, String sectionGroupId) {
        repository.read(sectionGroupId, true)
                .subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createPermissionsObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createPermissionsObserver(final Callbacks<T> callbacks) {
        DisposableObserver<List<T>> permissionsObserver = new DisposableObserver<List<T>>() {
            @Override
            public void onNext(List<T> value) {
                if(!isDisposed()) {
                    callbacks.onSGPermissionsLoaded(value);
                }
            }

            @Override
            public void onError(Throwable e) {
                if(!isDisposed()) {
                    callbacks.onSGPermissionsLoadingFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(permissionsObserver);

        return permissionsObserver;
    }

    public interface Callbacks<T> {

        void onSGPermissionsLoaded(List<T> permissions);

        void onSGPermissionsLoadingFailed(ExceptionBundle error);
    }
}
