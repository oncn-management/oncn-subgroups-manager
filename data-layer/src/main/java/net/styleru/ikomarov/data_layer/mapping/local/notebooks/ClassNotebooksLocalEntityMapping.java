package net.styleru.ikomarov.data_layer.mapping.local.notebooks;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

import net.styleru.ikomarov.data_layer.contracts.database.DatabaseContracts;
import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;
import net.styleru.ikomarov.data_layer.mapping.utils.QueryBuilder;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by i_komarov on 28.02.17.
 */

public class ClassNotebooksLocalEntityMapping implements ILocalMapping<ClassNotebookLocalEntity> {

    @NonNull
    public static final String TABLE_NOTEBOOKS = "class_notebooks";

    @NonNull
    public static final String TABLE_TEACHERS_LINKING = "teachers_to_notebook_link";

    @NonNull
    public static final String TABLE_STUDENTS_LINKING = "students_to_notebook_link";

    @NonNull
    public static final String INDEX_TEACHERS = "teachers_link_index";

    @NonNull
    public static final String INDEX_STUDENTS = "students_link_index";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_NOTEBOOK_ID = "NOTEBOOK_ID";

    @NonNull
    public static final String COLUMN_TEACHER_ID = "TEACHER_ID";

    @NonNull
    public static final String COLUMN_TEACHER_TYPE = "TEACHER_TYPE";

    @NonNull
    public static final String COLUMN_STUDENT_ID = "STUDENT_ID";

    @NonNull
    public static final String COLUMN_STUDENT_TYPE = "STUDENT_TYPE";

    @NonNull
    public static final String COLUMN_NAME = "NAME";

    @NonNull
    public static final String COLUMN_IS_DEFAULT = "IS_DEFAULT";

    @NonNull
    public static final String COLUMN_IS_SHARED = "IS_SHARED";

    @NonNull
    public static final String COLUMN_CREATED_BY = "CREATED_BY";

    @NonNull
    public static final String COLUMN_CREATED_TIME = "CREATED_TIME";

    @NonNull
    public static final String COLUMN_LAST_MODIFIED_BY = "LAST_MODIFIED_BY";

    @NonNull
    public static final String COLUMN_LAST_MODIFIED_TIME = "LAST_MODIFIED_TIME";

    @NonNull
    public static final String COLUMN_WEB_URL = "WEB_URL";

    @NonNull
    public static final String COLUMN_CLIENT_URL = "CLIENT_URL";

    @NonNull
    public static final String COLUMN_SECTIONS_URL = "SECTIONS_URL";

    @NonNull
    public static final String COLUMN_SECTION_GROUPS_URL = "SECTION_GROUPS_URL";

    @NonNull
    public static final String COLUMN_SELF = "SELF";

    @NonNull
    public static final String COLUMN_USER_ROLE = "USER_ROLE";

    @NonNull
    public static final String COLUMN_HAS_TEACHER_ONLY_SECTION_GROUP = "HAS_TEACHER_ONLY_SECTION_GROUP";

    @NonNull
    public static final String COLUMN_IS_COLLABORATION_SPACE_LOCKED = "IS_COLLABORATION_SPACE_LOCKED";


    public static final String COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_ID;

    public static final String COLUMN_TEACHER_ID_WITH_TABLE_PREFIX = PrincipalObjectsLocalEntityMapping.TABLE + "." + PrincipalObjectsLocalEntityMapping.COLUMN_ID;
    public static final String COLUMN_STUDENT_ID_WITH_TABLE_PREFIX = PrincipalObjectsLocalEntityMapping.TABLE + "." + PrincipalObjectsLocalEntityMapping.COLUMN_ID;;
    public static final String COLUMN_NOTEBOOK_ID_WITH_TEACHERS_LINKING_TABLE_PREFIX = TABLE_TEACHERS_LINKING + "." + COLUMN_NOTEBOOK_ID;
    public static final String COLUMN_NOTEBOOK_ID_WITH_STUDENTS_LINKING_TABLE_PREFIX = TABLE_STUDENTS_LINKING + "." + COLUMN_NOTEBOOK_ID;
    public static final String COLUMN_TEACHER_ID_WITH_TEACHERS_LINKING_TABLE_PREFIX = TABLE_TEACHERS_LINKING + "." + COLUMN_TEACHER_ID;
    public static final String COLUMN_STUDENT_ID_WITH_STUDENTS_LINKING_TABLE_PREFIX = TABLE_STUDENTS_LINKING + "." + COLUMN_STUDENT_ID;

    public static final String COLUMN_NOTEBOOK_NAME_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_NAME;
    public static final String COLUMN_NOTEBOOK_IS_DEFAULT_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_IS_DEFAULT;
    public static final String COLUMN_NOTEBOOK_IS_SHARED_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_IS_SHARED;
    public static final String COLUMN_NOTEBOOK_CREATED_BY_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_CREATED_BY;
    public static final String COLUMN_NOTEBOOK_CREATED_TIME_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_CREATED_TIME;
    public static final String COLUMN_NOTEBOOK_LAST_MODIFIED_BY_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_LAST_MODIFIED_BY;
    public static final String COLUMN_NOTEBOOK_LAST_MODIFIED_TIME_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_LAST_MODIFIED_TIME;
    public static final String COLUMN_NOTEBOOK_WEB_URL_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_WEB_URL;
    public static final String COLUMN_NOTEBOOK_CLIENT_URL_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_CLIENT_URL;
    public static final String COLUMN_NOTEBOOK_SECTIONS_URL_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_SECTIONS_URL;
    public static final String COLUMN_NOTEBOOK_SECTION_GROUPS_URL_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_SECTION_GROUPS_URL;
    public static final String COLUMN_NOTEBOOK_SELF_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_SELF;
    public static final String COLUMN_NOTEBOOK_USER_ROLE_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_USER_ROLE;
    public static final String COLUMN_NOTEBOOK_HAS_TEACHER_ONLY_SECTION_GROUP_WITH_TABLE_PREFIX = TABLE_NOTEBOOKS + "." + COLUMN_HAS_TEACHER_ONLY_SECTION_GROUP;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE_NOTEBOOKS)
            .build();

    public ClassNotebooksLocalEntityMapping() {

    }

    public static List<String> getInitializationQueries() {
        return Arrays.asList(
                getCreateTableQuery(),
                getStudentsLinkingTableCreateQuery(),
                getStudentsLinkingTableIndexCreateQuery(),
                getTeachersLinkingTableCreateQuery(),
                getTeachersLinkingTableIndexCreateQuery()
        );
    }

    private static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_NOTEBOOKS + "(" +
                    COLUMN_ID + " TEXT PRIMARY KEY NOT NULL," +
                    COLUMN_NAME + " TEXT NOT NULL," +
                    COLUMN_IS_DEFAULT + " BOOLEAN DEFAULT(0)," +
                    COLUMN_IS_SHARED + " BOOLEAN DEFAULT(0)," +
                    COLUMN_CREATED_BY + " TEXT DEFAULT ''," +
                    COLUMN_CREATED_TIME + " TIMESTAMP DEFAULT(STRFTIME(" + DateTimeContract.SQL_DATE_FORMAT + "," + DateTimeContract.SQL_DATE_CURRENT + "))," +
                    COLUMN_LAST_MODIFIED_BY + " TEXT DEFAULT ''," +
                    COLUMN_LAST_MODIFIED_TIME + " TIMESTAMP DEFAULT(STRFTIME(" + DateTimeContract.SQL_DATE_FORMAT + "," + DateTimeContract.SQL_DATE_CURRENT + "))," +
                    COLUMN_WEB_URL + " TEXT," +
                    COLUMN_CLIENT_URL + " TEXT," +
                    COLUMN_SECTIONS_URL + " TEXT," +
                    COLUMN_SECTION_GROUPS_URL + " TEXT," +
                    COLUMN_SELF + " TEXT," +
                    COLUMN_USER_ROLE + " TEXT," +
                    COLUMN_HAS_TEACHER_ONLY_SECTION_GROUP + " BOOLEAN DEFAULT(0)," +
                    COLUMN_IS_COLLABORATION_SPACE_LOCKED + " BOOLEAN DEFAULT(0)" +
                ");";

    }

    private static String getTeachersLinkingTableCreateQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_TEACHERS_LINKING + "(" +
                    COLUMN_NOTEBOOK_ID + " TEXT NOT NULL," +
                    COLUMN_TEACHER_ID + " TEXT NOT NULL" +
                ");";
    }

    private static String getTeachersLinkingTableIndexCreateQuery() {
        return "CREATE INDEX IF NOT EXISTS " + INDEX_TEACHERS + " ON " + TABLE_TEACHERS_LINKING + " (" + COLUMN_NOTEBOOK_ID + "," + COLUMN_TEACHER_ID + ");";
    }

    public static String onInsertTeacherExecuteQuery(String notebookId, String teacherId) {
        return "INSERT INTO " + TABLE_TEACHERS_LINKING + "(" + COLUMN_NOTEBOOK_ID + "," + COLUMN_TEACHER_ID + ") VALUES(\"" + notebookId + "\",\"" + teacherId + "\");";
    }

    public static String onDeleteTeacherExecuteQuery(String notebookId, String teacherId) {
        return "DELETE FROM " + TABLE_TEACHERS_LINKING + " WHERE " + COLUMN_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"" + " AND " + COLUMN_TEACHER_ID + " = " + "\"" + teacherId + "\"";
    }

    private static String getStudentsLinkingTableCreateQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE_STUDENTS_LINKING + "(" +
                    COLUMN_NOTEBOOK_ID + " TEXT NOT NULL," +
                    COLUMN_STUDENT_ID + " TEXT NOT NULL" +
                ");";
    }

    private static String getStudentsLinkingTableIndexCreateQuery() {
        return "CREATE INDEX IF NOT EXISTS " + INDEX_STUDENTS + " ON " + TABLE_STUDENTS_LINKING + " (" + COLUMN_NOTEBOOK_ID + "," + COLUMN_STUDENT_ID + ");";
    }

    public static String onInsertStudentExecuteQuery(String notebookId, String studentId) {
        return "INSERT INTO " + TABLE_STUDENTS_LINKING + "(" + COLUMN_NOTEBOOK_ID + "," + COLUMN_STUDENT_ID + ") VALUES(" + "\"" + notebookId + "\",\"" + studentId + "\");";
    }

    public static String onDeleteStudentExecuteQuery(String notebookId, String studentId) {
        return "DELETE FROM " + TABLE_STUDENTS_LINKING + " WHERE " + COLUMN_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"" + " AND " + COLUMN_STUDENT_ID + " = " + "\"" + studentId + "\"";
    }

    public static String getTeachersQuery(String notebookId, int offset, int limit) {
        return createGetTeachersByNotebookIdQuery(notebookId, offset, limit, null, null, null);
    }

    public static String getTeachersQuery(String notebookId, int offset, int limit, String orderByColumn, QueryBuilder.Order order) {
        return createGetTeachersByNotebookIdQuery(notebookId, offset, limit, orderByColumn, order, null);
    }

    public static String getTeachersQuery(String notebookId, int offset, int limit, String orderByColumn, QueryBuilder.Order order, String nameSubstring) {
        return createGetTeachersByNotebookIdQuery(notebookId, offset, limit, orderByColumn, order, nameSubstring);
    }

    private static String createGetTeachersByNotebookIdQuery(String notebookId, int offset, int limit, String orderByColumn, QueryBuilder.Order order, String nameSubstring) {
        QueryBuilder.SelectBuilder subselectBuilder = QueryBuilder.selectQuery(PrincipalObjectsLocalEntityMapping.TABLE)
                .limit(offset, limit)
                .orderBy(orderByColumn, order);

        if(nameSubstring != null) {
            subselectBuilder.where(PrincipalObjectsLocalEntityMapping.COLUMN_NAME + " = " + "\"" + nameSubstring + "\"" + " OR " + PrincipalObjectsLocalEntityMapping.COLUMN_NAME + " LIKE " + "\"" + nameSubstring + "%\"");
        }

        QueryBuilder.SelectBuilder builder = QueryBuilder.selectQuery(PrincipalObjectsLocalEntityMapping.TABLE)
                .overrideTableWithNestedSelect(subselectBuilder.build())
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_ID)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_NAME_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_NAME)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_EMAIL_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_EMAIL)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_DEPARTMENT_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_DEPARTMENT)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_TYPE_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_TYPE)
                .innerJoin(TABLE_TEACHERS_LINKING, PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, COLUMN_TEACHER_ID_WITH_TEACHERS_LINKING_TABLE_PREFIX)
                .innerJoin(TABLE_NOTEBOOKS, COLUMN_NOTEBOOK_ID_WITH_TEACHERS_LINKING_TABLE_PREFIX, COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX)
                .where(COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX + " = " + "\'" + notebookId + "\'");

        return builder.build();
    }

    public static String getStudentsQuery(String notebookId, int offset, int limit) {
        return createGetStudentsByNotebookIdQuery(notebookId, offset, limit, null, null, null);
    }

    public static String getStudentsQuery(String notebookId, int offset, int limit, String orderByColumn, QueryBuilder.Order order) {
        return createGetStudentsByNotebookIdQuery(notebookId, offset, limit, orderByColumn, order, null);
    }

    public static String getStudentsQuery(String notebookId, int offset, int limit, String orderByColumn, QueryBuilder.Order order, String nameSubstring) {
        return createGetStudentsByNotebookIdQuery(notebookId, offset, limit, orderByColumn, order, nameSubstring);
    }

    private static String createGetStudentsByNotebookIdQuery(String notebookId, int offset, int limit, String orderByColumn, QueryBuilder.Order order, String nameSubstring) {
        QueryBuilder.SelectBuilder subselectBuilder = QueryBuilder.selectQuery(PrincipalObjectsLocalEntityMapping.TABLE)
                .limit(offset, limit)
                .orderBy(orderByColumn, order);

        if(nameSubstring != null) {
            subselectBuilder.where(PrincipalObjectsLocalEntityMapping.COLUMN_NAME + " = " + "\"" + nameSubstring + "\"" + " OR " + PrincipalObjectsLocalEntityMapping.COLUMN_NAME + " LIKE " + "\"" + nameSubstring + "%\"");
        }

        QueryBuilder.SelectBuilder builder = QueryBuilder.selectQuery(PrincipalObjectsLocalEntityMapping.TABLE)
                .overrideTableWithNestedSelect(subselectBuilder.build())
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_ID)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_NAME_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_NAME)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_EMAIL_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_EMAIL)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_DEPARTMENT_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_DEPARTMENT)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_TYPE_WITH_TABLE_PREFIX, DatabaseContracts.PrincipalObjectsContract.COLUMN_TYPE)
                .innerJoin(TABLE_STUDENTS_LINKING, PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, COLUMN_STUDENT_ID_WITH_STUDENTS_LINKING_TABLE_PREFIX)
                .innerJoin(TABLE_NOTEBOOKS, COLUMN_NOTEBOOK_ID_WITH_STUDENTS_LINKING_TABLE_PREFIX, COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX)
                .where(COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX + " = " + "\'" + notebookId + "\'");

        return builder.build();
    }

    @Override
    public ClassNotebookLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException, ParseException {
        if(cursor.moveToFirst()) {
            return ClassNotebookLocalEntity.newInstance(
                    cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_IS_DEFAULT)) == 1,
                    cursor.getInt(cursor.getColumnIndex(COLUMN_IS_SHARED)) == 1,
                    cursor.getString(cursor.getColumnIndex(COLUMN_CREATED_BY)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_CREATED_TIME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_LAST_MODIFIED_BY)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_LAST_MODIFIED_TIME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_WEB_URL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_CLIENT_URL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_SECTIONS_URL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_SECTION_GROUPS_URL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_SELF)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_USER_ROLE)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_HAS_TEACHER_ONLY_SECTION_GROUP)) == 1,
                    cursor.getInt(cursor.getColumnIndex(COLUMN_IS_COLLABORATION_SPACE_LOCKED)) == 1
            );
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<ClassNotebookLocalEntity> listFromCursor(Cursor cursor) throws ParseException {
        List<ClassNotebookLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                entities.add(
                        ClassNotebookLocalEntity.newInstance(
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_NAME)),
                                cursor.getInt(indexCache.indexFor(cursor, COLUMN_IS_DEFAULT)) == 1,
                                cursor.getInt(indexCache.indexFor(cursor, COLUMN_IS_SHARED)) == 1,
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_CREATED_BY)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_CREATED_TIME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_LAST_MODIFIED_BY)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_LAST_MODIFIED_TIME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_WEB_URL)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_CLIENT_URL)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_SECTIONS_URL)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_SECTION_GROUPS_URL)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_SELF)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_ROLE)),
                                cursor.getInt(indexCache.indexFor(cursor, COLUMN_HAS_TEACHER_ONLY_SECTION_GROUP)) == 1,
                                cursor.getInt(indexCache.indexFor(cursor, COLUMN_IS_COLLABORATION_SPACE_LOCKED)) == 1
                        )
                );
            } while(cursor.moveToNext());
        }

        return entities;
    }
}
