package net.styleru.ikomarov.domain_layer.mapping.class_notebooks;

import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ClassNotebookRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.03.17.
 */

public class RemoteClassNotebooksMapper implements Function<List<ClassNotebookRemoteEntity>, List<ClassNotebookDTO>> {

    @Override
    public List<ClassNotebookDTO> apply(List<ClassNotebookRemoteEntity> classNotebookRemoteEntities) throws Exception {
        return Observable.fromIterable(classNotebookRemoteEntities)
                .map(new RemoteClassNotebookMapper())
                .toList()
                .blockingGet();
    }
}
