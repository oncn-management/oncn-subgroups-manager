package net.styleru.ikomarov.domain_layer.dto.base;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 17.04.17.
 */

public class ReportSubgroupDTO {

    @NonNull
    private final String name;

    public ReportSubgroupDTO(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getName() {
        return name;
    }
}
