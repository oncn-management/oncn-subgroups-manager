package net.styleru.ikomarov.domain_layer.di.endpoints.notebooks;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * Created by i_komarov on 14.03.17.
 */

public class NotebooksEnvironmentComponent implements INotebooksEnvironmentComponent {

    @NonNull
    private final NotebooksMappingModule mapping;

    @NonNull
    private final NotebooksRemoteServiceModule remote;

    @NonNull
    private final NotebooksLocalServiceModule local;

    @NonNull
    private final NotebooksCacheModule cache;

    @NonNull
    private final ICacheComponent cacheComponent;

    public NotebooksEnvironmentComponent(@NonNull IEnvironmentComponent environment, @NonNull IClientComponent client, @NonNull ICacheComponent cacheComponent) {
        this.mapping = new NotebooksMappingModule();
        this.remote = new NotebooksRemoteServiceModule(client);
        this.local = new NotebooksLocalServiceModule(environment);
        this.cacheComponent = cacheComponent;
        this.cache = new NotebooksCacheModule(this);
    }

    @NonNull
    @Override
    public ICacheComponent cacheComponent() {
        return cacheComponent;
    }

    @NonNull
    @Override
    public NotebooksMappingModule mapping() {
        return this.mapping;
    }

    @NonNull
    @Override
    public NotebooksRemoteServiceModule remote() {
        return this.remote;
    }

    @NonNull
    @Override
    public NotebooksLocalServiceModule local() {
        return this.local;
    }

    @NonNull
    @Override
    public NotebooksCacheModule cache() {
        return this.cache;
    }
}
