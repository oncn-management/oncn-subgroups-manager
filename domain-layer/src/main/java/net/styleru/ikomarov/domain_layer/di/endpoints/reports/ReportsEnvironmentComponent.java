package net.styleru.ikomarov.domain_layer.di.endpoints.reports;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 18.04.17.
 */

public class ReportsEnvironmentComponent implements IReportsEnvironmentComponent {

    @NonNull
    private final ReportGeneratorModule generators;

    public ReportsEnvironmentComponent(@NonNull IAppComponent component) {
        this.generators = new ReportGeneratorModule(component);
    }

    @NonNull
    @Override
    public ReportGeneratorModule generators() {
        return generators;
    }
}
