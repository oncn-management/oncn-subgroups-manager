package net.styleru.ikomarov.data_layer.mapping.remote.permissions;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;

import java.util.List;

/**
 * Created by i_komarov on 14.02.17.
 */

public class PermissionsRemoteEntityMapping implements IRemoteMapping<PermissionRemoteEntity> {

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_SELF = "self";

    @NonNull
    public static final String FIELD_USER_ID = "userId";

    @NonNull
    public static final String FIELD_USER_ROLE = "userRole";

    @NonNull
    public static final String FIELD_NAME = "name";

    private final Gson gson;

    public PermissionsRemoteEntityMapping() {
        this.gson = new GsonBuilder()
                .registerTypeAdapter(PermissionRemoteEntity.class, new PermissionRemoteEntity.Deserializer())
                .registerTypeAdapter(PermissionRemoteEntity.class, new PermissionRemoteEntity.Serializer())
                .create();
    }

    @Override
    public PermissionRemoteEntity fromJson(JsonObject json) {
        return gson.fromJson(json, PermissionRemoteEntity.class);
    }

    @Override
    public List<PermissionRemoteEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<PermissionRemoteEntity>>(){}.getType());
    }

    @Override
    public String toJson(PermissionRemoteEntity entity) {
        return gson.toJson(entity, PermissionRemoteEntity.class);
    }

    @Override
    public String toJsonArray(List<PermissionRemoteEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<PermissionRemoteEntity>>(){}.getType());
    }
}
