package net.styleru.ikomarov.domain_layer.jobs.base;

import android.content.Intent;

/**
 * Created by i_komarov on 21.04.17.
 */

public interface IBroadcastFactory<JobParams> {

    Intent createBroadcast(JobParams params);
}
