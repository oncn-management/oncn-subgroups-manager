package net.styleru.ikomarov.domain_layer.di.endpoints.section_groups;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.SectionGroupsLocalStorageService;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsLocalServiceModule {

    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent environment;

    @Nullable
    private volatile ISectionGroupsLocalStorageService localService;

    public SectionGroupsLocalServiceModule(@NonNull IEnvironmentComponent environment) {
        this.environment = environment;
    }

    @NonNull
    public ISectionGroupsLocalStorageService provideLocalService() {
        ISectionGroupsLocalStorageService localInstance = localService;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = localService;
                if(localInstance == null) {
                    localInstance = localService = SectionGroupsLocalStorageService.Factory.create(environment.database().provideDatabase());
                }
            }
        }

        return localInstance;
    }
}
