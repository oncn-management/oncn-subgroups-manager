package net.styleru.ikomarov.data_layer.mapping.local.notebooks;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.StudentSectionLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by i_komarov on 28.02.17.
 */

public class StudentSectionsLocalEntityMapping implements ILocalMapping<StudentSectionLocalEntity> {

    @NonNull
    public static final String TABLE = "student_sections";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_PARENT_ID = "PARENT_ID";

    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_PARENT_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_PARENT_ID;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    public StudentSectionsLocalEntityMapping() {

    }

    public static List<String> getInitializationQueries() {
        return Arrays.asList(getCreateTableQuery());
    }

    private static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE + "(" +
                    COLUMN_ID + " TEXT PRIMARY KEY NOT NULL," +
                    COLUMN_PARENT_ID + " TEXT NOT NULL" +
                ");";
    }

    @Override
    public StudentSectionLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException, ParseException {
        if(cursor.moveToFirst()) {
            return StudentSectionLocalEntity.newInstance(
                    cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PARENT_ID))
            );
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<StudentSectionLocalEntity> listFromCursor(Cursor cursor) throws ParseException {
        List<StudentSectionLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                entities.add(
                        StudentSectionLocalEntity.newInstance(
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_PARENT_ID))
                        )
                );
            } while(cursor.moveToNext());
        }

        return entities;
    }
}
