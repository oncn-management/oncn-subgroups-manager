package net.styleru.ikomarov.data_layer.mapping.utils;

import java.util.LinkedList;

/**
 * Created by i_komarov on 04.03.17.
 */

public class QueryBuilder {

    private static final String QUERY_SELECT = "SELECT";
    private static final String QUERY_DELETE = "DELETE";
    private static final String QUERY_UPDATE = "UPDATE";
    private static final String QUERY_INSERT = "INSERT";

    public enum Order {
        ASC("ASC"),
        DESC("DESC");

        private final String val;

        Order(String val) {
            this.val = val;
        }

        public String val() {
            return val;
        }
    }

    protected String table;

    public static SelectBuilder selectQuery(String source) {
        return new SelectBuilder(source);
    }

    public static DeleteBuilder deleteQuery(String table) {
        return new DeleteBuilder(table);
    }

    public static UpdateBuilder updateQuery(String table) {
        return new UpdateBuilder(table);
    }

    public static InsertBuilder insertQuery(String table) {
        return new InsertBuilder(table);
    }

    protected QueryBuilder(String table) {
        this.table = table;
    }

    public static class SelectBuilder extends QueryBuilder {

        private LinkedList<String> params;
        private LinkedList<JoinBuilder> joins;
        private LinkedList<String> whereConditions;
        private LinkedList<String> orderBy;

        private int offset = Integer.MIN_VALUE;
        private int limit = Integer.MIN_VALUE;

        protected SelectBuilder(String table) {
            super(table);
            this.params = new LinkedList<>();
            this.joins = new LinkedList<>();
            this.whereConditions = new LinkedList<>();
            this.orderBy = new LinkedList<>();
        }

        public SelectBuilder overrideTableWithNestedSelect(String nestedSelect) {
            this.table = "(" + nestedSelect + ") " + table;
            return this;
        }

        public SelectBuilder withParam(String oldColumnName, String actualColumnName) {
            if(!oldColumnName.equals(actualColumnName)) {
                this.params.addLast(oldColumnName + " AS " + actualColumnName);
            } else {
                this.params.addLast(actualColumnName);
            }
            return this;
        }

        public SelectBuilder leftJoin(String table, String leftColumn, String rightColumn) {
            this.joins.addLast(JoinBuilder.newLeftJoin(table, leftColumn, rightColumn));
            return this;
        }

        public SelectBuilder innerJoin(String table, String leftColumn, String rightColumn) {
            this.joins.addLast(JoinBuilder.newInnerJoin(table, leftColumn, rightColumn));
            return this;
        }

        public SelectBuilder where(String whereCondition) {
            this.whereConditions.addLast(whereCondition);
            return this;
        }

        public SelectBuilder limit(int offset, int limit) {
            this.offset = offset;
            this.limit = limit;
            return this;
        }

        public SelectBuilder orderBy(String column, Order order) {
            this.orderBy.add(column + " " + order.val());
            return this;
        }

        public String build() {
            String query = QUERY_SELECT;

            if(params.size() != 0) {
                query += " " + params.get(0);
                if(params.size() > 1) {
                    params.removeFirst();
                    for(String param : params) {
                        query += ", " + param;
                    }
                }
            } else {
                query += " *";
            }

            query += " FROM " + table + " ";

            if(joins.size() != 0) {
                for (JoinBuilder join : joins) {
                    query += " " + join.build();
                }
            }

            if (whereConditions.size() != 0) {
                query += " WHERE " + whereConditions.getFirst();
                if(whereConditions.size() > 1) {
                    whereConditions.removeFirst();
                    for(String condition : whereConditions) {
                        query += " AND " + condition;
                    }
                }
            }

            if (orderBy.size() != 0) {
                query += " ORDER BY " + orderBy.getFirst();
                if(orderBy.size() > 1) {
                    orderBy.removeFirst();
                    for(String order : orderBy) {
                        query += ", " + order;
                    }
                }
            }

            if(limit != Integer.MIN_VALUE) {
                query += " LIMIT " + limit;
                if(offset != Integer.MIN_VALUE) {
                    query += " OFFSET " + offset;
                }
            }

            return query;
        }
    }

    public static class DeleteBuilder extends QueryBuilder {

        protected DeleteBuilder(String table) {
            super(table);
        }
    }

    public static class UpdateBuilder extends QueryBuilder {

        protected UpdateBuilder(String table) {
            super(table);
        }
    }

    public static class InsertBuilder extends QueryBuilder {

        private boolean hasParams = false;

        private String queryChunk;
        private String valuesChunk;

        protected InsertBuilder(String table) {
            super(table);
            this.queryChunk = QUERY_INSERT + " INTO " + table + "(";
            this.valuesChunk = "VALUES (";
        }

        public InsertBuilder withStringData(String columnName, String columnValue) {
            if(hasParams) {
                this.queryChunk += ",";
                this.valuesChunk += ",";
            } else {
                hasParams = true;
            }

            this.queryChunk += columnName;
            this.valuesChunk += "\"" + columnValue + "\"";
            return this;
        }

        public InsertBuilder withIntData(String columnName, int columnValue) {
            if(hasParams) {
                this.queryChunk += ",";
                this.valuesChunk += ",";
            } else {
                hasParams = true;
            }

            this.queryChunk += columnName;
            this.valuesChunk += "\"" + columnValue + "\"";
            return this;
        }

        public String build() {
            this.queryChunk += ")";
            this.valuesChunk += ")";
            return queryChunk + valuesChunk + ";";
        }
    }

    private static class JoinBuilder {

        private static final String LEFT_JOIN = "LEFT JOIN";
        private static final String INNER_JOIN = "INNER JOIN";

        private String join;
        private String table;
        private String leftColumn;
        private String rightColumn;

        public static JoinBuilder newLeftJoin(String table, String leftColumn, String rightColumn) {
            JoinBuilder join = new JoinBuilder(table, leftColumn, rightColumn);
            join.join = LEFT_JOIN;
            return join;
        }

        public static JoinBuilder newInnerJoin(String table, String leftColumn, String rightColumn) {
            JoinBuilder join = new JoinBuilder(table, leftColumn, rightColumn);
            join.join = INNER_JOIN;
            return join;
        }

        private JoinBuilder(String table, String leftColumn, String rightColumn) {
            this.table = table;
            this.leftColumn = leftColumn;
            this.rightColumn = rightColumn;
        }

        public String build() {
            return join + " " + table + " ON " + leftColumn + " = " + rightColumn;
        }
    }

    public static final class NameValuePair<T1, T2> {

        private T1 name;
        private T2 value;

        public NameValuePair(T1 name, T2 value) {
            this.name = name;
            this.value = value;
        }

        public T1 getName() {
            return this.name;
        }

        public T2 getValue() {
            return this.value;
        }
    }

    public static final class EqualityCondition {

        private String leftColumn;
        private String value;

        public EqualityCondition(String leftColumn, String value) {
            this.leftColumn = leftColumn;
            this.value = value;
        }

        public String build() {
            return leftColumn + " = " + "\"" + value + "\"";
        }
    }
}
