package net.styleru.ikomarov.domain_layer.di.level_4.session_consumer;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.manager.session.interceptor.GraphSessionInterceptor;
import net.styleru.ikomarov.data_layer.manager.session.interceptor.OneNoteSessionInterceptor;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by i_komarov on 13.03.17.
 */

public class ClientModule {

    private final long READ_TIMEOUT = 30L;

    private final long WRITE_TIMEOUT = 15L;

    private final long CONNECT_TIMEOUT = 15L;

    private final Object oneNoteLock = new Object();

    private final Object graphLock = new Object();

    @NonNull
    private final ISessionComponent component;

    @Nullable
    private volatile OkHttpClient oneNoteClient;

    @Nullable
    private volatile OkHttpClient graphClient;

    public ClientModule(@NonNull ISessionComponent component) {
        this.component = component;
    }

    @NonNull
    public OkHttpClient provideOneNoteHttpClient() {
        OkHttpClient localInstance = oneNoteClient;
        if(localInstance == null) {
            synchronized (oneNoteLock) {
                localInstance = oneNoteClient;
                if(localInstance == null) {
                    localInstance = oneNoteClient = new OkHttpClient.Builder()
                                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                                .addInterceptor(new OneNoteSessionInterceptor(component.session().provideSessionManager()))
                            .build();
                }
            }
        }

        return localInstance;
    }

    @NonNull
    public OkHttpClient provideGraphHttpClient() {
        OkHttpClient localInstance = graphClient;
        if(localInstance == null) {
            synchronized (graphLock) {
                localInstance = graphClient;
                if(localInstance == null) {
                    localInstance = graphClient = new OkHttpClient.Builder()
                            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                            .addInterceptor(new GraphSessionInterceptor(component.session().provideSessionManager()))
                            .build();
                }
            }
        }

        return localInstance;
    }
}
