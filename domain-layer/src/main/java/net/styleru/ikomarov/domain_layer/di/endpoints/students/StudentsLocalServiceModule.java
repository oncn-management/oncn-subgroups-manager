package net.styleru.ikomarov.domain_layer.di.endpoints.students;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.SectionGroupsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.students.IStudentsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.students.StudentsLocalStorageService;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsLocalServiceModule {

    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent environment;

    @Nullable
    private volatile IStudentsLocalStorageService localService;

    public StudentsLocalServiceModule(@NonNull IEnvironmentComponent environment) {
        this.environment = environment;
    }

    @NonNull
    public IStudentsLocalStorageService provideLocalService() {
        IStudentsLocalStorageService localInstance = localService;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = localService;
                if(localInstance == null) {
                    localInstance = localService = StudentsLocalStorageService.Factory.create(environment.database().provideDatabase());
                }
            }
        }

        return localInstance;
    }
}
