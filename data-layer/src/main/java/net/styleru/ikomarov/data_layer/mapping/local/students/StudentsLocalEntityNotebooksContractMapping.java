package net.styleru.ikomarov.data_layer.mapping.local.students;

import android.database.Cursor;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static net.styleru.ikomarov.data_layer.contracts.database.DatabaseContracts.PrincipalObjectsContract.*;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsLocalEntityNotebooksContractMapping implements ILocalMapping<PrincipalObjectLocalEntity> {

    @Override
    public PrincipalObjectLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException, ParseException {
        if(cursor.moveToFirst()) {
            return PrincipalObjectLocalEntity.newInstance(
                    cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_DEPARTMENT)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_TYPE))
            );
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<PrincipalObjectLocalEntity> listFromCursor(Cursor cursor) throws ParseException {
        List<PrincipalObjectLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                entities.add(
                        PrincipalObjectLocalEntity.newInstance(
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_NAME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_EMAIL)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_DEPARTMENT)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_TYPE))
                        )
                );
            } while(cursor.moveToNext());
        }

        return entities;
    }
}
