package net.styleru.ikomarov.domain_layer.di.endpoints.reports;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.mapping.reports.PermissionsReportMapper;
import net.styleru.ikomarov.domain_layer.repository.reports.INotebookReportsRepository;
import net.styleru.ikomarov.domain_layer.repository.reports.NotebookReportsRepository;

/**
 * Created by i_komarov on 18.04.17.
 */

public class NotebooksReportsRepositoryModule {

    private final Object lock = new Object();

    @NonNull
    private final IReportsEnvironmentComponent component;

    @Nullable
    private volatile INotebookReportsRepository repository;

    public NotebooksReportsRepositoryModule(@NonNull IReportsEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public INotebookReportsRepository provideNotebooksRepository() {
        INotebookReportsRepository localInstance = repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = new NotebookReportsRepository(
                            component.generators().providePermissionsReportGenerator(),
                            new PermissionsReportMapper()
                    );
                }
            }
        }

        return localInstance;
    }
}
