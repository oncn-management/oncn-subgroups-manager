package net.styleru.ikomarov.domain_layer.di.endpoints.permissions;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SGPermissionsEnvironmentComponent implements ISGPermissionsEnvironmentComponent {

    @NonNull
    private final ICacheComponent cacheComponent;

    @NonNull
    private final SGPermissionsLocalServiceModule localService;

    @NonNull
    private final SGPermissionsRemoteServiceModule remoteService;

    @NonNull
    private final SGPermissionsMappingModule mapping;

    public SGPermissionsEnvironmentComponent(@NonNull IEnvironmentComponent environment, @NonNull IClientComponent client, @NonNull ICacheComponent cacheComponent) {
        this.cacheComponent = cacheComponent;
        this.localService = new SGPermissionsLocalServiceModule(environment);
        this.remoteService = new SGPermissionsRemoteServiceModule(client);
        this.mapping = new SGPermissionsMappingModule();
    }

    @NonNull
    @Override
    public ICacheComponent cacheComponent() {
        return cacheComponent;
    }

    @NonNull
    @Override
    public SGPermissionsLocalServiceModule localService() {
        return localService;
    }

    @NonNull
    @Override
    public SGPermissionsRemoteServiceModule remoteService() {
        return remoteService;
    }

    @NonNull
    @Override
    public SGPermissionsMappingModule mapping() {
        return mapping;
    }
}
