package net.styleru.ikomarov.domain_layer.repository.students;

import net.styleru.ikomarov.data_layer.common.Tuple;
import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;
import net.styleru.ikomarov.data_layer.entities.remote.section_groups.SectionGroupRemoteEntity;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.services.students.IStudentsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.students.IStudentsRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.utils.IClaster;
import net.styleru.ikomarov.data_layer.services.utils.IMemCache;
import net.styleru.ikomarov.domain_layer.cache.ICacheStrategy;
import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.contracts.policies.CachePolicy;
import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.mapping.principal_objects.LocalPrincipalObjectsMapper;
import net.styleru.ikomarov.domain_layer.mapping.principal_objects.RemotePrincipalObjectsMapper;
import net.styleru.ikomarov.domain_layer.mapping.remote_to_local.RemoteToLocalPrincipalObjectMapper;
import net.styleru.ikomarov.domain_layer.mapping.section_groups.LocalSectionGroupsMapper;
import net.styleru.ikomarov.domain_layer.mapping.section_groups.RemoteSectionGroupsMapper;
import net.styleru.ikomarov.domain_layer.repository.base.AbstractRepository;
import net.styleru.ikomarov.domain_layer.repository.utils.ODataUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_PARTIAL;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsRepository extends AbstractRepository implements IStudentsRepository {

    private static final String SELF = "me";

    private final IStudentsLocalStorageService localService;
    private final IStudentsRemoteStorageService remoteService;
    private final ICacheStrategy strategy;
    private final IRemoteMapping<PrincipalObjectRemoteEntity> mapping;

    private StudentsRepository(IStudentsLocalStorageService localService,
                               IStudentsRemoteStorageService remoteService,
                               ICacheStrategy strategy,
                               IRemoteMapping<PrincipalObjectRemoteEntity> mapping) {

        super();
        this.localService = localService;
        this.remoteService = remoteService;
        this.strategy = strategy;
        this.mapping = mapping;
    }

    @Override
    public Observable<OperationStatus> create(UUID operaionId, String notebookId, PrincipalObjectDTO.SimpleBuilder builder) {
        CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            //send an operation to the delayed operations stack
            //TODO: implement the operations stack
            return Observable.just(OperationStatus.DELAYED);
        }

        return performCache(notebookId, remoteService.create(SELF, notebookId, builder.build()), HTTP_CREATED)
                .map(data -> OperationStatus.SUCCESS);
    }

    @Override
    public Observable<List<PrincipalObjectDTO>> read(String notebookId, int offset, int limit) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(notebookId, offset, limit, shouldThrowOnNoCachedResourcesFound(policy)) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(notebookId, offset, limit) : Observable.empty()
        );
    }

    @Override
    public Observable<List<PrincipalObjectDTO>> read(String notebookId, int offset, int limit, String nameSubstring) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(notebookId, offset, limit, nameSubstring, shouldThrowOnNoCachedResourcesFound(policy)) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(notebookId, offset, limit, nameSubstring) : Observable.empty()
        );
    }

    @Override
    public Observable<OperationStatus> delete(UUID operaionId, String notebookId, String studentId) {
        CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            //send an operation to the delayed operations stack
            //TODO: implement the operations stack
            return Observable.just(OperationStatus.DELAYED);
        }

        return remoteService.delete(SELF, notebookId, studentId)
                .flatMap(verifyResponse(Object.class, HTTP_NO_CONTENT))
                .doOnNext(objectDataResponse -> removeFromCache(notebookId, studentId))
                .map(objectDataResponse -> OperationStatus.SUCCESS);
    }

    private Observable<List<PrincipalObjectDTO>> tryReadFromCache(String notebookId, int offset, int limit, boolean shouldThrowOnNoResourcesFound) {
        return localService.read(notebookId, offset, limit).map(new LocalPrincipalObjectsMapper())
                .flatMap((list) -> list.size() == 0 && shouldThrowOnNoResourcesFound ?
                        Observable.error(new ExceptionBundle(ExceptionBundle.Reason.NO_CACHED_RESOURCES)) :
                        Observable.just(list)
        );
    }


    private Observable<List<PrincipalObjectDTO>> tryReadFromCache(String notebookId, int offset, int limit, String nameSubstring, boolean shouldThrowOnNoResourcesFound) {
        return localService.read(notebookId, offset, limit, nameSubstring).map(new LocalPrincipalObjectsMapper())
                .flatMap((list) -> list.size() == 0 && shouldThrowOnNoResourcesFound ?
                        Observable.error(new ExceptionBundle(ExceptionBundle.Reason.NO_CACHED_RESOURCES)) :
                        Observable.just(list)
        );
    }

    private Observable<List<PrincipalObjectDTO>> readUpdatesFromNetwork(String notebookId, int offset, int limit) {
        return performCache(notebookId, remoteService.read(SELF, notebookId, offset, limit, ODataUtils.orderByName()),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<PrincipalObjectDTO>> readUpdatesFromNetwork(String notebookId, int offset, int limit, String nameSubstring) {
        return performCache(notebookId, remoteService.read(SELF, notebookId, offset, limit, ODataUtils.orderByName(), ODataUtils.byNameSubstring(nameSubstring)),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<PrincipalObjectDTO>> performCache(String notebookId, Observable<Response<DataResponse<PrincipalObjectRemoteEntity>>> fromNetwork, Integer... successCodes) {
        return fromNetwork.flatMap(verifyResponse(PrincipalObjectRemoteEntity.class, successCodes))
                .doOnNext(response -> addToCache(notebookId, response, mapping))
                .flatMap(response -> Observable.just(response.getBody(mapping))
                        .map(new RemotePrincipalObjectsMapper()));
    }

    private void addToCache(String notebookId, DataResponse<PrincipalObjectRemoteEntity> data, IRemoteMapping<PrincipalObjectRemoteEntity> mapping) {
        Observable.fromIterable(data.getBody(mapping))
                .map(new RemoteToLocalPrincipalObjectMapper())
                .flatMap(local -> localService.create(notebookId, local).map(putResult -> new Tuple<>(notebookId, local.getId(), local.getName())))
                .filter(tuple -> tuple.getFirst() != null)
                .subscribe(tuple -> {
                    //TODO: well, at least here might be some name-based cache version onModified action
                });
    }

    private void removeFromCache(String notebookId, String studentId) {
        localService.delete(notebookId, studentId)
                .subscribe(deleteResult -> {
                    //TODO: well, at least here might be some name-based cache version onDeleted action
                });
    }

    public static final class Factory {

        private Factory() {

        }

        public static IStudentsRepository create(IStudentsLocalStorageService localService,
                                                 IStudentsRemoteStorageService remoteService,
                                                 ICacheStrategy strategy,
                                                 IRemoteMapping<PrincipalObjectRemoteEntity> mapping) {

            return new StudentsRepository(localService, remoteService, strategy, mapping);
        }
    }
}
