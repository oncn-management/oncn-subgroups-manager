package net.styleru.ikomarov.presentation_layer.adapter.utils;

/**
 * Created by i_komarov on 14.03.17.
 */

public class SortingStrategy implements ISortingStrategy {

    private volatile boolean isDefault = true;

    public void onSearchStarted() {
        isDefault = false;
    }

    public void onSearchFinished() {
        isDefault = true;
    }

    @Override
    public SortingStrategyType current() {
        return isDefault ? SortingStrategyType.LAST_MODIFIED_DESCENDANCE : SortingStrategyType.NAME_ASCENDANCE;
    }
}
