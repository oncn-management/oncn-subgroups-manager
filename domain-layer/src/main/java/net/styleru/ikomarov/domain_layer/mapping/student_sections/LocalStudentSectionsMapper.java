package net.styleru.ikomarov.domain_layer.mapping.student_sections;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.StudentSectionLocalEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.03.17.
 */

public class LocalStudentSectionsMapper implements Function<List<StudentSectionLocalEntity>, List<String>> {

    @Override
    public List<String> apply(final List<StudentSectionLocalEntity> studentSectionLocalEntities) throws Exception {
        return Observable.fromIterable(studentSectionLocalEntities)
                .map(new LocalStudentSectionMapper())
                .toList()
                .blockingGet();
    }
}
