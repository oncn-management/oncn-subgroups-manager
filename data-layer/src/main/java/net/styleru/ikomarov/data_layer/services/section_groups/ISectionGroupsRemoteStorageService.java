package net.styleru.ikomarov.data_layer.services.section_groups;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.section_groups.SectionGroupRemoteEntity;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static net.styleru.ikomarov.data_layer.common.Endpoints.*;
import static net.styleru.ikomarov.data_layer.common.Paths.*;

/**
 * Created by i_komarov on 15.03.17.
 */

public interface ISectionGroupsRemoteStorageService {

    String PARAM_OFFSET = "$skip";
    String PARAM_LIMIT = "$top";
    String PARAM_FILTER = "$filter";
    String PARAM_ORDER_BY = "$orderby";

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_CREATED} code
     * */
    @POST(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> create(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Body  JsonObject sectionGroup
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}")
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Query(value = PARAM_ORDER_BY, encoded = true) String order
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Query(value = PARAM_ORDER_BY, encoded = true) String orderBy,
            @Query(value = PARAM_FILTER, encoded = true) String fetchingFilter
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Query(value = PARAM_OFFSET, encoded = true) int offset,
            @Query(value = PARAM_LIMIT, encoded = true) int limit
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Query(value = PARAM_OFFSET, encoded = true) int offset,
            @Query(value = PARAM_LIMIT, encoded = true) int limit,
            @Query(value = PARAM_ORDER_BY, encoded = true) String orderBy
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Query(value = PARAM_OFFSET, encoded = true) int offset,
            @Query(value = PARAM_LIMIT, encoded = true) int limit,
            @Query(value = PARAM_ORDER_BY, encoded = true) String orderBy,
            @Query(value = PARAM_FILTER, encoded = true) String filter
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_CREATED} code
     * */
    @POST(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> createNested(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId,
            @Body JsonObject sectionGroup
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId,
            @Query(PARAM_ORDER_BY) String orderBy
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId,
            @Query(PARAM_ORDER_BY) String orderBy,
            @Query(value = PARAM_FILTER, encoded = true) String fetchingFilter
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId,
            @Query(value = PARAM_OFFSET, encoded = true) int offset,
            @Query(value = PARAM_LIMIT, encoded = true) int limit
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId,
            @Query(value = PARAM_OFFSET, encoded = true) int offset,
            @Query(value = PARAM_LIMIT, encoded = true) int limit,
            @Query(value = PARAM_ORDER_BY, encoded = true) String orderBy
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_SECTIONGROUPS)
    Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId,
            @Query(value = PARAM_OFFSET, encoded = true) int offset,
            @Query(value = PARAM_LIMIT, encoded = true) int limit,
            @Query(value = PARAM_ORDER_BY, encoded = true) String orderBy,
            @Query(value = PARAM_FILTER, encoded = true) String filter
    );
}
