package net.styleru.ikomarov.data_layer.entities.remote.section_groups;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import static net.styleru.ikomarov.data_layer.mapping.remote.section_groups.SectionGroupsRemoteEntityMapping.*;

/**
 * Created by i_komarov on 14.02.17.
 */

public class SectionGroupRemoteEntity {

    @SerializedName(FIELD_CREATED_BY)
    private String createdBy;
    @SerializedName(FIELD_CREATED_TIME)
    private Date createdTime;
    @SerializedName(FIELD_LAST_MODIFIED_BY)
    private String lastModifiedBy;
    @SerializedName(FIELD_LAST_MODIFIED_TIME)
    private Date lastModifiedTime;
    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_NAME)
    private String name;
    @SerializedName(FIELD_SECTION_GROUPS_URL)
    private String sectionGroupsUrl;
    @SerializedName(FIELD_SECTIONS_URl)
    private String sectionsUrl;
    @SerializedName(FIELD_SELF)
    private String self;
    @SerializedName(FIELD_PARENT_NOTEBOOK)
    private ContainerRemoteEntity parentNotebook;
    @SerializedName(FIELD_PARENT_SECTION_GROUP)
    private ContainerRemoteEntity parentSectionGroup;

    private SectionGroupRemoteEntity() {

    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSectionGroupsUrl() {
        return sectionGroupsUrl;
    }

    public String getSectionsUrl() {
        return sectionsUrl;
    }

    public String getSelf() {
        return self;
    }

    public ContainerRemoteEntity getParentNotebook() {
        return parentNotebook;
    }

    public ContainerRemoteEntity getParentSectionGroup() {
        return parentSectionGroup;
    }

    public boolean hasParentNotebook() {
        return parentNotebook != null;
    }

    public boolean hasParentSectionGroup() {
        return parentSectionGroup != null;
    }

    @Override
    public String toString() {
        return "SectionGroupRemoteEntity{" +
                "createdBy='" + createdBy + '\'' +
                ", createdTime='" + createdTime + '\'' +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", lastModifiedTime='" + lastModifiedTime + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", sectionGroupsUrl='" + sectionGroupsUrl + '\'' +
                ", sectionsUrl='" + sectionsUrl + '\'' +
                ", self='" + self + '\'' +
                '}';
    }
}
