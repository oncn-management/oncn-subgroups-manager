package net.styleru.ikomarov.domain_layer.di.level_2.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.analytics.IAnalyticsComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;

/**
 * Created by i_komarov on 13.03.17.
 */

/**
 * depends on {@link IAppComponent}
 * */
public interface IEnvironmentComponent {

    @NonNull
    ICacheComponent plusCacheComponent();

    @NonNull
    ISessionComponent plusSessionComponent();

    @NonNull
    IAnalyticsComponent plusAnalyticsComponent();

    @NonNull
    NetworkModule network();

    @NonNull
    LoggingModule logging();

    @NonNull
    DatabaseModule database();

    @NonNull
    PreferencesModule preferences();

    @NonNull
    ErrorTranslationModule errorTranslation();
}
