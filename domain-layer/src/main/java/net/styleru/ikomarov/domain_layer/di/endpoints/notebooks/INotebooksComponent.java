package net.styleru.ikomarov.domain_layer.di.endpoints.notebooks;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 14.03.17.
 */

public interface INotebooksComponent {

    @NonNull
    NotebooksRepositoryModule repository();
}
