package net.styleru.ikomarov.presentation_layer.presenter.section_group_create;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.events.IEventsLogger;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.use_cases.section_groups.SectionGroupCreateUseCase;
import net.styleru.ikomarov.presentation_layer.presenter.base.Presenter;
import net.styleru.ikomarov.domain_layer.contracts.errors.IErrorCodeTranslator;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.section_group_create.ISectionGroupCreateView;

import java.util.Arrays;
import java.util.UUID;

/**
 * Created by i_komarov on 21.03.17.
 */

public class SectionGroupCreatePresenter extends Presenter<ISectionGroupCreateView> implements SectionGroupCreateUseCase.Callbacks {

    @NonNull
    private final String notebookId;

    @NonNull
    private final SectionGroupCreateUseCase sectionGroupCreate;

    @NonNull
    private final IErrorCodeTranslator oneNoteCodeTranslator;

    @NonNull
    private final IEventsLogger loggingManager;

    @Nullable
    private UUID currentTaskId;

    private SectionGroupCreatePresenter(@NonNull String notebookId,
                                        @NonNull SectionGroupCreateUseCase sectionGroupCreate,
                                        @NonNull IErrorCodeTranslator oneNoteCodeTranslator,
                                        @NonNull IEventsLogger loggingManager) {
        this.notebookId = notebookId;
        this.sectionGroupCreate = sectionGroupCreate;
        this.oneNoteCodeTranslator = oneNoteCodeTranslator;
        this.loggingManager = loggingManager;
    }

    @Override
    protected void onViewAttached() {
        if(currentTaskId != null) {
            postAction((view) -> {
                final CreateOperationResult result = sectionGroupCreate.retrieveResult(currentTaskId);
                currentTaskId = null;
                if(result != null) {
                    view.onSectionGroupCreated(result.getResourceId());
                }
            });
        }
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {
        sectionGroupCreate.dispose();
    }

    public void createSectionGroup(String name) {
        if(currentTaskId == null) {
            currentTaskId = sectionGroupCreate.execute(this, notebookId, new SectionGroupDTO.Builder(name));
        }
    }

    @Override
    public void onSectionGroupCreationSucceeded(UUID operationId, String realResourceId) {
        postAction((view) -> {
            currentTaskId = null;
            view.onSectionGroupCreated(realResourceId);
        });
    }

    @Override
    public void onSectionGroupCreationFailed(UUID operationId, ExceptionBundle error) {
        currentTaskId = null;
        handleException(
                error,
                SectionGroupCreatePresenter.class.getSimpleName(),
                "onSectionGroupCreationFailed"
        );
    }

    @Override
    public void onSectionGroupCreationDelayed(UUID operationId) {
        postAction(ISectionGroupCreateView::showOperationDelayedDialog);
    }

    private void handleException(ExceptionBundle error, String clazz, String method, ExceptionBundle.Reason... ignoreReasons) {
        if(-1 == Arrays.binarySearch(ignoreReasons, error.getReason())) {
            return;
        }

        postAction((view) -> {
            switch (error.getReason()) {
                case NO_CACHED_RESOURCES: {
                    view.showNoCachedDataPlug();
                    break;
                }
                case NO_RESOURCES: {
                    view.showNoDataPlug();
                    break;
                }
                case NO_SESSION: {
                    view.showAuthorizationDialog();
                    break;
                }
                case ONE_NOTE_ERROR: {
                    final int code = error.getIntExtra(ExceptionBundle.OneNoteErrorContract.KEY_CODE);
                    final String message = error.getStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_MESSAGE);
                    view.showError(oneNoteCodeTranslator.getLocalizedMessage(code, message));
                    break;
                }
                case NETWORK_UNAVAILABLE: {
                    view.showConnectionFailedIndicator();
                    break;
                }
                default: {
                    if(error.getReason().isInternalError()) {
                        loggingManager.logEvent(clazz, method, error.getReason(), error.getThrowable(ExceptionBundle.InternalErrorContract.KEY_THROWABLE));
                    } else {
                        loggingManager.logEvent(clazz, method, error.getReason());
                    }
                }
            }
        });
    }

    public static final class Factory implements RetainContainer.Factory<SectionGroupCreatePresenter> {

        @NonNull
        private final String notebookId;

        @NonNull
        private final SectionGroupCreateUseCase sectionGroupCreate;

        @NonNull
        private final IErrorCodeTranslator oneNoteCodeTranslator;

        @NonNull
        private final IEventsLogger loggingManager;


        public Factory(@NonNull String notebookId,
                        @NonNull SectionGroupCreateUseCase sectionGroupCreate,
                        @NonNull IErrorCodeTranslator oneNoteCodeTranslator,
                        @NonNull IEventsLogger loggingManager) {

            this.notebookId = notebookId;
            this.sectionGroupCreate = sectionGroupCreate;
            this.oneNoteCodeTranslator = oneNoteCodeTranslator;
            this.loggingManager = loggingManager;
        }

        @Override
        public SectionGroupCreatePresenter create() {
            return new SectionGroupCreatePresenter(notebookId, sectionGroupCreate, oneNoteCodeTranslator, loggingManager);
        }
    }
}
