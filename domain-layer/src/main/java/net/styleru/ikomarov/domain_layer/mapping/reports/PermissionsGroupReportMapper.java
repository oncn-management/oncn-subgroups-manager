package net.styleru.ikomarov.domain_layer.mapping.reports;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;
import net.styleru.ikomarov.domain_layer.dto.PermissionReportSubgroupDTO;
import net.styleru.ikomarov.domain_layer.dto.base.ReportGroupDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 18.04.17.
 */

public class PermissionsGroupReportMapper implements Function<List<PermissionDTO>, ReportGroupDTO<PermissionReportSubgroupDTO>> {

    @NonNull
    private final String groupId;

    @NonNull
    private final String groupName;

    public PermissionsGroupReportMapper(@NonNull String groupId, @NonNull String groupName) {
        this.groupId = groupId;
        this.groupName = groupName;
    }

    @Override
    public ReportGroupDTO<PermissionReportSubgroupDTO> apply(List<PermissionDTO> permissionDTOs) throws Exception {
        return Observable.fromIterable(permissionDTOs)
                .map(permission -> new PermissionReportSubgroupDTO(permission.getName(), permission.getRole().value()))
                .reduce(new ReportGroupDTO.Builder<PermissionReportSubgroupDTO>(groupId, groupName), ReportGroupDTO.Builder::<PermissionReportSubgroupDTO>addSubgroup)
                .map(ReportGroupDTO.Builder::<PermissionReportSubgroupDTO>build)
                .blockingGet();
    }
}
