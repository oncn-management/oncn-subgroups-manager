package net.styleru.ikomarov.data_layer.mapping.local.users;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

import net.styleru.ikomarov.data_layer.entities.local.users.UserBusinessPhoneLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by i_komarov on 25.02.17.
 */

public class UsersBusinessPhonesLocalEntityMapping implements ILocalMapping<UserBusinessPhoneLocalEntity> {

    @NonNull
    public static final String TABLE = "users_business_phones";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_USER_ID = "USER_ID";

    @NonNull
    public static final String COLUMN_PHONE = "PHONE";


    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_USER_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_USER_ID;
    public static final String COLUMN_PHONE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_PHONE;


    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    public UsersBusinessPhonesLocalEntityMapping() {

    }

    public static List<String> getInitializationQueries() {
        return Collections.singletonList(getCreateTableQuery());
    }

    private static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE + "(" +
                    COLUMN_ID + " INTEGER PRIMARY KEY NOT NULL," +
                    COLUMN_USER_ID + " TEXT NOT NULL," +
                    COLUMN_PHONE + " TEXT NOT NULL" +
                ");";
    }

    @Override
    public UserBusinessPhoneLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException {
        if(cursor.moveToFirst()) {
            return UserBusinessPhoneLocalEntity.newInstance(
                    cursor.getLong(cursor.getColumnIndex(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PHONE))
            );
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<UserBusinessPhoneLocalEntity> listFromCursor(Cursor cursor) throws ParseException {
        List<UserBusinessPhoneLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                entities.add(
                        UserBusinessPhoneLocalEntity.newInstance(
                                cursor.getLong(indexCache.indexFor(cursor, COLUMN_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_PHONE))
                        )
                );
            } while(cursor.moveToNext());
        }

        return entities;
    }
}
