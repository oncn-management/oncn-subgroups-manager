package net.styleru.ikomarov.domain_layer.di.endpoints.section_groups;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsComponent implements ISectionGroupsComponent {

    @NonNull
    private final SectionGroupsRepositoryModule repository;

    public SectionGroupsComponent(@NonNull ISectionGroupsEnvironmentComponent environment) {
        this.repository = new SectionGroupsRepositoryModule(environment);
    }

    @NonNull
    @Override
    public SectionGroupsRepositoryModule repository() {
        return repository;
    }
}
