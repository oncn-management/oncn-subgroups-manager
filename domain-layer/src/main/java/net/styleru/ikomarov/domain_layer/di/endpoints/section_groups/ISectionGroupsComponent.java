package net.styleru.ikomarov.domain_layer.di.endpoints.section_groups;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 15.03.17.
 */

public interface ISectionGroupsComponent {

    @NonNull
    SectionGroupsRepositoryModule repository();
}
