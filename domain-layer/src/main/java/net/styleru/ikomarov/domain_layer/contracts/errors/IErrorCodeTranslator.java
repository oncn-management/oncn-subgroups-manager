package net.styleru.ikomarov.domain_layer.contracts.errors;

import android.support.annotation.StringRes;

/**
 * Created by i_komarov on 09.03.17.
 */

public interface IErrorCodeTranslator {

    @StringRes
    int getLocalizedMessage(int code, String message);
}
