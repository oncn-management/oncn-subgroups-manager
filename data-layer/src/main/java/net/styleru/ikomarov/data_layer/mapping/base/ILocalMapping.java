package net.styleru.ikomarov.data_layer.mapping.base;

import android.database.Cursor;

import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;

import java.text.ParseException;
import java.util.List;

/**
 * Created by i_komarov on 25.02.17.
 */

public interface ILocalMapping<T> {

    T fromCursor(Cursor cursor) throws EmptyCursorException, ParseException;

    List<T> listFromCursor(Cursor cursor) throws ParseException;
}
