package net.styleru.ikomarov.data_layer.database;

import android.content.Context;

import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;

import net.styleru.ikomarov.data_layer.BuildConfig;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntityStorIOSQLiteDeleteResolver;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntityStorIOSQLiteGetResolver;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntityStorIOSQLitePutResolver;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntityStorIOSQLiteDeleteResolver;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntityStorIOSQLiteGetResolver;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntityStorIOSQLitePutResolver;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.StudentSectionLocalEntityStorIOSQLiteDeleteResolver;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.StudentSectionLocalEntityStorIOSQLiteGetResolver;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.StudentSectionLocalEntityStorIOSQLitePutResolver;
import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntityStorIOSQLiteDeleteResolver;
import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntityStorIOSQLiteGetResolver;
import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntityStorIOSQLitePutResolver;
import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntityStorIOSQLiteDeleteResolver;
import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntityStorIOSQLiteGetResolver;
import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntityStorIOSQLitePutResolver;
import net.styleru.ikomarov.data_layer.entities.local.sections.SectionLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.StudentSectionLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.sections.SectionLocalEntityStorIOSQLiteDeleteResolver;
import net.styleru.ikomarov.data_layer.entities.local.sections.SectionLocalEntityStorIOSQLiteGetResolver;
import net.styleru.ikomarov.data_layer.entities.local.sections.SectionLocalEntityStorIOSQLitePutResolver;
import net.styleru.ikomarov.data_layer.entities.local.users.UserBusinessPhoneLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.users.UserBusinessPhoneLocalEntityStorIOSQLiteDeleteResolver;
import net.styleru.ikomarov.data_layer.entities.local.users.UserBusinessPhoneLocalEntityStorIOSQLiteGetResolver;
import net.styleru.ikomarov.data_layer.entities.local.users.UserBusinessPhoneLocalEntityStorIOSQLitePutResolver;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntityStorIOSQLiteDeleteResolver;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntityStorIOSQLiteGetResolver;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntityStorIOSQLitePutResolver;

/**
 * Created by i_komarov on 13.03.17.
 */

public class SQLiteFactory {

    private SQLiteFactory() {

    }

    public static StorIOSQLite create(Context context) {
        return DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(new DatabaseOpenHelper(context, BuildConfig.DATABASE_NAME, null, BuildConfig.DATABASE_SCHEMA_VERSION))
                .addTypeMapping(
                        ClassNotebookLocalEntity.class,
                        SQLiteTypeMapping.<ClassNotebookLocalEntity>builder()
                                .putResolver(new ClassNotebookLocalEntityStorIOSQLitePutResolver())
                                .getResolver(new ClassNotebookLocalEntityStorIOSQLiteGetResolver())
                                .deleteResolver(new ClassNotebookLocalEntityStorIOSQLiteDeleteResolver())
                                .build()
                )
                .addTypeMapping(
                        PrincipalObjectLocalEntity.class,
                        SQLiteTypeMapping.<PrincipalObjectLocalEntity>builder()
                                .putResolver(new PrincipalObjectLocalEntityStorIOSQLitePutResolver())
                                .getResolver(new PrincipalObjectLocalEntityStorIOSQLiteGetResolver())
                                .deleteResolver(new PrincipalObjectLocalEntityStorIOSQLiteDeleteResolver())
                                .build()
                )
                .addTypeMapping(
                        SectionLocalEntity.class,
                        SQLiteTypeMapping.<SectionLocalEntity>builder()
                                .putResolver(new SectionLocalEntityStorIOSQLitePutResolver())
                                .getResolver(new SectionLocalEntityStorIOSQLiteGetResolver())
                                .deleteResolver(new SectionLocalEntityStorIOSQLiteDeleteResolver())
                                .build()
                )
                .addTypeMapping(
                        SectionGroupLocalEntity.class,
                        SQLiteTypeMapping.<SectionGroupLocalEntity>builder()
                                .putResolver(new SectionGroupLocalEntityStorIOSQLitePutResolver())
                                .getResolver(new SectionGroupLocalEntityStorIOSQLiteGetResolver())
                                .deleteResolver(new SectionGroupLocalEntityStorIOSQLiteDeleteResolver())
                                .build()
                )
                .addTypeMapping(
                        PermissionLocalEntity.class,
                        SQLiteTypeMapping.<PermissionLocalEntity>builder()
                                .putResolver(new PermissionLocalEntityStorIOSQLitePutResolver())
                                .getResolver(new PermissionLocalEntityStorIOSQLiteGetResolver())
                                .deleteResolver(new PermissionLocalEntityStorIOSQLiteDeleteResolver())
                                .build()
                )
                .addTypeMapping(
                        UserLocalEntity.class,
                        SQLiteTypeMapping.<UserLocalEntity>builder()
                                .putResolver(new UserLocalEntityStorIOSQLitePutResolver())
                                .getResolver(new UserLocalEntityStorIOSQLiteGetResolver())
                                .deleteResolver(new UserLocalEntityStorIOSQLiteDeleteResolver())
                                .build()
                )
                .addTypeMapping(
                        UserBusinessPhoneLocalEntity.class,
                        SQLiteTypeMapping.<UserBusinessPhoneLocalEntity>builder()
                                .putResolver(new UserBusinessPhoneLocalEntityStorIOSQLitePutResolver())
                                .getResolver(new UserBusinessPhoneLocalEntityStorIOSQLiteGetResolver())
                                .deleteResolver(new UserBusinessPhoneLocalEntityStorIOSQLiteDeleteResolver())
                                .build()
                )
                .addTypeMapping(
                        StudentSectionLocalEntity.class,
                        SQLiteTypeMapping.<StudentSectionLocalEntity>builder()
                                .putResolver(new StudentSectionLocalEntityStorIOSQLitePutResolver())
                                .getResolver(new StudentSectionLocalEntityStorIOSQLiteGetResolver())
                                .deleteResolver(new StudentSectionLocalEntityStorIOSQLiteDeleteResolver())
                                .build()
                )
                .build();
    }
}
