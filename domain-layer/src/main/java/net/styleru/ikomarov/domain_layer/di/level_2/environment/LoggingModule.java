package net.styleru.ikomarov.domain_layer.di.level_2.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.manager.events.EventsLogger;
import net.styleru.ikomarov.data_layer.manager.events.IEventsLogger;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 13.03.17.
 */

public class LoggingModule {

    private final Object lockLM = new Object();

    @NonNull
    private final IAppComponent component;

    @Nullable
    private volatile IEventsLogger loggingManager;

    public LoggingModule(@NonNull IAppComponent component) {
        this.component = component;
    }

    @NonNull
    public IEventsLogger provideLoggingManager() {
        IEventsLogger localInstance = loggingManager;
        if(localInstance == null) {
            synchronized (lockLM) {
                localInstance = loggingManager;
                if (localInstance == null) {
                    localInstance = loggingManager = new EventsLogger(component.app().provideContext());
                }
            }
        }

        return localInstance;
    }
}
