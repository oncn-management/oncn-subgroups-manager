package net.styleru.ikomarov.domain_layer.di.endpoints.jobs;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.birbit.android.jobqueue.JobManager;
import com.birbit.android.jobqueue.config.Configuration;
import com.birbit.android.jobqueue.log.CustomLogger;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 20.04.17.
 */

public class JobEnvironmentModule {

    private final Object lock = new Object();

    @NonNull
    private final IAppComponent component;

    @Nullable
    private volatile JobManager jobManager;

    public JobEnvironmentModule(@NonNull IAppComponent component) {
        this.component = component;
    }

    @NonNull
    public JobManager provideJobManager() {
        JobManager localInstance = jobManager;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = jobManager;
                if(localInstance == null) {
                    Configuration config = new Configuration.Builder(component.app().provideContext())
                            .customLogger(new CustomLogger() {
                                @Override
                                public boolean isDebugEnabled() {
                                    return true;
                                }

                                @Override
                                public void d(String text, Object... args) {
                                    Log.d("JobManager", "notification debug: " + text);
                                }

                                @Override
                                public void e(Throwable t, String text, Object... args) {
                                    Log.e("JobManager", "error non-silent: " + text, t);
                                }

                                @Override
                                public void e(String text, Object... args) {
                                    Log.d("JobManager", "error silent: " + text);
                                }

                                @Override
                                public void v(String text, Object... args) {
                                    Log.d("JobManager", "verbose: " + text);
                                }
                            })
                            .consumerKeepAlive(30)
                            .build();

                    localInstance = jobManager = new JobManager(config);
                }
            }
        }

        return localInstance;
    }
}
