package net.styleru.ikomarov.data_layer.services.base;

import android.util.Log;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.exceptions.ExternalException;
import net.styleru.ikomarov.data_layer.exceptions.external.InternalException;
import net.styleru.ikomarov.data_layer.exceptions.external.NetworkException;

import java.net.ConnectException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLException;
import javax.net.ssl.SSLHandshakeException;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.ObservableTransformer;
import io.reactivex.functions.Function;
import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by i_komarov on 25.02.17.
 */

public abstract class NetworkService<S> {

    private Retrofit retrofit;

    private S service;

    public NetworkService(String baseUrl, OkHttpClient client) {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .client(client)
                .build();

        bindService();
    }

    private void bindService() {
        this.service = createService(retrofit);
    }

    protected abstract S createService(Retrofit retrofit);

    protected abstract Gson getGson();

    protected S getService() {
        return this.service;
    }

    protected final <T> ObservableTransformer<T, T> handleError() {
        return upstream -> upstream.onErrorResumeNext(
                throwable -> {
                    return Observable.error(parseException(throwable));
                }
        );
    }

    private ExceptionBundle parseException(Throwable thr) {
        ExceptionBundle error;

        if(thr instanceof UnknownHostException) {
            error = new ExceptionBundle(ExceptionBundle.Reason.NETWORK_UNAVAILABLE);
        } else if(thr instanceof SSLHandshakeException) {
            error = new ExceptionBundle(ExceptionBundle.Reason.NETWORK_SSL_HANDSHAKE_FAILED);
        } else if(thr.getCause() != null && thr.getCause() instanceof ExceptionBundle) {
            error = (ExceptionBundle) thr.getCause();
        } else {
            error = new ExceptionBundle(ExceptionBundle.Reason.INTERNAL_DATA);
            error.addThrowable(ExceptionBundle.InternalErrorContract.KEY_THROWABLE, thr);
            error.addStringExtra(ExceptionBundle.InternalErrorContract.KEY_MESSAGE, thr.getMessage());
        }

        Log.e("NetworkService", "exception was raised", error);

        return error;
    }
}
