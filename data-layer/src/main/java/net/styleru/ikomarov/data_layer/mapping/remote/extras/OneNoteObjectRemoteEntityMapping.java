package net.styleru.ikomarov.data_layer.mapping.remote.extras;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.entities.remote.extras.OneNoteObjectRemoteEntity;

import java.util.List;

/**
 * Created by i_komarov on 14.02.17.
 */

public class OneNoteObjectRemoteEntityMapping implements IRemoteMapping<OneNoteObjectRemoteEntity> {

    @NonNull
    public static final String FIELD_ODATA_CONTEXT = "@odata.context";

    private final Gson gson;

    private OneNoteObjectRemoteEntityMapping() {
        this.gson = new Gson();
    }


    @Override
    public OneNoteObjectRemoteEntity fromJson(JsonObject json) {
        return gson.fromJson(json, OneNoteObjectRemoteEntity.class);
    }

    @Override
    public List<OneNoteObjectRemoteEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<OneNoteObjectRemoteEntity>>(){}.getType());
    }

    @Override
    public String toJson(OneNoteObjectRemoteEntity entity) {
        return gson.toJson(entity, OneNoteObjectRemoteEntity.class);
    }

    @Override
    public String toJsonArray(List<OneNoteObjectRemoteEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<OneNoteObjectRemoteEntity>>(){}.getType());
    }
}
