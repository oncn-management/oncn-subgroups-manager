package net.styleru.ikomarov.domain_layer.repository.reports;

import net.styleru.ikomarov.domain_layer.dto.PermissionReportSubgroupDTO;
import net.styleru.ikomarov.domain_layer.dto.base.ReportDTO;
import net.styleru.ikomarov.domain_layer.dto.base.ReportGroupDTO;

import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 17.04.17.
 */

public interface INotebookReportsRepository {

    Observable<String> createReportPDF(UUID operationId, ReportDTO<ReportGroupDTO<PermissionReportSubgroupDTO>> report);

    Observable<String> createReportCSV(UUID operationId, ReportDTO<ReportGroupDTO<PermissionReportSubgroupDTO>> report);
}
