package net.styleru.ikomarov.data_layer.services.class_notebooks;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ClassNotebookRemoteEntity;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static net.styleru.ikomarov.data_layer.common.Endpoints.*;
import static net.styleru.ikomarov.data_layer.common.Paths.*;

/**
 * Created by i_komarov on 18.02.17.
 */

public interface IClassNotebooksRemoteStorageService {

    String PARAM_OFFSET = "$skip";
    String PARAM_LIMIT = "$top";
    String PARAM_FILTER = "$filter";
    String PARAM_ORDER_BY = "$orderby";

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_CREATED} code
     * */
    @POST(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS)
    Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> create(
            @Path(PATH_USER_ID) String userId,
            @Body JsonObject notebook
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}")
    Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS)
    Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Query(PARAM_OFFSET) int offset,
            @Query(PARAM_LIMIT) int limit
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS)
    Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Query(PARAM_OFFSET) int offset,
            @Query(PARAM_LIMIT) int limit,
            @Query(value = PARAM_ORDER_BY, encoded = true) String orderBy
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS)
    Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Query(PARAM_OFFSET) int offset,
            @Query(PARAM_LIMIT) int limit,
            @Query(value = PARAM_ORDER_BY, encoded = true) String orderBy,
            @Query(value = PARAM_FILTER, encoded = true) String filter
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_CREATED} code
     * */
    @POST(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_TEACHERS)
    Observable<Response<DataResponse<PrincipalObjectRemoteEntity>>> updateAddTeacher(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Body JsonObject teacher
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_CREATED} code
     * */
    @POST(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_STUDENTS)
    Observable<Response<DataResponse<PrincipalObjectRemoteEntity>>> updateAddStudent(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Body JsonObject student
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_NO_CONTENT} code
     * */
    @DELETE(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_TEACHERS + "/{" + PATH_TEACHER_ID + "}")
    Observable<Response<DataResponse<Object>>> updateRemoveTeacher(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Path(PATH_TEACHER_ID) String teacherId
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_NO_CONTENT} code
     * */
    @DELETE(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_STUDENTS + "/{" + PATH_STUDENT_ID + "}")
    Observable<Response<DataResponse<Object>>> updateRemoveStudent(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Path(PATH_STUDENT_ID) String studentId
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_CREATED} code
     * */
    @POST(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_COPY_SECTIONS_TO_CONTENT_LIBRARY)
    Observable<Response<DataResponse<Object>>> updateAddSections(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Body ClassNotebookRemoteEntity.SectionsInsertRequestBody sections
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_NO_CONTENT} code
     * */
    @DELETE(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}")
    Observable<Response<DataResponse<Object>>> delete(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId
    );
}
