package net.styleru.ikomarov.data_layer.services.section_groups;

import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;

import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntity;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 15.03.17.
 */

public interface ISectionGroupsLocalStorageService {

    Observable<PutResult> create(final SectionGroupLocalEntity entity);

    Observable<PutResults<SectionGroupLocalEntity>> create(final List<SectionGroupLocalEntity> entities);

    Observable<SectionGroupLocalEntity> read(final String id);

    Observable<List<SectionGroupLocalEntity>> read(String notebookId, Date ifModifiedSince);

    Observable<List<SectionGroupLocalEntity>> readAll(String notebookId);

    Observable<List<SectionGroupLocalEntity>> read(final String notebookId, final int offset, final int limit);

    Observable<List<SectionGroupLocalEntity>> read(final String notebookId, final int offset, final int limit, final String nameSubstring);

    Observable<List<SectionGroupLocalEntity>> readNested(String sectionGroupId, Date ifModifiedSince);

    Observable<List<SectionGroupLocalEntity>> readAllNested(String sectionGroupId);

    Observable<List<SectionGroupLocalEntity>> readNested(final String sectionGroupId, final int offset, final int limit);

    Observable<List<SectionGroupLocalEntity>> readNested(final String sectionGroupId, final int offset, final int limit, final String nameSubstring);
}
