package net.styleru.ikomarov.domain_layer.repository.permissions;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.permissions.PermissionCategory;
import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.services.permissions.section_groups.ISGPermissionsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.permissions.section_groups.ISGPermissionsRemoteStorageService;
import net.styleru.ikomarov.domain_layer.cache.ICacheStrategy;
import net.styleru.ikomarov.domain_layer.contracts.policies.CachePolicy;
import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;
import net.styleru.ikomarov.domain_layer.mapping.permissions.LocalPermissionsMapper;
import net.styleru.ikomarov.domain_layer.mapping.permissions.RemotePermissionsMapper;
import net.styleru.ikomarov.domain_layer.mapping.remote_to_local.RemoteToLocalPermissionMapper;
import net.styleru.ikomarov.domain_layer.repository.base.AbstractRepository;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.repository.base.DeleteOperationResult;

import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_PARTIAL;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SGPermissionsRepository extends AbstractRepository implements ISGPermissionsRepository {

    private static final String SELF = "me";

    @NonNull
    private final ISGPermissionsLocalStorageService localService;

    @NonNull
    private final ISGPermissionsRemoteStorageService remoteService;

    @NonNull
    private final ICacheStrategy strategy;

    @NonNull
    private final IRemoteMapping<PermissionRemoteEntity> mapping;

    private SGPermissionsRepository(@NonNull ISGPermissionsLocalStorageService localService,
                                    @NonNull ISGPermissionsRemoteStorageService remoteService,
                                    @NonNull ICacheStrategy strategy,
                                    @NonNull IRemoteMapping<PermissionRemoteEntity> mapping) {
        super();
        this.localService = localService;
        this.remoteService = remoteService;
        this.strategy = strategy;
        this.mapping = mapping;
    }

    @Override
    public Observable<CreateOperationResult> create(UUID operationId, String sectionGroupId, String userId, UserRole role) {
        CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            //send an operation to the delayed operations stack
            //TODO: implement the operations stack
            return Observable.just(CreateOperationResult.delayedResult(operationId));
        }

        return performCache(sectionGroupId, remoteService.create(SELF, sectionGroupId, new PermissionDTO.Builder(userId, role).create()), HTTP_CREATED)
                .concatMap(Observable::fromIterable)
                .map(data -> CreateOperationResult.successfulResult(operationId, data.getId()));
    }

    @Override
    public Observable<List<PermissionDTO>> read(String sectionGroupId, boolean fast) {
        CachePolicy policy = strategy.currentPolicy();
        if(fast) {
            return Observable.merge(
                    policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(sectionGroupId, shouldThrowOnNoCachedResourcesFound(policy)) : Observable.empty(),
                    policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(sectionGroupId) : Observable.empty()
            );
        } else {
            return Observable.merge(
                    policy == CachePolicy.CACHE_ONLY ? tryReadFromCache(sectionGroupId, true) : Observable.empty(),
                    policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(sectionGroupId) : Observable.empty()
            );
        }
    }

    @Override
    public Observable<List<PermissionDTO>> read(String sectionGroupId, String userId) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(sectionGroupId, userId, shouldThrowOnNoCachedResourcesFound(policy)) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(sectionGroupId, userId) : Observable.empty()
        );
    }

    @Override
    public Observable<DeleteOperationResult> delete(UUID operationId, String sectionGroupId, String permissionId) {
        CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            //send an operation to the delayed operations stack
            //TODO: implement the operations stack
            return Observable.just(DeleteOperationResult.delayedResult(operationId));
        }

        return remoteService.delete(SELF, sectionGroupId, permissionId)
                .flatMap(verifyResponse(Object.class, HTTP_NO_CONTENT))
                .doOnNext((data) -> removeFromCache(sectionGroupId, permissionId))
                .map(data -> DeleteOperationResult.successfulResult(operationId));
    }

    /*@Override
    public Observable<String> createReport(UUID operationId, String reportName, List<ReportGroupDTO> sectionGroups) {
        return Observable.fromIterable(sectionGroups)
                .flatMap(groupId -> SGPermissionsRepository.this.read(groupId.getId())
                        .flatMap(Observable::fromIterable)
                        .map(dto -> new PermissionReportSubgroupEntity(groupId.getName(), dto.getName(), dto.getRole().value()))
                        .toList()
                        .toObservable())
                .compose(RxUtils.accumulate())
                .flatMap(items -> reportGenerator.generate(reportName, items))
                .map(ReportDataResponse::getFilePath);

    }*/

    private Observable<List<PermissionDTO>> tryReadFromCache(String sectionGroupId, boolean shouldThrowOnNoResourcesFound) {
        return localService.read(sectionGroupId)
                .map(new LocalPermissionsMapper())
                .flatMap(list -> list.size() == 0 ?
                        raiseNoResourcesException(shouldThrowOnNoResourcesFound) :
                        Observable.just(list)
                );
    }

    private Observable<List<PermissionDTO>> tryReadFromCache(String sectionGroupId, String userId, boolean shouldThrowOnNoResourcesFound) {
        return localService.read(sectionGroupId, userId)
                .map(new LocalPermissionsMapper())
                .flatMap(list -> list.size() == 0 ?
                        raiseNoResourcesException(shouldThrowOnNoResourcesFound) :
                        Observable.just(list)
                );
    }

    private Observable<List<PermissionDTO>> readUpdatesFromNetwork(String sectionGroupId) {
        return performCache(sectionGroupId,
                remoteService.read(SELF, sectionGroupId),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<PermissionDTO>> readUpdatesFromNetwork(String sectionGroupId, String userId) {
        return performCache(sectionGroupId,
                remoteService.read(SELF, sectionGroupId, userId),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<PermissionDTO>> performCache(String sectionGroupId, Observable<Response<DataResponse<PermissionRemoteEntity>>> fromNetwork, Integer... successCodes) {
        return fromNetwork.flatMap(verifyResponse(PermissionRemoteEntity.class, successCodes))
                .doOnNext((data) -> addToCache(sectionGroupId, data, mapping))
                .flatMap(response -> Observable.just(response.getBody(mapping))
                        .map(new RemotePermissionsMapper()));
    }

    private void addToCache(String sectionGroupId, DataResponse<PermissionRemoteEntity> data, IRemoteMapping<PermissionRemoteEntity> mapping) {
        Observable.fromIterable(data.getBody(mapping))
                .map(new RemoteToLocalPermissionMapper(PermissionCategory.SECTION_GROUP, sectionGroupId))
                .flatMap(localService::create)
                .subscribe(
                        (item) -> {
                            //TODO: well, at least here might be some name-based cache version onModified action
                        }
                );
    }

    private void removeFromCache(String sectionGroupId, String permissionId) {
        localService.delete(sectionGroupId, permissionId)
                .subscribe(
                        (item) -> {
                            //TODO: well, at least here might be some name-based cache version onDeleted action
                        }
                );
    }

    public static final class Factory {

        private Factory() {

        }

        public static ISGPermissionsRepository create(@NonNull ISGPermissionsLocalStorageService localService,
                                                      @NonNull ISGPermissionsRemoteStorageService remoteService,
                                                      @NonNull ICacheStrategy strategy,
                                                      @NonNull IRemoteMapping<PermissionRemoteEntity> mapping) {

            return new SGPermissionsRepository(localService, remoteService, strategy, mapping);
        }
    }
}
