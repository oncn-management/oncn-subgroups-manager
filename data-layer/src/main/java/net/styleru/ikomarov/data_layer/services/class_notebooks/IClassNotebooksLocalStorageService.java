package net.styleru.ikomarov.data_layer.services.class_notebooks;

import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.StudentSectionLocalEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 01.03.17.
 */

public interface IClassNotebooksLocalStorageService {

    Observable<PutResult> create(ClassNotebookLocalEntity entity);

    Observable<PutResults<ClassNotebookLocalEntity>> create(List<ClassNotebookLocalEntity> entities);

    Observable<ClassNotebookLocalEntity> read(String notebookId);

    Observable<List<ClassNotebookLocalEntity>> readAll();

    Observable<List<ClassNotebookLocalEntity>> read(int offset, int limit);

    Observable<List<ClassNotebookLocalEntity>> read(int offset, int limit, String nameSubstring);

    Observable<PutResult> addTeacher(final String notebookId, final PrincipalObjectLocalEntity teacher);

    Observable<PutResult> addStudent(final String notebookId, final PrincipalObjectLocalEntity student);

    Observable<PutResult> addStudentSection(final String notebookId, final StudentSectionLocalEntity section);

    Observable<DeleteResult> removeTeacher(final String notebookId, final String teacherId);

    Observable<DeleteResult> removeStudent(final String notebookId, final String studentId);

    Observable<DeleteResult> deleteNotebook(String notebookId);
}
