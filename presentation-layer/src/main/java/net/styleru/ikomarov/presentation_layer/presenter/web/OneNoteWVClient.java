package net.styleru.ikomarov.presentation_layer.presenter.web;

import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.styleru.ikomarov.authentication.web_interfaces.OAuth2;

/**
 * Created by i_komarov on 09.04.17.
 */

public class OneNoteWVClient extends WebViewClient {

    private final Callbacks callbacks;

    public OneNoteWVClient(Callbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);

        if(url.contains(OAuth2.BASE_URL)) {
            callbacks.onPageLoaded();
        }
    }

    public interface Callbacks {

        void onPageLoaded();
    }
}
