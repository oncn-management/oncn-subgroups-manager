package net.styleru.ikomarov.presentation_layer.view.students_list;

import android.support.annotation.StringRes;

import net.styleru.ikomarov.presentation_layer.view.base.IView;

import java.util.List;

/**
 * Created by i_komarov on 19.03.17.
 */

public interface IStudentsListView extends IView {

    void addStudents(List<StudentViewObject> items);

    void deleteStudent(String studentId);

    void showError(@StringRes int error);

    void showConnectionFailedIndicator();

    void showOperationDelayedDialog();

    void showNoCachedDataPlug();

    void showNoDataPlug();

    void showNoPermissionsDialog();

    void showAuthorizationDialog();
}
