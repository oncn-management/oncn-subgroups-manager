package net.styleru.ikomarov.presentation_layer.view.base;

import android.content.Intent;
import android.support.annotation.Nullable;

import java.util.LinkedList;

/**
 * Created by i_komarov on 12.03.17.
 */

public class ActivityResultBundle {

    private LinkedList<Result> results;

    public ActivityResultBundle() {
        this.results = new LinkedList<>();
    }

    public void push(int requestCode, int resultCode, Intent data) {
        this.results.push(new Result(requestCode, resultCode, data));
    }

    @Nullable
    public Result pollIfExists() {
        return this.results.isEmpty() ? null : this.results.poll();
    }

    public static final class Result {

        private final int requestCode;
        private final int resultCode;
        private final Intent data;

        private Result(int requestCode, int resultCode, Intent data) {
            this.requestCode = requestCode;
            this.resultCode = resultCode;
            this.data = data;
        }

        public int getRequestCode() {
            return requestCode;
        }

        public int getResultCode() {
            return resultCode;
        }

        public Intent getData() {
            return data;
        }
    }
}
