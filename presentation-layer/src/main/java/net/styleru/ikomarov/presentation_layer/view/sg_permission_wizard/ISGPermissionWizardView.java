package net.styleru.ikomarov.presentation_layer.view.sg_permission_wizard;

import android.support.annotation.StringRes;

import net.styleru.ikomarov.presentation_layer.view.base.IView;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;

import java.util.List;

/**
 * Created by i_komarov on 05.04.17.
 */

public interface ISGPermissionWizardView extends IView {

    void addStudents(List<StudentViewObject> students);

    void applySyncState(SyncState syncState);

    void showError(@StringRes int error);

    void showConnectionFailedIndicator();

    void showOperationDelayedDialog();

    void showNoCachedDataPlug();

    void showNoDataPlug();

    void showNoPermissionsDialog();

    void showAuthorizationDialog();

    enum SyncState {
        SYNC_PROGRESS,
        SYNC_SUCCESS,
        SYNC_FAILURE,
        SYNC_DELAY,
        DEFAULT
    }
}
