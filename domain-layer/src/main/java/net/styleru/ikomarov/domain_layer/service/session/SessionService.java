package net.styleru.ikomarov.domain_layer.service.session;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.session.ISessionManager;
import net.styleru.ikomarov.data_layer.manager.session.SessionManager;
import net.styleru.ikomarov.domain_layer.di.level_1.AppComponent;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.service.base.ServiceException;

/**
 * Created by i_komarov on 06.04.17.
 */

public class SessionService extends Service {

    private ISessionControl control;

    private RemoteCallbackList<ISessionCallbacks> callbacks;

    private ISessionManager manager;

    public SessionService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IAppComponent self = AppComponent.fromExternalProcess(this);
        IEnvironmentComponent environment = self.plusEnvironmentComponent();
        manager = environment.plusSessionComponent().session().provideSessionManager();

        this.callbacks = new RemoteCallbackList<>();

        this.control = new ISessionControl.Stub() {
            @Override
            public SessionStatus getCurrentSessionStatus() throws RemoteException {
                return null;
            }

            @Override
            public String blockingGetSessionToken(String resourceType) throws RemoteException {
                try {
                    return manager.blockingGetSessionToken(resourceType);
                } catch (ExceptionBundle exceptionBundle) {
                    exceptionBundle.printStackTrace();
                    return null;
                }
            }

            @Override
            public String blockingRefreshAndGetSessionToken(String resourceType) throws RemoteException {
                try {
                    return manager.blockingRefreshAndGetSessionToken(resourceType);
                } catch (ExceptionBundle exceptionBundle) {
                    exceptionBundle.printStackTrace();
                    return null;
                }
            }

            @Override
            public void authenticate(String code) throws RemoteException {
                try {
                    manager.authenticate(code);
                } catch (ExceptionBundle exceptionBundle) {
                    exceptionBundle.printStackTrace();
                }
            }

            @Override
            public void deauthenticate() throws RemoteException {
                manager.deauthenticate();
            }

            @Override
            public void registerCallback(ISessionCallbacks callback) throws RemoteException {
                callbacks.register(callback);
            }

            @Override
            public void unregisterCallback(ISessionCallbacks callback) throws RemoteException {
                callbacks.unregister(callback);
            }
        };

        manager.registerCallback(SessionService.class.getSimpleName(), new SessionManager.IAuthenticationCallbacks() {
            @Override
            public void onAuthenticated() {
                if(callbacks != null) {
                    int num = callbacks.beginBroadcast();
                    while(num > 0) {
                        num--;
                        try {
                            callbacks.getBroadcastItem(num).onAuthenticated();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    callbacks.finishBroadcast();
                }
            }

            @Override
            public void onDeauthenticated() {
                if(callbacks != null) {
                    int num = callbacks.beginBroadcast();
                    while(num > 0) {
                        num--;
                        try {
                            callbacks.getBroadcastItem(num).onDeauthenticated();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    callbacks.finishBroadcast();
                }
            }

            @Override
            public void onAuthenticationFailed(ExceptionBundle error) {
                if(callbacks != null) {
                    int num = callbacks.beginBroadcast();
                    while(num > 0) {
                        num--;
                        try {
                            callbacks.getBroadcastItem(num).onAuthenticationFailure(new ServiceException(error));
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                    callbacks.finishBroadcast();
                }
            }
        });
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return control.asBinder();
    }
}
