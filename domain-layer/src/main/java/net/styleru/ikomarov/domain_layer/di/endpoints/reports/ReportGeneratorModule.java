package net.styleru.ikomarov.domain_layer.di.endpoints.reports;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.report.base.ReportEntity;
import net.styleru.ikomarov.data_layer.entities.report.base.ReportGroupEntity;
import net.styleru.ikomarov.data_layer.entities.report.base.ReportSubgroupEntity;
import net.styleru.ikomarov.data_layer.entities.report.permissions.PermissionReportSubgroupEntity;
import net.styleru.ikomarov.data_layer.reports.base.IReportGenerator;
import net.styleru.ikomarov.data_layer.reports.permission.PermissionsReportGenerator;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 18.04.17.
 */

public class ReportGeneratorModule {

    private final Object lock = new Object();

    @NonNull
    private final IAppComponent component;

    @Nullable
    private volatile IReportGenerator<PermissionReportSubgroupEntity> permissionsReportGenerator;

    public ReportGeneratorModule(@NonNull IAppComponent component) {
        this.component = component;
    }

    @NonNull
    public IReportGenerator<PermissionReportSubgroupEntity> providePermissionsReportGenerator() {
        IReportGenerator<PermissionReportSubgroupEntity> localInstance = permissionsReportGenerator;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = permissionsReportGenerator;
                if(localInstance == null) {
                    localInstance = permissionsReportGenerator = new PermissionsReportGenerator(component.app().provideContext());
                }
            }
        }

        return localInstance;
    }
}
