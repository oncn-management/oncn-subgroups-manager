package net.styleru.ikomarov.authentication.login;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.WebView;
import android.widget.ProgressBar;

import net.styleru.ikomarov.authentication.BuildConfig;
import net.styleru.ikomarov.authentication.R;
import net.styleru.ikomarov.authentication.account.OncnAccount;
import net.styleru.ikomarov.authentication.http.AzureWebView;
import net.styleru.ikomarov.authentication.http.OAuthError;
import net.styleru.ikomarov.authentication.retaincontainer.Factory;
import net.styleru.ikomarov.authentication.retaincontainer.RetainFragmentContainer;
import net.styleru.ikomarov.authentication.retaincontainer.SingletonFactory;
import net.styleru.ikomarov.authentication.web_interfaces.OAuth2;

/**
 * Created by i_komarov on 18.02.17.
 */

public class LoginActivity extends AccountAuthenticatorActivity implements AzureWebView.Callback, ILoginView {

    public static final String KEY_OAUTH_CODE = "net.styleru.ikomarov.oncnsubgroupsmanager.OAUTH_CODE";

    public static final String KEY_TOKEN_TYPE = "net.styleru.ikomarov.oncnsubgroupsmanager.TOKEN_TYPE";

    private static final String KEY_RETAIN_CONTAINER = "net.styleru.ikomarov.oncnsubgroupsmanager.LoginActivity.RETAIN_CONTAINER";

    private LoginPresenter presenter;
    private WebView authenticationView;
    private ProgressBar progressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        authenticationView = (WebView) findViewById(R.id.activity_login_web_view);
        progressBar = (ProgressBar) findViewById(R.id.activity_login_progress_bar);

        progressBar.setEnabled(true);
        progressBar.setVisibility(View.VISIBLE);

        authenticationView.clearCache(true);
        authenticationView.clearHistory();

        clearCookies(this);

        authenticationView.setWebViewClient(new AzureWebView(this));
        authenticationView.getSettings().setJavaScriptEnabled(true);
        authenticationView.getSettings().setSupportMultipleWindows(false);
        authenticationView.getSettings().setBuiltInZoomControls(false);

        authenticationView.loadUrl(
                OAuth2.BASE_URL + OAuth2.ENDPOINT_AUTHORIZE + "?" +
                        OAuth2.FIELD_CLIENT_ID + "=" + BuildConfig.AZURE_AD_API_KEY + "&" +
                        OAuth2.FIELD_REDIRECT_URI + "=" + BuildConfig.AZURE_AD_REDIRECT_URI + "&" +
                        OAuth2.FIELD_RESPONSE_TYPE + "=" + OAuth2.QUERY_PARAM_CODE + "&" +
                        OAuth2.FIELD_RESOURCE + "=" + OncnAccount.RESOURCE_ONENOTE
        );
    }

    @Override
    public void onPageLoaded() {
        progressBar.setEnabled(false);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAccessTokenLoaded(String displayName, String accessToken, String refreshToken) {
        Intent res = new Intent();

        res.putExtra(AccountManager.KEY_ACCOUNT_NAME, displayName);
        res.putExtra(AccountManager.KEY_ACCOUNT_TYPE, OncnAccount.TYPE);
        res.putExtra(AccountManager.KEY_PASSWORD, "sometrickypassword");

        final Account account = new Account(displayName, OncnAccount.TYPE);

        AccountManager am = AccountManager.get(this);

        am.addAccountExplicitly(account, res.getStringExtra(AccountManager.KEY_PASSWORD), null);
        am.setAuthToken(account, OncnAccount.TOKEN_REFRESH, refreshToken);
        am.setAuthToken(account, OncnAccount.TOKEN_GRAPH, accessToken);

        setAccountAuthenticatorResult(res.getExtras());
        setResult(RESULT_OK, res);
        finish();
    }

    @Override
    public void onSuccess(String code) {
        authenticationView.loadUrl("about:blank");

        Intent intent = new Intent();
        intent.putExtra(KEY_OAUTH_CODE, code);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onFailure(OAuthError error, String description) {
        authenticationView.loadUrl("about:blank");
    }

    @Override
    public void onStart() {
        super.onStart();
        if(getFragmentManager().findFragmentByTag(KEY_RETAIN_CONTAINER) == null) {
            RetainFragmentContainer<LoginPresenter> container = new RetainFragmentContainer<>();
            container.setContentProducer(new SingletonFactory<>(
                    new Factory<LoginPresenter>() {
                        @Override
                        public LoginPresenter create() {
                            return new LoginPresenter();
                        }
                    }
            ));

            presenter = container.getContent();

            getFragmentManager().beginTransaction().add(container, KEY_RETAIN_CONTAINER).commit();
        } else {
            presenter = ((RetainFragmentContainer<LoginPresenter>) getFragmentManager().findFragmentByTag(KEY_RETAIN_CONTAINER)).getContent();
        }

        presenter.bindView(this);
    }

    @Override
    public void onStop() {
        presenter.unbindView();
        presenter = null;
        super.onStop();
    }

    @SuppressWarnings("deprecation")
    private static void clearCookies(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else {
            CookieSyncManager cookieSyncMngr = CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }
}
