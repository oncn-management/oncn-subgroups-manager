package net.styleru.ikomarov.presentation_layer.view.notebooks_list;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;
import java.util.UUID;

/**
 * Created by i_komarov on 08.03.17.
 */

public class NotebookViewObject implements Parcelable {

    private String id;
    private String browserUrl;
    private String clientUrl;
    private Date lastModified;
    private String name;
    private boolean isSynchronized;
    private boolean hasTeacherOnlySectionGroup;
    private boolean isCollaborationSpaceLocked;

    public NotebookViewObject(String id, String browserUrl, String clientUrl, Date lastModified, String name, boolean hasTeacherOnlySectionGroup, boolean isCollaborationSpaceLocked) {
        this.id = id;
        this.browserUrl = browserUrl;
        this.clientUrl = clientUrl;
        this.lastModified = lastModified;
        this.name = name;
        this.isSynchronized = true;
        this.hasTeacherOnlySectionGroup = hasTeacherOnlySectionGroup;
        this.isCollaborationSpaceLocked = isCollaborationSpaceLocked;
    }

    public NotebookViewObject(UUID uuid, String browserUrl, String clientUrl, Date lastModified, String name, boolean hasTeacherOnlySectionGroup, boolean isCollaborationSpaceLocked) {
        this.id = uuid.toString();
        this.browserUrl = browserUrl;
        this.clientUrl = clientUrl;
        this.lastModified = lastModified;
        this.name = name;
        this.isSynchronized = false;
        this.hasTeacherOnlySectionGroup = hasTeacherOnlySectionGroup;
        this.isCollaborationSpaceLocked = isCollaborationSpaceLocked;
    }

    private NotebookViewObject(Parcel in) {
        id = in.readString();
        browserUrl = in.readString();
        clientUrl = in.readString();
        long lastModifiedTime = in.readLong();
        lastModified = lastModifiedTime != -1L ? new Date(lastModifiedTime) : null;
        name = in.readString();
        isSynchronized = in.readByte() != 0;
        hasTeacherOnlySectionGroup = in.readByte() != 0;
        isCollaborationSpaceLocked = in.readByte() != 0;
    }

    public String getId() {
        return id;
    }

    public String getBrowserUrl() {
        return browserUrl;
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public String getName() {
        return name;
    }

    public boolean isSynchronized() {
        return isSynchronized;
    }

    public boolean isHasTeacherOnlySectionGroup() {
        return hasTeacherOnlySectionGroup;
    }

    public boolean isCollaborationSpaceLocked() {
        return isCollaborationSpaceLocked;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(browserUrl);
        dest.writeString(clientUrl);
        dest.writeLong(lastModified != null ? lastModified.getTime() : -1);
        dest.writeString(name);
        dest.writeByte((byte) (isSynchronized ? 1 : 0));
        dest.writeByte((byte) (hasTeacherOnlySectionGroup ? 1 : 0));
        dest.writeByte((byte) (isCollaborationSpaceLocked ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NotebookViewObject> CREATOR = new Creator<NotebookViewObject>() {
        @Override
        public NotebookViewObject createFromParcel(Parcel in) {
            return new NotebookViewObject(in);
        }

        @Override
        public NotebookViewObject[] newArray(int size) {
            return new NotebookViewObject[size];
        }
    };
}
