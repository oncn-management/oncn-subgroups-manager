package net.styleru.ikomarov.data_layer.services.class_notebooks;

import android.database.Cursor;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.StudentSectionLocalEntity;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.ClassNotebooksLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.PrincipalObjectsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.section_groups.SectionGroupsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.sections.SectionsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.StudentSectionsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.services.base.DatabaseService;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 01.03.17.
 */

public class ClassNotebooksLocalStorageService extends DatabaseService implements IClassNotebooksLocalStorageService {

    private ClassNotebooksLocalStorageService(StorIOSQLite sqlite) {
        super(sqlite);
    }

    @Override
    public Observable<PutResult> create(final ClassNotebookLocalEntity entity) {
        return wrapTransactionSynchronousOff(() -> getSQLite().put()
                .object(entity)
                .prepare()
                .executeAsBlocking());
    }

    @Override
    public Observable<PutResults<ClassNotebookLocalEntity>> create(final List<ClassNotebookLocalEntity> entities) {
        return wrapTransactionSynchronousOff(() -> getSQLite().put()
                .objects(entities)
                .prepare()
                .executeAsBlocking());
    }

    @Override
    public Observable<ClassNotebookLocalEntity> read(final String notebookId) {
        return wrap(
                () -> {
                    Cursor cursor = getSQLite().get()
                            .cursor()
                            .withQuery(Query.builder().table(ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS).where(ClassNotebooksLocalEntityMapping.COLUMN_ID + " = \"" + notebookId + "\"").build())
                            .prepare()
                            .executeAsBlocking();

                    try {
                        return new ClassNotebooksLocalEntityMapping().fromCursor(cursor);
                    } finally {
                        cursor.close();
                    }
                }
        );
    }

    @Override
    public Observable<List<ClassNotebookLocalEntity>> readAll() {
        return wrap(
                () -> {
                    Cursor cursor = getSQLite().get()
                            .cursor()
                            .withQuery(Query.builder().table(ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS).build())
                            .prepare()
                            .executeAsBlocking();

                    try {
                        return new ClassNotebooksLocalEntityMapping().listFromCursor(cursor);
                    } finally {
                        cursor.close();
                    }
                }
        );
    }

    @Override
    public Observable<List<ClassNotebookLocalEntity>> read(final int offset, final int limit) {
        return wrap(
                () -> {
                    Cursor cursor = getSQLite().get()
                            .cursor()
                            .withQuery(Query.builder().table(ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS).limit(offset, limit).orderBy(ClassNotebooksLocalEntityMapping.COLUMN_LAST_MODIFIED_BY).build())
                            .prepare()
                            .executeAsBlocking();

                    try {
                        return new ClassNotebooksLocalEntityMapping().listFromCursor(cursor);
                    } finally {
                        cursor.close();
                    }
                }
        );
    }

    @Override
    public Observable<List<ClassNotebookLocalEntity>> read(final int offset, final int limit, final String nameSubstring) {
        return wrap(
                () -> {
                    Cursor cursor = getSQLite().get()
                            .cursor()
                            .withQuery(Query.builder().table(ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS).limit(offset, limit).orderBy(ClassNotebooksLocalEntityMapping.COLUMN_NAME).where(ClassNotebooksLocalEntityMapping.COLUMN_NAME + " LIKE \"" + nameSubstring + "%\"").build())
                            .prepare()
                            .executeAsBlocking();

                    try {
                        return new ClassNotebooksLocalEntityMapping().listFromCursor(cursor);
                    } finally {
                        cursor.close();
                    }
                }
        );
    }

    @Override
    public Observable<PutResult> addTeacher(final String notebookId, final PrincipalObjectLocalEntity teacher) {
        return wrapTransactionSynchronousOff(() -> {
            getSQLite().lowLevel().executeSQL(RawQuery.builder().query(ClassNotebooksLocalEntityMapping.onInsertTeacherExecuteQuery(notebookId, teacher.getId())).build());
            return getSQLite().put()
                    .object(teacher)
                    .prepare()
                    .executeAsBlocking();
        });
    }

    @Override
    public Observable<PutResult> addStudent(final String notebookId, final PrincipalObjectLocalEntity student) {
        return wrapTransactionSynchronousOff(() -> {
            getSQLite().lowLevel().executeSQL(RawQuery.builder().query(ClassNotebooksLocalEntityMapping.onInsertStudentExecuteQuery(notebookId, student.getId())).build());
            return getSQLite().put()
                    .object(student)
                    .prepare()
                    .executeAsBlocking();
        });
    }

    @Override
    public Observable<PutResult> addStudentSection(final String notebookId, final StudentSectionLocalEntity section) {
        return wrapTransactionSynchronousOff(() -> getSQLite().put()
                .object(section)
                .prepare()
                .executeAsBlocking());
    }

    @Override
    public Observable<DeleteResult> removeTeacher(final String notebookId, final String teacherId) {
        return wrapTransactionSynchronousOff(() -> {
            getSQLite().lowLevel().executeSQL(RawQuery.builder().query(ClassNotebooksLocalEntityMapping.onInsertTeacherExecuteQuery(notebookId, teacherId)).build());
            return getSQLite().delete()
                    .byQuery(
                            DeleteQuery.builder().table(PrincipalObjectsLocalEntityMapping.TABLE)
                                    .where(PrincipalObjectsLocalEntityMapping.COLUMN_ID + " = " + "\"" + teacherId +  "\"")
                                    .build()
                    )
                    .prepare()
                    .executeAsBlocking();
        });
    }

    @Override
    public Observable<DeleteResult> removeStudent(final String notebookId, final String studentId) {
        return wrapTransactionSynchronousOff(() -> {
            getSQLite().lowLevel().executeSQL(RawQuery.builder().query(ClassNotebooksLocalEntityMapping.onDeleteStudentExecuteQuery(notebookId, studentId)).build());
            return getSQLite().delete()
                    .byQuery(
                            DeleteQuery.builder().table(PrincipalObjectsLocalEntityMapping.TABLE)
                                    .where(PrincipalObjectsLocalEntityMapping.COLUMN_ID + " = " + "\"" + studentId + "\"")
                                    .build()
                    )
                    .prepare()
                    .executeAsBlocking();
        });
    }

    @Override
    public Observable<DeleteResult> deleteNotebook(final String notebookId) {
        return wrapTransactionSynchronousOff(() -> {

            getSQLite().delete()
                    .byQuery(DeleteQuery.builder().table(ClassNotebooksLocalEntityMapping.TABLE_STUDENTS_LINKING).where(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"").build())
                    .prepare()
                    .executeAsBlocking();
            getSQLite().delete()
                    .byQuery(DeleteQuery.builder().table(ClassNotebooksLocalEntityMapping.TABLE_TEACHERS_LINKING).where(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"").build())
                    .prepare()
                    .executeAsBlocking();
            getSQLite().delete()
                    .byQuery(DeleteQuery.builder().table(SectionsLocalEntityMapping.TABLE).where(SectionsLocalEntityMapping.COLUMN_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"").build())
                    .prepare()
                    .executeAsBlocking();
            getSQLite().delete()
                    .byQuery(DeleteQuery.builder().table(SectionGroupsLocalEntityMapping.TABLE).where(SectionGroupsLocalEntityMapping.COLUMN_PARENT_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"").build())
                    .prepare()
                    .executeAsBlocking();
            getSQLite().delete()
                    .byQuery(DeleteQuery.builder().table(StudentSectionsLocalEntityMapping.TABLE).where(StudentSectionsLocalEntityMapping.COLUMN_PARENT_ID + " = " + "\"" + notebookId + "\"").build())
                    .prepare()
                    .executeAsBlocking();

            return getSQLite().delete()
                    .byQuery(DeleteQuery.builder().table(ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS).where(ClassNotebooksLocalEntityMapping.COLUMN_ID + " = " + "\"" + notebookId + "\"").build())
                    .prepare()
                    .executeAsBlocking();
        });
    }

    private interface Producer<T> {

        T produce() throws Exception;
    }

    public static class Factory {

        public static IClassNotebooksLocalStorageService create(StorIOSQLite sqlite) {
            return new ClassNotebooksLocalStorageService(sqlite);
        }
    }
}
