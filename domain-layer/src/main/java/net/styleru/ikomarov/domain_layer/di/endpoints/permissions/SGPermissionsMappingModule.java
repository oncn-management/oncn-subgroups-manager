package net.styleru.ikomarov.domain_layer.di.endpoints.permissions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.mapping.remote.permissions.PermissionsRemoteEntityMapping;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SGPermissionsMappingModule {

    private final Object lock = new Object();

    @Nullable
    private volatile IRemoteMapping<PermissionRemoteEntity> mapping;

    @NonNull
    public IRemoteMapping<PermissionRemoteEntity> provideMapping() {
        IRemoteMapping<PermissionRemoteEntity> localInstance = mapping;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = mapping;
                if(localInstance == null) {
                    localInstance = mapping = new PermissionsRemoteEntityMapping();
                }
            }
        }

        return localInstance;
    }
}
