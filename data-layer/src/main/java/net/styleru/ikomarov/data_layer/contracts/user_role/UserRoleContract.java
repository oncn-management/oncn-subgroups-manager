package net.styleru.ikomarov.data_layer.contracts.user_role;

/**
 * Created by i_komarov on 18.02.17.
 */

class UserRoleContract {

    static String ROLE_OWNER = "Owner";
    static String ROLE_CONTRIBUTOR = "Contributor";
    static String ROLE_READER = "Reader";
    static String ROLE_NONE = "None";

    private UserRoleContract() {
        throw new IllegalStateException("No instances please");
    }
}
