package net.styleru.ikomarov.domain_layer.repository.reports;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.report.base.ReportEntity;
import net.styleru.ikomarov.data_layer.entities.report.base.ReportGroupEntity;
import net.styleru.ikomarov.data_layer.entities.report.permissions.PermissionReportSubgroupEntity;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.reports.base.IReportGenerator;
import net.styleru.ikomarov.data_layer.reports.base.ReportDataResponse;
import net.styleru.ikomarov.domain_layer.dto.PermissionReportSubgroupDTO;
import net.styleru.ikomarov.domain_layer.dto.base.ReportDTO;
import net.styleru.ikomarov.domain_layer.dto.base.ReportGroupDTO;

import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 17.04.17.
 */

public class NotebookReportsRepository implements INotebookReportsRepository {

    @NonNull
    private final IReportGenerator<PermissionReportSubgroupEntity> reportService;

    @NonNull
    private final Function<ReportDTO<ReportGroupDTO<PermissionReportSubgroupDTO>>, ReportEntity<PermissionReportSubgroupEntity, ReportGroupEntity<PermissionReportSubgroupEntity>>> mapping;

    public NotebookReportsRepository(@NonNull IReportGenerator<PermissionReportSubgroupEntity> reportService,
                                     @NonNull Function<ReportDTO<ReportGroupDTO<PermissionReportSubgroupDTO>>, ReportEntity<PermissionReportSubgroupEntity, ReportGroupEntity<PermissionReportSubgroupEntity>>> mapping) {

        this.reportService = reportService;
        this.mapping = mapping;
    }

    @Override
    public Observable<String> createReportPDF(UUID operationId, ReportDTO<ReportGroupDTO<PermissionReportSubgroupDTO>> report) {
        return Observable.just(report)
                .map(mapping)
                .doOnNext(reportEntity -> {
                    System.out.println(reportEntity.toString());
                })
                .flatMap(reportService::generate)
                .flatMap(response -> response.getStatus() == ReportDataResponse.Status.SUCCESS ?
                        Observable.just(response.getFilePath()) :
                        Observable.error(new ExceptionBundle(ExceptionBundle.Reason.PDF_DOCUMENT_FAILURE))
                );
    }

    @Override
    public Observable<String> createReportCSV(UUID operationId, ReportDTO<ReportGroupDTO<PermissionReportSubgroupDTO>> report) {
        //TODO: add support for CSV reports!
        throw new UnsupportedOperationException("CSV Reports are not supported yet!");
    }
}
