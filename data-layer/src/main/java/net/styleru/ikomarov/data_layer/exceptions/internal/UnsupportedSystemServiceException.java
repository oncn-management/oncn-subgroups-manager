package net.styleru.ikomarov.data_layer.exceptions.internal;

/**
 * Created by i_komarov on 25.02.17.
 */

public class UnsupportedSystemServiceException extends UnsupportedOperationException {

    private static final String MESSAGE_FORMAT = "System service \"%1$s\" is not supported on current device.\nStacktrace:\n%2$s";

    public UnsupportedSystemServiceException(String systemServiceName) {
        super(String.format(MESSAGE_FORMAT, systemServiceName, "OS Version: " + System.getProperty("os.version") + "(" + android.os.Build.VERSION.INCREMENTAL + ")\n" + "OS API Level: " + android.os.Build.VERSION.RELEASE + "(" + android.os.Build.VERSION.SDK_INT + ")\n" + "Device: " + android.os.Build.DEVICE + "\n" + "Model (and Product): " + android.os.Build.MODEL + " ("+ android.os.Build.PRODUCT + ")\n"));
    }
}
