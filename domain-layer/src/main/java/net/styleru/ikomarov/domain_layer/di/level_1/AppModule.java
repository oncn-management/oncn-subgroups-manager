package net.styleru.ikomarov.domain_layer.di.level_1;

import android.content.Context;
import android.support.annotation.NonNull;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by i_komarov on 13.03.17.
 */

public class AppModule {

    @NonNull
    private final Context context;

    @NonNull
    private final Scheduler ioScheduler = Schedulers.io();

    public AppModule(@NonNull Context context) {
        this.context = context;
    }

    @NonNull
    public Context provideContext() {
        return this.context;
    }

    @NonNull
    public Scheduler prodiveSchedulerIO() {
        return this.ioScheduler;
    }
}
