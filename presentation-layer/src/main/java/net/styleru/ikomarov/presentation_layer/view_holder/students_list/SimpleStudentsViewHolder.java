package net.styleru.ikomarov.presentation_layer.view_holder.students_list;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;

/**
 * Created by i_komarov on 05.04.17.
 */

public class SimpleStudentsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private AppCompatTextView textAvatarHolder;
    private AppCompatTextView nameHolder;
    private AppCompatTextView emailHolder;

    private int position;
    private StudentViewObject item;
    private InteractionCallbacks callbacks;

    public SimpleStudentsViewHolder(View itemView) {
        super(itemView);

        textAvatarHolder    = (AppCompatTextView) itemView.findViewById(R.id.list_item_student_short_name);
        nameHolder          = (AppCompatTextView) itemView.findViewById(R.id.list_item_student_display_name);
        emailHolder         = (AppCompatTextView) itemView.findViewById(R.id.list_item_student_email);

        itemView.setOnClickListener(this);
    }

    public void bind(int position, StudentViewObject item, InteractionCallbacks callbacks) {
        this.position = position;
        this.item = item;
        this.callbacks = callbacks;

        textAvatarHolder.setText(item.getName() != null ? formatShortName(item.getName()) : "");
        nameHolder.setText(item.getName() != null ? item.getName() : "");
        emailHolder.setText(item.getEmail() != null ? item.getEmail() : "");
    }

    @Override
    public void onClick(View v) {
        if(callbacks != null) {
            callbacks.onStudentClicked(position, item);
        }
    }

    private String formatShortName(String name) {
        String[] parts = name.split(" ");
        String firstName = "";
        String lastName = "";
        if(parts.length > 0 && parts[0].length() > 0) {
            firstName = parts[0].substring(0, 1);
        }
        if(parts.length > 1 && parts[1].length() > 0) {
            lastName = parts[1].substring(0, 1);
        }

        return firstName + lastName;
    }

    public interface InteractionCallbacks {

        void onStudentClicked(int position, StudentViewObject item);
    }
}
