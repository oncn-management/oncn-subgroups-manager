package net.styleru.ikomarov.data_layer.exceptions;

import android.os.Bundle;

/**
 * Created by i_komarov on 07.03.17.
 */

public class ExceptionBundle extends Exception {

    private ExceptionBundle.Reason reason;
    private Bundle extras;

    public ExceptionBundle(Reason reason) {
        this.reason = reason;
        this.extras = new Bundle();
    }

    public ExceptionBundle(Reason reason, Bundle extras) {
        this.reason = reason;
        this.extras = extras == null ? new Bundle() : extras;
    }

    public Reason getReason() {
        return reason;
    }

    public void addThrowable(String key, Throwable throwable) {
        this.extras.putSerializable(key, throwable);
    }

    public void addStringExtra(String key, String value) {
        this.extras.putString(key, value);
    }

    public void addIntExtra(String key, int extra) {
        this.extras.putInt(key, extra);
    }

    public Throwable getThrowable(String key) {
        return (Throwable) this.extras.getSerializable(key);
    }

    public String getStringExtra(String key) {
        return this.extras.getString(key);
    }

    public int getIntExtra(String key) {
        return this.extras.getInt(key);
    }

    public Bundle getExtras() {
        return this.extras;
    }

    public enum Reason {
        NO_CACHED_RESOURCES(000),
        NO_RESOURCES(001),

        NO_SESSION(100),

        ONE_NOTE_ERROR(200),

        DATABASE_UNKNOWN(300),
        DATABASE_CONSTRAINT(301),

        HTTP_BAD_CODE(400),

        NETWORK_UNAVAILABLE(500),
        NETWORK_SSL_HANDSHAKE_FAILED(501),
        NETWORK_UNKNOWN(502),

        PDF_DOCUMENT_FAILURE(600),

        INTERNAL_DATA(700),

        INTERNAL_DOMAIN(800),

        INTERNAL_PRESENTATION(900);

        private final int code;

        Reason(int code) {
            this.code = code;
        }

        public boolean isResourcesError() {
            return 0 <= code && code < 100;
        }

        public boolean isSessionError() {
            return 100 <= code && code < 200;
        }

        public boolean isOneNoteError() {
            return 200 <= code && code < 300;
        }

        public boolean isDatabaseError() {
            return 300 <= code && code < 400;
        }

        public boolean isHttpError() {
            return 400 <= code && code < 500;
        }

        public boolean isNetworkError() {
            return 500 <= code && code < 600;
        }

        public boolean isPdfError() {
            return 600 <= code && code < 700;
        }

        public boolean isInternalError() {
            return 700 <= code;
        }
    }

    public static final class InternalErrorContract {

        private static final String PREFIX = InternalErrorContract.class.getSimpleName() + ".";

        public static final String KEY_MESSAGE = PREFIX + "message";
        public static final String KEY_THROWABLE = PREFIX + "throwable";
    }

    public static final class OneNoteErrorContract {

        private static final String PREFIX = OneNoteErrorContract.class.getSimpleName() + ".";

        public static final String KEY_API_URL = PREFIX + "api_url";
        public static final String KEY_CODE = PREFIX + "code";
        public static final String KEY_MESSAGE = PREFIX + "message";

        public static final int CODE_RESOURCE_NOT_FOUND = 20102;

        public static final int CODE_TOKEN_EXPIRED = 40001;
        public static final int CODE_NO_WRITE_PERMISSIONS = 40002;
        public static final int CODE_NO_ACCESS_PERMISSIONS = 40003;
        public static final int CODE_NO_REQUIRED_SCOPE = 40004;

        public static boolean isServiceProblem(int code) {
            return 10000 < code && code < 20000;
        }

        public static boolean isApplicationProblem(int code) {
            return 20000 < code && code < 30000;
        }

        public static boolean isUserAccountProblem(int code) {
            return 30000 < code && code < 40000;
        }

        public static boolean isPermissionsProblem(int code) {
            return 40000 < code && code < 50000;
        }
    }

    public static final class DatabaseErrorContract {

        private static final String PREFIX = DatabaseErrorContract.class.getSimpleName() + ".";

        public static final String KEY_CODE = PREFIX + "code";
        public static final String KEY_SQL_STATE = PREFIX + "sql_state";
        public static final String KEY_MESSAGE = PREFIX + "message";
    }

    public static final class HttpErrorContract {

        private static final String PREFIX = HttpErrorContract.class.getSimpleName() + ".";

        public static final String KEY_CODE = PREFIX + "code";
        public static final String KEY_MESSAGE = PREFIX + "message";
    }

    public static final class PdfErrorContract {

        private static final String PREFIX = PdfErrorContract.class.getCanonicalName() + ".";

        public static final String KEY_THROWABLE = PREFIX + "throwable";
    }
}
