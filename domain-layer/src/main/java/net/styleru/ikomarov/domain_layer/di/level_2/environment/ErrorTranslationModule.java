package net.styleru.ikomarov.domain_layer.di.level_2.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.contracts.errors.IErrorCodeTranslator;
import net.styleru.ikomarov.domain_layer.contracts.errors.OneNoteErrorCodeTranslator;

/**
 * Created by i_komarov on 21.03.17.
 */

public class ErrorTranslationModule {

    private final Object lock = new Object();

    @Nullable
    private volatile IErrorCodeTranslator oneNoteErrorCodeTranslator;

    public ErrorTranslationModule() {

    }

    @NonNull
    public IErrorCodeTranslator provideOneNoteErrorCodeTranslator() {
        IErrorCodeTranslator localInstance = oneNoteErrorCodeTranslator;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = oneNoteErrorCodeTranslator;
                if(localInstance == null) {
                    localInstance = oneNoteErrorCodeTranslator = new OneNoteErrorCodeTranslator();
                }
            }
        }

        return localInstance;
    }
}
