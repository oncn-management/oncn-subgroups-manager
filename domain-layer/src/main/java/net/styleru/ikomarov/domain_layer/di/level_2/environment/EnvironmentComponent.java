package net.styleru.ikomarov.domain_layer.di.level_2.environment;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.analytics.AnalyticsComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.analytics.IAnalyticsComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.CacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.SessionComponent;

/**
 * Created by i_komarov on 13.03.17.
 */

public class EnvironmentComponent implements IEnvironmentComponent {

    @NonNull
    private final IAppComponent appComponent;

    @NonNull
    private final NetworkModule network;

    @NonNull
    private final LoggingModule logging;

    @NonNull
    private final DatabaseModule database;

    @NonNull
    private final PreferencesModule preferences;

    @NonNull
    private final ErrorTranslationModule errorTranslation;

    public EnvironmentComponent(@NonNull IAppComponent parent) {
        this.appComponent = parent;
        this.network = new NetworkModule(parent);
        this.logging = new LoggingModule(parent);
        this.database = new DatabaseModule(parent);
        this.preferences = new PreferencesModule(parent);
        this.errorTranslation = new ErrorTranslationModule();
    }

    @NonNull
    @Override
    public ICacheComponent plusCacheComponent() {
        return new CacheComponent(this);
    }

    @NonNull
    @Override
    public ISessionComponent plusSessionComponent() {
        return new SessionComponent(appComponent, this);
    }

    @NonNull
    @Override
    public IAnalyticsComponent plusAnalyticsComponent() {
        return new AnalyticsComponent(this);
    }

    @NonNull
    @Override
    public NetworkModule network() {
        return network;
    }

    @NonNull
    @Override
    public LoggingModule logging() {
        return logging;
    }

    @NonNull
    @Override
    public DatabaseModule database() {
        return database;
    }

    @NonNull
    @Override
    public PreferencesModule preferences() {
        return preferences;
    }

    @NonNull
    @Override
    public ErrorTranslationModule errorTranslation() {
        return errorTranslation;
    }
}
