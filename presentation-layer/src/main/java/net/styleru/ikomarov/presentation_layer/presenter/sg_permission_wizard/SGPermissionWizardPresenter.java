package net.styleru.ikomarov.presentation_layer.presenter.sg_permission_wizard;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.events.IEventsLogger;
import net.styleru.ikomarov.domain_layer.contracts.errors.IErrorCodeTranslator;
import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;
import net.styleru.ikomarov.domain_layer.use_cases.permissions.SGPermissionsBatchCreateUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.students.StudentsGetUseCase;
import net.styleru.ikomarov.presentation_layer.mapping.students.StudentMapper;
import net.styleru.ikomarov.presentation_layer.mapping.students.StudentsMapper;
import net.styleru.ikomarov.presentation_layer.presenter.base.Presenter;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.sg_permission_wizard.ISGPermissionWizardView;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 05.04.17.
 */

public class SGPermissionWizardPresenter extends Presenter<ISGPermissionWizardView> implements StudentsGetUseCase.Callbacks<StudentViewObject>, SGPermissionsBatchCreateUseCase.Callbacks {

    @NonNull
    private final StudentsGetUseCase studentsGet;

    @NonNull
    private final SGPermissionsBatchCreateUseCase permissionsCreate;

    @NonNull
    private final IErrorCodeTranslator oneNoteCodeTranslator;

    @NonNull
    private final IEventsLogger loggingManager;

    @NonNull
    private final Function<PrincipalObjectDTO, StudentViewObject> singleMapper;

    @NonNull
    private final Function<List<PrincipalObjectDTO>, List<StudentViewObject>> listMapper;

    @NonNull
    private final String notebookId;

    @NonNull
    private final String sectionGroupId;

    private SGPermissionWizardPresenter(@NonNull StudentsGetUseCase studentsGet,
                                       @NonNull SGPermissionsBatchCreateUseCase permissionsCreate,
                                       @NonNull IErrorCodeTranslator oneNoteCodeTranslator,
                                       @NonNull IEventsLogger loggingManager,
                                       @NonNull String notebookId,
                                       @NonNull String sectionGroupId) {
        super();
        this.studentsGet = studentsGet;
        this.permissionsCreate = permissionsCreate;
        this.oneNoteCodeTranslator = oneNoteCodeTranslator;
        this.loggingManager = loggingManager;
        this.singleMapper = new StudentMapper();
        this.listMapper = new StudentsMapper();
        this.notebookId = notebookId;
        this.sectionGroupId = sectionGroupId;
    }

    @Override
    protected void onViewAttached() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {
        studentsGet.dispose();
        permissionsCreate.dispose();
    }

    public void createPermissions(List<String> students) {
        permissionsCreate.execute(UUID.randomUUID(), this, sectionGroupId, students);
    }

    public void loadStudents(String nameSubstring, int offset, int limit) {
        studentsGet.execute(listMapper, this, notebookId, offset, limit, nameSubstring);
    }

    @Override
    public void onStudentsLoaded(List<StudentViewObject> students) {
        postAction((view) -> view.addStudents(students));
    }

    @Override
    public void onStudentsLoadingFailed(ExceptionBundle error) {
        handleException(
                error,
                SGPermissionWizardPresenter.class.getSimpleName(),
                "onStudentsLoadingFailed",
                ExceptionBundle.Reason.ONE_NOTE_ERROR,
                ExceptionBundle.Reason.NETWORK_UNAVAILABLE,
                ExceptionBundle.Reason.NETWORK_SSL_HANDSHAKE_FAILED,
                ExceptionBundle.Reason.NETWORK_UNKNOWN
        );
    }

    @Override
    public void onSGPermissionsCreated(UUID operationId) {
        postAction(view -> view.applySyncState(ISGPermissionWizardView.SyncState.SYNC_SUCCESS));
    }

    @Override
    public void onSGPermissionsCreationConflict(UUID operationId) {
        postAction(view -> view.applySyncState(ISGPermissionWizardView.SyncState.SYNC_FAILURE));
    }

    @Override
    public void onSGPermissionsCreationFailed(UUID operationId, ExceptionBundle error) {
        postAction(view -> view.applySyncState(ISGPermissionWizardView.SyncState.SYNC_FAILURE));
        handleException(
                error,
                SGPermissionWizardPresenter.class.getSimpleName(),
                "onSGPermissionsCreationFailed"
        );
    }

    @Override
    public void onSGPermissionsCreationDelayed(UUID operationId) {
        postAction(view -> view.applySyncState(ISGPermissionWizardView.SyncState.SYNC_DELAY));
    }

    private void handleException(ExceptionBundle error, String clazz, String method, ExceptionBundle.Reason... ignoreReasons) {
        if(-1 == Arrays.binarySearch(ignoreReasons, error.getReason())) {
            return;
        }

        postAction((view) -> {
            switch (error.getReason()) {
                case NO_CACHED_RESOURCES: {
                    view.showNoCachedDataPlug();
                    break;
                }
                case NO_RESOURCES: {
                    view.showNoDataPlug();
                    break;
                }
                case NO_SESSION: {
                    view.showAuthorizationDialog();
                    break;
                }
                case ONE_NOTE_ERROR: {
                    final int code = error.getIntExtra(ExceptionBundle.OneNoteErrorContract.KEY_CODE);
                    final String message = error.getStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_MESSAGE);
                    view.showError(oneNoteCodeTranslator.getLocalizedMessage(code, message));
                    break;
                }
                case NETWORK_UNAVAILABLE: {
                    view.showConnectionFailedIndicator();
                    break;
                }
                case NETWORK_SSL_HANDSHAKE_FAILED: {
                    view.showConnectionFailedIndicator();
                    break;
                }
                default: {
                    if(error.getReason().isInternalError()) {
                        loggingManager.logEvent(clazz, method, error.getReason(), error.getThrowable(ExceptionBundle.InternalErrorContract.KEY_THROWABLE));
                    } else {
                        loggingManager.logEvent(clazz, method, error.getReason());
                    }
                }
            }
        });
    }

    public static final class Factory implements RetainContainer.Factory<SGPermissionWizardPresenter> {

        @NonNull
        private final StudentsGetUseCase studentsGet;

        @NonNull
        private final SGPermissionsBatchCreateUseCase permissionsCreate;

        @NonNull
        private final IErrorCodeTranslator oneNoteCodeTranslator;

        @NonNull
        private final IEventsLogger loggingManager;

        @NonNull
        private final String notebookId;

        @NonNull
        private final String sectionGroupId;

        public Factory(@NonNull StudentsGetUseCase studentsGet,
                       @NonNull SGPermissionsBatchCreateUseCase permissionsCreate,
                       @NonNull IErrorCodeTranslator oneNoteCodeTranslator,
                       @NonNull IEventsLogger loggingManager,
                       @NonNull String notebookId,
                       @NonNull String sectionGroupId) {

            super();
            this.studentsGet = studentsGet;
            this.permissionsCreate = permissionsCreate;
            this.oneNoteCodeTranslator = oneNoteCodeTranslator;
            this.loggingManager = loggingManager;
            this.notebookId = notebookId;
            this.sectionGroupId = sectionGroupId;
        }

        @Override
        public SGPermissionWizardPresenter create() {
            return new SGPermissionWizardPresenter(studentsGet, permissionsCreate, oneNoteCodeTranslator, loggingManager, notebookId, sectionGroupId);
        }
    }
}
