package net.styleru.ikomarov.presentation_layer.mapping.section_groups;

import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.presentation_layer.view.section_groups_list.SectionGroupViewObject;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupMapper implements Function<SectionGroupDTO, SectionGroupViewObject> {

    @Override
    public SectionGroupViewObject apply(SectionGroupDTO dto) throws Exception {
        return new SectionGroupViewObject(
                dto.getId(),
                dto.getLastModifiedTime(),
                dto.getName()
        );
    }
}
