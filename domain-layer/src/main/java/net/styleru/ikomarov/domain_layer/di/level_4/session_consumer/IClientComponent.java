package net.styleru.ikomarov.domain_layer.di.level_4.session_consumer;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;

/**
 * depends on {@link ISessionComponent}
 * */
public interface IClientComponent {

    @NonNull
    ClientModule clients();
}
