package net.styleru.ikomarov.domain_layer.dto.base;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 17.04.17.
 */

public class ReportDTO<G extends ReportGroupDTO<? extends ReportSubgroupDTO>> {

    @NonNull
    private final String name;

    @NonNull
    private final List<G> groups;

    public ReportDTO(@NonNull String name, @NonNull List<G> groups) {
        this.name = name;
        this.groups = groups;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public List<G> getGroups() {
        return groups;
    }

    public static final class Builder<S extends ReportSubgroupDTO, G extends ReportGroupDTO<S>> {

        @NonNull
        private String name;

        @NonNull
        private final List<G> groups;

        public Builder(@NonNull String name) {
            this.name = name;
            this.groups = new ArrayList<G>();
        }

        public Builder<S, G> addPostfix(String postfix) {
            this.name += postfix;
            return this;
        }

        public Builder<S, G> addGroup(G group) {
            this.groups.add(group);
            return this;
        }

        public ReportDTO<G> build() {
            return new ReportDTO<G>(name, groups);
        }
    }
}
