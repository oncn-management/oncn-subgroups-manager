package net.styleru.ikomarov.domain_layer.di.endpoints.students;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 19.03.17.
 */

public interface IStudentsComponent {

    @NonNull
    StudentsRepositoryModule repository();
}
