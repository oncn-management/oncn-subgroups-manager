package net.styleru.ikomarov.data_layer.contracts.database;

import net.styleru.ikomarov.data_layer.mapping.local.notebooks.PrincipalObjectsLocalEntityMapping;

/**
 * Created by i_komarov on 28.02.17.
 */

public class DatabaseContracts {

    public static final class PrincipalObjectsContract {

        public static final String COLUMN_ID = PrincipalObjectsLocalEntityMapping.TABLE + "_" + PrincipalObjectsLocalEntityMapping.COLUMN_ID;

        public static final String COLUMN_NAME = PrincipalObjectsLocalEntityMapping.TABLE + "_" + PrincipalObjectsLocalEntityMapping.COLUMN_NAME;

        public static final String COLUMN_EMAIL = PrincipalObjectsLocalEntityMapping.TABLE + "_" + PrincipalObjectsLocalEntityMapping.COLUMN_EMAIL;

        public static final String COLUMN_DEPARTMENT = PrincipalObjectsLocalEntityMapping.TABLE + "_" + PrincipalObjectsLocalEntityMapping.COLUMN_DEPARTMENT;

        public static final String COLUMN_TYPE = PrincipalObjectsLocalEntityMapping.TABLE + "_" + PrincipalObjectsLocalEntityMapping.COLUMN_TYPE;
    }
}
