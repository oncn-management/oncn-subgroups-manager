package net.styleru.ikomarov.domain_layer.use_cases.permissions;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.JobManager;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.jobs.base.IBroadcastFactory;
import net.styleru.ikomarov.domain_layer.jobs.permissions.SGPermissionsBatchCreateJob;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.repository.permissions.ISGPermissionsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 05.04.17.
 */

public class SGPermissionsBatchCreateUseCase extends UseCase {

    @NonNull
    private final JobManager jobManager;

    @NonNull
    private final ISGPermissionsRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    @NonNull
    private final SGPermissionsBatchCreateJob.Factory jobFactory;

    public SGPermissionsBatchCreateUseCase(@NonNull IBroadcastFactory<SGPermissionsBatchCreateJob.JobParams> broadcastFactory,
                                           @NonNull JobManager jobManager,
                                           @NonNull ISGPermissionsRepository repository,
                                           @NonNull Scheduler executeScheduler,
                                           @NonNull Scheduler postScheduler) {

        super();
        this.jobManager = jobManager;
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
        this.jobFactory = new SGPermissionsBatchCreateJob.Factory(broadcastFactory, repository);
    }

    public void execute(UUID operationId, Callbacks callbacks, String sectionGroupId, List<String> participantIds) {
        Observable.fromIterable(participantIds)
                .flatMap((id) -> {
                    return repository.create(UUID.randomUUID(), sectionGroupId, id, UserRole.CONTRIBUTOR)
                            .subscribeOn(executeScheduler);
                })
                .toList()
                .toObservable()
                .observeOn(postScheduler)
                .subscribe(createPermissionsBatchCreateObserver(operationId, callbacks, new SGPermissionsBatchCreateJob.JobParams(operationId, sectionGroupId, participantIds)));
    }

    private DisposableObserver<List<CreateOperationResult>> createPermissionsBatchCreateObserver(UUID operationId, Callbacks callbacks, SGPermissionsBatchCreateJob.JobParams params) {
        DisposableObserver<List<CreateOperationResult>> batchUpdateObserver = new DisposableObserver<List<CreateOperationResult>>() {
            @Override
            public void onNext(List<CreateOperationResult> value) {
                int resultStatus = 0;
                for(CreateOperationResult result : value) {
                    resultStatus += result.getStatus().code();
                }

                if(!isDisposed()) {
                    //check if all of actions succeeded
                    if(resultStatus == 0) {
                        callbacks.onSGPermissionsCreated(operationId);
                        //next check if all of actions were delayed
                    } else if(resultStatus == 2 * value.size()) {
                        callbacks.onSGPermissionsCreationDelayed(operationId);
                        //if not - let's say the conflict happened, so we have to refresh the page and try again
                    } else {
                        callbacks.onSGPermissionsCreationConflict(operationId);
                    }
                }

                if(resultStatus == 2 * value.size()) {
                    jobManager.addJobInBackground(jobFactory.create(params));
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if(!isDisposed()) {
                    //here it is obvious that something bad happened, let's say it's error
                    callbacks.onSGPermissionsCreationFailed(operationId, e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(batchUpdateObserver);

        return batchUpdateObserver;
    }

    public interface Callbacks {

        void onSGPermissionsCreated(UUID operationId);

        void onSGPermissionsCreationConflict(UUID operationId);

        void onSGPermissionsCreationFailed(UUID operationId, ExceptionBundle error);

        void onSGPermissionsCreationDelayed(UUID operationId);
    }
}
