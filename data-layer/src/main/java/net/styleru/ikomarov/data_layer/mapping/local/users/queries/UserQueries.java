package net.styleru.ikomarov.data_layer.mapping.local.users.queries;

import net.styleru.ikomarov.data_layer.mapping.utils.QueryBuilder;
import net.styleru.ikomarov.data_layer.mapping.local.users.UsersBusinessPhonesLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.users.UsersLocalEntityMapping;

import java.util.Collections;
import java.util.List;

import static net.styleru.ikomarov.data_layer.mapping.local.users.queries.UserQueries.Contract.*;

/**
 * Created by i_komarov on 01.03.17.
 */

public class UserQueries {

    public static final class Contract {

        public static final String COLUMN_USER_ID = UsersLocalEntityMapping.TABLE + UsersLocalEntityMapping.COLUMN_ID;
        public static final String COLUMN_USER_DISPLAY_NAME = UsersLocalEntityMapping.TABLE + "_" + UsersLocalEntityMapping.COLUMN_DISPLAY_NAME;
        public static final String COLUMN_USER_GIVEN_NAME = UsersLocalEntityMapping.TABLE + "_" + UsersLocalEntityMapping.COLUMN_GIVEN_NAME;
        public static final String COLUMN_USER_JOB_TITLE = UsersLocalEntityMapping.TABLE + "_" + UsersLocalEntityMapping.COLUMN_JOB_TITLE;
        public static final String COLUMN_USER_MAIL = UsersLocalEntityMapping.TABLE + "_" + UsersLocalEntityMapping.COLUMN_MAIL;
        public static final String COLUMN_USER_MOBILE_PHONE = UsersLocalEntityMapping.TABLE + "_" + UsersLocalEntityMapping.COLUMN_MOBILE_PHONE;
        public static final String COLUMN_USER_OFFICE_LOCATION = UsersLocalEntityMapping.TABLE + "_" + UsersLocalEntityMapping.COLUMN_OFFICE_LOCATION;
        public static final String COLUMN_USER_PREFERRED_LANGUAGE = UsersLocalEntityMapping.TABLE + "_" + UsersLocalEntityMapping.COLUMN_PREFERRED_LANGUAGE;
        public static final String COLUMN_USER_LAST_NAME = UsersLocalEntityMapping.TABLE + "_" + UsersLocalEntityMapping.COLUMN_LAST_NAME;
        public static final String COLUMN_USER_PRINCIPAL_NAME = UsersLocalEntityMapping.TABLE + "_" + UsersLocalEntityMapping.COLUMN_PRINCIPAL_NAME;

        public static final String COLUMN_BUSINESS_PHONE_ID = UsersBusinessPhonesLocalEntityMapping.TABLE + UsersBusinessPhonesLocalEntityMapping.COLUMN_ID;
        public static final String COLUMN_BUSINESS_PHONE_USER_ID = UsersBusinessPhonesLocalEntityMapping.TABLE + UsersBusinessPhonesLocalEntityMapping.COLUMN_USER_ID;
        public static final String COLUMN_BUSINESS_PHONE_VALUE = UsersBusinessPhonesLocalEntityMapping.TABLE + UsersBusinessPhonesLocalEntityMapping.COLUMN_PHONE;
    }

    public static String query(String userId) {
        return query(Integer.MIN_VALUE, Integer.MIN_VALUE, Collections.singletonList(UsersLocalEntityMapping.COLUMN_ID + " = " + "\"" + userId + "\""), null);
    }

    public static String query(int offset, int limit) {
        return query(offset, limit, null, Collections.singletonList(new QueryBuilder.NameValuePair<>(UsersLocalEntityMapping.COLUMN_DISPLAY_NAME, QueryBuilder.Order.ASC)));
    }

    public static String query(int offset, int limit, String displayNameSubstring) {
        return query(offset, limit, Collections.singletonList(UsersLocalEntityMapping.COLUMN_DISPLAY_NAME + " = \"" + displayNameSubstring + "\" OR " + UsersLocalEntityMapping.COLUMN_DISPLAY_NAME + " LIKE \"" + displayNameSubstring + "%\""), Collections.singletonList(new QueryBuilder.NameValuePair<>(UsersLocalEntityMapping.COLUMN_DISPLAY_NAME, QueryBuilder.Order.ASC)));
    }

    private static String query(int offset, int limit, List<String> whereConditions, List<QueryBuilder.NameValuePair<String, QueryBuilder.Order>> orderBy) {
        QueryBuilder.SelectBuilder nestedQuery = QueryBuilder.selectQuery(UsersLocalEntityMapping.TABLE)
                .limit(offset, limit);

        if(orderBy != null) {
            for(QueryBuilder.NameValuePair<String, QueryBuilder.Order> orderByElement : orderBy) {
                nestedQuery = nestedQuery.orderBy(orderByElement.getName(), orderByElement.getValue());
            }
        }

        if(whereConditions != null) {
            for(String whereCondition : whereConditions) {
                nestedQuery = nestedQuery.where(whereCondition);
            }
        }

        return QueryBuilder.selectQuery(UsersLocalEntityMapping.TABLE)
                .overrideTableWithNestedSelect(nestedQuery.build())
                    .withParam(UsersLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, COLUMN_USER_ID)
                    .withParam(UsersLocalEntityMapping.COLUMN_DISPLAY_NAME_WITH_TABLE_PREFIX, COLUMN_USER_DISPLAY_NAME)
                    .withParam(UsersLocalEntityMapping.COLUMN_GIVEN_NAME_WITH_TABLE_PREFIX, COLUMN_USER_GIVEN_NAME)
                    .withParam(UsersLocalEntityMapping.COLUMN_JOB_TITLE_WITH_TABLE_PREFIX, COLUMN_USER_JOB_TITLE)
                    .withParam(UsersLocalEntityMapping.COLUMN_MAIL_WITH_TABLE_PREFIX, COLUMN_USER_MAIL)
                    .withParam(UsersLocalEntityMapping.COLUMN_MOBILE_PHONE_WITH_TABLE_PREFIX, COLUMN_USER_MOBILE_PHONE)
                    .withParam(UsersLocalEntityMapping.COLUMN_OFFICE_LOCATION_WITH_TABLE_PREFIX, COLUMN_USER_OFFICE_LOCATION)
                    .withParam(UsersLocalEntityMapping.COLUMN_PREFERRED_LANGUAGE_WITH_TABLE_PREFIX, COLUMN_USER_PREFERRED_LANGUAGE)
                    .withParam(UsersLocalEntityMapping.COLUMN_LAST_NAME_WITH_TABLE_PREFIX, COLUMN_USER_LAST_NAME)
                    .withParam(UsersLocalEntityMapping.COLUMN_PRINCIPAL_NAME_WITH_TABLE_PREFIX, COLUMN_USER_PRINCIPAL_NAME)
                    .withParam(UsersBusinessPhonesLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, COLUMN_BUSINESS_PHONE_ID)
                    .withParam(UsersBusinessPhonesLocalEntityMapping.COLUMN_USER_ID_WITH_TABLE_PREFIX, COLUMN_BUSINESS_PHONE_USER_ID)
                    .withParam(UsersBusinessPhonesLocalEntityMapping.COLUMN_PHONE_WITH_TABLE_PREFIX, COLUMN_BUSINESS_PHONE_VALUE)
                .leftJoin(UsersBusinessPhonesLocalEntityMapping.TABLE, UsersLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, UsersBusinessPhonesLocalEntityMapping.COLUMN_USER_ID_WITH_TABLE_PREFIX)
                .build();
    }
}
