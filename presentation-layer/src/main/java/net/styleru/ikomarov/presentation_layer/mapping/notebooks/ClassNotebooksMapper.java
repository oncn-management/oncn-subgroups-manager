package net.styleru.ikomarov.presentation_layer.mapping.notebooks;

import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;
import net.styleru.ikomarov.presentation_layer.view.notebooks_list.NotebookViewObject;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 10.03.17.
 */

public class ClassNotebooksMapper implements Function<List<ClassNotebookDTO>, List<NotebookViewObject>> {

    private final Function<ClassNotebookDTO, NotebookViewObject> mapper;

    public ClassNotebooksMapper(Function<ClassNotebookDTO, NotebookViewObject> mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<NotebookViewObject> apply(List<ClassNotebookDTO> classNotebookDTOs) throws Exception {
        return Observable.fromIterable(classNotebookDTOs)
                .map(mapper)
                .toList()
                .blockingGet();
    }
}
