package net.styleru.ikomarov.domain_layer.di.level_2.environment;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 06.04.17.
 */

public class PreferencesModule {

    private static final String DEFAULT_PREFERENCES_NAME = PreferencesModule.class.getSimpleName() + ".PREFERENCES_DEFAULT";

    private final Object lock = new Object();

    @NonNull
    private final IAppComponent component;

    @Nullable
    private volatile SharedPreferences preferences;

    public PreferencesModule(@NonNull IAppComponent component) {
        this.component = component;
    }

    @NonNull
    public SharedPreferences provideSharedPreferences() {
        SharedPreferences localInstance = preferences;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = preferences;
                if(localInstance == null) {
                    localInstance = preferences = component.app().provideContext().getSharedPreferences(DEFAULT_PREFERENCES_NAME, Context.MODE_PRIVATE);
                }
            }
        }

        return localInstance;
    }
}
