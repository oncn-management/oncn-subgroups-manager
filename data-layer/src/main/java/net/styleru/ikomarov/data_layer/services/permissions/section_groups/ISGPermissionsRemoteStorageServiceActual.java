package net.styleru.ikomarov.data_layer.services.permissions.section_groups;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static net.styleru.ikomarov.data_layer.common.Endpoints.*;
import static net.styleru.ikomarov.data_layer.common.Paths.*;

/**
 * Created by i_komarov on 21.03.17.
 */

interface ISGPermissionsRemoteStorageServiceActual {

    String PARAM_OFFSET = "$skip";
    String PARAM_LIMIT = "$top";
    String PARAM_FILTER = "$filter";
    String PARAM_ORDER_BY = "$orderby";

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_CREATED} code
     * */
    @POST(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_PERMISSIONS)
    Observable<Response<DataResponse<PermissionRemoteEntity>>> create(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId,
            @Body JsonObject permissionJson
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_PERMISSIONS)
    Observable<Response<DataResponse<PermissionRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_OK} code
     * */
    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_PERMISSIONS)
    Observable<Response<DataResponse<PermissionRemoteEntity>>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId,
            @Query(PARAM_FILTER) String filter
    );

    /**
     * Successful response contains {@link java.net.HttpURLConnection#HTTP_NO_CONTENT} code
     * */
    @DELETE(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_SECTIONGROUPS + "/{" + PATH_SECTIONGROUP_ID + "}" + ENDPOINT_PERMISSIONS + "/{" + PATH_PERMISSION_ID + "}")
    Observable<Response<DataResponse<Object>>> delete(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_SECTIONGROUP_ID) String sectionGroupId,
            @Path(PATH_PERMISSION_ID) String permissionId
    );
}
