package net.styleru.ikomarov.domain_layer.di.endpoints.notebooks;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;

/**
 * Created by i_komarov on 13.03.17.
 */

public interface INotebooksEnvironmentComponent {

    @NonNull
    ICacheComponent cacheComponent();

    @NonNull
    NotebooksMappingModule mapping();

    @NonNull
    NotebooksRemoteServiceModule remote();

    @NonNull
    NotebooksLocalServiceModule local();

    @NonNull
    NotebooksCacheModule cache();
}
