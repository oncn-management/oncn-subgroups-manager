package net.styleru.ikomarov.domain_layer.di.endpoints.section_groups;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.entities.remote.section_groups.SectionGroupRemoteEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.mapping.remote.section_groups.SectionGroupsRemoteEntityMapping;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsMappingModule {

    private final Object lock = new Object();

    @Nullable
    private volatile IRemoteMapping<SectionGroupRemoteEntity> mapping;

    @NonNull
    public IRemoteMapping<SectionGroupRemoteEntity> provideMapping() {
        IRemoteMapping<SectionGroupRemoteEntity> localInstance = mapping;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = mapping;
                if(localInstance == null) {
                    localInstance = mapping = new SectionGroupsRemoteEntityMapping();
                }
            }
        }

        return localInstance;
    }
}
