package net.styleru.ikomarov.domain_layer.dto;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.mapping.remote.notebooks.ClassNotebooksRemoteEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.remote.notebooks.PrincipalObjectsRemoteEntityMapping;

import java.util.Date;
import java.util.List;

/**
 * Created by i_komarov on 20.02.17.
 */

public class ClassNotebookDTO {

    private String id;
    private String name;
    private String browserUrl;
    private String clientUrl;
    private String createdBy;
    private Date createdTime;
    private String lastModifiedBy;
    private Date lastModifiedTime;
    private UserRole role;
    private Boolean isDefault;
    private Boolean isShared;
    private Boolean hasTeacherOnlySectionGroup;
    private Boolean isCollaborationSpaceLocked;

    public static ClassNotebookDTO newInstance(String id, String name, String browserUrl, String clientUrl, String createdBy, Date createdTime, String lastModifiedBy, Date lastModifiedTime, UserRole role, Boolean isDefault, Boolean isShared, Boolean hasTeacherOnlySectionGroup, Boolean isCollaborationSpaceLocked) {
        ClassNotebookDTO dto = new ClassNotebookDTO(name, hasTeacherOnlySectionGroup);

        dto.id = id;
        dto.browserUrl = browserUrl;
        dto.clientUrl = clientUrl;
        dto.createdBy = createdBy;
        dto.createdTime = createdTime;
        dto.lastModifiedBy = lastModifiedBy;
        dto.lastModifiedTime = lastModifiedTime;
        dto.role = role;
        dto.isDefault = isDefault;
        dto.isShared = isShared;
        dto.isCollaborationSpaceLocked = isCollaborationSpaceLocked;

        return dto;
    }

    private ClassNotebookDTO(String name, Boolean hasTeacherOnlySectionGroup) {
        this.name = name;
        this.hasTeacherOnlySectionGroup = hasTeacherOnlySectionGroup;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBrowserUrl() {
        return browserUrl;
    }

    public String getClientUrl() {
        return clientUrl;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    public UserRole getRole() {
        return role;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public Boolean getShared() {
        return isShared;
    }

    public Boolean getHasTeacherOnlySectionGroup() {
        return hasTeacherOnlySectionGroup;
    }

    public Boolean getCollaborationSpaceLocked() {
        return isCollaborationSpaceLocked;
    }

    public static class Builder {

        private JsonObject json;
        private JsonArray studentSections;
        private JsonArray students;
        private JsonArray teachers;

        public Builder(String name, Boolean hasTeacherOnlySectionGroup) {
            this.studentSections = new JsonArray();
            this.students = new JsonArray();
            this.teachers = new JsonArray();

            this.json = new JsonObject();
            this.json.addProperty(ClassNotebooksRemoteEntityMapping.FIELD_NAME, name);
            this.json.addProperty(ClassNotebooksRemoteEntityMapping.FIELD_HAS_TEACHER_ONLY_SECTION_GROUP, hasTeacherOnlySectionGroup);
        }

        public Builder withStudentSections(List<String> studentSections) {
            for(String s : studentSections) {
                this.studentSections.add(s);
            }

            return this;
        }

        public Builder withTeachers(List<PrincipalObjectDTO> teachers) {
            for(PrincipalObjectDTO o : teachers) {
                JsonObject on = new JsonObject();
                on.addProperty(PrincipalObjectsRemoteEntityMapping.FIELD_ID, o.getId());
                on.addProperty(PrincipalObjectsRemoteEntityMapping.FIELD_PRINCIPAL_TYPE, o.getType().value());
                this.teachers.add(on);
            }

            return this;
        }

        public Builder withStudents(List<PrincipalObjectDTO> students) {
            for(PrincipalObjectDTO o : students) {
                JsonObject on = new JsonObject();
                on.addProperty(PrincipalObjectsRemoteEntityMapping.FIELD_ID, o.getId());
                on.addProperty(PrincipalObjectsRemoteEntityMapping.FIELD_PRINCIPAL_TYPE, o.getType().value());
                this.students.add(on);
            }

            return this;
        }

        public JsonObject build() {
            json.add(ClassNotebooksRemoteEntityMapping.FIELD_TEACHERS, teachers);
            json.add(ClassNotebooksRemoteEntityMapping.FIELD_STUDENTS, students);
            json.add(ClassNotebooksRemoteEntityMapping.FIELD_STUDENT_SECTIONS, studentSections);

            return json;
        }
    }
}
