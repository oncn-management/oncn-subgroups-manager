package net.styleru.ikomarov.data_layer.contracts.principal_object;

import net.styleru.ikomarov.data_layer.exceptions.internal.UnknownEnumValueException;

import static net.styleru.ikomarov.data_layer.contracts.principal_object.PrincipalObjectContract.*;

/**
 * Created by i_komarov on 14.02.17.
 */

public enum PrincipalObjectEntityType {
    GROUP {
        @Override
        public String value() {
            return TYPE_GROUP;
        }
    },
    PERSON {
        @Override
        public String value() {
            return TYPE_PERSON;
        }
    };

    public abstract String value();

    public static PrincipalObjectEntityType forValue(String value) {
        if(value.equals(TYPE_GROUP)) {
            return GROUP;
        } else if(value.equals(TYPE_PERSON)) {
            return PERSON;
        } else {
            throw new UnknownEnumValueException(value, PrincipalObjectEntityType.class.getSimpleName(), "forValue");
        }
    }
}
