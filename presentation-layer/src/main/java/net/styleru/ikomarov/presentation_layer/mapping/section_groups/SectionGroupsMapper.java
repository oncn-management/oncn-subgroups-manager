package net.styleru.ikomarov.presentation_layer.mapping.section_groups;

import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.presentation_layer.view.section_groups_list.SectionGroupViewObject;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsMapper implements Function<List<SectionGroupDTO>, List<SectionGroupViewObject>> {

    @Override
    public List<SectionGroupViewObject> apply(List<SectionGroupDTO> sectionGroupDTOs) throws Exception {
        return Observable.fromIterable(sectionGroupDTOs)
                .map(new SectionGroupMapper())
                .toList()
                .blockingGet();
    }
}
