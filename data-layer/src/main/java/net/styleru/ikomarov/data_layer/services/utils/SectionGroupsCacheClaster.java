package net.styleru.ikomarov.data_layer.services.utils;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsLocalStorageService;

import java.util.Date;

/**
 * Created by i_komarov on 17.03.17.
 */

public class SectionGroupsCacheClaster extends AbstractCacheClaster {

    @NonNull
    private final ISectionGroupsLocalStorageService localService;

    public SectionGroupsCacheClaster(@NonNull ISectionGroupsLocalStorageService localService) {
        super();
        this.localService = localService;
    }

    @Override
    protected IMemCache create(String id) {
        return new SectionGroupsRecordsCache(id, localService);
    }
}
