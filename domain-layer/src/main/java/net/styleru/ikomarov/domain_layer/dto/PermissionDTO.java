package net.styleru.ikomarov.domain_layer.dto;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.mapping.remote.permissions.PermissionsRemoteEntityMapping;

/**
 * Created by i_komarov on 22.02.17.
 */

public class PermissionDTO {

    private String id;
    private String name;
    private String userId;
    private UserRole role;

    public static PermissionDTO newInstance(String id, String name, String userId, UserRole role) {
        PermissionDTO dto = new PermissionDTO();

        dto.id = id;
        dto.name = name;
        dto.userId = userId;
        dto.role = role;

        return dto;
    }

    private PermissionDTO() {

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUserId() {
        return userId;
    }

    public UserRole getRole() {
        return role;
    }

    public static class Builder {

        private JsonObject object;

        public Builder(String userId, UserRole role) {
            this.object = new JsonObject();
            this.object.addProperty(PermissionsRemoteEntityMapping.FIELD_USER_ID, userId);
            this.object.addProperty(PermissionsRemoteEntityMapping.FIELD_USER_ROLE, role.value());
        }

        public JsonObject create() {
            return object;
        }
    }
}
