package net.styleru.ikomarov.domain_layer.mapping.principal_objects;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.03.17.
 */

public class LocalPrincipalObjectsMapper implements Function<List<PrincipalObjectLocalEntity>, List<PrincipalObjectDTO>> {

    @Override
    public List<PrincipalObjectDTO> apply(List<PrincipalObjectLocalEntity> principalObjectLocalEntities) throws Exception {
        return Observable.fromIterable(principalObjectLocalEntities)
                .map(new LocalPrincipalObjectMapper())
                .toList()
                .blockingGet();
    }
}
