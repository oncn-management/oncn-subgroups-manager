package net.styleru.ikomarov.domain_layer.di.endpoints.notebooks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.class_notebooks.ClassNotebooksRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.class_notebooks.IClassNotebooksRemoteStorageService;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * Created by i_komarov on 14.03.17.
 */

public class NotebooksRemoteServiceModule {

    private final Object lock = new Object();

    private final IClientComponent client;

    @Nullable
    private volatile IClassNotebooksRemoteStorageService service;

    public NotebooksRemoteServiceModule(@NonNull IClientComponent client) {
        this.client = client;
    }

    @NonNull
    public IClassNotebooksRemoteStorageService provideService() {
        IClassNotebooksRemoteStorageService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = ClassNotebooksRemoteStorageService.Factory.create(client.clients().provideOneNoteHttpClient());
                }
            }
        }

        return localInstance;
    }
}
