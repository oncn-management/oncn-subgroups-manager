package net.styleru.ikomarov.presentation_layer.view.sg_permissions_list;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SGPermissionViewObject {

    private String id;
    private String userId;
    private String name;
    private UserRole role;

    public SGPermissionViewObject(String id, String userId, String name, UserRole role) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.role = role;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
