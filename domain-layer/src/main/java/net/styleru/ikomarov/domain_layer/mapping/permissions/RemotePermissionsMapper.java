package net.styleru.ikomarov.domain_layer.mapping.permissions;

import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 22.03.17.
 */

public class RemotePermissionsMapper implements Function<List<PermissionRemoteEntity>, List<PermissionDTO>> {

    @Override
    public List<PermissionDTO> apply(List<PermissionRemoteEntity> permissionRemoteEntities) throws Exception {
        return Observable.fromIterable(permissionRemoteEntities)
                .map(new RemotePermissionMapper())
                .toList()
                .blockingGet();
    }
}
