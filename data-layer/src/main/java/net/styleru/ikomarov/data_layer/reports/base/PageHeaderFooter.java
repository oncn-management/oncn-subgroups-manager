package net.styleru.ikomarov.data_layer.reports.base;

import android.support.annotation.NonNull;

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Created by i_komarov on 13.04.17.
 */

public class PageHeaderFooter extends PdfPageEventHelper {

    @NonNull
    private Font headerFooterFont;

    @NonNull
    private String poweredBy;

    public PageHeaderFooter(@NonNull Font headerFooterFont, @NonNull String poweredBy) {
        this.headerFooterFont = headerFooterFont;
        this.poweredBy = poweredBy;
    }

    public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte cb = writer.getDirectContent();

        Phrase footer_poweredBy = new Phrase(poweredBy, headerFooterFont);
        Phrase footer_pageNumber = new Phrase(String.valueOf(document.getPageNumber()), headerFooterFont);

        ColumnText.showTextAligned(cb,
                Element.ALIGN_RIGHT,
                footer_pageNumber,
                (document.getPageSize().getWidth() - 10),
                document.bottom() - 10,
                0
        );

        ColumnText.showTextAligned(
                cb,
                Element.ALIGN_CENTER,
                footer_poweredBy,
                (document.right() - document.left()) / 2 + document.leftMargin(), document.bottom() - 10,
                0
        );
    }
}
