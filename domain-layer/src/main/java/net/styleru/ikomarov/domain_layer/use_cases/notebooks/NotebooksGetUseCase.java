package net.styleru.ikomarov.domain_layer.use_cases.notebooks;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;
import net.styleru.ikomarov.domain_layer.repository.notebooks.INotebooksRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 09.03.17.
 */

public class NotebooksGetUseCase extends UseCase {

    @NonNull
    private final INotebooksRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public NotebooksGetUseCase(@NonNull INotebooksRepository repository,
                               @NonNull Scheduler executeScheduler,
                               @NonNull Scheduler postScheduler) {
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<List<ClassNotebookDTO>, List<T>> mapper, Callbacks<T> callbacks, int offset, int limit) {
        execute(repository.getList(offset, limit), mapper, callbacks);
    }

    public <T> void execute(Function<List<ClassNotebookDTO>, List<T>> mapper, Callbacks<T> callbacks, int offset, int limit, String nameSubstring) {
        execute(repository.getList(offset, limit, nameSubstring), mapper, callbacks);
    }

    private <T> void execute(Observable<List<ClassNotebookDTO>> upstream, Function<List<ClassNotebookDTO>, List<T>> mapper, Callbacks<T> callbacks) {
        upstream.subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createNotebooksObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createNotebooksObserver(final Callbacks<T> callbacks) {
        DisposableObserver<List<T>> notebooksObserver = new DisposableObserver<List<T>>() {
            @Override
            public void onNext(List<T> value) {
                if(!isDisposed()) {
                    callbacks.onNotebooksLoaded(value);
                }
            }

            @Override
            public void onError(Throwable e) {
                if(!isDisposed()) {
                    callbacks.onNotebooksLoadingFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(notebooksObserver);

        return notebooksObserver;
    }

    public interface Callbacks<T> {

        void onNotebooksLoaded(List<T> notebooks);

        void onNotebooksLoadingFailed(ExceptionBundle error);
    }
}
