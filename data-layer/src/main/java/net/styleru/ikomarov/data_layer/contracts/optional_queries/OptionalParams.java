package net.styleru.ikomarov.data_layer.contracts.optional_queries;

/**
 * Created by i_komarov on 22.02.17.
 */

public class OptionalParams {

    public static final String TOP = "$top";

    public static final String SKIP_TOKEN = "$skiptoken";

    public static final String FILTER = "$filter";

    public static final String ORDERBY = "$orderby";
}
