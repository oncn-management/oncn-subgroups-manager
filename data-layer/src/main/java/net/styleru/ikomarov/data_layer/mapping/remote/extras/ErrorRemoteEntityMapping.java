package net.styleru.ikomarov.data_layer.mapping.remote.extras;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.entities.remote.extras.OneNoteError;

import java.util.List;

/**
 * Created by i_komarov on 20.02.17.
 */

public class ErrorRemoteEntityMapping implements IRemoteMapping<OneNoteError> {

    @NonNull
    public static final String FIELD_ERROR = "error";

    @NonNull
    public static final String FIELD_CODE = "code";

    @NonNull
    public static final String FIELD_MESSAGE = "message";

    @NonNull
    public static final String FIELD_API_URL = "@api.url";

    @NonNull
    public static final String FIELD_WARNINGS = "@api.diagnostics";

    private final Gson gson;

    public ErrorRemoteEntityMapping() {
        this.gson = new GsonBuilder()
                .registerTypeAdapter(OneNoteError.class, new OneNoteError.Deserializer())
                .create();
    }


    @Override
    public OneNoteError fromJson(JsonObject json) {
        return gson.fromJson(json, OneNoteError.class);
    }

    @Override
    public List<OneNoteError> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<OneNoteError>>(){}.getType());
    }

    @Override
    public String toJson(OneNoteError entity) {
        return gson.toJson(entity, OneNoteError.class);
    }

    @Override
    public String toJsonArray(List<OneNoteError> entities) {
        return gson.toJson(entities, new TypeToken<List<OneNoteError>>(){}.getType());
    }
}
