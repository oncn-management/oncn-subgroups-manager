package net.styleru.ikomarov.domain_layer.repository.students;

import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;

import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 19.03.17.
 */

public interface IStudentsRepository {

    Observable<OperationStatus> create(UUID operaionId, String notebookId, PrincipalObjectDTO.SimpleBuilder builder);

    Observable<List<PrincipalObjectDTO>> read(String notebookId, int offset, int limit);

    Observable<List<PrincipalObjectDTO>> read(String notebookId, int offset, int limit, String nameSubstring);

    Observable<OperationStatus> delete(UUID operationId, String notebookId, String studentId);
}
