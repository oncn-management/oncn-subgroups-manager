package net.styleru.ikomarov.presentation_layer.presenter.students_list;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.events.IEventsLogger;
import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;
import net.styleru.ikomarov.domain_layer.use_cases.students.StudentDeleteUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.students.StudentsGetUseCase;
import net.styleru.ikomarov.presentation_layer.mapping.students.StudentMapper;
import net.styleru.ikomarov.presentation_layer.mapping.students.StudentsMapper;
import net.styleru.ikomarov.presentation_layer.presenter.base.Presenter;
import net.styleru.ikomarov.presentation_layer.presenter.notebooks_list.NotebooksListPresenter;
import net.styleru.ikomarov.domain_layer.contracts.errors.IErrorCodeTranslator;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.students_list.IStudentsListView;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsListPresenter extends Presenter<IStudentsListView> implements StudentsGetUseCase.Callbacks<StudentViewObject>, StudentDeleteUseCase.Callbacks {

    @NonNull
    private final StudentsGetUseCase studentsGet;

    @NonNull
    private final StudentDeleteUseCase studentDelete;

    @NonNull
    private final IErrorCodeTranslator oneNoteCodeTranslator;

    @NonNull
    private final IEventsLogger loggingManager;

    @NonNull
    private final Function<PrincipalObjectDTO, StudentViewObject> singleMapper;

    @NonNull
    private final Function<List<PrincipalObjectDTO>, List<StudentViewObject>> listMapper;

    @NonNull
    private final String notebookId;

    private Map<UUID, String> deleteResults;

    private StudentsListPresenter(@NonNull StudentsGetUseCase studentsGet,
                                  @NonNull StudentDeleteUseCase studentDelete,
                                  @NonNull IErrorCodeTranslator oneNoteCodeTranslator,
                                  @NonNull IEventsLogger loggingManager,
                                  @NonNull String notebookId) {

        this.studentsGet = studentsGet;
        this.studentDelete = studentDelete;
        this.oneNoteCodeTranslator = oneNoteCodeTranslator;
        this.loggingManager = loggingManager;
        this.notebookId = notebookId;
        this.singleMapper = new StudentMapper();
        this.listMapper = new StudentsMapper();
        this.deleteResults = new HashMap<>();
    }

    @Override
    protected void onViewAttached() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {
        studentsGet.dispose();
    }

    public void deleteStudent(String studentId) {
        deleteResults.put(
                studentDelete.execute(this, notebookId, studentId),
                studentId
        );
    }

    @Override
    public void onStudentDeleted(UUID operationId) {
        postAction((view) -> {
            if(deleteResults.get(operationId) != null) {
                view.deleteStudent(deleteResults.get(operationId));
                deleteResults.remove(operationId);
            }
        });
    }

    @Override
    public void onStudentDeletionDelayed(UUID operationId) {
        postAction(IStudentsListView::showOperationDelayedDialog);
    }

    public void loadStudents(int offset, int limit) {
        studentsGet.execute(listMapper, this, notebookId, offset, limit);
    }

    public void loadStudents(int offset, int limit, String nameSubstring) {
        studentsGet.execute(listMapper, this, notebookId, offset, limit, nameSubstring);
    }

    @Override
    public void onStudentDeletionFailed(UUID operationId, ExceptionBundle error) {
        handleException(
                error,
                StudentsListPresenter.class.getSimpleName(),
                "onStudentDeletionFailed"
        );
    }

    @Override
    public void onStudentsLoaded(List<StudentViewObject> students) {
        postAction((view) -> {
            view.addStudents(students);
        });
    }

    @Override
    public void onStudentsLoadingFailed(ExceptionBundle error) {
        handleException(
                error,
                StudentsListPresenter.class.getSimpleName(),
                "onStudentsLoadingFailed",
                ExceptionBundle.Reason.ONE_NOTE_ERROR,
                ExceptionBundle.Reason.NETWORK_UNAVAILABLE,
                ExceptionBundle.Reason.NETWORK_SSL_HANDSHAKE_FAILED,
                ExceptionBundle.Reason.NETWORK_UNKNOWN
        );
    }

    private void handleException(ExceptionBundle error, String clazz, String method, ExceptionBundle.Reason... ignoreReasons) {
        if(-1 == Arrays.binarySearch(ignoreReasons, error.getReason())) {
            return;
        }

        postAction((view) -> {
            switch (error.getReason()) {
                case NO_CACHED_RESOURCES: {
                    view.showNoCachedDataPlug();
                    break;
                }
                case NO_RESOURCES: {
                    view.showNoDataPlug();
                    break;
                }
                case NO_SESSION: {
                    view.showAuthorizationDialog();
                    break;
                }
                case ONE_NOTE_ERROR: {
                    final int code = error.getIntExtra(ExceptionBundle.OneNoteErrorContract.KEY_CODE);
                    final String message = error.getStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_MESSAGE);
                    view.showError(oneNoteCodeTranslator.getLocalizedMessage(code, message));
                    break;
                }
                case NETWORK_UNAVAILABLE: {
                    view.showConnectionFailedIndicator();
                    break;
                }
                default: {
                    if(error.getReason().isInternalError()) {
                        loggingManager.logEvent(clazz, method, error.getReason(), error.getThrowable(ExceptionBundle.InternalErrorContract.KEY_THROWABLE));
                    } else {
                        loggingManager.logEvent(clazz, method, error.getReason());
                    }
                }
            }
        });
    }

    public static final class Factory implements RetainContainer.Factory<StudentsListPresenter> {

        @NonNull
        private final StudentsGetUseCase studentsGet;

        @NonNull
        private final StudentDeleteUseCase studentDelete;

        @NonNull
        private final IErrorCodeTranslator oneNoteCodeTranslator;

        @NonNull
        private final IEventsLogger loggingManager;

        @NonNull
        private final String notebookId;

        public Factory(@NonNull StudentsGetUseCase studentsGet,
                       @NonNull StudentDeleteUseCase studentDelete,
                       @NonNull IErrorCodeTranslator oneNoteCodeTranslator,
                       @NonNull IEventsLogger loggingManager,
                       @NonNull String notebookId) {

            this.studentsGet = studentsGet;
            this.studentDelete = studentDelete;
            this.oneNoteCodeTranslator = oneNoteCodeTranslator;
            this.loggingManager = loggingManager;
            this.notebookId = notebookId;
        }

        @Override
        public StudentsListPresenter create() {
            return new StudentsListPresenter(studentsGet, studentDelete, oneNoteCodeTranslator, loggingManager, notebookId);
        }
    }
}
