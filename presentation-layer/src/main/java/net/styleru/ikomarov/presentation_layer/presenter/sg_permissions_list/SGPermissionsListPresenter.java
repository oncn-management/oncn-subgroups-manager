package net.styleru.ikomarov.presentation_layer.presenter.sg_permissions_list;

import android.support.annotation.NonNull;
import android.util.Log;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.events.IEventsLogger;
import net.styleru.ikomarov.domain_layer.contracts.errors.IErrorCodeTranslator;
import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;
import net.styleru.ikomarov.domain_layer.use_cases.permissions.SGPermissionsBatchUpdateUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.permissions.SGPermissionsGetUseCase;
import net.styleru.ikomarov.presentation_layer.mapping.sg_permissions.SGPermissionMapper;
import net.styleru.ikomarov.presentation_layer.mapping.sg_permissions.SGPermissionsMapper;
import net.styleru.ikomarov.presentation_layer.presenter.base.Presenter;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.sg_permissions_list.ISGPermissionsListView;
import net.styleru.ikomarov.presentation_layer.view.sg_permissions_list.SGPermissionViewObject;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 02.04.17.
 */

public class SGPermissionsListPresenter extends Presenter<ISGPermissionsListView> implements SGPermissionsGetUseCase.Callbacks<SGPermissionViewObject>, SGPermissionsBatchUpdateUseCase.Callbacks {

    @NonNull
    private final SGPermissionsGetUseCase permissionsGet;

    @NonNull
    private final SGPermissionsBatchUpdateUseCase permissionsBatchUpdate;

    @NonNull
    private final IErrorCodeTranslator oneNoteCodeTranslator;

    @NonNull
    private final IEventsLogger loggingManager;

    @NonNull
    private final Function<PermissionDTO, SGPermissionViewObject> singleMapper;

    @NonNull
    private final Function<List<PermissionDTO>, List<SGPermissionViewObject>> listMapper;

    @NonNull
    private final String sectionGroupId;

    private SGPermissionsListPresenter(@NonNull SGPermissionsGetUseCase permissionsGet,
                                       @NonNull SGPermissionsBatchUpdateUseCase permissionsBatchUpdate,
                                      @NonNull IErrorCodeTranslator oneNoteCodeTranslator,
                                      @NonNull IEventsLogger loggingManager,
                                      @NonNull String sectionGroupId) {

        this.permissionsGet = permissionsGet;
        this.permissionsBatchUpdate = permissionsBatchUpdate;
        this.oneNoteCodeTranslator = oneNoteCodeTranslator;
        this.loggingManager = loggingManager;
        this.sectionGroupId = sectionGroupId;
        this.singleMapper = new SGPermissionMapper();
        this.listMapper = new SGPermissionsMapper();
    }

    @Override
    protected void onViewAttached() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {
        permissionsGet.dispose();
    }

    public void loadPermissions() {
        permissionsGet.execute(listMapper, this, sectionGroupId);
    }

    public void batchUpdatePermissions(List<SGPermissionsBatchUpdateUseCase.Permission> changes) {
        postAction((view) -> {
            view.applySyncState(ISGPermissionsListView.SyncState.SYNC_PROGRESS);
        });
        permissionsBatchUpdate.execute(UUID.randomUUID(), this, sectionGroupId, changes);
    }

    @Override
    public void onSGPermissionsLoaded(List<SGPermissionViewObject> permissions) {
        postAction((view) -> view.addPermissions(permissions));
    }

    @Override
    public void onSGPermissionsLoadingFailed(ExceptionBundle error) {
        handleException(
                error,
                SGPermissionsListPresenter.class.getSimpleName(),
                "onSGPermissionsLoadingFailed",
                ExceptionBundle.Reason.NETWORK_SSL_HANDSHAKE_FAILED,
                ExceptionBundle.Reason.NETWORK_UNKNOWN
        );
    }

    @Override
    public void onSGPermissionsUpdated(UUID operationId) {
        postAction((view) -> {
            view.applySyncState(ISGPermissionsListView.SyncState.SYNC_SUCCESS);
        });
    }

    @Override
    public void onSGPermissionsUpdateConflict(UUID operationId) {
        postAction((view) -> {
            view.applySyncState(ISGPermissionsListView.SyncState.SYNC_FAILURE);
            view.showBatchUpdateConflictNotification();
        });
    }

    @Override
    public void onSGPermissionsUpdateFailed(UUID operationId, ExceptionBundle error) {
        postAction((view) -> {
            view.applySyncState(ISGPermissionsListView.SyncState.SYNC_FAILURE);
        });
        handleException(
                error,
                SGPermissionsListPresenter.class.getSimpleName(),
                "onSGPermissionsLoadingFailed",
                ExceptionBundle.Reason.NETWORK_SSL_HANDSHAKE_FAILED,
                ExceptionBundle.Reason.NETWORK_UNKNOWN
        );
    }

    @Override
    public void onSGPermissionsUpdateDelayed(UUID operationId) {
        postAction((view) -> {
            view.applySyncState(ISGPermissionsListView.SyncState.SYNC_DELAY);
        });
    }

    private void handleException(ExceptionBundle error, String clazz, String method, ExceptionBundle.Reason... ignoreReasons) {
        if(-1 == Arrays.binarySearch(ignoreReasons, error.getReason())) {
            return;
        }

        postAction((view) -> {
            switch (error.getReason()) {
                case NO_CACHED_RESOURCES: {
                    view.showNoCachedDataPlug();
                    break;
                }
                case NO_RESOURCES: {
                    view.showNoDataPlug();
                    break;
                }
                case NO_SESSION: {
                    view.showAuthorizationDialog();
                    break;
                }
                case ONE_NOTE_ERROR: {
                    final int code = error.getIntExtra(ExceptionBundle.OneNoteErrorContract.KEY_CODE);
                    final String message = error.getStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_MESSAGE);
                    view.showError(oneNoteCodeTranslator.getLocalizedMessage(code, message));
                    break;
                }
                case NETWORK_UNAVAILABLE: {
                    view.showConnectionFailedIndicator();
                    break;
                }
                default: {
                    if(error.getReason().isInternalError()) {
                        loggingManager.logEvent(clazz, method, error.getReason(), error.getThrowable(ExceptionBundle.InternalErrorContract.KEY_THROWABLE));
                    } else {
                        loggingManager.logEvent(clazz, method, error.getReason());
                    }
                }
            }
        });
    }

    public static final class Factory implements RetainContainer.Factory<SGPermissionsListPresenter> {

        @NonNull
        private final SGPermissionsGetUseCase permissionsGet;

        @NonNull
        private final SGPermissionsBatchUpdateUseCase permissionsBatchUpdate;

        @NonNull
        private final IErrorCodeTranslator translator;

        @NonNull
        private final IEventsLogger logger;

        @NonNull
        private final String notebookId;

        public Factory(@NonNull SGPermissionsGetUseCase permissionsGet,
                       @NonNull SGPermissionsBatchUpdateUseCase permissionsBatchUpdate,
                       @NonNull IErrorCodeTranslator translator,
                       @NonNull IEventsLogger logger,
                       @NonNull String notebookId) {

            this.permissionsGet = permissionsGet;
            this.permissionsBatchUpdate = permissionsBatchUpdate;
            this.translator = translator;
            this.logger = logger;
            this.notebookId = notebookId;
        }

        @Override
        public SGPermissionsListPresenter create() {
            return new SGPermissionsListPresenter(permissionsGet, permissionsBatchUpdate, translator, logger, notebookId);
        }
    }
}
