package net.styleru.ikomarov.domain_layer.service.base;

import android.os.Parcel;
import android.os.Parcelable;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 06.04.17.
 */

public class ServiceException implements Parcelable {

    private final ExceptionBundle error;

    ServiceException(Parcel in) {
        error = new ExceptionBundle(ExceptionBundle.Reason.valueOf(in.readString()), in.readBundle(getClass().getClassLoader()));
    }

    public ServiceException(ExceptionBundle error) {
        this.error = error;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(error.getReason().name());
        dest.writeBundle(error.getExtras());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ServiceException> CREATOR = new Creator<ServiceException>() {
        @Override
        public ServiceException createFromParcel(Parcel in) {
            return new ServiceException(in);
        }

        @Override
        public ServiceException[] newArray(int size) {
            return new ServiceException[size];
        }
    };
}
