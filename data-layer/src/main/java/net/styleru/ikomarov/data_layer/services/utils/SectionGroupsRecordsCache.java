package net.styleru.ikomarov.data_layer.services.utils;

import android.support.annotation.NonNull;
import android.util.Pair;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsLocalStorageService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executors;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsRecordsCache implements IMemCache {

    private final Object lock = new Object();

    private final String id;

    private final ISectionGroupsLocalStorageService localService;

    private final Map<String, Date> timeStamps;

    private final SimpleDateFormat contractDateFormat;

    private volatile boolean isCold = false;

    private Disposable initializationDisposable;

    public SectionGroupsRecordsCache(String id, ISectionGroupsLocalStorageService localService) {
        this.id = id;
        this.localService = localService;
        this.timeStamps = new HashMap<>();
        this.contractDateFormat = new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault());
        this.safeInitialize();
    }

    @Override
    public Date get(@NonNull String id) {
        synchronized (lock) {
            return this.timeStamps.get(id);
        }
    }

    @Override
    public long size() {
        synchronized (lock) {
            return timeStamps.size();
        }
    }

    @Override
    public Date lastModified() {
        Date lastModified = null;
        synchronized (lock) {
            for (Date date : timeStamps.values()) {
                if (lastModified == null) {
                    lastModified = date;
                } else if (lastModified.compareTo(date) < 0) {
                    lastModified = date;
                }
            }

            return lastModified;
        }
    }

    @Override
    public void onModified(@NonNull String id, @NonNull String timeStamp) {
        try {
            Date actual = contractDateFormat.parse(timeStamp);
            synchronized (lock) {
                Date existing = this.timeStamps.get(id);
                if(existing == null || existing.compareTo(actual) < 0) {
                    this.timeStamps.put(id, actual);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeleted(@NonNull String id) {
        synchronized (lock) {
            this.timeStamps.remove(id);
        }
    }

    @Override
    public void invalidate() {
        synchronized (lock) {
            if(initializationDisposable != null && !initializationDisposable.isDisposed()) {
                initializationDisposable.dispose();
            }
            this.timeStamps.clear();
        }
    }

    @Override
    public boolean isCold() {
        return isCold;
    }

    private void safeInitialize() {
        isCold = true;
        initializationDisposable = localService.readAll(id).flatMap(Observable::fromIterable)
                .map((entity) -> new Pair<>(entity.getId(), contractDateFormat.parse(entity.getLastModifiedTime())))
                .subscribeOn(Schedulers.from(Executors.newSingleThreadExecutor()))
                .subscribe(
                        (data) -> {
                            synchronized (lock) {
                                Date existing = timeStamps.get(data.first);
                                if(existing == null || existing.compareTo(data.second) < 0) {
                                    timeStamps.put(data.first, data.second);
                                }
                            }
                        },
                        Throwable::printStackTrace,
                        () -> {
                            isCold = false;
                            System.out.println(String.format("Section groups cache claster node %1$s is hot now", id));
                        }
                );
    }
}
