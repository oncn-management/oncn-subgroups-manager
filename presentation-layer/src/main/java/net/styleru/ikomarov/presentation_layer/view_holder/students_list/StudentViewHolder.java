package net.styleru.ikomarov.presentation_layer.view_holder.students_list;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener, View.OnClickListener {

    private AppCompatTextView textAvatarHolder;
    private AppCompatTextView nameHolder;
    private AppCompatTextView emailHolder;
    private AppCompatImageButton actionOptionsHolder;

    private PopupMenu menu;

    private int position;
    private StudentViewObject item;
    private InteractionCallbacks callbacks;

    public StudentViewHolder(View itemView) {
        super(itemView);

        textAvatarHolder    = (AppCompatTextView) itemView.findViewById(R.id.list_item_student_short_name);
        nameHolder          = (AppCompatTextView) itemView.findViewById(R.id.list_item_student_display_name);
        emailHolder         = (AppCompatTextView) itemView.findViewById(R.id.list_item_student_email);
        actionOptionsHolder = (AppCompatImageButton) itemView.findViewById(R.id.list_item_student_options_button);

        menu = new PopupMenu(itemView.getContext(), actionOptionsHolder);
        menu.getMenuInflater().inflate(R.menu.menu_list_item_student, menu.getMenu());

        actionOptionsHolder.setOnClickListener((view) -> {
            prepareOptionsMenu(menu.getMenu());
            menu.show();
        });
        menu.setOnMenuItemClickListener(this);
        itemView.setOnClickListener(this);
    }

    public void bind(int position, StudentViewObject item, InteractionCallbacks callbacks) {
        this.position = position;
        this.item = item;
        this.callbacks = callbacks;

        textAvatarHolder.setText(item.getName() != null ? formatShortName(item.getName()) : "");
        nameHolder.setText(item.getName() != null ? item.getName() : "");
        emailHolder.setText(item.getEmail() != null ? item.getEmail() : "");
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch(menuItem.getItemId()) {
            case R.id.action_delete : {
                postAction(position, item, menuItem);
                return true;
            }
            case R.id.action_edit_permissions : {
                postAction(position, item, menuItem);
                return true;
            }
            case R.id.action_open_in_browser : {
                postAction(position, item, menuItem);
                return true;
            }
            default: {
                return false;
            }
        }
    }

    @Override
    public void onClick(View v) {
        postClick(position, item);
    }

    private void postClick(int position, StudentViewObject item) {
        if(callbacks != null) {
            callbacks.onStudentClicked(position, item);
        }
    }

    private void postAction(int position, StudentViewObject item, MenuItem menuItem) {
        if(callbacks != null) {
            callbacks.onStudentAction(position, item, menuItem);
        }
    }

    private void prepareOptionsMenu(Menu menu) {
        //TODO: complete according to real ViewObject conditions
    }

    private String formatShortName(String name) {
        String[] parts = name.split(" ");
        String firstName = "";
        String lastName = "";
        if(parts.length > 0 && parts[0].length() > 0) {
            firstName = parts[0].substring(0, 1);
        }
        if(parts.length > 1 && parts[1].length() > 0) {
            lastName = parts[1].substring(0, 1);
        }

        return firstName + lastName;
    }

    public interface InteractionCallbacks {

        void onStudentClicked(int position, StudentViewObject item);

        void onStudentAction(int position, StudentViewObject item, MenuItem menuItem);
    }
}
