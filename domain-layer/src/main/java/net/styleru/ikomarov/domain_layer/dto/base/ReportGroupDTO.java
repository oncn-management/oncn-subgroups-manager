package net.styleru.ikomarov.domain_layer.dto.base;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 17.04.17.
 */

//A report group, that both can be nested in report and in other report group
public class ReportGroupDTO<T extends ReportSubgroupDTO> extends ReportSubgroupDTO {

    @NonNull
    private final String id;

    @NonNull
    private final List<T> subgroups;

    public ReportGroupDTO(@NonNull String id, @NonNull String name, @NonNull List<T> subgroups) {
        super(name);
        this.id = id;
        this.subgroups = subgroups;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public List<T> getSubgroups() {
        return subgroups;
    }

    public static final class Builder<S extends ReportSubgroupDTO> {

        @NonNull
        private final String id;

        @NonNull
        private final String name;

        @NonNull
        private final List<S> subgroups;

        public Builder(@NonNull String id, @NonNull String name) {
            this.id = id;
            this.name = name;
            this.subgroups = new ArrayList<>();
        }

        public Builder addSubgroup(S subgroup) {
            this.subgroups.add(subgroup);
            return this;
        }

        public ReportGroupDTO<S> build() {
            return new ReportGroupDTO<>(id, name, subgroups);
        }
    }
}
