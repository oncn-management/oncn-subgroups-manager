package net.styleru.ikomarov.data_layer.common;

/**
 * Created by i_komarov on 18.02.17.
 */

public class Endpoints {

    public static final String ONE_NOTE_BASE_URL = "https://www.onenote.com";

    public static final String ENDPOINT_API = "/api";

    public static final String ENDPOINT_V1 = "/v1.0";

    public static final String ENDPOINT_USERS = "/users";

    public static final String ENDPOINT_ME = "/me";

    public static final String ENDPOINT_NOTES = "/notes";

    public static final String ENDPOINT_PERMISSIONS = "/permissions";

    public static final String ENDPOINT_NOTEBOOKS = "/notebooks";

    public static final String ENDPOINT_SECTIONGROUPS = "/sectionGroups";

    public static final String ENDPOINT_SECTIONS = "/sections";

    public static final String ENDPOINT_CLASS_NOTEBOOKS = "/classNotebooks";

    public static final String ENDPOINT_COPY_SECTIONS_TO_CONTENT_LIBRARY = "/copySectionsToContentLibrary";

    public static final String ENDPOINT_STUDENTS = "/students";

    public static final String ENDPOINT_TEACHERS = "/teachers";
}
