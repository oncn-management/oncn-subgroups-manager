package net.styleru.ikomarov.data_layer.services.users;

import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;

import net.styleru.ikomarov.data_layer.entities.local.users.UserWithBusinessPhonesLocalEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 01.03.17.
 */

public interface IUsersLocalStorageService {

    Observable<PutResult> create(UserWithBusinessPhonesLocalEntity entity);

    Observable<UserWithBusinessPhonesLocalEntity> read(String id);

    Observable<List<UserWithBusinessPhonesLocalEntity>> read(int offset, int limit);

    Observable<List<UserWithBusinessPhonesLocalEntity>> read(int offset, int limit, String substring);

    Observable<DeleteResult> delete(String id);
}
