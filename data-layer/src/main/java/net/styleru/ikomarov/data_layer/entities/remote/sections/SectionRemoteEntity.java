package net.styleru.ikomarov.data_layer.entities.remote.sections;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

import static net.styleru.ikomarov.data_layer.mapping.remote.sections.SectionsRemoteEntityMapping.*;

/**
 * Created by i_komarov on 14.02.17.
 */

public class SectionRemoteEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_NAME)
    private String name;
    @SerializedName(FIELD_SELF)
    private String self;
    @SerializedName(FIELD_CREATED_BY)
    private String createdBy;
    @SerializedName(FIELD_CREATED_TIME)
    private Date createdTime;
    @SerializedName(FIELD_LAST_MODIFIED_BY)
    private String lastModifiedBy;
    @SerializedName(FIELD_LAST_MODIFIED_TIME)
    private Date lastModifiedTime;

    private SectionRemoteEntity() {

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSelf() {
        return self;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    @Override
    public String toString() {
        return "SectionRemoteEntity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", self='" + self + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdTime=" + createdTime +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", lastModifiedTime=" + lastModifiedTime +
                '}';
    }
}
