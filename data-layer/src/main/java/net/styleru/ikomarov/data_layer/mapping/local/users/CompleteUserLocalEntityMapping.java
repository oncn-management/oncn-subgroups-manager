package net.styleru.ikomarov.data_layer.mapping.local.users;

import android.database.Cursor;

import net.styleru.ikomarov.data_layer.entities.local.users.UserBusinessPhoneLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.entities.local.users.UserWithBusinessPhonesLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static net.styleru.ikomarov.data_layer.mapping.local.users.queries.UserQueries.Contract.*;

/**
 * Created by i_komarov on 01.03.17.
 */

public class CompleteUserLocalEntityMapping implements ILocalMapping<UserWithBusinessPhonesLocalEntity> {

    public CompleteUserLocalEntityMapping() {

    }

    @Override
    public UserWithBusinessPhonesLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException, ParseException {
        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            String userId;

            UserLocalEntity user = UserLocalEntity.newInstance(
                    userId = cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_ID)),
                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_DISPLAY_NAME)),
                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_GIVEN_NAME)),
                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_JOB_TITLE)),
                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_MAIL)),
                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_MOBILE_PHONE)),
                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_OFFICE_LOCATION)),
                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_PREFERRED_LANGUAGE)),
                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_LAST_NAME)),
                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_PRINCIPAL_NAME))
            );

            List<UserBusinessPhoneLocalEntity> phones = new ArrayList<>(cursor.getCount());

            do {
                phones.add(
                        UserBusinessPhoneLocalEntity.newInstance(
                                cursor.getLong(indexCache.indexFor(cursor, COLUMN_BUSINESS_PHONE_ID)),
                                userId,
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_BUSINESS_PHONE_VALUE))
                        )
                );
            } while(cursor.moveToNext());


            return new UserWithBusinessPhonesLocalEntity(user, phones);
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<UserWithBusinessPhonesLocalEntity> listFromCursor(Cursor cursor) throws ParseException {
        Map<String, UserWithBusinessPhonesLocalEntity.Builder> entitiesMap = new HashMap<>();
        LinkedList<String> originalOrder = new LinkedList<>();

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                UserWithBusinessPhonesLocalEntity.Builder builder;

                String userId;
                builder = entitiesMap.get(userId = cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_ID)));

                if(builder == null) {
                    builder = new UserWithBusinessPhonesLocalEntity.Builder(
                            UserLocalEntity.newInstance(
                                    userId,
                                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_DISPLAY_NAME)),
                                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_GIVEN_NAME)),
                                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_JOB_TITLE)),
                                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_MAIL)),
                                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_MOBILE_PHONE)),
                                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_OFFICE_LOCATION)),
                                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_PREFERRED_LANGUAGE)),
                                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_LAST_NAME)),
                                    cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_PRINCIPAL_NAME))
                            )
                    );

                    if(!originalOrder.contains(userId)) {
                        originalOrder.addLast(userId);
                    }
                }

                String phoneValue;
                if(null != (phoneValue = cursor.getString(indexCache.indexFor(cursor, COLUMN_BUSINESS_PHONE_VALUE)))) {
                    builder.addPhone(
                            UserBusinessPhoneLocalEntity.newInstance(
                                    cursor.getLong(indexCache.indexFor(cursor, COLUMN_BUSINESS_PHONE_ID)),
                                    userId,
                                    phoneValue
                            )
                    );
                }

                entitiesMap.put(userId, builder);

            } while(cursor.moveToNext());
        }

        List<UserWithBusinessPhonesLocalEntity> entities = new ArrayList<>();

        if(originalOrder.size() != 0) {
            for (String id : originalOrder) {
                entities.add(entitiesMap.get(id).build());
            }
        }

        return entities;
    }
}
