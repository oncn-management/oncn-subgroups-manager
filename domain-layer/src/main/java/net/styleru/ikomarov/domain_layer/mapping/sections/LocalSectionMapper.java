package net.styleru.ikomarov.domain_layer.mapping.sections;

import net.styleru.ikomarov.data_layer.entities.local.sections.SectionLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.SectionDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class LocalSectionMapper implements Function<SectionLocalEntity, SectionDTO> {

    @Override
    public SectionDTO apply(SectionLocalEntity entity) throws Exception {
        return SectionDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getCreatedBy(),
                entity.getCreatedTime(),
                entity.getLastModifiedBy(),
                entity.getLastModifiedTime()
        );
    }
}
