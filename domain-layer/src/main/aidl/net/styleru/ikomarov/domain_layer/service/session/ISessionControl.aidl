// ISessionControl.aidl
package net.styleru.ikomarov.domain_layer.service.session;

// Declare any non-default types here with import statements

import net.styleru.ikomarov.domain_layer.service.session.ISessionCallbacks;
import net.styleru.ikomarov.domain_layer.service.session.SessionStatus;

interface ISessionControl {

    SessionStatus getCurrentSessionStatus();

    String blockingGetSessionToken(in String resourceType);

    String blockingRefreshAndGetSessionToken(in String resourceType);

    void authenticate(in String code);

    void deauthenticate();

    void registerCallback(in ISessionCallbacks callback);

    void unregisterCallback(in ISessionCallbacks callback);
}
