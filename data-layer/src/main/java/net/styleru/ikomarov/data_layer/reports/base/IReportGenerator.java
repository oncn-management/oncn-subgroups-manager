package net.styleru.ikomarov.data_layer.reports.base;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.report.base.ReportEntity;
import net.styleru.ikomarov.data_layer.entities.report.base.ReportGroupEntity;
import net.styleru.ikomarov.data_layer.entities.report.base.ReportSubgroupEntity;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 13.04.17.
 */

public interface IReportGenerator<S extends ReportSubgroupEntity> {

    Observable<ReportDataResponse> generate(@NonNull ReportEntity<S, ReportGroupEntity<S>> report);
}
