package net.styleru.ikomarov.presentation_layer.adapter.sg_permissions_list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.presentation_layer.R;

/**
 * Created by i_komarov on 22.03.17.
 */

public class PermissionsSpinnerAdapter extends ArrayAdapter<String> {

    public PermissionsSpinnerAdapter(@NonNull Context context) {
        super(context,
              R.layout.dropdown_item_permission,
              new String[]{
                      context.getResources().getString(R.string.permission_owner_description),
                      context.getResources().getString(R.string.permission_writer_description),
                      context.getResources().getString(R.string.permission_reader_description),
                      context.getResources().getString(R.string.permission_none)
              }
        );
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dropdown_item_permission, parent, false);
        }

        ((AppCompatTextView) convertView.findViewById(R.id.list_item_permission_role)).setText(getItem(position));

        return convertView;
    }

    public UserRole getEnumBySelection(int selection) {
        switch(selection) {
            case 0: {
                return UserRole.OWNER;
            }
            case 1: {
                return UserRole.CONTRIBUTOR;
            }
            case 2: {
                return UserRole.READER;
            }
            case 3: {
                return UserRole.NONE;
            }
            default: {
                return UserRole.NONE;
            }
        }
    }

    public int getSelectionByEnum(UserRole role) {
        switch(role) {
            case NONE: {
                return 3;
            }
            case READER: {
                return 2;
            }
            case CONTRIBUTOR: {
                return 1;
            }
            case OWNER: {
                return 0;
            }
            default: {
                return -1;
            }
        }
    }
}
