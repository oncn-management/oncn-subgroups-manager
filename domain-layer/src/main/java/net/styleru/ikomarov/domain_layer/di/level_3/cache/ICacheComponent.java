package net.styleru.ikomarov.domain_layer.di.level_3.cache;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;

/**
 * depends on {@link IEnvironmentComponent}
 * */
public interface ICacheComponent {

    @NonNull
    CacheStrategyModule strategy();
}
