package net.styleru.ikomarov.domain_layer.contracts.errors;

import net.styleru.ikomarov.domain_layer.R;

/**
 * Created by i_komarov on 21.03.17.
 */

public class OneNoteErrorCodeTranslator implements IErrorCodeTranslator {

    private final int CODE_ITEM_DOES_NOT_EXIST = 20102;
    private final int CODE_ITEM_WAS_DELETED = 20113;
    private final int CODE_ITEM_NAME_CONTAINS_INVALID_CHARS = 20115;
    private final int CODE_ITEM_NAME_ALREADY_EXISTS = 20117;
    private final int CODE_ENTITY_NAME_CONTAINS_INVALID_CHARS = 20153;
    private final int CODE_ITEM_NAME_STARTS_WITH_WHITESPACES = 20154;
    private final int CODE_ITEM_NAME_TOO_LONG = 20155;

    @Override
    public int getLocalizedMessage(int code, String message) {
        switch(code) {
            case CODE_ITEM_DOES_NOT_EXIST: {
                return R.string.error_item_does_not_exist;
            }
            case CODE_ITEM_WAS_DELETED: {
                return R.string.error_item_was_deleted;
            }
            case CODE_ITEM_NAME_CONTAINS_INVALID_CHARS: {
                return R.string.error_item_name_contains_invalid_chars;
            }
            case CODE_ITEM_NAME_ALREADY_EXISTS: {
                return R.string.error_item_name_already_exists;
            }
            case CODE_ENTITY_NAME_CONTAINS_INVALID_CHARS: {
                return R.string.error_item_name_contains_invalid_chars;
            }
            case CODE_ITEM_NAME_STARTS_WITH_WHITESPACES : {
                return R.string.error_item_name_starts_with_whitespaces;
            }
            case CODE_ITEM_NAME_TOO_LONG: {
                return R.string.error_item_name_too_long;
            }
            default: {
                return R.string.error_one_note_unknown;
            }
        }
    }
}
