package net.styleru.ikomarov.data_layer.reports.permission;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.util.Log;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ByteBuffer;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import net.styleru.ikomarov.data_layer.R;
import net.styleru.ikomarov.data_layer.entities.report.base.ReportEntity;
import net.styleru.ikomarov.data_layer.entities.report.base.ReportGroupEntity;
import net.styleru.ikomarov.data_layer.entities.report.base.ReportSubgroupEntity;
import net.styleru.ikomarov.data_layer.entities.report.permissions.PermissionReportSubgroupEntity;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.reports.base.IReportGenerator;
import net.styleru.ikomarov.data_layer.reports.base.PageHeaderFooter;
import net.styleru.ikomarov.data_layer.reports.base.ReportDataResponse;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 13.04.17.
 */

public class PermissionsReportGenerator implements IReportGenerator<PermissionReportSubgroupEntity> {

    private static final String OUTPUT_REPORTS_DIR = Environment.getExternalStorageDirectory().toString() + "/ONCNSubgroupsManager/";
    private static final String EXTENSION_PDF = ".pdf";

    private Font FONT_HEADER_FOOTER;
    private Font FONT_MAIN;

    @NonNull
    private final Context context;

    public PermissionsReportGenerator(@NonNull Context context) {
        this.context = context;
        try {
            InputStream is = context.getAssets().open("fonts/Roboto-Regular.ttf");
            byte[] buffer = new byte[1024 * 8];
            byte[] total = new byte[0];
            while(-1 != is.read(buffer)) {
                final int offset = total.length;
                total = Arrays.copyOf(total, total.length + buffer.length);
                int i = offset;
                for(byte b : buffer) {
                    total[i++] = b;
                }
            }
            is.close();

            BaseFont font = BaseFont.createFont("roboto-regular.ttf", BaseFont.IDENTITY_H, true, false, total, null);

            FONT_MAIN = new Font(font, 10);
            FONT_HEADER_FOOTER = new Font(font, 14);
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Observable<ReportDataResponse> generate(@NonNull ReportEntity<PermissionReportSubgroupEntity, ReportGroupEntity<PermissionReportSubgroupEntity>> report) {
        try {
            File dir = new File(OUTPUT_REPORTS_DIR);
            if(!dir.exists()) {
                dir.mkdirs();
            }
            String fullPath = OUTPUT_REPORTS_DIR + report.getName() + EXTENSION_PDF;
            File output = new File(fullPath);
            Document document = new Document(PageSize.A4);

            try {
                PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(output));
                PageHeaderFooter headerFooter = new PageHeaderFooter(FONT_HEADER_FOOTER, context.getResources().getString(R.string.report_creator));
                writer.setPageEvent(headerFooter);

                document.open();
                addMetaData(report.getName(), document);
                addContent(document, report.getGroups());

                document.close();
            } catch (DocumentException e) {
                e.printStackTrace();
                ExceptionBundle error = new ExceptionBundle(ExceptionBundle.Reason.PDF_DOCUMENT_FAILURE);
                error.addThrowable(ExceptionBundle.PdfErrorContract.KEY_THROWABLE, e);
                return Observable.error(error);
            } catch (IOException e) {
                e.printStackTrace();
                ExceptionBundle error = new ExceptionBundle(ExceptionBundle.Reason.PDF_DOCUMENT_FAILURE);
                error.addThrowable(ExceptionBundle.PdfErrorContract.KEY_THROWABLE, e);
                return Observable.error(error);
            }

            return Observable.just(new ReportDataResponse(ReportDataResponse.Status.SUCCESS, fullPath));
        } catch(Exception e) {
            e.printStackTrace();
            ExceptionBundle error = new ExceptionBundle(ExceptionBundle.Reason.PDF_DOCUMENT_FAILURE);
            error.addThrowable(ExceptionBundle.PdfErrorContract.KEY_THROWABLE, e);
            return Observable.error(error);
        }
    }

    private void addMetaData(String title, Document document) {
        document.addTitle(title);
        document.addSubject(context.getResources().getString(R.string.report_subject));
        document.addKeywords(context.getResources().getString(R.string.report_tags));
        document.addAuthor(context.getResources().getString(R.string.report_author));
        document.addCreator(context.getResources().getString(R.string.report_creator));
    }

    private void addContent(Document document, List<ReportGroupEntity<PermissionReportSubgroupEntity>> groups) throws DocumentException {
        Paragraph reportBody = new Paragraph();
        reportBody.setFont(FONT_MAIN);
        createTable(reportBody, groups);
        document.add(reportBody);
    }

    private void createTable(Paragraph reportBody, List<ReportGroupEntity<PermissionReportSubgroupEntity>> groups)
            throws BadElementException {

        float[] columnWidths = {1, 1, 1};
        PdfPTable table = new PdfPTable(columnWidths);

        table.setWidthPercentage(100);
        table.getDefaultCell().setUseAscender(true);

        PdfPCell cell = new PdfPCell(new Phrase(context.getResources().getString(R.string.report_group), FONT_HEADER_FOOTER));
        cell.setBackgroundColor(new GrayColor(0.75f));
        cell.setFixedHeight(30);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(context.getResources().getString(R.string.report_student), FONT_HEADER_FOOTER));
        cell.setBackgroundColor(new GrayColor(0.75f));
        cell.setFixedHeight(30);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase(context.getResources().getString(R.string.report_role), FONT_HEADER_FOOTER));
        cell.setBackgroundColor(new GrayColor(0.75f));
        cell.setFixedHeight(30);
        table.addCell(cell);

        for(ReportGroupEntity<PermissionReportSubgroupEntity> group : groups) {
            for(PermissionReportSubgroupEntity permission : group.getSubgroups()) {
                Log.d("PermissionsReportGenerator#createTable", "groupName: " + group.getName() + ",name: " + permission.getName() +  "permission: " + permission.getPermission());
                cell = new PdfPCell(new Phrase(group.getName(), FONT_MAIN));
                cell.setFixedHeight(28);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(permission.getName(), FONT_MAIN));
                cell.setFixedHeight(28);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(permission.getPermission(), FONT_MAIN));
                cell.setFixedHeight(28);
                table.addCell(cell);
            }
        }

        reportBody.add(table);
    }
}
