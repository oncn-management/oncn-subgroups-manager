package net.styleru.ikomarov.data_layer.mapping.remote.sections;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 14.02.17.
 */

public class SectionsRemoteEntityMapping {

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_SELF = "self";

    @NonNull
    public static final String FIELD_CREATED_BY = "createdBy";

    @NonNull
    public static final String FIELD_CREATED_TIME = "createdTime";

    @NonNull
    public static final String FIELD_LAST_MODIFIED_BY = "lastModifiedBy";

    @NonNull
    public static final String FIELD_LAST_MODIFIED_TIME = "lastModifiedTime";

    @NonNull
    public static final String FIELD_NAME = "name";

    @NonNull
    public static final String FIELD_IS_DEFAULT = "isDefault";

    private SectionsRemoteEntityMapping() {
        throw new IllegalStateException("No instances please");
    }
}
