package net.styleru.ikomarov.domain_layer.mapping.reports;

import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.report.base.ReportEntity;
import net.styleru.ikomarov.data_layer.entities.report.base.ReportGroupEntity;
import net.styleru.ikomarov.data_layer.entities.report.permissions.PermissionReportSubgroupEntity;
import net.styleru.ikomarov.domain_layer.dto.PermissionReportSubgroupDTO;
import net.styleru.ikomarov.domain_layer.dto.base.ReportDTO;
import net.styleru.ikomarov.domain_layer.dto.base.ReportGroupDTO;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 18.04.17.
 */

public class PermissionsReportMapper implements Function<ReportDTO<ReportGroupDTO<PermissionReportSubgroupDTO>>, ReportEntity<PermissionReportSubgroupEntity, ReportGroupEntity<PermissionReportSubgroupEntity>>> {

    @Override
    public ReportEntity<PermissionReportSubgroupEntity, ReportGroupEntity<PermissionReportSubgroupEntity>> apply(ReportDTO<ReportGroupDTO<PermissionReportSubgroupDTO>> reportGroupDTOReportDTO) throws Exception {
        return Observable.fromIterable(reportGroupDTOReportDTO.getGroups())
                //first - parse the permissions in each group dto one by one and accumulate them into group entities
                .flatMap(group -> Observable.fromIterable(group.getSubgroups())
                        .map(permission -> new PermissionReportSubgroupEntity(permission.getName(), permission.getPermission()))
                        .toList()
                        .map(permissions -> new ReportGroupEntity<PermissionReportSubgroupEntity>(group.getId(), group.getName(), permissions))
                        .toObservable())
                //second - accumulate the groups; as the accumulator here goes the builder for the top-level report hierarchy class, that accumulates groups
                .reduce(
                        new ReportEntity.Builder<PermissionReportSubgroupEntity, ReportGroupEntity<PermissionReportSubgroupEntity>>(reportGroupDTOReportDTO.getName()),
                        ReportEntity.Builder::<PermissionReportSubgroupEntity, ReportGroupEntity<PermissionReportSubgroupEntity>>addGroup
                )
                //third - build the report entity and pass it back
                .map(ReportEntity.Builder::<ReportEntity<PermissionReportSubgroupEntity, ReportGroupEntity<PermissionReportSubgroupEntity>>>build)
                .blockingGet();
    }
}
