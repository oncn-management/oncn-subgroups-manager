package net.styleru.ikomarov.domain_layer.di.endpoints.permissions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.permissions.section_groups.ISGPermissionsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.permissions.section_groups.SGPermissionsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsLocalStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.SectionGroupsLocalStorageService;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SGPermissionsLocalServiceModule {

    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent environment;

    @Nullable
    private volatile ISGPermissionsLocalStorageService localService;

    public SGPermissionsLocalServiceModule(@NonNull IEnvironmentComponent environment) {
        this.environment = environment;
    }

    @NonNull
    public ISGPermissionsLocalStorageService provideLocalService() {
        ISGPermissionsLocalStorageService localInstance = localService;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = localService;
                if(localInstance == null) {
                    localInstance = localService = SGPermissionsLocalStorageService.Factory.create(environment.database().provideDatabase());
                }
            }
        }

        return localInstance;
    }
}
