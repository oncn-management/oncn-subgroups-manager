package net.styleru.ikomarov.domain_layer.mapping.permissions;

import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 22.03.17.
 */

public class LocalPermissionsMapper implements Function<List<PermissionLocalEntity>, List<PermissionDTO>> {

    @Override
    public List<PermissionDTO> apply(List<PermissionLocalEntity> permissionLocalEntities) throws Exception {
        return Observable.fromIterable(permissionLocalEntities)
                .map(new LocalPermissionMapper())
                .toList()
                .blockingGet();
    }
}
