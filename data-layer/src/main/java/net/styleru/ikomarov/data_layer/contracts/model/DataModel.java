package net.styleru.ikomarov.data_layer.contracts.model;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.exceptions.internal.MalformedJsonException;

import java.util.List;

/**
 * Created by i_komarov on 21.02.17.
 */

public abstract class DataModel<T> {

    public abstract boolean containsError();

    public abstract boolean containsData();

    public abstract List<T> getBody(IRemoteMapping<T> mapping) throws MalformedJsonException;

    public abstract ExceptionBundle getError();
}
