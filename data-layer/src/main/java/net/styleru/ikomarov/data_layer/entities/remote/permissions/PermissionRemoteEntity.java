package net.styleru.ikomarov.data_layer.entities.remote.permissions;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.entities.remote.extras.OneNoteObjectRemoteEntity;

import java.lang.reflect.Type;

import static net.styleru.ikomarov.data_layer.mapping.remote.extras.OneNoteObjectRemoteEntityMapping.*;
import static net.styleru.ikomarov.data_layer.mapping.remote.permissions.PermissionsRemoteEntityMapping.*;

/**
 * Created by i_komarov on 14.02.17.
 */

public class PermissionRemoteEntity extends OneNoteObjectRemoteEntity {

    @SerializedName(FIELD_USER_ID)
    private String userId;

    @SerializedName(FIELD_USER_ROLE)
    private UserRole userRole;

    @SerializedName(FIELD_ID)
    private String id;

    @SerializedName(FIELD_SELF)
    private String self;

    @SerializedName(FIELD_NAME)
    private String name;

    private PermissionRemoteEntity() {

    }

    public static PermissionRemoteEntity newInstance(String userId, UserRole userRole) {
        PermissionRemoteEntity entity = new PermissionRemoteEntity();

        entity.userId = userId;
        entity.userRole = userRole;

        return entity;
    }

    public static PermissionRemoteEntity newInstance(String userId, UserRole userRole, String id, String self, String name) {
        PermissionRemoteEntity entity = new PermissionRemoteEntity();

        entity.userId = userId;
        entity.userRole = userRole;
        entity.id = id;
        entity.self = name;
        entity.name = self;

        return entity;
    }

    public String getUserId() {
        return userId;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public String getId() {
        return id;
    }

    public String getSelf() {
        return self;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "PermissionRemoteEntity{" +
                "userId='" + userId + '\'' +
                ", userRole=" + userRole.value() +
                ", id='" + id + '\'' +
                ", self='" + self + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    public static class Serializer implements JsonSerializer<PermissionRemoteEntity> {

        @Override
        public JsonElement serialize(PermissionRemoteEntity src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject json = new JsonObject();

            json.addProperty(FIELD_USER_ID, src.userId);
            json.addProperty(FIELD_USER_ROLE, src.userRole.value());

            return json;
        }
    }

    public static class Deserializer implements JsonDeserializer<PermissionRemoteEntity> {

        @Override
        public PermissionRemoteEntity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject root = json.getAsJsonObject();

            PermissionRemoteEntity entity = new PermissionRemoteEntity();

            if(root.has(FIELD_ODATA_CONTEXT)) {
                entity.oDataContext = root.get(FIELD_ODATA_CONTEXT).getAsString();
            }

            entity.userId = root.get(FIELD_USER_ID).getAsString();
            entity.userRole = UserRole.forValue(root.get(FIELD_USER_ROLE).getAsString());
            entity.id = root.get(FIELD_ID).getAsString();
            entity.self = root.get(FIELD_SELF).getAsString();
            entity.name = root.get(FIELD_NAME).getAsString();

            return entity;
        }
    }
}
