package net.styleru.ikomarov.data_layer.mapping.local.notebooks;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by i_komarov on 28.02.17.
 */

public class PrincipalObjectsLocalEntityMapping implements ILocalMapping<PrincipalObjectLocalEntity> {

    @NonNull
    public static final String TABLE = "principal_objects";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_NAME = "NAME";

    @NonNull
    public static final String COLUMN_EMAIL = "EMAIL";

    @NonNull
    public static final String COLUMN_DEPARTMENT = "DEPARTMENT";

    @NonNull
    public static final String COLUMN_TYPE = "TYPE";

    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_NAME;
    public static final String COLUMN_EMAIL_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_EMAIL;
    public static final String COLUMN_DEPARTMENT_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_DEPARTMENT;
    public static final String COLUMN_TYPE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_TYPE;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    public PrincipalObjectsLocalEntityMapping() {

    }

    public static List<String> getInitializationQueries() {
        return Arrays.asList(
                getCreateTableQuery()
        );
    }

    private static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE + "(" +
                    COLUMN_ID + " TEXT NOT NULL PRIMARY KEY," +
                    COLUMN_NAME + " TEXT," +
                    COLUMN_EMAIL + " TEXT," +
                    COLUMN_DEPARTMENT + " TEXT," +
                    COLUMN_TYPE + " TEXT NOT NULL" +
                ");";

    }

    @Override
    public PrincipalObjectLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException, ParseException {
        if(cursor.moveToFirst()) {
            return PrincipalObjectLocalEntity.newInstance(
                    cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_EMAIL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_DEPARTMENT)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_TYPE))
            );
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<PrincipalObjectLocalEntity> listFromCursor(Cursor cursor) throws ParseException {
        List<PrincipalObjectLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                entities.add(
                        PrincipalObjectLocalEntity.newInstance(
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_NAME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_EMAIL)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_DEPARTMENT)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_TYPE))
                        )
                );
            } while(cursor.moveToNext());
        }

        return entities;
    }
}
