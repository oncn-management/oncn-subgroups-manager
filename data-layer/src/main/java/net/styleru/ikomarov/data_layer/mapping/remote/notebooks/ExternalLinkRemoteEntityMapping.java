package net.styleru.ikomarov.data_layer.mapping.remote.notebooks;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ExternalLinkRemoteEntity;

import java.util.List;

/**
 * Created by i_komarov on 14.02.17.
 */

public class ExternalLinkRemoteEntityMapping implements IRemoteMapping<ExternalLinkRemoteEntity> {

    @NonNull
    public static final String FIELD_HREF = "href";

    private final Gson gson;

    public ExternalLinkRemoteEntityMapping() {
        this.gson = new Gson();
    }

    @Override
    public ExternalLinkRemoteEntity fromJson(JsonObject json) {
        return gson.fromJson(json, ExternalLinkRemoteEntity.class);
    }

    @Override
    public List<ExternalLinkRemoteEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<ExternalLinkRemoteEntity>>(){}.getType());
    }

    @Override
    public String toJson(ExternalLinkRemoteEntity entity) {
        return gson.toJson(entity);
    }

    @Override
    public String toJsonArray(List<ExternalLinkRemoteEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<ExternalLinkRemoteEntity>>(){}.getType());
    }
}
