package net.styleru.ikomarov.domain_layer.contracts.operations;

/**
 * Created by i_komarov on 08.03.17.
 */

public enum OperationStatus {
    SUCCESS(0),
    FAILURE(1),
    DELAYED(2);

    private final int code;

    OperationStatus(int code) {
        this.code = code;
    }

    public int code() {
        return this.code;
    }
}
