package net.styleru.ikomarov.data_layer.manager.session.interceptor;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.session.ISessionManager;

/**
 * Created by i_komarov on 13.03.17.
 */

public class GraphSessionInterceptor extends SessionInterceptor {

    private final ISessionManager sessionManager;

    public GraphSessionInterceptor(ISessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    @Override
    protected String getToken() throws ExceptionBundle {
        return sessionManager.blockingGetSessionToken(ISessionManager.RESOURCE_GRAPH);
    }

    @Override
    protected String refreshAndGetToken() throws ExceptionBundle {
        return sessionManager.blockingRefreshAndGetSessionToken(ISessionManager.RESOURCE_GRAPH);
    }
}
