package net.styleru.ikomarov.authentication.common;

/**
 * Created by i_komarov on 18.02.17.
 */

public interface Authenticator {

    String getResource();

    String getClientID();

    String getRedirectUri();
}
