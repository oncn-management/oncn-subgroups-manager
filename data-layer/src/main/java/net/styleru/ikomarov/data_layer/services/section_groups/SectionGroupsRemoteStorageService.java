package net.styleru.ikomarov.data_layer.services.section_groups;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.section_groups.SectionGroupRemoteEntity;
import net.styleru.ikomarov.data_layer.services.base.NetworkService;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsRemoteStorageService extends NetworkService<ISectionGroupsRemoteStorageService> implements ISectionGroupsRemoteStorageService {

    private SectionGroupsRemoteStorageService(OkHttpClient client) {
        super(Endpoints.ONE_NOTE_BASE_URL, client);
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> create(String userId, String notebookId, JsonObject sectionGroup) {
        return getService().create(userId, notebookId, sectionGroup)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(String userId, String sectionGroupId) {
        return getService().read(userId, sectionGroupId)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(String userId, String sectionGroupId, String filter) {
        return getService().read(userId, sectionGroupId, filter)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(String userId, String notebookId, String orderBy, String fetchingFilter) {
        return getService().read(userId, notebookId, orderBy, fetchingFilter)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(String userId, String notebookId, int offset, int limit) {
        return getService().read(userId, notebookId, offset, limit)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(String userId, String notebookId, int offset, int limit, String orderBy) {
        return getService().read(userId, notebookId, offset, limit, orderBy)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> read(String userId, String notebookId, int offset, int limit, String orderBy, String filter) {
        return getService().read(userId, notebookId, offset, limit, orderBy, filter)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> createNested(String userId, String sectionGroupId, JsonObject sectionGroup) {
        return getService().createNested(userId, sectionGroupId, sectionGroup)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(String userId, String sectionGroupId, String orderBy) {
        return getService().readNested(userId, sectionGroupId, orderBy)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(String userId, String sectionGroupId, String order, String filter) {
        return getService().readNested(userId, sectionGroupId, order, filter)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(String userId, String sectionGroupId, int offset, int limit) {
        return getService().readNested(userId, sectionGroupId, offset, limit)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(String userId, String sectionGroupId, int offset, int limit, String order) {
        return getService().readNested(userId, sectionGroupId, offset, limit, order)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<SectionGroupRemoteEntity>>> readNested(String userId, String sectionGroupId, int offset, int limit, String order, String filter) {
        return getService().readNested(userId, sectionGroupId, offset, limit, order, filter)
                .compose(handleError());
    }


    @Override
    protected ISectionGroupsRemoteStorageService createService(Retrofit retrofit) {
        return retrofit.create(ISectionGroupsRemoteStorageService.class);
    }

    @Override
    protected Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(DataResponse.class, new DataResponse.Deserializer())
                .create();
    }

    public static final class Factory {

        private Factory() {

        }

        public static ISectionGroupsRemoteStorageService create(OkHttpClient client) {
            return new SectionGroupsRemoteStorageService(client);
        }
    }
}
