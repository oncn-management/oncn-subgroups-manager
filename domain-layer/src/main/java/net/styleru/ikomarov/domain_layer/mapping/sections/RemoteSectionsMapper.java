package net.styleru.ikomarov.domain_layer.mapping.sections;

import net.styleru.ikomarov.data_layer.entities.remote.sections.SectionRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.SectionDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 06.03.17.
 */

public class RemoteSectionsMapper implements Function<List<SectionRemoteEntity>, List<SectionDTO>> {

    @Override
    public List<SectionDTO> apply(List<SectionRemoteEntity> sectionRemoteEntities) throws Exception {
        return Observable.fromIterable(sectionRemoteEntities)
                .map(new RemoteSectionMapper())
                .toList()
                .blockingGet();
    }
}
