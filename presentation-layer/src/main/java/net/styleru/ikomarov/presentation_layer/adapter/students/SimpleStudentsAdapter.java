package net.styleru.ikomarov.presentation_layer.adapter.students;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;
import net.styleru.ikomarov.presentation_layer.view_holder.students_list.SimpleStudentsViewHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by i_komarov on 05.04.17.
 */

public class SimpleStudentsAdapter extends RecyclerView.Adapter<SimpleStudentsViewHolder> {

    private SimpleStudentsViewHolder.InteractionCallbacks callbacks;

    private SortedList<StudentViewObject> items;

    private Map<String, StudentViewObject> itemMap;

    private Set<String> ignoreItems;

    private final Object lock = new Object();

    public SimpleStudentsAdapter() {
        this.itemMap = new HashMap<>();
        this.ignoreItems = new HashSet<>();
        initialize(itemMap);
    }

    public SimpleStudentsAdapter(Bundle savedInstanceState) {
        SimpleStudentsAdapter.AdapterState state = new SimpleStudentsAdapter.AdapterState(savedInstanceState);
        this.itemMap = state.restoreItemMap();
        this.ignoreItems = state.restoreIgnoreItems();
        initialize(itemMap);
    }

    public SimpleStudentsAdapter(SimpleStudentsAdapter previousInstance) {
        this.itemMap = previousInstance.itemMap;
        this.ignoreItems = previousInstance.ignoreItems;
        initialize(itemMap);
    }

    private void initialize(Map<String, StudentViewObject> itemMap) {
        this.items = new SortedList<>(StudentViewObject.class, new SortedListAdapterCallback<StudentViewObject>(this) {
            @Override
            public int compare(StudentViewObject o1, StudentViewObject o2) {
                return o1.getName().compareTo(o2.getName());
            }

            @Override
            public boolean areContentsTheSame(StudentViewObject o1, StudentViewObject o2) {
                return  o1.getName().equals(o2.getName()) &&
                        o1.getEmail().equals(o2.getEmail());
            }

            @Override
            public boolean areItemsTheSame(StudentViewObject o1, StudentViewObject o2) {
                return o1.getId() != null && o2.getId() != null && o1.getId().equals(o2.getId());

            }
        });

        this.items.addAll(itemMap.values());
    }

    @Override
    public SimpleStudentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SimpleStudentsViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_student_simple, parent, false));
    }

    @Override
    public void onBindViewHolder(SimpleStudentsViewHolder holder, int position) {
        holder.bind(position, items.get(position), callbacks);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void onSaveInstanceState(Bundle outState) {
        SimpleStudentsAdapter.AdapterState state = new SimpleStudentsAdapter.AdapterState(itemMap, ignoreItems);
        state.onSaveInstanceState(outState);
    }

    public void registerCallbacks(SimpleStudentsViewHolder.InteractionCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void unregisterCallbacks() {
        this.callbacks = null;
    }

    public void clear() {
        synchronized (lock) {
            this.itemMap.clear();
            this.items.clear();
        }
    }

    public void addItemsToIgnore(String... items) {
        synchronized (lock) {
            this.ignoreItems.addAll(Arrays.asList(items));

            this.items.beginBatchedUpdates();

            for (String ignore : items) {
                if (this.itemMap.containsKey(ignore)) {
                    this.items.remove(this.itemMap.get(ignore));
                    this.itemMap.remove(ignore);
                }
            }

            this.items.endBatchedUpdates();
        }
    }

    public void removeItemsFromIgnore(String... items) {
        synchronized (lock) {
            this.ignoreItems.removeAll(Arrays.asList(items));
        }
    }

    public void add(StudentViewObject newItem) {
        synchronized (lock) {
            if (ignoreItems.contains(newItem.getId())) {
                return;
            }
            StudentViewObject oldItem = itemMap.get(newItem.getId());
            if (oldItem == null) {
                this.items.add(newItem);
            } else {
                this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
            }

            itemMap.put(newItem.getId(), newItem);
        }
    }

    public void add(List<StudentViewObject> items) {
        synchronized (lock) {
            boolean allExists = true;

            for (int i = 0; i < items.size(); i++) {
                StudentViewObject newItem = items.get(i);
                StudentViewObject oldItem = itemMap.get(newItem.getId());

                if (oldItem == null) {
                    allExists = false;
                    break;
                }
            }

            if (allExists) {
                performBatchUpdateWithoutNewItems(items);
            } else {
                performBatchUpdateWithNewItems(items);
            }
        }
    }

    public void remove(String id) {
        synchronized (lock) {
            if (this.itemMap.containsKey(id)) {
                this.items.remove(this.itemMap.get(id));
            }
        }
    }

    public void remove(final int position) {
        synchronized (lock) {
            String uri = this.items.get(position).getId();
            this.items.removeItemAt(position);
            this.itemMap.remove(uri);
        }
    }

    private void performBatchUpdateWithoutNewItems(List<StudentViewObject> items) {
        for(int i = 0; i < items.size(); i++) {
            StudentViewObject newItem = items.get(i);
            StudentViewObject oldItem = itemMap.get(newItem.getId());

            this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
            this.itemMap.put(newItem.getId(), newItem);
        }
    }

    private void performBatchUpdateWithNewItems(List<StudentViewObject> items) {
        try {
            this.items.beginBatchedUpdates();
            for (int i = 0; i < items.size(); i++) {
                StudentViewObject newItem = items.get(i);
                if(!ignoreItems.contains(newItem.getId())) {
                    StudentViewObject oldItem = itemMap.get(newItem.getId());

                    if (oldItem != null) {
                        this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
                    } else {
                        this.items.add(newItem);
                    }

                    this.itemMap.put(newItem.getId(), newItem);
                }
            }

        } finally {
            this.items.endBatchedUpdates();
        }
    }

    private static final class AdapterState {

        private static final String PREFIX = AdapterState.class.getCanonicalName() + ".";

        private static final String KEY_CONTAINER = PREFIX + "CONTAINER";

        private static final String KEY_ITEMS = PREFIX + "ITEMS";

        private static final String KEY_IGNORE_ITEMS = PREFIX + "IGNORE_ITEMS";

        private Bundle state;

        public AdapterState(Bundle savedInstanceState) {
            this.state = savedInstanceState.containsKey(KEY_CONTAINER) ? savedInstanceState.getBundle(KEY_CONTAINER) : null;
        }

        public AdapterState(Map<String, StudentViewObject> itemMap, Set<String> ignoreItems) {
            this.state = new Bundle();
            this.state.putStringArray(KEY_IGNORE_ITEMS, ignoreItems.toArray(new String[ignoreItems.size()]));
            this.state.putParcelableArrayList(KEY_ITEMS, new ArrayList<>(itemMap.values()));
        }

        public void onSaveInstanceState(Bundle outState) {
            outState.putBundle(KEY_CONTAINER, state);
        }

        public Map<String, StudentViewObject> restoreItemMap() {
            ArrayList<Parcelable> itemList = state != null && state.containsKey(KEY_ITEMS) ? state.getParcelableArrayList(KEY_ITEMS) : new ArrayList<>();
            Map<String, StudentViewObject> itemMap = new HashMap<>(itemList.size());
            for(Parcelable item : itemList) {
                StudentViewObject itemActual = (StudentViewObject) item;
                itemMap.put(itemActual.getId(), itemActual);
            }

            return itemMap;
        }

        public Set<String> restoreIgnoreItems() {
            return state != null && state.containsKey(KEY_IGNORE_ITEMS) ? new HashSet<>(Arrays.asList((state.getStringArray(KEY_IGNORE_ITEMS)))) : new HashSet<>();
        }
    }
}
