package net.styleru.ikomarov.presentation_layer.application;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.analytics.IScreenAnalyticsManager;
import net.styleru.ikomarov.domain_layer.di.endpoints.jobs.JobEnvironmentModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.notebooks.NotebooksRepositoryModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.permissions.SGPermissionsRepositoryModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.reports.ReportRepositoryFacadeModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.section_groups.SectionGroupsRepositoryModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.students.StudentsRepositoryModule;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.DatabaseModule;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.ErrorTranslationModule;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.LoggingModule;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.NetworkModule;
import net.styleru.ikomarov.domain_layer.di.level_3.analytics.AnalyticsModule;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.CacheStrategyModule;
import net.styleru.ikomarov.domain_layer.di.level_3.session.SessionModule;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.ClientModule;
import net.styleru.ikomarov.domain_layer.repository.permissions.SGPermissionsRepository;

import io.reactivex.Scheduler;

/**
 * Created by i_komarov on 13.03.17.
 */

public interface ISingletonProvider {

    @NonNull
    NetworkModule network();

    @NonNull
    LoggingModule logging();

    @NonNull
    SessionModule session();

    @NonNull
    NotebooksRepositoryModule notebooksRepository();

    @NonNull
    SectionGroupsRepositoryModule sectionGroupsRepository();

    @NonNull
    StudentsRepositoryModule studentsRepository();

    @NonNull
    SGPermissionsRepositoryModule sgPermissionsRepository();

    @NonNull
    ReportRepositoryFacadeModule reportReporitoryFacade();

    @NonNull
    JobEnvironmentModule jobEnvironment();

    @NonNull
    ErrorTranslationModule errorTranslation();

    @NonNull
    AnalyticsModule analytics();

    @NonNull
    Scheduler ioScheduler();

    @NonNull
    Scheduler mainThreadScheduler();
}
