package net.styleru.ikomarov.presentation_layer.adapter.students;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;
import net.styleru.ikomarov.presentation_layer.view_holder.students_list.StudentViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsAdapter extends RecyclerView.Adapter<StudentViewHolder> {

    private StudentViewHolder.InteractionCallbacks callbacks;

    private SortedList<StudentViewObject> items;

    private Map<String, StudentViewObject> itemMap;

    public StudentsAdapter() {
        this.itemMap = new HashMap<>();
        initialize(itemMap);
    }

    public StudentsAdapter(Bundle savedInstanceState) {
        AdapterState state = new AdapterState(savedInstanceState);
        this.itemMap = state.restoreItemMap();
        initialize(itemMap);
    }

    public StudentsAdapter(StudentsAdapter previousInstance) {
        this.itemMap = previousInstance.itemMap;
        initialize(itemMap);
    }

    private void initialize(Map<String, StudentViewObject> itemMap) {
        this.items = new SortedList<>(StudentViewObject.class, new SortedListAdapterCallback<StudentViewObject>(this) {
            @Override
            public int compare(StudentViewObject o1, StudentViewObject o2) {
                return o1.getName().compareTo(o2.getName());
            }

            @Override
            public boolean areContentsTheSame(StudentViewObject o1, StudentViewObject o2) {
                return  o1.getName().equals(o2.getName()) &&
                        o1.getEmail().equals(o2.getEmail());
            }

            @Override
            public boolean areItemsTheSame(StudentViewObject o1, StudentViewObject o2) {
                return o1.getId() != null && o2.getId() != null && o1.getId().equals(o2.getId());

            }
        });

        this.items.addAll(itemMap.values());
    }

    @Override
    public StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new StudentViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_student, parent, false));
    }

    @Override
    public void onBindViewHolder(StudentViewHolder holder, int position) {
        holder.bind(position, items.get(position), callbacks);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void onSaveInstanceState(Bundle outState) {
        AdapterState state = new AdapterState(itemMap);
        state.onSaveInstanceState(outState);
    }

    public void registerCallbacks(StudentViewHolder.InteractionCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void unregisterCallbacks() {
        this.callbacks = null;
    }

    public void add(StudentViewObject newItem) {
        StudentViewObject oldItem = itemMap.get(newItem.getId());
        if(oldItem == null) {
            this.items.add(newItem);
        } else {
            this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
        }

        itemMap.put(newItem.getId(), newItem);
    }

    public void add(List<StudentViewObject> items) {
        boolean allExists = true;

        for(int i = 0; i < items.size(); i++) {
            StudentViewObject newItem = items.get(i);
            StudentViewObject oldItem = itemMap.get(newItem.getId());

            if(oldItem == null) {
                allExists = false;
                break;
            }
        }

        if(allExists) {
            performBatchUpdateWithoutNewItems(items);
        } else {
            performBatchUpdateWithNewItems(items);
        }
    }

    public void remove(String id) {
        if(this.itemMap.containsKey(id)) {
            this.items.remove(this.itemMap.get(id));
            this.itemMap.remove(id);
        }
    }

    public void remove(final int position) {
        String uri = this.items.get(position).getId();
        this.items.removeItemAt(position);
        this.itemMap.remove(uri);
    }

    private void performBatchUpdateWithoutNewItems(List<StudentViewObject> items) {
        for(int i = 0; i < items.size(); i++) {
            StudentViewObject newItem = items.get(i);
            StudentViewObject oldItem = itemMap.get(newItem.getId());

            this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
            this.itemMap.put(newItem.getId(), newItem);
        }
    }

    private void performBatchUpdateWithNewItems(List<StudentViewObject> items) {
        try {
            this.items.beginBatchedUpdates();
            for (int i = 0; i < items.size(); i++) {
                StudentViewObject newItem = items.get(i);
                StudentViewObject oldItem = itemMap.get(newItem.getId());

                if (oldItem != null) {
                    this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
                } else {
                    this.items.add(newItem);
                }

                this.itemMap.put(newItem.getId(), newItem);
            }

        } finally {
            this.items.endBatchedUpdates();
        }
    }

    private static final class AdapterState {

        private static final String PREFIX = AdapterState.class.getCanonicalName() + ".";

        private static final String KEY_CONTAINER = PREFIX + "CONTAINER";

        private static final String KEY_ITEMS = PREFIX + "ITEMS";

        private Bundle state;

        public AdapterState(Bundle savedInstanceState) {
            this.state = savedInstanceState.containsKey(KEY_CONTAINER) ? savedInstanceState.getBundle(KEY_CONTAINER) : null;
        }

        public AdapterState(Map<String, StudentViewObject> itemMap) {
            this.state = new Bundle();
            this.state.putParcelableArrayList(KEY_ITEMS, new ArrayList<>(itemMap.values()));
        }

        public void onSaveInstanceState(Bundle outState) {
            outState.putBundle(KEY_CONTAINER, state);
        }

        public Map<String, StudentViewObject> restoreItemMap() {
            ArrayList<Parcelable> itemList = state != null && state.containsKey(KEY_ITEMS) ? state.getParcelableArrayList(KEY_ITEMS) : new ArrayList<>();
            Map<String, StudentViewObject> itemMap = new HashMap<>(itemList.size());
            for(Parcelable item : itemList) {
                StudentViewObject itemActual = (StudentViewObject) item;
                itemMap.put(itemActual.getId(), itemActual);
            }

            return itemMap;
        }
    }
}
