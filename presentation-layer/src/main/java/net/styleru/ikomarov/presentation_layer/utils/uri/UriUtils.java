package net.styleru.ikomarov.presentation_layer.utils.uri;

/**
 * Created by i_komarov on 09.04.17.
 */

public class UriUtils {

    private static final String ENCODED_SPACE = "%20";

    public static String safeTrimSpaces(String in) {
        return in.replaceAll(" ", ENCODED_SPACE);
    }
}
