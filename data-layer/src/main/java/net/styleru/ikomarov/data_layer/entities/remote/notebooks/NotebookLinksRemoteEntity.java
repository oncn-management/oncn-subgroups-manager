package net.styleru.ikomarov.data_layer.entities.remote.notebooks;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.remote.notebooks.NotebookLinksRemoteEntityMapping.*;

/**
 * Created by i_komarov on 14.02.17.
 */

public class NotebookLinksRemoteEntity {

    @SerializedName(FIELD_ONENOTE_CLIENT_URL)
    private ExternalLinkRemoteEntity clientUrl;
    @SerializedName(FIELD_ONENOTE_WEB_URL)
    private ExternalLinkRemoteEntity webUrl;

    private NotebookLinksRemoteEntity() {

    }

    public ExternalLinkRemoteEntity getClientUrl() {
        return clientUrl;
    }

    public ExternalLinkRemoteEntity getWebUrl() {
        return webUrl;
    }

    @Override
    public String toString() {
        return "NotebookLinksRemoteEntity{" +
                "clientUrl=" + clientUrl +
                ", webUrl=" + webUrl +
                '}';
    }
}
