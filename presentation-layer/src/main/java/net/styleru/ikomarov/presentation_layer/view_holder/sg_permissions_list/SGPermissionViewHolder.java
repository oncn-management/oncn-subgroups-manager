package net.styleru.ikomarov.presentation_layer.view_holder.sg_permissions_list;

import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.adapter.sg_permissions_list.PermissionsSpinnerAdapter;
import net.styleru.ikomarov.presentation_layer.view.sg_permissions_list.SGPermissionViewObject;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SGPermissionViewHolder extends RecyclerView.ViewHolder implements AdapterView.OnItemSelectedListener {

    private AppCompatTextView nameHolder;
    private AppCompatSpinner permissionSpinner;

    private PermissionsSpinnerAdapter adapter;

    private int position;
    private SGPermissionViewObject item;
    private IPermissionChangeListener listener;

    public SGPermissionViewHolder(View itemView) {
        super(itemView);
        nameHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_permission_display_name);
        permissionSpinner = (AppCompatSpinner) itemView.findViewById(R.id.list_item_permission_permissions_spinner);
        adapter = new PermissionsSpinnerAdapter(itemView.getContext());
        permissionSpinner.setAdapter(adapter);
        permissionSpinner.setOnItemSelectedListener(this);
    }

    public void bind(int position, SGPermissionViewObject item, IPermissionChangeListener listener) {
        this.position = position;
        this.item = item;
        this.listener = listener;

        nameHolder.setText(item.getName());
        int selection;
        if(-1 != (selection = adapter.getSelectionByEnum(item.getRole()))) {
            permissionSpinner.setSelection(selection);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if(listener != null) {
            listener.onPermissionAssigned(item.getId(), item.getUserId(), item.getRole(), adapter.getEnumBySelection(position));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public interface IPermissionChangeListener {

        void onPermissionAssigned(String permissionId, String userId, UserRole oldRole, UserRole newRole);
    }
}
