package net.styleru.ikomarov.data_layer.services.permissions.section_groups;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;

import net.styleru.ikomarov.data_layer.contracts.permissions.PermissionCategory;
import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntity;
import net.styleru.ikomarov.data_layer.mapping.local.permissions.PermissionsLocalEntitiesMapping;
import net.styleru.ikomarov.data_layer.mapping.utils.QueryBuilder;
import net.styleru.ikomarov.data_layer.services.base.DatabaseService;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 21.03.17.
 */

public class SGPermissionsLocalStorageService extends DatabaseService implements ISGPermissionsLocalStorageService {

    private PermissionsLocalEntitiesMapping mapping;

    private SGPermissionsLocalStorageService(StorIOSQLite sqlite) {
        super(sqlite);
        this.mapping = new PermissionsLocalEntitiesMapping();
    }

    @Override
    public Observable<PutResult> create(PermissionLocalEntity entity) {
        return wrapTransactionSynchronousOff(() -> {
            return getSQLite().put()
                    .object(entity)
                    .prepare()
                    .executeAsBlocking();
        });
    }

    @Override
    public Observable<List<PermissionLocalEntity>> read(String sectionGroupId) {
        return wrap(() -> {
            Cursor cursor = getSQLite().get().cursor()
                    .withQuery(RawQuery.builder().query(
                            QueryBuilder.selectQuery(PermissionsLocalEntitiesMapping.TABLE)
                                    .where(PermissionsLocalEntitiesMapping.COLUMN_HOST_TYPE + " = " + String.valueOf(PermissionCategory.SECTION_GROUP.code()))
                                    .where(PermissionsLocalEntitiesMapping.COLUMN_HOST_ID + " = " + "\"" + sectionGroupId + "\"")
                                    .build()
                    ).build())
                    .prepare()
                    .executeAsBlocking();

            try {
                return mapping.listFromCursor(cursor);
            } finally {
                cursor.close();
            }
        });
    }

    @Override
    public Observable<List<PermissionLocalEntity>> read(String sectionGroupId, String userId) {
        return wrap(() -> {
            Cursor cursor = getSQLite().get().cursor()
                    .withQuery(RawQuery.builder().query(
                            QueryBuilder.selectQuery(PermissionsLocalEntitiesMapping.TABLE)
                                    .where(PermissionsLocalEntitiesMapping.COLUMN_HOST_TYPE + " = " + String.valueOf(PermissionCategory.SECTION_GROUP.code()))
                                    .where(PermissionsLocalEntitiesMapping.COLUMN_HOST_ID + " = " + "\"" + sectionGroupId + "\"")
                                    .where(PermissionsLocalEntitiesMapping.COLUMN_USER_ID + " = " + "\"" + userId + "\"")
                                    .build()
                    ).build())
                    .prepare()
                    .executeAsBlocking();

            try {
                return mapping.listFromCursor(cursor);
            } finally {
                cursor.close();
            }
        });
    }


    @Override
    public Observable<DeleteResult> delete(String sectionGroupId, String permissionId) {
        return wrapTransactionSynchronousOff(() -> {
            return getSQLite().delete()
                    .byQuery(
                            DeleteQuery.builder().table(PermissionsLocalEntitiesMapping.TABLE)
                                    .where(PermissionsLocalEntitiesMapping.COLUMN_HOST_TYPE + " = " + "\"" + PermissionCategory.SECTION_GROUP.code() + "\"")
                                    .where(PermissionsLocalEntitiesMapping.COLUMN_HOST_ID + " = " + "\"" + sectionGroupId + "\"")
                                    .where(PermissionsLocalEntitiesMapping.COLUMN_ID + " = " + "\"" + permissionId + "\"")
                                    .build()
                    ).prepare()
                    .executeAsBlocking();
        });
    }

    public static final class Factory {

        private Factory() {

        }

        public static ISGPermissionsLocalStorageService create(@NonNull StorIOSQLite sqlite) {
            return new SGPermissionsLocalStorageService(sqlite);
        }
    }
}
