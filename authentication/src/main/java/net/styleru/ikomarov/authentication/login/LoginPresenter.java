package net.styleru.ikomarov.authentication.login;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.authentication.BuildConfig;
import net.styleru.ikomarov.authentication.dto.entities.AzureToken;
import net.styleru.ikomarov.authentication.http.RetrofitFactory;
import net.styleru.ikomarov.authentication.web_interfaces.OAuth2;

import io.reactivex.ObservableSource;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by i_komarov on 18.02.17.
 */

public class LoginPresenter {

    private ILoginView view;

    private OAuth2 api;

    private Scheduler io;
    private CompositeDisposable disposables;

    public LoginPresenter() {
        this.disposables = new CompositeDisposable();
        this.api = RetrofitFactory.buildRetrofit().create(OAuth2.class);
        this.io = Schedulers.io();
    }

    public void bindView(ILoginView view) {
        this.view = view;
    }

    public void unbindView() {
        this.disposables.dispose();
        this.view = null;
    }

    public void onAuthenticationCodeLoaded(String code, String resource) {
        api.tradeCodeAsObservable(OAuth2.GRANT_TYPE_TRADE_TOKEN, BuildConfig.AZURE_AD_API_KEY, BuildConfig.AZURE_AD_REDIRECT_URI, code, resource)
                .subscribeOn(io)
                .flatMap(new Function<AzureToken, ObservableSource<Wrapper<AzureToken, String>>>() {
                    @Override
                    public ObservableSource<Wrapper<AzureToken, String>> apply(final AzureToken azureToken) throws Exception {
                        return api.me(azureToken.getType() + " " + azureToken.getAccessToken(), OAuth2.ME_BASE_URL)
                                .subscribeOn(io)
                                .map(new Function<JsonObject, String>() {
                                    @Override
                                    public String apply(JsonObject response) throws Exception {
                                        return response.get(OAuth2.FIELD_MAIL).getAsString();
                                    }
                                })
                                .map(new Function<String, Wrapper<AzureToken, String>>() {
                                    @Override
                                    public Wrapper<AzureToken, String> apply(String displayName) throws Exception {
                                        return new Wrapper<>(azureToken, displayName);
                                    }
                                });
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(createTokenLoadedEventObserver());
    }

    private DisposableObserver<Wrapper<AzureToken, String>> createTokenLoadedEventObserver() {
        return new DisposableObserver<Wrapper<AzureToken, String>>() {
            @Override
            public void onNext(Wrapper<AzureToken, String> value) {
                view.onAccessTokenLoaded(value.getExtra(), value.getObj().getAccessToken(), value.getObj().getRefreshToken());
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private static class Wrapper<T, E> {

        private T obj;
        private E extra;

        private Wrapper(T obj, E extra) {
            this.obj = obj;
            this.extra = extra;
        }

        public E getExtra() {
            return extra;
        }

        public T getObj() {
            return obj;
        }
    }
}
