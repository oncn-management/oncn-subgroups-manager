package net.styleru.ikomarov.domain_layer.di.endpoints.notebooks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.repository.notebooks.INotebooksRepository;
import net.styleru.ikomarov.domain_layer.repository.notebooks.NotebooksRepository;

/**
 * Created by i_komarov on 14.03.17.
 */

public class NotebooksRepositoryModule {

    private final Object lock = new Object();

    @NonNull
    private final INotebooksEnvironmentComponent environment;

    @Nullable
    private volatile INotebooksRepository repository;

    public NotebooksRepositoryModule(@NonNull INotebooksEnvironmentComponent environment) {
        this.environment = environment;
    }

    @NonNull
    public INotebooksRepository provideRepository() {
        INotebooksRepository localInstance = repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = NotebooksRepository.Factory.create(
                            environment.local().provideService(),
                            environment.remote().provideService(),
                            environment.cache().prodiveCache(),
                            environment.cacheComponent().strategy().provideCacheStrategy(),
                            environment.mapping().provideMapping()
                    );
                }
            }
        }

        return localInstance;
    }
}
