package net.styleru.ikomarov.data_layer.mapping.remote.users;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.entities.remote.users.UserRemoteEntity;

import java.util.List;

/**
 * Created by i_komarov on 24.02.17.
 */

public class UsersRemoteEntityMapping implements IRemoteMapping<UserRemoteEntity> {

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_BUSINESS_PHONES = "businessPhones";

    @NonNull
    public static final String FIELD_DISPLAY_NAME = "displayName";

    @NonNull
    public static final String FIELD_GIVEN_NAME = "givenName";

    @NonNull
    public static final String FIELD_JOB_TITLE = "jobTitle";

    @NonNull
    public static final String FIELD_MAIL = "mail";

    @NonNull
    public static final String FIELD_MOBILE_PHONE = "mobilePhone";

    @NonNull
    public static final String FIELD_OFFICE_LOCATION = "officeLocation";

    @NonNull
    public static final String FIELD_PREFERRED_LANGUAGE = "preferredLanguage";

    @NonNull
    public static final String FIELD_LAST_NAME = "surname";

    @NonNull
    public static final String FIELD_USER_PRINCIPAL_NAME = "userPrincipalName";

    private final Gson gson;

    public UsersRemoteEntityMapping() {
        this.gson = new GsonBuilder().setDateFormat(DateTimeContract.API_DATE_FORMAT).create();
    }

    @Override
    public UserRemoteEntity fromJson(JsonObject json) {
        return gson.fromJson(json, UserRemoteEntity.class);
    }

    @Override
    public List<UserRemoteEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<UserRemoteEntity>>(){}.getType());
    }

    @Override
    public String toJson(UserRemoteEntity entity) {
        return gson.toJson(entity, UserRemoteEntity.class);
    }

    @Override
    public String toJsonArray(List<UserRemoteEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<UserRemoteEntity>>(){}.getType());
    }
}
