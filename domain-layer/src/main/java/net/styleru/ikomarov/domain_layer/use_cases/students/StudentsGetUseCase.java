package net.styleru.ikomarov.domain_layer.use_cases.students;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;
import net.styleru.ikomarov.domain_layer.repository.students.IStudentsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsGetUseCase extends UseCase {

    @NonNull
    private IStudentsRepository repository;

    @NonNull
    private Scheduler executeScheduler;

    @NonNull
    private Scheduler postScheduler;

    public StudentsGetUseCase(@NonNull IStudentsRepository repository,
                              @NonNull Scheduler executeScheduler,
                              @NonNull Scheduler postScheduler) {
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<List<PrincipalObjectDTO>, List<T>> mapper, Callbacks<T> callbacks, String notebookId, int offset, int limit) {
        execute(repository.read(notebookId, offset, limit), mapper, callbacks);
    }

    public <T> void execute(Function<List<PrincipalObjectDTO>, List<T>> mapper, Callbacks<T> callbacks, String notebookId, int offset, int limit, String nameSubstring) {
        execute(repository.read(notebookId, offset, limit, nameSubstring), mapper, callbacks);
    }

    private <T> void execute(Observable<List<PrincipalObjectDTO>> upstream, Function<List<PrincipalObjectDTO>, List<T>> mapper, Callbacks<T> callbacks) {
        upstream.subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createStudentsObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createStudentsObserver(final Callbacks<T> callbacks) {
        DisposableObserver<List<T>> studentsObserver = new DisposableObserver<List<T>>() {
            @Override
            public void onNext(List<T> value) {
                callbacks.onStudentsLoaded(value);
            }

            @Override
            public void onError(Throwable e) {
                if(e instanceof ExceptionBundle) {
                    callbacks.onStudentsLoadingFailed((ExceptionBundle) e);
                } else {
                    callbacks.onStudentsLoadingFailed(newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(studentsObserver);

        return studentsObserver;
    }

    public interface Callbacks<T> {

        void onStudentsLoaded(List<T> students);

        void onStudentsLoadingFailed(ExceptionBundle error);
    }
}
