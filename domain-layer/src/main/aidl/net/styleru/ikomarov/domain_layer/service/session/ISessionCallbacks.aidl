// ISessionCallbacks.aidl
package net.styleru.ikomarov.domain_layer.service.session;

// Declare any non-default types here with import statements

import net.styleru.ikomarov.domain_layer.service.base.ServiceException;

interface ISessionCallbacks {

    void onAuthenticated();

    void onDeauthenticated();

    void onAuthenticationFailure(in ServiceException exception);
}
