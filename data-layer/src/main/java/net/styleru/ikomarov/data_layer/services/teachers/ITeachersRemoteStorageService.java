package net.styleru.ikomarov.data_layer.services.teachers;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static net.styleru.ikomarov.data_layer.common.Endpoints.*;
import static net.styleru.ikomarov.data_layer.common.Paths.*;

/**
 * Created by i_komarov on 19.03.17.
 */

public interface ITeachersRemoteStorageService {

    String PARAM_OFFSET = "$skip";
    String PARAM_LIMIT = "$top";
    String PARAM_FILTER = "$filter";
    String PARAM_ORDER_BY = "$orderby";

    @POST(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_TEACHERS)
    Observable<DataResponse<PrincipalObjectRemoteEntity>> create(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Body JsonObject teacherJson
    );

    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_TEACHERS)
    Observable<DataResponse<PrincipalObjectRemoteEntity>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Query(PARAM_OFFSET) int offset,
            @Query(PARAM_LIMIT) int limit
    );

    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_TEACHERS)
    Observable<DataResponse<PrincipalObjectRemoteEntity>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Query(PARAM_OFFSET) int offset,
            @Query(PARAM_LIMIT) int limit,
            @Query(PARAM_ORDER_BY) String orderBy
    );

    @GET(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_TEACHERS)
    Observable<DataResponse<PrincipalObjectRemoteEntity>> read(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Query(PARAM_OFFSET) int offset,
            @Query(PARAM_LIMIT) int limit,
            @Query(PARAM_ORDER_BY) String orderBy,
            @Query(PARAM_FILTER) String filter
    );

    @DELETE(ENDPOINT_API + ENDPOINT_V1 + "/{" + PATH_USER_ID + "}" + ENDPOINT_NOTES + ENDPOINT_CLASS_NOTEBOOKS + "/{" + PATH_NOTEBOOK_ID + "}" + ENDPOINT_TEACHERS + "/{" + PATH_TEACHER_ID + "}")
    Observable<DataResponse<PrincipalObjectRemoteEntity>> delete(
            @Path(PATH_USER_ID) String userId,
            @Path(PATH_NOTEBOOK_ID) String notebookId,
            @Path(PATH_TEACHER_ID) String teacherId
    );
}
