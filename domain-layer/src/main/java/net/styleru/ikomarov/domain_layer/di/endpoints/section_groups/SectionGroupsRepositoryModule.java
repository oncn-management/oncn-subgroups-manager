package net.styleru.ikomarov.domain_layer.di.endpoints.section_groups;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.repository.section_groups.ISectionGroupsRepository;
import net.styleru.ikomarov.domain_layer.repository.section_groups.SectionGroupsRepository;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsRepositoryModule {

    private final Object lock = new Object();

    @NonNull
    private final ISectionGroupsEnvironmentComponent component;

    @Nullable
    private volatile ISectionGroupsRepository repository;

    public SectionGroupsRepositoryModule(@NonNull ISectionGroupsEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public ISectionGroupsRepository provideRepository() {
        ISectionGroupsRepository localInstance = this.repository;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = repository;
                if(localInstance == null) {
                    localInstance = repository = SectionGroupsRepository.Factory.create(
                            component.localService().provideLocalService(),
                            component.remoteService().provideRemoteService(),
                            component.cache().provideSectionGroupsCacheClaster(),
                            component.cacheComponent().strategy().provideCacheStrategy(),
                            component.mapping().provideMapping()
                    );
                }
            }
        }

        return localInstance;
    }
}
