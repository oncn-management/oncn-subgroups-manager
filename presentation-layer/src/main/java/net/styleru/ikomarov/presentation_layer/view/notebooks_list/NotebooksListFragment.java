package net.styleru.ikomarov.presentation_layer.view.notebooks_list;

import android.Manifest;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import net.styleru.ikomarov.domain_layer.facades.repository.IReportRepositoryFacade;
import net.styleru.ikomarov.domain_layer.repository.notebooks.INotebooksRepository;
import net.styleru.ikomarov.domain_layer.use_cases.notebooks.NotebooksGetUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.reports.PDFReportCreateUseCase;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.adapter.notebooks_list.NotebooksAdapter;
import net.styleru.ikomarov.presentation_layer.adapter.utils.SortingStrategyType;
import net.styleru.ikomarov.presentation_layer.application.ISingletonProvider;
import net.styleru.ikomarov.presentation_layer.presenter.notebooks_list.NotebooksListPresenter;
import net.styleru.ikomarov.presentation_layer.utils.pagination.PaginationListener;
import net.styleru.ikomarov.presentation_layer.utils.pagination.State;
import net.styleru.ikomarov.presentation_layer.utils.uri.UriUtils;
import net.styleru.ikomarov.presentation_layer.view.base.PresenterFragment;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.drawer.IDrawerView;
import net.styleru.ikomarov.presentation_layer.view.section_groups_list.SectionGroupsListFragment;
import net.styleru.ikomarov.presentation_layer.view_holder.notebooks_list.NotebookViewHolder;

import java.io.File;
import java.util.List;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

/**
 * Created by i_komarov on 09.03.17.
 */

public class NotebooksListFragment extends PresenterFragment<INotebooksListView, NotebooksListPresenter> implements INotebooksListView, PaginationListener.Callback, NotebookViewHolder.InteractionCallbacks {

    private static final int REQUEST_WRITE_EXT = 9910;

    private RecyclerView notebooksList;

    private NotebooksAdapter adapter;

    private PaginationListener paginationListener;

    public static Fragment newInstance() {
        Fragment fragment = new NotebooksListFragment();

        return fragment;
    }

    @Override
    public void onListStateChanged(State state) {
        getPresenter().loadNotebooks(state.getOffset(), state.getLimit());
    }

    @Override
    public void onNotebookClicked(int position, NotebookViewObject item) {
        getFragmentManager().beginTransaction()
                .replace(R.id.activity_drawer_fragment_container, SectionGroupsListFragment.newInstance(item.getId(), item.getName(), SectionGroupsListFragment.ContentState.ROOT))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onNotebookAction(int position, NotebookViewObject item, MenuItem menuItem) {
        switch(menuItem.getItemId()) {
            case R.id.action_extended_content: {
                String webUrl = UriUtils.safeTrimSpaces(item.getBrowserUrl());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(webUrl));
                startActivity(intent);
                break;
            }
            case R.id.action_lock: {

                break;
            }
            case R.id.action_unlock: {

                break;
            }
            case R.id.action_enable_teachers_only_section_group: {

                break;
            }
            case R.id.action_report_create: {
                getPresenter().createReport(item.getId(), !ensureReadExt());
                break;
            }
        }
    }

    @Override
    public void onReportCreated(String path) {
        Snackbar pathSnack = Snackbar.make(getView(), getResources().getString(R.string.notification_report_created), Snackbar.LENGTH_INDEFINITE);
        pathSnack.setAction(R.string.action_open, (view) -> {
            Intent intent = newPdfIntent(path);
            if(getActivity().getPackageManager().queryIntentActivities(intent, 0).size() > 0) {
                startActivity(intent);
            } else {
                showError(R.string.notification_no_pdf_viewer);
            }
        });
        pathSnack.setActionTextColor(getResources().getColor(R.color.colorAccent));
        pathSnack.show();
    }

    @Override
    public void addNotebooks(List<NotebookViewObject> items) {
        adapter.add(items);
    }

    @Override
    public void showError(@StringRes int error) {
        Snackbar errorSnack = Snackbar.make(getView(), getResources().getString(error), Snackbar.LENGTH_INDEFINITE);
        errorSnack.setAction(R.string.action_ok, (view) -> errorSnack.dismiss());
        errorSnack.setActionTextColor(getResources().getColor(R.color.colorAccent));
        errorSnack.show();
    }

    @Override
    public void showConnectionFailedIndicator() {
        showError(R.string.error_connection_lost);
    }

    @Override
    public void showOperationDelayedDialog() {
        showError(R.string.notification_operation_delayed);
    }

    @Override
    public void showNoCachedDataPlug() {
        showError(R.string.notification_no_cached_data);
    }

    @Override
    public void showNoDataPlug() {
        showError(R.string.notification_no_data);
    }

    @Override
    public void showNoPermissionsDialog() {
        showError(R.string.notification_no_permissions);
    }

    @Override
    public void showAuthorizationDialog() {
        ((IDrawerView) getActivity()).showAuthenticationActivity();
    }

    @Override
    protected void bindUserInterface(View view) {
        paginationListener = new PaginationListener(this);
        notebooksList = (RecyclerView) view.findViewById(R.id.fragment_notebooks_list_recycler_view);
        notebooksList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new NotebooksAdapter(SortingStrategyType.LAST_MODIFIED_DESCENDANCE);
        adapter.registerCallbacks(this);
        notebooksList.setAdapter(adapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.title_notebooks_list);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle("");
        notebooksList.setOnScrollListener(paginationListener);
        if(adapter.getItemCount() == 0) {
            getPresenter().loadNotebooks(0, paginationListener.getLimit());
        }
    }

    @Override
    public void onPause() {
        notebooksList.setOnScrollListener(null);
        super.onPause();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if(adapter != null) {
            adapter.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] resultCodes) {
        super.onRequestPermissionsResult(requestCode, permissions, resultCodes);
        if(requestCode == REQUEST_WRITE_EXT && resultCodes[0] == PERMISSION_GRANTED) {
            getPresenter().onExternalStoragePermissionsGranted();
        }
    }

    private Intent newPdfIntent(String path) {
        Uri uri = Uri.fromFile(new File(path));
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.setDataAndType(uri, "application/pdf");
        return intent;
    }

    private boolean ensureReadExt() {
        if(ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PERMISSION_GRANTED) {
            return true;
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_EXT);
            return false;
        }

        return true;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_notebooks_list;
    }

    @Override
    public INotebooksListView getViewInterface() {
        return this;
    }

    @Override
    protected RetainContainer.Factory<NotebooksListPresenter> providePresenterFactory() {
        ISingletonProvider provider = (ISingletonProvider) getActivity().getApplication();
        INotebooksRepository repository = provider.notebooksRepository().provideRepository();
        IReportRepositoryFacade reportRepository = provider.reportReporitoryFacade().provideReportRepositoryFacade();
        return new NotebooksListPresenter.Factory(
                new NotebooksGetUseCase(
                        repository,
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ),
                new PDFReportCreateUseCase(
                        reportRepository,
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ),
                provider.errorTranslation().provideOneNoteErrorCodeTranslator(),
                provider.analytics().provideScreenAnalytics(),
                provider.logging().provideLoggingManager()
        );
    }
}
