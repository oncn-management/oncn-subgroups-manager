package net.styleru.ikomarov.domain_layer.di.endpoints.notebooks;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.services.utils.NotebooksRecordsCache;
import net.styleru.ikomarov.data_layer.services.utils.IMemCache;

/**
 * Created by i_komarov on 14.03.17.
 */

public class NotebooksCacheModule {

    private final Object lock = new Object();

    private final INotebooksEnvironmentComponent environmentComponent;

    private volatile IMemCache cache;

    public NotebooksCacheModule(INotebooksEnvironmentComponent environmentComponent) {
        this.environmentComponent = environmentComponent;
    }

    @NonNull
    public IMemCache prodiveCache() {
        IMemCache localInstance = cache;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = cache;
                if(localInstance == null) {
                    localInstance = cache = new NotebooksRecordsCache(environmentComponent.local().provideService());
                }
            }
        }

        return localInstance;
    }
}
