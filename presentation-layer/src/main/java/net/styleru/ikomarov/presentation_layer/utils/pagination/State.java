package net.styleru.ikomarov.presentation_layer.utils.pagination;

/**
 * Created by i_komarov on 14.03.17.
 */

public class State {

    private final int offset;
    private final int limit;

    public State(int offset, int limit) {
        this.offset = offset;
        this.limit = limit;
    }

    public int getOffset() {
        return offset;
    }

    public int getLimit() {
        return limit;
    }
}
