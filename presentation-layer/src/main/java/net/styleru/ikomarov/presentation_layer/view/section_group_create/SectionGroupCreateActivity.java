package net.styleru.ikomarov.presentation_layer.view.section_group_create;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;

import net.styleru.ikomarov.domain_layer.use_cases.section_groups.NestedSectionGroupCreateUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.section_groups.SectionGroupCreateUseCase;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.application.ISingletonProvider;
import net.styleru.ikomarov.presentation_layer.presenter.section_group_create.SectionGroupCreatePresenter;
import net.styleru.ikomarov.presentation_layer.view.base.PresenterActivity;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;

/**
 * Created by i_komarov on 21.03.17.
 */

public class SectionGroupCreateActivity extends PresenterActivity<ISectionGroupCreateView, SectionGroupCreatePresenter> implements ISectionGroupCreateView {

    public static final String KEY_RESULT_IDENTIFIER = SectionGroupCreateActivity.class.getCanonicalName() + "." + "CREATED_RESOURCE_IDENTIFIER";
    public static final String KEY_NOTEBOOK_ID = ActivityState.KEY_NOTEBOOK_ID;
    public static final String KEY_NOTEBOOK_NAME = ActivityState.KEY_NOTEBOOK_NAME;
    public static final String KEY_SECTION_GROUP_ID = ActivityState.KEY_SECTION_GROUP_ID;
    public static final String KEY_SECTION_GROUP_NAME = ActivityState.KEY_SECTION_GROUP_NAME;

    private Toolbar toolbar;

    private AppCompatEditText notebookNameHolder;
    private AppCompatEditText sectionGroupNameHolder;
    private AppCompatButton createSectionGroupButton;

    private MaterialDialog progressDialog;
    private MaterialDialog notificationDialog;

    private ActivityState state;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        state = new ActivityState(getIntent(), savedInstanceState);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_group_create);
        bindViewComponents();
    }

    @Override
    public void onSectionGroupCreated(String sectionGroupId) {
        progressDialog.dismiss();
        Intent intent = new Intent();
        intent.putExtra(KEY_RESULT_IDENTIFIER, sectionGroupId);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void showNotification(@StringRes int notification) {
        progressDialog.dismiss();
        notificationDialog.setContent(notification);
        notificationDialog.show();
    }

    @Override
    public void showError(@StringRes int error) {
        progressDialog.dismiss();
        notificationDialog.setTitle(R.string.error_title);
        notificationDialog.setContent(error);
        notificationDialog.show();
    }

    @Override
    public void showConnectionFailedIndicator() {
        showError(R.string.error_connection_lost);
    }

    @Override
    public void showOperationDelayedDialog() {
        showNotification(R.string.notification_operation_delayed);
        new Handler().postDelayed(() -> {
            notificationDialog.dismiss();
            setResult(RESULT_CANCELED);
            finish();
        }, 2500);
    }

    @Override
    public void showNoCachedDataPlug() {
        showNotification(R.string.notification_no_cached_data);
    }

    @Override
    public void showNoDataPlug() {
        showNotification(R.string.notification_no_data);
    }

    @Override
    public void showNoPermissionsDialog() {
        showError(R.string.notification_no_permissions);
    }

    @Override
    public void showAuthorizationDialog() {
        //This is not a very clean workaround for restarting session if does not present.
        //On start(restart/resume/other events), the root Activity instance would check session again and
        //start authorization process if needed
        progressDialog.dismiss();
        notificationDialog.dismiss();
        finish();
    }

    @Override
    protected void handleActivityResult(int requestCode, int resultCode, Intent data) {
        //Just ignore this as no result is expected
    }

    @Override
    protected RetainContainer.Factory<SectionGroupCreatePresenter> providePresenterFactory() {
        ISingletonProvider provider = (ISingletonProvider) getApplication();
        SectionGroupCreateUseCase sectionGroupCreate = state.isInRoot() ?
                new SectionGroupCreateUseCase(
                        new SectionGroupCreateBroadcastFactory(getApplicationContext()),
                        provider.jobEnvironment().provideJobManager(),
                        provider.sectionGroupsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ) :
                new NestedSectionGroupCreateUseCase(
                        new SectionGroupCreateBroadcastFactory(getApplicationContext()),
                        provider.jobEnvironment().provideJobManager(),
                        provider.sectionGroupsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                );

        return new SectionGroupCreatePresenter.Factory(
                state.getId(),
                sectionGroupCreate,
                provider.errorTranslation().provideOneNoteErrorCodeTranslator(),
                provider.logging().provideLoggingManager()
        );
    }

    @Override
    protected ISectionGroupCreateView getView() {
        return this;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home : {
                setResult(RESULT_CANCELED);
                finish();
                return true;
            }
            default: {
                return super.onOptionsItemSelected(item);
            }
        }
    }

    private void bindViewComponents() {
        toolbar = (Toolbar) findViewById(R.id.activity_section_group_create_toolbar);
        notebookNameHolder = (AppCompatEditText) findViewById(R.id.activity_section_group_create_notebook_title);
        sectionGroupNameHolder = (AppCompatEditText) findViewById(R.id.activity_section_group_create_subgroup_title);
        createSectionGroupButton = (AppCompatButton) findViewById(R.id.activity_section_group_create_create_subgroup_button);

        toolbar.setTitle(R.string.title_section_group_create);
        toolbar.setSubtitle("");
        toolbar.setNavigationIcon(R.drawable.ic_clear_white_24dp);

        setSupportActionBar(toolbar);

        notebookNameHolder.setText(state.getName());
        createSectionGroupButton.setOnClickListener(this::onCreateSectionGroupButtonClicked);

        notificationDialog = new MaterialDialog.Builder(this)
                .cancelable(false)
                .canceledOnTouchOutside(false)
                .neutralText(R.string.action_ok)
                .onNeutral((dialog, action) -> dialog.dismiss())
                .build();

        progressDialog = new MaterialDialog.Builder(this)
                .content(R.string.notification_operation_is_in_progress)
                .progress(true, 0)
                .build();
    }

    private void onCreateSectionGroupButtonClicked(View view) {
        if(!TextUtils.isEmpty(sectionGroupNameHolder.getText())) {
            getPresenter().createSectionGroup(sectionGroupNameHolder.getText().toString());
            progressDialog.show();
        } else {
            showError(R.string.error_content_empty);
        }
    }

    private static final class ActivityState {

        private static final String PREFIX = SectionGroupCreateActivity.class.getCanonicalName() + ".";

        private static final String KEY_NOTEBOOK_ID = PREFIX + "NOTEBOOK_ID";
        private static final String KEY_NOTEBOOK_NAME = PREFIX + "NOTEBOOK_NAME";
        private static final String KEY_SECTION_GROUP_ID = PREFIX + "SECTION_GROUP_ID";
        private static final String KEY_SECTION_GROUP_NAME = PREFIX + "SECTION_GROUP_NAME";

        private String id;

        private String name;

        private boolean isInRoot;

        ActivityState(Intent intent, Bundle savedInstanceState) {
            Bundle data;
            if(intent != null && null != (data = intent.getExtras())) {
                if(data.containsKey(KEY_NOTEBOOK_ID) && data.containsKey(KEY_NOTEBOOK_NAME)) {
                    this.id = data.getString(KEY_NOTEBOOK_ID);
                    this.name = data.getString(KEY_NOTEBOOK_NAME);
                    this.isInRoot = true;
                } else if(data.containsKey(KEY_SECTION_GROUP_ID) && data.containsKey(KEY_SECTION_GROUP_NAME)) {
                    this.id = data.getString(KEY_SECTION_GROUP_ID);
                    this.name = data.getString(KEY_SECTION_GROUP_NAME);
                    this.isInRoot = false;
                }
            } else if(null != (data = savedInstanceState)) {
                if(data.containsKey(KEY_NOTEBOOK_ID) && data.containsKey(KEY_NOTEBOOK_NAME)) {
                    this.id = data.getString(KEY_NOTEBOOK_ID);
                    this.name = data.getString(KEY_NOTEBOOK_NAME);
                    this.isInRoot = true;
                } else if(data.containsKey(KEY_SECTION_GROUP_ID) && data.containsKey(KEY_SECTION_GROUP_NAME)) {
                    this.id = data.getString(KEY_SECTION_GROUP_ID);
                    this.name = data.getString(KEY_SECTION_GROUP_NAME);
                    this.isInRoot = false;
                }
            } else {
                throw new IllegalStateException("Empty intent and savedInstanceState found (sic!)");
            }
        }

        public boolean isInRoot() {
            return this.isInRoot;
        }

        public String getId() {
            return this.id;
        }

        public String getName() {
            return name;
        }
    }
}
