package net.styleru.ikomarov.domain_layer.mapping.section_groups;

import net.styleru.ikomarov.data_layer.entities.remote.section_groups.SectionGroupRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class RemoteSectionGroupMapper implements Function<SectionGroupRemoteEntity, SectionGroupDTO> {
    @Override
    public SectionGroupDTO apply(SectionGroupRemoteEntity entity) throws Exception {
        return SectionGroupDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getCreatedBy(),
                entity.getCreatedTime(),
                entity.getLastModifiedBy(),
                entity.getLastModifiedTime(),
                entity.getSectionGroupsUrl(),
                entity.getSectionsUrl()
        );
    }
}
