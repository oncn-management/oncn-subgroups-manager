package net.styleru.ikomarov.data_layer.entities.remote.users;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import static net.styleru.ikomarov.data_layer.mapping.remote.users.UsersRemoteEntityMapping.*;

/**
 * Created by i_komarov on 24.02.17.
 */

public class UserRemoteEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_BUSINESS_PHONES)
    private List<String> businessPhones;
    @SerializedName(FIELD_DISPLAY_NAME)
    private String displayName;
    @SerializedName(FIELD_GIVEN_NAME)
    private String givenName;
    @SerializedName(FIELD_JOB_TITLE)
    private String jobTitle;
    @SerializedName(FIELD_MAIL)
    private String mail;
    @SerializedName(FIELD_MOBILE_PHONE)
    private String mobilePhone;
    @SerializedName(FIELD_OFFICE_LOCATION)
    private String officeLocation;
    @SerializedName(FIELD_PREFERRED_LANGUAGE)
    private String preferredLanguage;
    @SerializedName(FIELD_LAST_NAME)
    private String lastName;
    @SerializedName(FIELD_USER_PRINCIPAL_NAME)
    private String principalName;

    private UserRemoteEntity() {

    }

    public String getId() {
        return id;
    }

    public List<String> getBusinessPhones() {
        return businessPhones;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getGivenName() {
        return givenName;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public String getMail() {
        return mail;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public String getOfficeLocation() {
        return officeLocation;
    }

    public String getPreferredLanguage() {
        return preferredLanguage;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPrincipalName() {
        return principalName;
    }

    @Override
    public String toString() {
        return "UserRemoteEntity{" +
                "id='" + id + '\'' +
                ", businessPhones=" + businessPhones +
                ", displayName='" + displayName + '\'' +
                ", givenName='" + givenName + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", mail='" + mail + '\'' +
                ", mobilePhone='" + mobilePhone + '\'' +
                ", officeLocation='" + officeLocation + '\'' +
                ", preferredLanguage='" + preferredLanguage + '\'' +
                ", lastName='" + lastName + '\'' +
                ", principalName='" + principalName + '\'' +
                '}';
    }
}
