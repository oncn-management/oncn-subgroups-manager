package net.styleru.ikomarov.domain_layer.repository.base;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.contracts.response.DataResponseMapping;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.mapping.remote.extras.ErrorRemoteEntityMapping;
import net.styleru.ikomarov.domain_layer.contracts.policies.CachePolicy;

import java.io.IOException;
import java.nio.charset.Charset;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

/**
 * Created by i_komarov on 15.03.17.
 */

public abstract class AbstractRepository {

    private Gson gson;

    protected AbstractRepository() {
        this.gson = new GsonBuilder().registerTypeAdapter(DataResponse.class, new DataResponse.Deserializer())
                .create();
    }

    protected final boolean shouldThrowOnNoCachedResourcesFound(CachePolicy policy) {
        return policy == CachePolicy.CACHE_ONLY;
    }

    protected final <T> Function<Response<DataResponse<T>>, ObservableSource<DataResponse<T>>> verifyResponse(Class<T> clazz, final Integer... successCodes) {
        return response -> {
            boolean fitsConditions = false;

            for(int i = 0; i < successCodes.length; i++) {
                if(response.code() == successCodes[i]) {
                    fitsConditions = true;
                    break;
                }
            }

            if(fitsConditions && response.body() != null) {
                return Observable.just(response.body());
            } else if(fitsConditions) {
                return Observable.just(DataResponse.<T>emptyRequestResult());
            } else if(response.body() != null && response.body().containsError()) {
                return Observable.error(response.body().getError());
            } else {
                try {
                    ExceptionBundle oneNoteError;
                    if(null != (oneNoteError = tryParseOneNoteException(parseResponseBody(response.errorBody())))) {
                        return Observable.error(oneNoteError);
                    }
                } catch (Exception e) {

                }

                if(response.code() == HTTP_UNAUTHORIZED) {
                    return Observable.error(new ExceptionBundle(ExceptionBundle.Reason.NO_SESSION));
                } else {
                    ExceptionBundle error = new ExceptionBundle(ExceptionBundle.Reason.HTTP_BAD_CODE);
                    error.addIntExtra(ExceptionBundle.HttpErrorContract.KEY_CODE, response.code());
                    error.addStringExtra(ExceptionBundle.HttpErrorContract.KEY_MESSAGE, response.message());
                    return Observable.error(error);
                }
            }
        };
    }

    protected final <T> Observable<T> raiseNoResourcesException(boolean shouldThrowOnNoResourcesFound) {
        return shouldThrowOnNoResourcesFound ? Observable.error(new ExceptionBundle(ExceptionBundle.Reason.NO_CACHED_RESOURCES)) :
                Observable.empty();
    }

    private ExceptionBundle tryParseOneNoteException(String responseJson) throws IOException {
        JsonObject json = gson.toJsonTree(responseJson).getAsJsonObject();
        ExceptionBundle error = new ExceptionBundle(ExceptionBundle.Reason.ONE_NOTE_ERROR);
        JsonObject errorJson = json.get(DataResponseMapping.FIELD_ERROR).getAsJsonObject();
        error.addIntExtra(ExceptionBundle.OneNoteErrorContract.KEY_CODE, errorJson.get(ErrorRemoteEntityMapping.FIELD_CODE).getAsInt());
        error.addStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_MESSAGE, errorJson.get(ErrorRemoteEntityMapping.FIELD_MESSAGE).getAsString());
        error.addStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_API_URL, errorJson.get(ErrorRemoteEntityMapping.FIELD_API_URL).getAsString());
        return error;
    }

    private String parseResponseBody(ResponseBody responseBody) throws IOException {
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE);
        Buffer buffer = source.buffer();

        Charset charset = Charset.forName("UTF-8");
        return buffer.readString(charset);
    }
}
