package net.styleru.ikomarov.presentation_layer.view.drawer;

import android.support.annotation.StringRes;

import net.styleru.ikomarov.presentation_layer.view.base.IView;

/**
 * Created by i_komarov on 18.02.17.
 */

public interface IDrawerView extends IView {

    void showError(@StringRes int error);

    void showConnectionFailedIndicator();

    void showAuthenticationActivity();

    void setupUI();
}
