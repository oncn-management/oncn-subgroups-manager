package net.styleru.ikomarov.data_layer.entities.session;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.session.AzureTokenMapping.*;

/**
 * Created by i_komarov on 09.03.17.
 */

public class AzureToken {

    @SerializedName(FIELD_TOKEN_TYPE)
    private String type;
    @SerializedName(FIELD_SCOPE)
    private String scope;
    @SerializedName(FIELD_EXPIRES_ON)
    private String expiresOn;
    @SerializedName(FIELD_NOT_BEFORE)
    private String notBefore;
    @SerializedName(FIELD_EXPIRES_IN)
    private String expiresIn;
    @SerializedName(FIELD_RESOURCE)
    private String resource;
    @SerializedName(FIELD_ACCESS_TOKEN)
    private String accessToken;
    @SerializedName(FIELD_REFRESH_TOKEN)
    private String refreshToken;
    @SerializedName(FIELD_ID_TOKEN)
    private String idToken;

    public AzureToken(String type, String scope, String expiresOn, String notBefore, String expiresIn, String resource, String accessToken, String refreshToken, String idToken) {
        this.type = type;
        this.scope = scope;
        this.expiresOn = expiresOn;
        this.notBefore = notBefore;
        this.expiresIn = expiresIn;
        this.resource = resource;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.idToken = idToken;
    }

    public String getType() {
        return type;
    }

    public String getScope() {
        return scope;
    }

    public String getExpiresOn() {
        return expiresOn;
    }

    public String getNotBefore() {
        return notBefore;
    }

    public String getExpiresIn() {
        return expiresIn;
    }

    public String getResource() {
        return resource;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public String getIdToken() {
        return idToken;
    }
}
