package net.styleru.ikomarov.data_layer.mapping.local.sections;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.entities.local.sections.SectionLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by i_komarov on 26.02.17.
 */

public class SectionsLocalEntityMapping implements ILocalMapping<SectionLocalEntity> {

    @NonNull
    public static final String TABLE = "sections";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_NOTEBOOK_ID = "notebook_id";

    @NonNull
    public static final String COLUMN_NAME = "NAME";

    @NonNull
    public static final String COLUMN_SELF = "SELF";

    @NonNull
    public static final String COLUMN_CREATED_BY = "CREATED_BY";

    @NonNull
    public static final String COLUMN_CREATED_TIME = "CREATED_TIME";

    @NonNull
    public static final String COLUMN_LAST_MODIFIED_BY = "LAST_MODIFIED_BY";

    @NonNull
    public static final String COLUMN_LAST_MODIFIED_TIME = "LAST_MODIFIED_TIME";

    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_NOTEBOOK_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_NOTEBOOK_ID;
    public static final String COLUMN_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_NAME;
    public static final String COLUMN_SELF_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SELF;
    public static final String COLUMN_CREATED_BY_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_CREATED_BY;
    public static final String COLUMN_CREATED_TIME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_CREATED_TIME;
    public static final String COLUMN_LAST_MODIFIED_BY_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_MODIFIED_BY;
    public static final String COLUMN_LAST_MODIFIED_TIME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_MODIFIED_TIME;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    private static SimpleDateFormat sqlDf = new SimpleDateFormat(DateTimeContract.SQL_COMPARE_DATE_FORMAT, Locale.getDefault());

    public SectionsLocalEntityMapping() {

    }

    public static List<String> getInitializationQueries() {
        return Collections.singletonList(getCreateTableQuery());
    }

    private static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE + "(" +
                    COLUMN_ID + " TEXT PRIMARY KEY NOT NULL," +
                    COLUMN_NOTEBOOK_ID + " TEXT NOT NULL," +
                    COLUMN_NAME + " TEXT NOT NULL," +
                    COLUMN_SELF + " TEXT NOT NULL," +
                    COLUMN_CREATED_BY + " TEXT NOT NULL," +
                    COLUMN_CREATED_TIME + " TIMESTAMP DEFAULT(STRFTIME(" + DateTimeContract.SQL_DATE_FORMAT + "," + DateTimeContract.SQL_DATE_CURRENT + "))," +
                    COLUMN_LAST_MODIFIED_BY + " TEXT NOT NULL," +
                    COLUMN_LAST_MODIFIED_TIME + " TIMESTAMP DEFAULT(STRFTIME(" + DateTimeContract.SQL_DATE_FORMAT + "," + DateTimeContract.SQL_DATE_CURRENT + "))" +
                ");";
    }

    @Override
    public SectionLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException, ParseException {
        if(cursor.moveToFirst()) {
            return SectionLocalEntity.newInstance(
                    cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NOTEBOOK_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_SELF)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_CREATED_BY)),
                    sqlDf.parse(cursor.getString(cursor.getColumnIndex(COLUMN_CREATED_TIME))),
                    cursor.getString(cursor.getColumnIndex(COLUMN_LAST_MODIFIED_BY)),
                    sqlDf.parse(cursor.getString(cursor.getColumnIndex(COLUMN_LAST_MODIFIED_TIME)))
            );
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<SectionLocalEntity> listFromCursor(Cursor cursor) throws ParseException {
        List<SectionLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                entities.add(
                        SectionLocalEntity.newInstance(
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_NOTEBOOK_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_NAME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_SELF)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_CREATED_BY)),
                                sqlDf.parse(cursor.getString(indexCache.indexFor(cursor, COLUMN_CREATED_TIME))),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_LAST_MODIFIED_BY)),
                                sqlDf.parse(cursor.getString(indexCache.indexFor(cursor, COLUMN_LAST_MODIFIED_TIME)))
                        )
                );
            } while(cursor.moveToNext());
        }

        return entities;
    }
}
