package net.styleru.ikomarov.domain_layer.use_cases.section_groups;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.repository.section_groups.ISectionGroupsRepository;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 07.04.17.
 */

public class NestedSectionGroupsGetUseCase extends SectionGroupsGetUseCase {

    public NestedSectionGroupsGetUseCase(@NonNull ISectionGroupsRepository repository, @NonNull Scheduler executeScheduler, @NonNull Scheduler postScheduler) {
        super(repository, executeScheduler, postScheduler);
    }

    @Override
    public <T> void execute(Function<List<SectionGroupDTO>, List<T>> mapper, Callbacks<T> callbacks, String sectionGroupId, int offset, int limit) {
        execute(repository.getListNested(sectionGroupId, offset, limit), mapper, callbacks);
    }

    @Override
    public <T> void execute(Function<List<SectionGroupDTO>, List<T>> mapper, Callbacks<T> callbacks, String sectionGroupId, int offset, int limit, String nameSubstring) {
        execute(repository.getListNested(sectionGroupId, offset, limit, nameSubstring), mapper, callbacks);
    }
}
