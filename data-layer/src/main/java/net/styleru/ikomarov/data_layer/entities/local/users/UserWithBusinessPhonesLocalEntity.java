package net.styleru.ikomarov.data_layer.entities.local.users;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 28.02.17.
 */

public class UserWithBusinessPhonesLocalEntity {

    @NonNull
    private UserLocalEntity user;
    @NonNull
    private List<UserBusinessPhoneLocalEntity> businessPhones;

    private UserWithBusinessPhonesLocalEntity(UserLocalEntity user) {
        this.user = user;
        this.businessPhones = new ArrayList<>();
    }

    public UserWithBusinessPhonesLocalEntity(UserLocalEntity user, List<UserBusinessPhoneLocalEntity> businessPhones) {
        this.user = user;
        this.businessPhones = businessPhones;
    }

    @NonNull
    public UserLocalEntity getUser() {
        return user;
    }

    @NonNull
    public List<UserBusinessPhoneLocalEntity> getBusinessPhones() {
        return businessPhones;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserWithBusinessPhonesLocalEntity that = (UserWithBusinessPhonesLocalEntity) o;

        if (!user.equals(that.user)) return false;
        return businessPhones.equals(that.businessPhones);

    }

    @Override
    public int hashCode() {
        int result = user.hashCode();
        result = 31 * result + businessPhones.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "UserWithBusinessPhonesLocalEntity{" +
                "user=" + user +
                ", businessPhones=" + businessPhones +
                '}';
    }

    public static class Builder {

        private List<Long> phoneIds;

        private UserWithBusinessPhonesLocalEntity entity;

        public Builder(UserLocalEntity user) {
            this.phoneIds = new ArrayList<>();
            this.entity = new UserWithBusinessPhonesLocalEntity(user);
        }

        public Builder addPhone(UserBusinessPhoneLocalEntity phone) {
            if(!this.phoneIds.contains(phone.getId())) {
                this.phoneIds.add(phone.getId());
                this.entity.businessPhones.add(phone);
            }

            return this;
        }

        public UserWithBusinessPhonesLocalEntity build() {
            return this.entity;
        }
    }
}
