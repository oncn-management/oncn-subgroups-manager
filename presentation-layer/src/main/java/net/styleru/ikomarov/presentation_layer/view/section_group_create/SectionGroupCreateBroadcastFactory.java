package net.styleru.ikomarov.presentation_layer.view.section_group_create;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.jobs.base.IBroadcastFactory;
import net.styleru.ikomarov.domain_layer.jobs.section_groups.SectionGroupCreateJob;
import net.styleru.ikomarov.presentation_layer.BuildConfig;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.notifications.LocalNotificationsReceiver;

/**
 * Created by i_komarov on 21.04.17.
 */

public class SectionGroupCreateBroadcastFactory implements IBroadcastFactory<SectionGroupCreateJob.JobParams> {

    @NonNull
    private final Context context;

    @NonNull
    private final String sectionGroupCreatedPlaceholder;

    public SectionGroupCreateBroadcastFactory(@NonNull Context context) {
        this.context = context;
        this.sectionGroupCreatedPlaceholder = context.getResources().getString(R.string.notification_section_group_created);
    }

    @Override
    public Intent createBroadcast(SectionGroupCreateJob.JobParams params) {
        Intent intent = new Intent();
        intent.setAction(BuildConfig.LOCAL_NOTIFICATIONS_ACTION);

        Bundle extras = new Bundle();
        extras.putInt(LocalNotificationsReceiver.KEY_NOTIFICATION_TYPE, LocalNotificationsReceiver.NOTIFICATION_SECTION_GROUP);
        extras.putString(LocalNotificationsReceiver.KEY_NOTIFICATION_TEXT, String.format(sectionGroupCreatedPlaceholder, params.getBuilder().getName()));
        extras.putString(LocalNotificationsReceiver.KEY_NOTIFICATION_UUID, params.getId().toString());
        extras.putString(LocalNotificationsReceiver.KEY_RESOURCE_ID, params.getResourceId());

        intent.putExtras(extras);

        return intent;
    }
}
