package net.styleru.ikomarov.domain_layer.utils;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.functions.BiFunction;

/**
 * Created by i_komarov on 20.03.17.
 */

public class RxUtils {

    private RxUtils() {

    }

    public static class NullSafe<T> {

        @Nullable
        private final T actual;

        public NullSafe(@Nullable T actual) {
            this.actual = actual;
        }

        @NonNull
        public Boolean isNull() {
            return actual == null;
        }

        @Nullable
        public T value() {
            return actual;
        }
    }

    public static <T> ObservableTransformer<List<T>, List<T>> shuffle() {
        return (o) -> o.map(list -> {
            long seed = System.nanoTime();
            Collections.shuffle(list, new Random(seed));
            return list;
        });
    }

    public static <T> ObservableTransformer<List<T>, List<T>> accumulate() {
        return o -> o.reduce((List<T>) new ArrayList<T>(), (accumulator, chunk) -> {
            accumulator.addAll(chunk);
            return accumulator;
        })
        .toObservable();
    }
}
