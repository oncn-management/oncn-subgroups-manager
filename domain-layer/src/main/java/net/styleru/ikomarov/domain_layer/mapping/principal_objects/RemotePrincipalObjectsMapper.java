package net.styleru.ikomarov.domain_layer.mapping.principal_objects;

import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.03.17.
 */

public class RemotePrincipalObjectsMapper implements Function<List<PrincipalObjectRemoteEntity>, List<PrincipalObjectDTO>> {

    @Override
    public List<PrincipalObjectDTO> apply(List<PrincipalObjectRemoteEntity> principalObjectRemoteEntities) throws Exception {
        return Observable.fromIterable(principalObjectRemoteEntities)
                .map(new RemotePrincipalObjectMapper())
                .toList()
                .blockingGet();
    }
}
