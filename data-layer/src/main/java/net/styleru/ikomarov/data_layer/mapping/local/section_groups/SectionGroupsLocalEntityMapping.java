package net.styleru.ikomarov.data_layer.mapping.local.section_groups;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

/**
 * Created by i_komarov on 26.02.17.
 */

public class SectionGroupsLocalEntityMapping implements ILocalMapping<SectionGroupLocalEntity> {

    @NonNull
    public static final String TABLE = "section_groups";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_PARENT_NOTEBOOK_ID = "parent_n_id";

    @NonNull
    public static final String COLUMN_PARENT_SECTION_GROUP_ID = "parent_sg_id";

    @NonNull
    public static final String COLUMN_NAME = "NAME";

    @NonNull
    public static final String COLUMN_SELF = "SELF";

    @NonNull
    public static final String COLUMN_SECTION_GROUPS_URL = "SECTION_GROUPS_URL";

    @NonNull
    public static final String COLUMN_SECTIONS_URL = "SECTIONS_URL";

    @NonNull
    public static final String COLUMN_CREATED_BY = "CREATED_BY";

    @NonNull
    public static final String COLUMN_CREATED_TIME = "CREATED_TIME";

    @NonNull
    public static final String COLUMN_LAST_MODIFIED_BY = "LAST_MODIFIED_BY";

    @NonNull
    public static final String COLUMN_LAST_MODIFIED_TIME = "LAST_MODIFIED_TIME";

    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_PARENT_NOTEBOOK_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_PARENT_NOTEBOOK_ID;
    public static final String COLUMN_PARENT_SECTION_GROUP_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_PARENT_SECTION_GROUP_ID;
    public static final String COLUMN_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_NAME;
    public static final String COLUMN_SELF_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SELF;
    public static final String COLUMN_SECTION_GROUPS_URL_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SECTION_GROUPS_URL;
    public static final String COLUMN_SECTIONS_URL_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SECTIONS_URL;
    public static final String COLUMN_CREATED_BY_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_CREATED_BY;
    public static final String COLUMN_CREATED_TIME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_CREATED_TIME;
    public static final String COLUMN_LAST_MODIFIED_BY_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_MODIFIED_BY;
    public static final String COLUMN_LAST_MODIFIED_TIME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_MODIFIED_TIME;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    private static SimpleDateFormat sqlDf = new SimpleDateFormat(DateTimeContract.SQL_COMPARE_DATE_FORMAT, Locale.getDefault());

    public SectionGroupsLocalEntityMapping() {

    }

    public static List<String> getInitializationQueries() {
        return Collections.singletonList(getCreateTableQuery());
    }

    private static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE + "(" +
                    COLUMN_ID + " TEXT PRIMARY KEY NOT NULL," +
                    COLUMN_PARENT_NOTEBOOK_ID + " TEXT," +
                    COLUMN_PARENT_SECTION_GROUP_ID + " TEXT," +
                    COLUMN_NAME + " TEXT NOT NULL," +
                    COLUMN_SELF + " TEXT NOT NULL," +
                    COLUMN_SECTION_GROUPS_URL + " TEXT NOT NULL," +
                    COLUMN_SECTIONS_URL + " TEXT NOT NULL," +
                    COLUMN_CREATED_BY + " TEXT NOT NULL," +
                    COLUMN_CREATED_TIME + " TIMESTAMP DEFAULT(STRFTIME(" + DateTimeContract.SQL_DATE_FORMAT + "," + DateTimeContract.SQL_DATE_CURRENT + "))," +
                    COLUMN_LAST_MODIFIED_BY + " TEXT NOT NULL," +
                    COLUMN_LAST_MODIFIED_TIME + " TIMESTAMP DEFAULT(STRFTIME(" + DateTimeContract.SQL_DATE_FORMAT + "," + DateTimeContract.SQL_DATE_CURRENT + "))" +
                ");";
    }

    @Override
    public SectionGroupLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException, ParseException {
        if(cursor.moveToFirst()) {
            return SectionGroupLocalEntity.newInstance(
                    cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PARENT_NOTEBOOK_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PARENT_SECTION_GROUP_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_SELF)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_SECTION_GROUPS_URL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_SECTIONS_URL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_CREATED_BY)),
                    sqlDf.parse(cursor.getString(cursor.getColumnIndex(COLUMN_CREATED_TIME))),
                    cursor.getString(cursor.getColumnIndex(COLUMN_LAST_MODIFIED_BY)),
                    sqlDf.parse(cursor.getString(cursor.getColumnIndex(COLUMN_LAST_MODIFIED_TIME)))
            );
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<SectionGroupLocalEntity> listFromCursor(Cursor cursor) throws ParseException {
        List<SectionGroupLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                entities.add(
                        SectionGroupLocalEntity.newInstance(
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_PARENT_NOTEBOOK_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_PARENT_SECTION_GROUP_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_NAME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_SELF)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_SECTION_GROUPS_URL)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_SECTIONS_URL)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_CREATED_BY)),
                                sqlDf.parse(cursor.getString(indexCache.indexFor(cursor, COLUMN_CREATED_TIME))),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_LAST_MODIFIED_BY)),
                                sqlDf.parse(cursor.getString(indexCache.indexFor(cursor, COLUMN_LAST_MODIFIED_TIME)))
                        )
                );

            } while(cursor.moveToNext());
        }

        return entities;
    }
}
