package net.styleru.ikomarov.domain_layer.service.session;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 06.04.17.
 */

public enum SessionStatus implements Parcelable {
    ACTIVE(0x0000000),
    AUTHENTICATING(0x0000001),
    INACTIVE(0x0000002);

    private final int code;

    SessionStatus(int code) {
        this.code = code;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SessionStatus> CREATOR = new Creator<SessionStatus>() {
        @Override
        public SessionStatus createFromParcel(Parcel in) {
            int code = in.readInt();
            if(code == ACTIVE.code) {
                return ACTIVE;
            } else if(code == AUTHENTICATING.code) {
                return AUTHENTICATING;
            } else if(code == INACTIVE.code) {
                return INACTIVE;
            } else {
                throw new IllegalStateException("Unknown SessionStatus#code value found in Parcel while restoring");
            }
        }

        @Override
        public SessionStatus[] newArray(int size) {
            return new SessionStatus[size];
        }
    };
}
