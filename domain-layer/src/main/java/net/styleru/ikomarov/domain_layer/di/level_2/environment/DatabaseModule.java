package net.styleru.ikomarov.domain_layer.di.level_2.environment;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;

import net.styleru.ikomarov.data_layer.database.SQLiteFactory;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 13.03.17.
 */

public class DatabaseModule {

    private final Object lockDB = new Object();

    @NonNull
    private final IAppComponent component;

    @Nullable
    private volatile StorIOSQLite database;

    public DatabaseModule(@NonNull IAppComponent component) {
        this.component = component;
    }

    @NonNull
    public StorIOSQLite provideDatabase() {
        StorIOSQLite localInstance = database;
        if(localInstance == null) {
            synchronized (lockDB) {
                localInstance = database;
                if(localInstance == null) {
                    localInstance = database = SQLiteFactory.create(component.app().provideContext());
                }
            }
        }

        return localInstance;
    }
}
