package net.styleru.ikomarov.domain_layer.mapping.principal_objects;

import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.03.17.
 */

public class RemotePrincipalObjectMapper implements Function<PrincipalObjectRemoteEntity, PrincipalObjectDTO> {

    @Override
    public PrincipalObjectDTO apply(PrincipalObjectRemoteEntity entity) throws Exception {
        return PrincipalObjectDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getEmail(),
                entity.getDepartment(),
                entity.getPrincipalType()
        );
    }
}
