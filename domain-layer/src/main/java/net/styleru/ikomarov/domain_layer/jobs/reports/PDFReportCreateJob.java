package net.styleru.ikomarov.domain_layer.jobs.reports;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import net.styleru.ikomarov.domain_layer.facades.repository.IReportRepositoryFacade;
import net.styleru.ikomarov.domain_layer.jobs.base.IBroadcastFactory;

import java.util.UUID;

/**
 * Created by i_komarov on 21.04.17.
 */

@Deprecated
/**
 * This class is deprecated since there is no need for it to present, as the report can be created both online and offline
 * */
public class PDFReportCreateJob extends Job {

    private static final int PRIORITY = 1;

    private static final int INITIAL_BACKOFF = 1 * 1000;

    @NonNull
    private final IBroadcastFactory<JobParams> broadcastFactory;

    @NonNull
    private final IReportRepositoryFacade repository;

    @NonNull
    private final JobParams params;

    private PDFReportCreateJob(@NonNull IBroadcastFactory<JobParams> broadcastFactory,
                                 @NonNull IReportRepositoryFacade repository,
                                 @NonNull JobParams params) {

        super(new Params(PRIORITY).setRequiresNetwork(true).setPersistent(false));
        this.broadcastFactory = broadcastFactory;
        this.repository = repository;
        this.params = params;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        params.setFilePath(repository.createReport(params.getNotebookId()).blockingFirst());
        getApplicationContext().sendOrderedBroadcast(broadcastFactory.createBroadcast(params), null);
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.createExponentialBackoff(runCount, INITIAL_BACKOFF);
    }

    public static final class Factory {

        @NonNull
        private final IBroadcastFactory<JobParams> broadcastFactory;

        @NonNull
        private final IReportRepositoryFacade repository;

        public Factory(@NonNull IBroadcastFactory<JobParams> broadcastFactory, @NonNull IReportRepositoryFacade repository) {
            this.broadcastFactory = broadcastFactory;
            this.repository = repository;
        }

        public Job create(JobParams params) {
            return new PDFReportCreateJob(broadcastFactory, repository, params);
        }
    }

    public static final class JobParams {

        @NonNull
        private final UUID operationId;

        @NonNull
        private final String notebookId;

        @Nullable
        private String filePath;

        public JobParams(@NonNull UUID operationId, @NonNull String notebookId) {
            this.operationId = operationId;
            this.notebookId = notebookId;
        }

        @NonNull
        public UUID getOperationId() {
            return operationId;
        }

        @NonNull
        public String getNotebookId() {
            return notebookId;
        }

        @Nullable
        public String getFilePath() {
            return filePath;
        }

        public void setFilePath(@Nullable String filePath) {
            this.filePath = filePath;
        }
    }
}
