package net.styleru.ikomarov.presentation_layer.utils.ui;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.ikomarov.presentation_layer.R;

/**
 * Created by i_komarov on 05.04.17.
 */

public class ParticipantsChipFactory {

    @NonNull
    private final LayoutInflater inflater;

    public ParticipantsChipFactory(@NonNull Context context) {
        this.inflater = LayoutInflater.from(context);
    }

    public View create(ViewGroup parent, String displayName, View.OnClickListener removeClicks) {
        View chip = inflater.inflate(R.layout.view_chip, parent, false);

        ((AppCompatTextView) chip.findViewById(R.id.view_chip_text)).setText(displayName);
        chip.findViewById(R.id.view_chip_action_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.removeView(chip);
                removeClicks.onClick(v);
            }
        });

        return chip;
    }
}
