package net.styleru.ikomarov.data_layer.services.students;

import android.database.Cursor;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.ClassNotebooksLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.PrincipalObjectsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.students.StudentsLocalEntityNotebooksContractMapping;
import net.styleru.ikomarov.data_layer.mapping.utils.QueryBuilder;
import net.styleru.ikomarov.data_layer.services.base.DatabaseService;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsLocalStorageService extends DatabaseService implements IStudentsLocalStorageService {

    private ILocalMapping<PrincipalObjectLocalEntity> contractMapping;

    private StudentsLocalStorageService(StorIOSQLite sqlite) {
        super(sqlite);
        this.contractMapping = new StudentsLocalEntityNotebooksContractMapping();
    }

    @Override
    public Observable<PutResult> create(String notebookId, PrincipalObjectLocalEntity entity) {
        return wrapTransactionSynchronousOff(() -> {
            PutResult result = getSQLite().put().object(entity)
                    .prepare()
                    .executeAsBlocking();
            getSQLite().executeSQL()
                    .withQuery(RawQuery.builder().query(ClassNotebooksLocalEntityMapping.onInsertStudentExecuteQuery(notebookId, entity.getId())).build())
                    .prepare()
                    .executeAsBlocking();

            return result;
        });
    }

    @Override
    public Observable<PutResults<PrincipalObjectLocalEntity>> create(String notebookId, List<PrincipalObjectLocalEntity> entities) {
        return wrapTransactionSynchronousOff(() -> {
            PutResults<PrincipalObjectLocalEntity> results = getSQLite().put()
                    .objects(entities)
                    .prepare()
                    .executeAsBlocking();
            for(PrincipalObjectLocalEntity entity : entities) {
                getSQLite().executeSQL()
                        .withQuery(RawQuery.builder().query(ClassNotebooksLocalEntityMapping.onInsertStudentExecuteQuery(notebookId, entity.getId())).build())
                        .prepare()
                        .executeAsBlocking();
            }

            return results;
        });
    }

    @Override
    public Observable<List<PrincipalObjectLocalEntity>> read(String notebookId, int offset, int limit) {
        return wrap(() -> {
            Cursor cursor;

            cursor = getSQLite().get()
                    .cursor()
                    .withQuery(RawQuery.builder().query(ClassNotebooksLocalEntityMapping.getStudentsQuery(notebookId, offset, limit, PrincipalObjectsLocalEntityMapping.COLUMN_NAME, QueryBuilder.Order.ASC)).build())
                    .prepare()
                    .executeAsBlocking();

            try {
                return contractMapping.listFromCursor(cursor);
            } finally {
                cursor.close();
            }
        });
    }

    @Override
    public Observable<List<PrincipalObjectLocalEntity>> read(String notebookId, int offset, int limit, String nameSubstring) {
        return wrap(() -> {
            Cursor cursor;

            cursor = getSQLite().get()
                    .cursor()
                    .withQuery(RawQuery.builder().query(ClassNotebooksLocalEntityMapping.getStudentsQuery(notebookId, offset, limit, PrincipalObjectsLocalEntityMapping.COLUMN_NAME, QueryBuilder.Order.ASC, nameSubstring)).build())
                    .prepare()
                    .executeAsBlocking();

            try {
                return contractMapping.listFromCursor(cursor);
            } finally {
                cursor.close();
            }
        });
    }

    @Override
    public Observable<DeleteResult> delete(String notebookId, String studentId) {
        return wrapTransactionSynchronousOff(() -> {
            return getSQLite().delete()
                    .byQuery(DeleteQuery.builder().table(ClassNotebooksLocalEntityMapping.TABLE_STUDENTS_LINKING).where(ClassNotebooksLocalEntityMapping.COLUMN_STUDENT_ID + " = " + "\"" + studentId + "\"" + " AND " + ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"").build())
                    .prepare()
                    .executeAsBlocking();
        });
    }

    public static final class Factory {

        private Factory() {

        }

        public static IStudentsLocalStorageService create(StorIOSQLite sqlite) {
            return new StudentsLocalStorageService(sqlite);
        }
    }
}
