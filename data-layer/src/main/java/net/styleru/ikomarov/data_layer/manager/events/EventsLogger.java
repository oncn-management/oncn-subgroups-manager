package net.styleru.ikomarov.data_layer.manager.events;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 12.03.17.
 */

public class EventsLogger implements IEventsLogger {

    @NonNull
    private final FirebaseAnalytics analytics;

    public EventsLogger(Context context) {
        analytics = FirebaseAnalytics.getInstance(context.getApplicationContext() == null ? context : context.getApplicationContext());
    }

    @Override
    public void logException(Exception exception) {
        Log.e("DrawerPresenter#IEventLoggingManager",
                exception.getLocalizedMessage(),
                exception
        );
    }

    @Override
    public void logEvent(String type, Bundle data) {
        analytics.logEvent(type, data);
    }

    @Override
    public void logEvent(String clazz, String method, ExceptionBundle.Reason exceptionReason) {
        Log.e("DrawerPresenter#IEventLoggingManager",
                "class: " + clazz +
                        "\nmethod: " + method +
                        "\nreason:" + exceptionReason
        );
    }

    @Override
    public void logEvent(String clazz, String method, ExceptionBundle.Reason exceptionReason, Throwable throwable) {
        Log.e("DrawerPresenter#IEventLoggingManager",
                "class: " + clazz +
                        "\nmethod: " + method +
                        "\nreason: " + exceptionReason +
                        "\nthrowable: " + throwable.getLocalizedMessage()
        );
    }
}
