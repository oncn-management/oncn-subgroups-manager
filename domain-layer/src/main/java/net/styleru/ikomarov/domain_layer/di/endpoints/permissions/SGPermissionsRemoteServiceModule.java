package net.styleru.ikomarov.domain_layer.di.endpoints.permissions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.permissions.section_groups.ISGPermissionsRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.permissions.section_groups.SGPermissionsRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.ISectionGroupsRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.section_groups.SectionGroupsRemoteStorageService;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SGPermissionsRemoteServiceModule {

    private final Object lock = new Object();

    @NonNull
    private final IClientComponent clientComponent;

    @Nullable
    private volatile ISGPermissionsRemoteStorageService localService;

    public SGPermissionsRemoteServiceModule(@NonNull IClientComponent clientComponent) {
        this.clientComponent = clientComponent;
    }

    @NonNull
    public ISGPermissionsRemoteStorageService provideRemoteService() {
        ISGPermissionsRemoteStorageService localInstance = localService;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = localService;
                if(localInstance == null) {
                    localInstance = localService = SGPermissionsRemoteStorageService.Factory.create(clientComponent.clients().provideOneNoteHttpClient());
                }
            }
        }

        return localInstance;
    }
}
