package net.styleru.ikomarov.domain_layer.di.endpoints.students;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsEnvironmentComponent implements IStudentsEnvironmentComponent {

    @NonNull
    private final ICacheComponent cacheComponent;

    @NonNull
    private final StudentsLocalServiceModule localService;

    @NonNull
    private final StudentsRemoteServiceModule remoteService;

    @NonNull
    private final StudentsMappingModule mappingModule;

    public StudentsEnvironmentComponent(@NonNull IEnvironmentComponent environment, @NonNull IClientComponent client, @NonNull ICacheComponent cacheComponent) {
        this.cacheComponent = cacheComponent;
        this.localService = new StudentsLocalServiceModule(environment);
        this.remoteService = new StudentsRemoteServiceModule(client);
        this.mappingModule = new StudentsMappingModule();
    }

    @NonNull
    @Override
    public ICacheComponent cacheComponent() {
        return cacheComponent;
    }

    @NonNull
    @Override
    public StudentsLocalServiceModule localService() {
        return localService;
    }

    @NonNull
    @Override
    public StudentsRemoteServiceModule remoteService() {
        return remoteService;
    }

    @NonNull
    @Override
    public StudentsMappingModule mapping() {
        return mappingModule;
    }
}
