package net.styleru.ikomarov.data_layer.mapping.local.notebooks.queries;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.data_layer.mapping.utils.QueryBuilder;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.ClassNotebooksLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.PrincipalObjectsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.section_groups.SectionGroupsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.sections.SectionsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.StudentSectionsLocalEntityMapping;

import static net.styleru.ikomarov.data_layer.mapping.local.notebooks.queries.NotebookQueries.Contract.*;

/**
 * Created by i_komarov on 01.03.17.
 */

public class NotebookQueries {

    public static final class Contract {

        private static final String PREFIX_TEACHERS = "teachers";
        private static final String PREFIX_STUDENTS = "students";

        public static final String COLUMN_NOTEBOOK_ID = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + ClassNotebooksLocalEntityMapping.COLUMN_ID;
        public static final String COLUMN_NOTEBOOK_NAME = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_NAME;
        public static final String COLUMN_NOTEBOOK_IS_DEFAULT = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_IS_DEFAULT;
        public static final String COLUMN_NOTEBOOK_IS_SHARED = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_IS_SHARED;
        public static final String COLUMN_NOTEBOOK_CREATED_BY = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_CREATED_BY;
        public static final String COLUMN_NOTEBOOK_CREATED_TIME = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_CREATED_TIME;
        public static final String COLUMN_NOTEBOOK_LAST_MODIFIED_BY = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_LAST_MODIFIED_BY;
        public static final String COLUMN_NOTEBOOK_LAST_MODIFIED_TIME = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME;
        public static final String COLUMN_NOTEBOOK_WEB_URL = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_WEB_URL;
        public static final String COLUMN_NOTEBOOK_CLIENT_URL = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_CLIENT_URL;
        public static final String COLUMN_NOTEBOOK_SECTIONS_URL = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_SECTIONS_URL;
        public static final String COLUMN_NOTEBOOK_SECTION_GROUPS_URL = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_SECTION_GROUPS_URL;
        public static final String COLUMN_NOTEBOOK_SELF = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_SELF;
        public static final String COLUMN_NOTEBOOK_USER_ROLE = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_USER_ROLE;
        public static final String COLUMN_NOTEBOOK_HAS_TEACHER_ONLY_SECTION_GROUP = ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS + "_" + ClassNotebooksLocalEntityMapping.COLUMN_HAS_TEACHER_ONLY_SECTION_GROUP;

        public static final String COLUMN_SECTION_ID = SectionsLocalEntityMapping.TABLE + SectionsLocalEntityMapping.COLUMN_ID;
        public static final String COLUMN_SECTION_NAME = SectionsLocalEntityMapping.TABLE + "_" + SectionsLocalEntityMapping.COLUMN_NAME;
        public static final String COLUMN_SECTION_SELF = SectionsLocalEntityMapping.TABLE + "_" + SectionsLocalEntityMapping.COLUMN_SELF;
        public static final String COLUMN_SECTION_CREATED_BY = SectionsLocalEntityMapping.TABLE + "_" + SectionsLocalEntityMapping.COLUMN_CREATED_BY;
        public static final String COLUMN_SECTION_CREATED_TIME = SectionsLocalEntityMapping.TABLE + "_" + SectionsLocalEntityMapping.COLUMN_CREATED_TIME;
        public static final String COLUMN_SECTION_LAST_MODIFIED_BY = SectionsLocalEntityMapping.TABLE + "_" + SectionsLocalEntityMapping.COLUMN_LAST_MODIFIED_BY;
        public static final String COLUMN_SECTION_LAST_MODIFIED_TIME = SectionsLocalEntityMapping.TABLE + "_" + SectionsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME;

        public static final String COLUMN_SECTION_GROUP_ID = SectionGroupsLocalEntityMapping.TABLE + SectionGroupsLocalEntityMapping.COLUMN_ID;
        public static final String COLUMN_SECTION_GROUP_NAME = SectionGroupsLocalEntityMapping.TABLE + "_" + SectionGroupsLocalEntityMapping.COLUMN_NAME;
        public static final String COLUMN_SECTION_GROUP_SELF = SectionGroupsLocalEntityMapping.TABLE + "_" + SectionGroupsLocalEntityMapping.COLUMN_SELF;
        public static final String COLUMN_SECTION_GROUP_SECTIONS_URL = SectionGroupsLocalEntityMapping.TABLE + "_" + SectionGroupsLocalEntityMapping.COLUMN_SECTIONS_URL;
        public static final String COLUMN_SECTION_GROUP_SECTION_GROUPS_URL = SectionGroupsLocalEntityMapping.TABLE + "_" + SectionGroupsLocalEntityMapping.COLUMN_SECTION_GROUPS_URL;
        public static final String COLUMN_SECTION_GROUP_CREATED_BY = SectionGroupsLocalEntityMapping.TABLE + "_" + SectionGroupsLocalEntityMapping.COLUMN_CREATED_BY;
        public static final String COLUMN_SECTION_GROUP_CREATED_TIME = SectionGroupsLocalEntityMapping.TABLE + "_" + SectionGroupsLocalEntityMapping.COLUMN_CREATED_TIME;
        public static final String COLUMN_SECTION_GROUP_LAST_MODIFIED_BY = SectionGroupsLocalEntityMapping.TABLE + "_" + SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_BY;
        public static final String COLUMN_SECTION_GROUP_LAST_MODIFIED_TIME = SectionGroupsLocalEntityMapping.TABLE + "_" + SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME;

        public static final String COLUMN_TEACHER_ID = PREFIX_TEACHERS + PrincipalObjectsLocalEntityMapping.COLUMN_ID;
        public static final String COLUMN_TEACHER_TYPE = PREFIX_TEACHERS + "_" + PrincipalObjectsLocalEntityMapping.COLUMN_TYPE;

        public static final String COLUMN_STUDENT_ID = PREFIX_STUDENTS + PrincipalObjectsLocalEntityMapping.COLUMN_ID;
        public static final String COLUMN_STUDENT_TYPE = PREFIX_STUDENTS + "_" + PrincipalObjectsLocalEntityMapping.COLUMN_TYPE;

        public static final String COLUMN_STUDENT_SECTION_ID = StudentSectionsLocalEntityMapping.TABLE + StudentSectionsLocalEntityMapping.COLUMN_ID;
    }

    public static String onInsertStudentQuery(String notebookId, PrincipalObjectLocalEntity student) {
        return "INSERT INTO " + ClassNotebooksLocalEntityMapping.TABLE_STUDENTS_LINKING + "(" + "\'" + ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID + "\'" + "," + "\'" + ClassNotebooksLocalEntityMapping.COLUMN_STUDENT_ID + "\'" + ")" +
                " VALUES" + "(" + "\"" + notebookId + "\"" + "," + "\"" + student.getId() + "\"" + ");";
    }

    public static String onInsertTeacherQuery(String notebookId, PrincipalObjectLocalEntity teacher) {
        return "INSERT INTO " + ClassNotebooksLocalEntityMapping.TABLE_TEACHERS_LINKING + "(" + "\'" + ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID + "\'" + "," + "\'" + ClassNotebooksLocalEntityMapping.COLUMN_TEACHER_ID + "\'" + ")" +
                " VALUES" + "(" + "\"" + notebookId + "\"" + "," + "\"" + teacher.getId() + "\"" + ");";
    }

    public static String query(String notebookId) {
        return query(
                Integer.MIN_VALUE,
                Integer.MIN_VALUE,
                ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX + " = " + "\"" + notebookId + "\"",
                null
        );
    }

    public static String query(int offset, int limit) {
        return query(
                offset,
                limit,
                null,
                null
        );
    }

    public static String query(int offset, int limit, String querySubstring) {
        return query(
                offset,
                limit,
                ClassNotebooksLocalEntityMapping.COLUMN_NAME + " = \"" + querySubstring + "\" OR " + ClassNotebooksLocalEntityMapping.COLUMN_NAME + " LIKE \"" + querySubstring + "%\"",
                null
        );
    }

    private static String query(int offset, int limit, String where, String querySubstring) {
        QueryBuilder.SelectBuilder nestedQuery = QueryBuilder.selectQuery(ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS)
                .orderBy("datetime(" + ClassNotebooksLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME + ")", QueryBuilder.Order.DESC);

        if(limit != Integer.MIN_VALUE && offset != Integer.MIN_VALUE) {
                nestedQuery = nestedQuery.limit(offset, limit);
        }

        if(where != null) {
            nestedQuery = nestedQuery.where(where);
        }

        if(querySubstring != null) {
            nestedQuery = nestedQuery.where(
                    ClassNotebooksLocalEntityMapping.COLUMN_NAME + " = \"" + querySubstring + "\" OR " +
                    ClassNotebooksLocalEntityMapping.COLUMN_NAME + " LIKE \"" + querySubstring + "%\""
            );
        }

        String selectTeacherIdsQuery = QueryBuilder.selectQuery(PrincipalObjectsLocalEntityMapping.TABLE)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX)
                .where(PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX + " = " + ClassNotebooksLocalEntityMapping.COLUMN_TEACHER_ID_WITH_TEACHERS_LINKING_TABLE_PREFIX)
                .build();

        String selectTeacherTypesQuery = QueryBuilder.selectQuery(PrincipalObjectsLocalEntityMapping.TABLE)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_TYPE_WITH_TABLE_PREFIX, PrincipalObjectsLocalEntityMapping.COLUMN_TYPE_WITH_TABLE_PREFIX)
                .where(PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX + " = " + ClassNotebooksLocalEntityMapping.COLUMN_TEACHER_ID_WITH_TEACHERS_LINKING_TABLE_PREFIX)
                .build();

        String selectStudentIdsQuery = QueryBuilder.selectQuery(PrincipalObjectsLocalEntityMapping.TABLE)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX)
                .where(PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX + " = " + ClassNotebooksLocalEntityMapping.COLUMN_STUDENT_ID_WITH_STUDENTS_LINKING_TABLE_PREFIX)
                .build();

        String selectStudentTypesQuery = QueryBuilder.selectQuery(PrincipalObjectsLocalEntityMapping.TABLE)
                .withParam(PrincipalObjectsLocalEntityMapping.COLUMN_TYPE_WITH_TABLE_PREFIX, PrincipalObjectsLocalEntityMapping.COLUMN_TYPE_WITH_TABLE_PREFIX)
                .where(PrincipalObjectsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX + " = " + ClassNotebooksLocalEntityMapping.COLUMN_TEACHER_ID_WITH_TEACHERS_LINKING_TABLE_PREFIX)
                .build();

        return QueryBuilder.selectQuery(ClassNotebooksLocalEntityMapping.TABLE_NOTEBOOKS)
                .overrideTableWithNestedSelect(nestedQuery.build())
                //notebook params
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX, COLUMN_NOTEBOOK_ID)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_NAME_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_NAME)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_IS_DEFAULT_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_IS_DEFAULT)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_IS_SHARED_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_IS_SHARED)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_CREATED_BY_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_CREATED_BY)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_CREATED_TIME_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_CREATED_TIME)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_LAST_MODIFIED_BY_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_LAST_MODIFIED_BY)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_LAST_MODIFIED_TIME_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_LAST_MODIFIED_TIME)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_WEB_URL_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_WEB_URL)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_CLIENT_URL_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_CLIENT_URL)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_SECTIONS_URL_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_SECTIONS_URL)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_SECTION_GROUPS_URL_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_SECTION_GROUPS_URL)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_SELF_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_SELF)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_USER_ROLE_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_USER_ROLE)
                .withParam(ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_HAS_TEACHER_ONLY_SECTION_GROUP_WITH_TABLE_PREFIX, COLUMN_NOTEBOOK_HAS_TEACHER_ONLY_SECTION_GROUP)
                //section params
                .withParam(SectionsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, COLUMN_SECTION_ID)
                .withParam(SectionsLocalEntityMapping.COLUMN_NAME_WITH_TABLE_PREFIX, COLUMN_SECTION_NAME)
                .withParam(SectionsLocalEntityMapping.COLUMN_SELF_WITH_TABLE_PREFIX, COLUMN_SECTION_SELF)
                .withParam(SectionsLocalEntityMapping.COLUMN_CREATED_BY_WITH_TABLE_PREFIX, COLUMN_SECTION_CREATED_BY)
                .withParam(SectionsLocalEntityMapping.COLUMN_CREATED_TIME_WITH_TABLE_PREFIX, COLUMN_SECTION_CREATED_TIME)
                .withParam(SectionsLocalEntityMapping.COLUMN_LAST_MODIFIED_BY_WITH_TABLE_PREFIX, COLUMN_SECTION_LAST_MODIFIED_BY)
                .withParam(SectionsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME_WITH_TABLE_PREFIX, COLUMN_SECTION_LAST_MODIFIED_TIME)
                //section group params
                .withParam(SectionGroupsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, COLUMN_SECTION_GROUP_ID)
                .withParam(SectionGroupsLocalEntityMapping.COLUMN_NAME_WITH_TABLE_PREFIX, COLUMN_SECTION_GROUP_NAME)
                .withParam(SectionGroupsLocalEntityMapping.COLUMN_SELF_WITH_TABLE_PREFIX, COLUMN_SECTION_GROUP_SELF)
                .withParam(SectionGroupsLocalEntityMapping.COLUMN_SECTIONS_URL_WITH_TABLE_PREFIX, COLUMN_SECTION_GROUP_SECTIONS_URL)
                .withParam(SectionGroupsLocalEntityMapping.COLUMN_SECTION_GROUPS_URL_WITH_TABLE_PREFIX, COLUMN_SECTION_GROUP_SECTION_GROUPS_URL)
                .withParam(SectionGroupsLocalEntityMapping.COLUMN_CREATED_BY_WITH_TABLE_PREFIX, COLUMN_SECTION_GROUP_CREATED_BY)
                .withParam(SectionGroupsLocalEntityMapping.COLUMN_CREATED_TIME_WITH_TABLE_PREFIX, COLUMN_SECTION_GROUP_CREATED_TIME)
                .withParam(SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_BY_WITH_TABLE_PREFIX, COLUMN_SECTION_GROUP_LAST_MODIFIED_BY)
                .withParam(SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME_WITH_TABLE_PREFIX, COLUMN_SECTION_GROUP_LAST_MODIFIED_TIME)
                //student section params
                .withParam(StudentSectionsLocalEntityMapping.COLUMN_ID_WITH_TABLE_PREFIX, COLUMN_STUDENT_SECTION_ID)
                //teacher and student params
                .withParam("(" + selectTeacherIdsQuery + ")", COLUMN_TEACHER_ID)
                .withParam("(" + selectTeacherTypesQuery + ")", COLUMN_TEACHER_TYPE)
                .withParam("(" + selectStudentIdsQuery + ")", COLUMN_STUDENT_ID)
                .withParam("(" + selectStudentTypesQuery + ")", COLUMN_STUDENT_TYPE)
                //joins
                .leftJoin(SectionsLocalEntityMapping.TABLE, ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX, SectionsLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_TABLE_PREFIX)
                .leftJoin(SectionGroupsLocalEntityMapping.TABLE, ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX, SectionGroupsLocalEntityMapping.COLUMN_PARENT_NOTEBOOK_ID_WITH_TABLE_PREFIX)
                .leftJoin(ClassNotebooksLocalEntityMapping.TABLE_TEACHERS_LINKING, ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX, ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_TEACHERS_LINKING_TABLE_PREFIX)
                .leftJoin(ClassNotebooksLocalEntityMapping.TABLE_STUDENTS_LINKING, ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX, ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_STUDENTS_LINKING_TABLE_PREFIX)
                .leftJoin(StudentSectionsLocalEntityMapping.TABLE, ClassNotebooksLocalEntityMapping.COLUMN_NOTEBOOK_ID_WITH_NOTEBOOKS_TABLE_PREFIX, StudentSectionsLocalEntityMapping.COLUMN_PARENT_ID_WITH_TABLE_PREFIX)
                .build();
    }
}
