package net.styleru.ikomarov.presentation_layer.mapping.sg_permissions;

import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;
import net.styleru.ikomarov.presentation_layer.view.sg_permissions_list.SGPermissionViewObject;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 02.04.17.
 */

public class SGPermissionMapper implements Function<PermissionDTO, SGPermissionViewObject> {

    @Override
    public SGPermissionViewObject apply(PermissionDTO dto) throws Exception {
        return new SGPermissionViewObject(
                dto.getId(),
                dto.getUserId(),
                dto.getName(),
                dto.getRole()
        );
    }
}
