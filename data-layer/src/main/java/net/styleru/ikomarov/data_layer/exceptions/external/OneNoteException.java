package net.styleru.ikomarov.data_layer.exceptions.external;

import net.styleru.ikomarov.data_layer.contracts.exception.ExceptionSource;
import net.styleru.ikomarov.data_layer.exceptions.ExternalException;

/**
 * Created by i_komarov on 07.03.17.
 */

public class OneNoteException extends ExternalException {

    private final String message;
    private final String apiUrl;
    private final int code;

    public OneNoteException(String message, String apiUrl, int code) {
        this.message = message;
        this.apiUrl = apiUrl;
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public String getApiUrl() {
        return this.apiUrl;
    }

    public int getCode() {
        return this.code;
    }

    @Override
    public ExceptionSource getSource() {
        return ExceptionSource.ONE_NOTE;
    }
}
