package net.styleru.ikomarov.domain_layer.di.endpoints.section_groups;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;

/**
 * Created by i_komarov on 15.03.17.
 */

public interface ISectionGroupsEnvironmentComponent {

    @NonNull
    ICacheComponent cacheComponent();

    @NonNull
    SectionGroupsCacheModule cache();

    @NonNull
    SectionGroupsLocalServiceModule localService();

    @NonNull
    SectionGroupsRemoteServiceModule remoteService();

    @NonNull
    SectionGroupsMappingModule mapping();
}
