package net.styleru.ikomarov.data_layer.reports.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Created by i_komarov on 13.04.17.
 */

public class ReportDataResponse {

    @Nullable
    private final Status status;

    @NonNull
    private final String filePath;

    public ReportDataResponse(@NonNull Status status, @Nullable String filePath) {
        this.status = status;
        this.filePath = filePath;
    }

    @Nullable
    public Status getStatus() {
        return status;
    }

    @NonNull
    public String getFilePath() {
        return filePath;
    }

    public enum Status {
        SUCCESS,
        FAILURE
    }
}
