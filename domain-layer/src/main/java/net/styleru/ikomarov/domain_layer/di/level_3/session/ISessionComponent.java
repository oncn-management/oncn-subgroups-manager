package net.styleru.ikomarov.domain_layer.di.level_3.session;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * depends on {@link IEnvironmentComponent}
 * */
public interface ISessionComponent {

    @NonNull
    IClientComponent plusClientComponent();

    @NonNull
    SessionModule session();
}
