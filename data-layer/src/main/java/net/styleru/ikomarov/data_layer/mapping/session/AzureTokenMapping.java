package net.styleru.ikomarov.data_layer.mapping.session;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 09.03.17.
 */

public class AzureTokenMapping {

    @NonNull
    public static final String FIELD_TOKEN_TYPE = "token_type";

    @NonNull
    public static final String FIELD_EXPIRES_ON = "expires_on";

    @NonNull
    public static final String FIELD_EXPIRES_IN = "expires_in";

    @NonNull
    public static final String FIELD_NOT_BEFORE = "not_before";

    @NonNull
    public static final String FIELD_RESOURCE = "resource";

    @NonNull
    public static final String FIELD_ACCESS_TOKEN = "access_token";

    @NonNull
    public static final String FIELD_ID_TOKEN = "id_token";

    @NonNull
    public static final String FIELD_SCOPE = "scope";

    @NonNull
    public static final String FIELD_REFRESH_TOKEN = "refresh_token";

    private AzureTokenMapping() {
        throw new IllegalStateException("No instances please");
    }
}
