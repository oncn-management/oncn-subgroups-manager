package net.styleru.ikomarov.domain_layer.di.level_3.session;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.ClientComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * Created by i_komarov on 13.03.17.
 */

public class SessionComponent implements ISessionComponent {

    @NonNull
    private final SessionModule session;

    public SessionComponent(@NonNull IAppComponent appComponent, @NonNull IEnvironmentComponent parent) {
        this.session = new SessionModule(appComponent, parent);
    }

    @NonNull
    @Override
    public IClientComponent plusClientComponent() {
        return new ClientComponent(this);
    }

    @NonNull
    @Override
    public SessionModule session() {
        return session;
    }
}
