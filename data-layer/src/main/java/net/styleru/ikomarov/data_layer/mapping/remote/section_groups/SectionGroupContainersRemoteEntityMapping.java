package net.styleru.ikomarov.data_layer.mapping.remote.section_groups;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.remote.section_groups.ContainerRemoteEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupContainersRemoteEntityMapping implements IRemoteMapping<ContainerRemoteEntity> {

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_NAME = "name";

    @NonNull
    public static final String FIELD_SELF = "self";

    private final Gson gson;

    public SectionGroupContainersRemoteEntityMapping(Gson gson) {
        this.gson = gson;
    }

    @Override
    public ContainerRemoteEntity fromJson(JsonObject json) {
        return gson.fromJson(json, ContainerRemoteEntity.class);
    }

    @Override
    public List<ContainerRemoteEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<ContainerRemoteEntity>>(){}.getType());
    }

    @Override
    public String toJson(ContainerRemoteEntity entity) {
        return gson.toJson(entity, ContainerRemoteEntity.class);
    }

    @Override
    public String toJsonArray(List<ContainerRemoteEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<ContainerRemoteEntity>>(){}.getType());
    }
}
