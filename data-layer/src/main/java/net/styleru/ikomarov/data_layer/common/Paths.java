package net.styleru.ikomarov.data_layer.common;

/**
 * Created by i_komarov on 18.02.17.
 */

public class Paths {

    public static final String PATH_NOTEBOOK_ID = "notebook-id";

    public static final String PATH_PERMISSION_ID = "permission-id";

    public static final String PATH_SECTIONGROUP_ID = "sectiongroup-id";

    public static final String PATH_SECTION_ID = "section-id";

    public static final String PATH_USER_ID = "user-id";

    public static final String PATH_TEACHER_ID = "teacher-id";

    public static final String PATH_STUDENT_ID = "student-id";
}
