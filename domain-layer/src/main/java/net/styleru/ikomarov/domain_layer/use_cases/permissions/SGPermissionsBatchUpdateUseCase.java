package net.styleru.ikomarov.domain_layer.use_cases.permissions;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.repository.permissions.ISGPermissionsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 02.04.17.
 */

public class SGPermissionsBatchUpdateUseCase extends UseCase {

    @NonNull
    private final ISGPermissionsRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public SGPermissionsBatchUpdateUseCase(@NonNull ISGPermissionsRepository repository,
                                     @NonNull Scheduler executeScheduler,
                                     @NonNull Scheduler postScheduler) {

        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public void execute(UUID operationId, Callbacks callbacks, String sectionGroupId, List<Permission> changes) {
        Observable.fromIterable(changes)
                .flatMap((chunk) -> {
                    return repository.delete(UUID.randomUUID(), sectionGroupId, chunk.getPermissionId())
                            .map((status) -> chunk);
                })
                .flatMap((chunk) -> {
                    //if the role is NONE, no reason exist for creating this permission
                    return chunk.getRole() == UserRole.NONE ?
                            Observable.empty() :
                            repository.create(UUID.randomUUID(), sectionGroupId, chunk.getUserId(), chunk.getRole());
                })
                .toList()
                .toObservable()
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createPermissionsBatchUpdateObserver(operationId, callbacks));
    }

    private DisposableObserver<List<CreateOperationResult>> createPermissionsBatchUpdateObserver(UUID operationId, Callbacks callbacks) {
        DisposableObserver<List<CreateOperationResult>> batchUpdateObserver = new DisposableObserver<List<CreateOperationResult>>() {
            @Override
            public void onNext(List<CreateOperationResult> value) {
                if(!isDisposed()) {
                    int resultStatus = 0;
                    for(CreateOperationResult result : value) {
                        resultStatus += result.getStatus().code();
                    }

                    //check if all of actions succeeded
                    if(resultStatus == 0) {
                        callbacks.onSGPermissionsUpdated(operationId);
                        //next check if all of actions were delayed
                    } else if(resultStatus == 2 * value.size()) {
                        callbacks.onSGPermissionsUpdateDelayed(operationId);
                        //if not - let's say the conflict happened, so we have to refresh the page and try again
                    } else {
                        callbacks.onSGPermissionsUpdateConflict(operationId);
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                if(!isDisposed()) {
                    //here it is obvious that something bad happened, let's say it's error
                    callbacks.onSGPermissionsUpdateFailed(operationId, e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(batchUpdateObserver);

        return batchUpdateObserver;
    }

    public interface Callbacks {

        void onSGPermissionsUpdated(UUID operationId);

        void onSGPermissionsUpdateConflict(UUID operationId);

        void onSGPermissionsUpdateFailed(UUID operationId, ExceptionBundle error);

        void onSGPermissionsUpdateDelayed(UUID operationId);
    }

    public static final class Permission {

        @NonNull
        private String permissionId;

        @NonNull
        private String userId;

        @NonNull
        private UserRole role;

        public Permission(@NonNull String permissionId, @NonNull String userId, @NonNull UserRole role) {
            this.permissionId = permissionId;
            this.userId = userId;
            this.role = role;
        }

        @NonNull
        public String getPermissionId() {
            return permissionId;
        }

        @NonNull
        public String getUserId() {
            return userId;
        }

        @NonNull
        public UserRole getRole() {
            return role;
        }
    }
}
