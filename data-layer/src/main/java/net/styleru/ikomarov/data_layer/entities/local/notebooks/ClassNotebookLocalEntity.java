package net.styleru.ikomarov.data_layer.entities.local.notebooks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import static net.styleru.ikomarov.data_layer.mapping.local.notebooks.ClassNotebooksLocalEntityMapping.*;

/**
 * Created by i_komarov on 28.02.17.
 */

@StorIOSQLiteType(table = TABLE_NOTEBOOKS)
public class ClassNotebookLocalEntity {

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_ID, key = true)
    String id;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_NAME, ignoreNull = true)
    String name;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_IS_DEFAULT, ignoreNull = true)
    Boolean isDefault;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_IS_SHARED, ignoreNull = true)
    Boolean isShared;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_CREATED_BY, ignoreNull = true)
    String createdBy;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_CREATED_TIME, ignoreNull = true)
    String createdTime;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_LAST_MODIFIED_BY, ignoreNull = true)
    String lastModifiedBy;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_LAST_MODIFIED_TIME, ignoreNull = true)
    String lastModifiedTime;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_WEB_URL, ignoreNull = true)
    String webUrl;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_CLIENT_URL, ignoreNull = true)
    String clientUrl;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_SECTIONS_URL, ignoreNull = true)
    String sectionsUrl;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_SECTION_GROUPS_URL, ignoreNull = true)
    String sectionGroupsUrl;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_SELF, ignoreNull = true)
    String self;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_USER_ROLE, ignoreNull = true)
    String userRole;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_HAS_TEACHER_ONLY_SECTION_GROUP, ignoreNull = true)
    Boolean hasTeacherOnlySectionGroup;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_IS_COLLABORATION_SPACE_LOCKED, ignoreNull = true)
    Boolean isCollaborationSpaceLocked;

    public static ClassNotebookLocalEntity newInstance(String name, Boolean hasTeacherOnlySectionGroup) {
        ClassNotebookLocalEntity entity = new ClassNotebookLocalEntity();

        entity.name = name;
        entity.hasTeacherOnlySectionGroup = hasTeacherOnlySectionGroup;

        return entity;
    }

    public static ClassNotebookLocalEntity newInstance(String id, String name, Boolean hasTeacherOnlySectionGroup) {
        ClassNotebookLocalEntity entity = new ClassNotebookLocalEntity();

        entity.id = id;
        entity.name = name;
        entity.hasTeacherOnlySectionGroup = hasTeacherOnlySectionGroup;

        return entity;
    }

    public static ClassNotebookLocalEntity newInstance(String id, String name, Boolean isDefault, Boolean isShared, String createdBy, String createdTime, String lastModifiedBy, String lastModifiedTime, String webUrl, String clientUrl, String sectionsUrl, String sectionGroupsUrl, String self, String userRole, Boolean hasTeacherOnlySectionGroup, Boolean isCollaborationSpaceLocked) {
        ClassNotebookLocalEntity entity = new ClassNotebookLocalEntity();

        entity.id = id;
        entity.name = name;
        entity.isDefault = isDefault;
        entity.isShared = isShared;
        entity.createdBy = createdBy;
        entity.createdTime = createdTime;
        entity.lastModifiedBy = lastModifiedBy;
        entity.lastModifiedTime = lastModifiedTime;
        entity.webUrl = webUrl;
        entity.clientUrl = clientUrl;
        entity.sectionsUrl = sectionsUrl;
        entity.sectionGroupsUrl = sectionGroupsUrl;
        entity.self = self;
        entity.userRole = userRole;
        entity.hasTeacherOnlySectionGroup = hasTeacherOnlySectionGroup;
        entity.isCollaborationSpaceLocked = isCollaborationSpaceLocked;

        return entity;
    }

    ClassNotebookLocalEntity() {

    }

    @Nullable
    public String getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Nullable
    public Boolean getDefault() {
        return isDefault;
    }

    @Nullable
    public Boolean getShared() {
        return isShared;
    }

    @Nullable
    public String getCreatedBy() {
        return createdBy;
    }

    @Nullable
    public String getCreatedTime() {
        return createdTime;
    }

    @Nullable
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    @Nullable
    public String getLastModifiedTime() {
        return lastModifiedTime;
    }

    @Nullable
    public String getWebUrl() {
        return webUrl;
    }

    @Nullable
    public String getClientUrl() {
        return clientUrl;
    }

    @Nullable
    public String getSectionsUrl() {
        return sectionsUrl;
    }

    @Nullable
    public String getSectionGroupsUrl() {
        return sectionGroupsUrl;
    }

    @Nullable
    public String getSelf() {
        return self;
    }

    @Nullable
    public String getUserRole() {
        return userRole;
    }

    @Nullable
    public Boolean getHasTeacherOnlySectionGroup() {
        return hasTeacherOnlySectionGroup;
    }

    @Nullable
    public Boolean getIsCollaborationSpaceLocked() {
        return isCollaborationSpaceLocked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClassNotebookLocalEntity that = (ClassNotebookLocalEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!name.equals(that.name)) return false;
        if (isDefault != null ? !isDefault.equals(that.isDefault) : that.isDefault != null)
            return false;
        if (isShared != null ? !isShared.equals(that.isShared) : that.isShared != null)
            return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null)
            return false;
        if (createdTime != null ? !createdTime.equals(that.createdTime) : that.createdTime != null)
            return false;
        if (lastModifiedBy != null ? !lastModifiedBy.equals(that.lastModifiedBy) : that.lastModifiedBy != null)
            return false;
        if (lastModifiedTime != null ? !lastModifiedTime.equals(that.lastModifiedTime) : that.lastModifiedTime != null)
            return false;
        if (webUrl != null ? !webUrl.equals(that.webUrl) : that.webUrl != null) return false;
        if (clientUrl != null ? !clientUrl.equals(that.clientUrl) : that.clientUrl != null)
            return false;
        if (sectionsUrl != null ? !sectionsUrl.equals(that.sectionsUrl) : that.sectionsUrl != null)
            return false;
        if (sectionGroupsUrl != null ? !sectionGroupsUrl.equals(that.sectionGroupsUrl) : that.sectionGroupsUrl != null)
            return false;
        if (self != null ? !self.equals(that.self) : that.self != null) return false;
        if (userRole != null ? !userRole.equals(that.userRole) : that.userRole != null)
            return false;
        if (hasTeacherOnlySectionGroup != null ? !hasTeacherOnlySectionGroup.equals(that.hasTeacherOnlySectionGroup) : that.hasTeacherOnlySectionGroup != null)
            return false;
        return isCollaborationSpaceLocked != null ? isCollaborationSpaceLocked.equals(that.isCollaborationSpaceLocked) : that.isCollaborationSpaceLocked == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + name.hashCode();
        result = 31 * result + (isDefault != null ? isDefault.hashCode() : 0);
        result = 31 * result + (isShared != null ? isShared.hashCode() : 0);
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        result = 31 * result + (createdTime != null ? createdTime.hashCode() : 0);
        result = 31 * result + (lastModifiedBy != null ? lastModifiedBy.hashCode() : 0);
        result = 31 * result + (lastModifiedTime != null ? lastModifiedTime.hashCode() : 0);
        result = 31 * result + (webUrl != null ? webUrl.hashCode() : 0);
        result = 31 * result + (clientUrl != null ? clientUrl.hashCode() : 0);
        result = 31 * result + (sectionsUrl != null ? sectionsUrl.hashCode() : 0);
        result = 31 * result + (sectionGroupsUrl != null ? sectionGroupsUrl.hashCode() : 0);
        result = 31 * result + (self != null ? self.hashCode() : 0);
        result = 31 * result + (userRole != null ? userRole.hashCode() : 0);
        result = 31 * result + (hasTeacherOnlySectionGroup != null ? hasTeacherOnlySectionGroup.hashCode() : 0);
        result = 31 * result + (isCollaborationSpaceLocked != null ? isCollaborationSpaceLocked.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ClassNotebookLocalEntity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", isDefault=" + isDefault +
                ", isShared=" + isShared +
                ", createdBy='" + createdBy + '\'' +
                ", createdTime='" + createdTime + '\'' +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", lastModifiedTime='" + lastModifiedTime + '\'' +
                ", webUrl='" + webUrl + '\'' +
                ", clientUrl='" + clientUrl + '\'' +
                ", sectionsUrl='" + sectionsUrl + '\'' +
                ", sectionGroupsUrl='" + sectionGroupsUrl + '\'' +
                ", self='" + self + '\'' +
                ", userRole='" + userRole + '\'' +
                ", hasTeacherOnlySectionGroup=" + hasTeacherOnlySectionGroup +
                ", isCollaborationSpaceLocked=" + isCollaborationSpaceLocked +
                '}';
    }
}
