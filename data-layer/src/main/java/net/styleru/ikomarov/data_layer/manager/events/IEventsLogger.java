package net.styleru.ikomarov.data_layer.manager.events;

import android.os.Bundle;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 09.03.17.
 */

public interface IEventsLogger {

    void logException(Exception exception);

    void logEvent(String type, Bundle data);

    void logEvent(String clazz, String method, ExceptionBundle.Reason exceptionReason);

    void logEvent(String clazz, String method, ExceptionBundle.Reason exceptionReason, Throwable throwable);
}
