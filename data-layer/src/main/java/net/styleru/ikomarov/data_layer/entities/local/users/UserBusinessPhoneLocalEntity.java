package net.styleru.ikomarov.data_layer.entities.local.users;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.ikomarov.data_layer.mapping.local.users.UsersBusinessPhonesLocalEntityMapping;

/**
 * Created by i_komarov on 25.02.17.
 */

@StorIOSQLiteType(table = UsersBusinessPhonesLocalEntityMapping.TABLE)
public class UserBusinessPhoneLocalEntity {

    @Nullable
    @StorIOSQLiteColumn(name = UsersBusinessPhonesLocalEntityMapping.COLUMN_ID, key = true, ignoreNull = true)
    Long id;

    @NonNull
    @StorIOSQLiteColumn(name = UsersBusinessPhonesLocalEntityMapping.COLUMN_USER_ID, ignoreNull = true)
    String userId;

    @NonNull
    @StorIOSQLiteColumn(name = UsersBusinessPhonesLocalEntityMapping.COLUMN_PHONE, ignoreNull = true)
    String phone;

    public static UserBusinessPhoneLocalEntity newInstance(@NonNull String userId, @NonNull String phone) {
        UserBusinessPhoneLocalEntity entity = new UserBusinessPhoneLocalEntity();

        entity.userId = userId;
        entity.phone = phone;

        return entity;
    }

    public static UserBusinessPhoneLocalEntity newInstance(@Nullable Long id, @NonNull String userId, @NonNull String phone) {
        UserBusinessPhoneLocalEntity entity = new UserBusinessPhoneLocalEntity();

        entity.id = id;
        entity.userId = userId;
        entity.phone = phone;

        return entity;
    }

    UserBusinessPhoneLocalEntity() {

    }

    @Nullable
    public Long getId() {
        return id;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserBusinessPhoneLocalEntity that = (UserBusinessPhoneLocalEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!userId.equals(that.userId)) return false;
        return phone.equals(that.phone);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + userId.hashCode();
        result = 31 * result + phone.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "UserBusinessPhoneLocalEntity{" +
                "id=" + id +
                ", userId='" + userId + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
