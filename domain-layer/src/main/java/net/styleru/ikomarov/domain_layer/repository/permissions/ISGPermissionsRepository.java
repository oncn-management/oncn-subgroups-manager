package net.styleru.ikomarov.domain_layer.repository.permissions;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.repository.base.DeleteOperationResult;

import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 22.03.17.
 */

public interface ISGPermissionsRepository {

    Observable<CreateOperationResult> create(UUID operationId, String sectionGroupId, String userId, UserRole role);

    Observable<List<PermissionDTO>> read(String sectionGroupId, boolean fast);

    Observable<List<PermissionDTO>> read(String sectionGroupId, String userId);

    Observable<DeleteOperationResult> delete(UUID operationId, String sectionGroupId, String permissionId);
}
