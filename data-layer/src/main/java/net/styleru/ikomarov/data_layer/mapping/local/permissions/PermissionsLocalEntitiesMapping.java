package net.styleru.ikomarov.data_layer.mapping.local.permissions;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by i_komarov on 26.02.17.
 */

public class PermissionsLocalEntitiesMapping implements ILocalMapping<PermissionLocalEntity> {

    @NonNull
    public static final String TABLE = "permissions";

    @NonNull
    public static final String INDEX_PRIMARY = TABLE + "_" + "PRIMARY_INDEX";

    @NonNull
    public static final String INDEX_SECONDARY = TABLE + "_" + "SECONDARY_INDEX";

    @NonNull
    public static final String COLUMN_PK = "_pk";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_HOST_ID = "HOST_RESOURCE_ID";

    @NonNull
    public static final String COLUMN_HOST_TYPE = "HOST_TYPE";

    @NonNull
    public static final String COLUMN_NAME = "NAME";

    @NonNull
    public static final String COLUMN_SELF = "SELF";

    @NonNull
    public static final String COLUMN_USER_ID = "USER_ID";

    @NonNull
    public static final String COLUMN_USER_ROLE = "USER_ROLE";

    public static final String COLUMN_PK_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_PK;
    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_HOST_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_HOST_ID;
    public static final String COLUMN_HOST_TYPE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_HOST_TYPE;
    public static final String COLUMN_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_NAME;
    public static final String COLUMN_SELF_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_SELF;
    public static final String COLUMN_USER_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_USER_ID;
    public static final String COLUMN_USER_ROLE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_USER_ROLE;

    @NonNull
    public static final Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();

    public PermissionsLocalEntitiesMapping() {

    }

    public static List<String> getInitializationQueries() {
        return Arrays.asList(
                getPermissionsCreateTableQuery(),
                getPermissionsPrimaryIndexCreateQuery(),
                getPermissionsSecondaryIndexCreateQuery()
        );
    }

    private static String getPermissionsCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE + "(" +
                    COLUMN_PK + " INTEGER PRIMARY KEY," +
                    COLUMN_ID + " TEXT NOT NULL," +
                    COLUMN_HOST_ID + " TEXT NOT NULL," +
                    COLUMN_HOST_TYPE + " INTEGER NOT NULL," +
                    COLUMN_NAME + " TEXT NOT NULL," +
                    COLUMN_SELF + " TEXT NOT NULL," +
                    COLUMN_USER_ID + " TEXT NOT NULL," +
                    COLUMN_USER_ROLE + " TEXT NOT NULL" +
                ");";
    }

    //index for performing queries like searching permissions for the particular type of resource for the particular resource for particular user
    private static String getPermissionsPrimaryIndexCreateQuery() {
        return "CREATE INDEX IF NOT EXISTS " + INDEX_PRIMARY +
                " ON " + TABLE +
                "(" +
                    COLUMN_USER_ID + " ASC," +
                    COLUMN_HOST_TYPE + " ASC," +
                    COLUMN_HOST_ID + " ASC" +
                ");";
    }

    private static String getPermissionsSecondaryIndexCreateQuery() {
        return "CREATE INDEX IF NOT EXISTS " + INDEX_SECONDARY +
                " ON " + TABLE +
                "(" +
                    COLUMN_USER_ID + " ASC," +
                    COLUMN_HOST_TYPE + " ASC" +
                ");";
    }

    @Override
    public PermissionLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException, ParseException {
        if(cursor.moveToFirst()) {
            return PermissionLocalEntity.newInstance(
                    cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_HOST_ID)),
                    cursor.getInt(cursor.getColumnIndex(COLUMN_HOST_TYPE)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_SELF)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_USER_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_USER_ROLE))
            );
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<PermissionLocalEntity> listFromCursor(Cursor cursor) throws ParseException {
        List<PermissionLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                entities.add(
                        PermissionLocalEntity.newInstance(
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_HOST_ID)),
                                cursor.getInt(indexCache.indexFor(cursor, COLUMN_HOST_TYPE)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_NAME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_SELF)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_USER_ROLE))

                        )
                );
            } while(cursor.moveToNext());
        }

        return entities;
    }
}
