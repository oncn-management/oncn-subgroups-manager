package net.styleru.ikomarov.data_layer.mapping.remote.extras;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.entities.remote.extras.OneNoteWarning;

import java.util.List;

/**
 * Created by i_komarov on 21.02.17.
 */

public class WarningRemoteEntityMapping implements IRemoteMapping<OneNoteWarning> {

    @NonNull
    public static final String FIELD_MESSAGE = "message";

    @NonNull
    public static final String FIELD_URL = "url";

    private final Gson gson;

    public WarningRemoteEntityMapping() {
        this.gson = new Gson();
    }

    @Override
    public OneNoteWarning fromJson(JsonObject json) {
        return gson.fromJson(json, OneNoteWarning.class);
    }

    @Override
    public List<OneNoteWarning> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<OneNoteWarning>>(){}.getType());
    }

    @Override
    public String toJson(OneNoteWarning entity) {
        return gson.toJson(entity, OneNoteWarning.class);
    }

    @Override
    public String toJsonArray(List<OneNoteWarning> entities) {
        return gson.toJson(entities, new TypeToken<List<OneNoteWarning>>(){}.getType());
    }
}
