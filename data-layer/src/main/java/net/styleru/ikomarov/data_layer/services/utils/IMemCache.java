package net.styleru.ikomarov.data_layer.services.utils;

import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by i_komarov on 08.03.17.
 */

public interface IMemCache {

    long size();

    Date lastModified();

    Date get(@NonNull String id);

    void onModified(@NonNull String id, @NonNull String timeStamp);

    void onDeleted(@NonNull String id);

    void invalidate();

    boolean isCold();
}
