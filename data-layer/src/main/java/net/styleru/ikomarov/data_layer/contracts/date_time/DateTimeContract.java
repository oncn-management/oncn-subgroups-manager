package net.styleru.ikomarov.data_layer.contracts.date_time;

/**
 * Created by i_komarov on 18.02.17.
 */

public class DateTimeContract {

    public static final String API_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public static final String SQL_COMPARE_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public static final String SQL_DATE_FORMAT = "\'%Y-%m-%d %H:%M:%s\'";

    public static final String VIEW_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String SQL_DATE_CURRENT = "\'NOW\'";

    private DateTimeContract() {
        throw new IllegalStateException("No instances please");
    }
}
