package net.styleru.ikomarov.domain_layer.mapping.section_groups;

import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;

import java.text.SimpleDateFormat;
import java.util.Locale;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class LocalSectionGroupMapper implements Function<SectionGroupLocalEntity, SectionGroupDTO> {

    private final SimpleDateFormat contractDateTimeFormat = new SimpleDateFormat(DateTimeContract.SQL_COMPARE_DATE_FORMAT, Locale.getDefault());

    @Nullable
    @Override
    public SectionGroupDTO apply(SectionGroupLocalEntity entity) throws Exception {
        return SectionGroupDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getCreatedBy(),
                entity.getCreatedTime(),
                entity.getLastModifiedBy(),
                contractDateTimeFormat.parse(entity.getLastModifiedTime()),
                entity.getSectionGroupsUrl(),
                entity.getSectionsUrl()
        );
    }
}
