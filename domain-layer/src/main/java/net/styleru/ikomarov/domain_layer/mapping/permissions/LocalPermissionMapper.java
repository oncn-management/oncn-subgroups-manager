package net.styleru.ikomarov.domain_layer.mapping.permissions;

import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 22.03.17.
 */

public class LocalPermissionMapper implements Function<PermissionLocalEntity, PermissionDTO> {

    @Override
    public PermissionDTO apply(PermissionLocalEntity entity) throws Exception {
        return PermissionDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getUserId(),
                entity.getUserRole()
        );
    }
}
