package net.styleru.ikomarov.authentication.http;

/**
 * Created by i_komarov on 18.02.17.
 */

public enum OAuthError {

    INVALID_REQUEST {
        public String value() {
            return FIELD_INVALID_REQUEST;
        }
    },

    UNAUTHORIZED_CLIENT {
        @Override
        public String value() {
            return FIELD_UNAUTHORIZED_CLIENT;
        }
    },

    ACCESS_DENIED {
        @Override
        public String value() {
            return FIELD_ACCESS_DENIED;
        }
    },

    UNSUPPORTED_RESPONSE_TYPE {
        @Override
        public String value() {
            return FIELD_UNSUPPORTED_RESPONSE_TYPE;
        }
    },
    SERVER_ERROR {
        @Override
        public String value() {
            return FIELD_SERVER_ERROR;
        }
    },

    TEMPORARILY_UNAVAILABLE {
        @Override
        public String value() {
            return FIELD_TEMPORARILY_UNAVAILABLE;
        }
    },

    INVALID_RESOURCE {
        @Override
        public String value() {
            return FIELD_INVALID_RESOURCE;
        }
    };


    static String FIELD_INVALID_REQUEST = "invalid_request";

    static String FIELD_UNAUTHORIZED_CLIENT = "unauthorized_client";

    static String FIELD_ACCESS_DENIED = "access_denied";

    static String FIELD_UNSUPPORTED_RESPONSE_TYPE = "unsupported_response_type";

    static String FIELD_SERVER_ERROR = "server_error";

    static String FIELD_TEMPORARILY_UNAVAILABLE = "temporarily_unavailable";

    static String FIELD_INVALID_RESOURCE = "invalid_resource";


    public abstract String value();

    public static OAuthError parse(String error) {
        if (error.equals(FIELD_INVALID_REQUEST)) {
            return INVALID_REQUEST;
        } else if(error.equals(FIELD_UNAUTHORIZED_CLIENT)) {
            return UNAUTHORIZED_CLIENT;
        }  else if(error.equals(FIELD_ACCESS_DENIED)) {
            return ACCESS_DENIED;
        }  else if(error.equals(FIELD_UNSUPPORTED_RESPONSE_TYPE)) {
            return UNSUPPORTED_RESPONSE_TYPE;
        }  else if(error.equals(FIELD_SERVER_ERROR)) {
            return SERVER_ERROR;
        }  else if(error.equals(FIELD_TEMPORARILY_UNAVAILABLE)) {
            return TEMPORARILY_UNAVAILABLE;
        }  else if(error.equals(FIELD_INVALID_RESOURCE)) {
            return INVALID_RESOURCE;
        } else {
            throw new IllegalArgumentException("Unknown error: " + error);
        }
    }
}
