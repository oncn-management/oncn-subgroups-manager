package net.styleru.ikomarov.data_layer.services.session;

import android.support.annotation.NonNull;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.entities.session.AzureToken;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Url;

/**
 * Created by i_komarov on 09.03.17.
 */

public interface ISessionService {

    @NonNull
    String BASE_URL = "https://login.microsoftonline.com/common/oauth2/";

    @NonNull
    String ME_BASE_URL = "https://graph.microsoft.com/v1.0/me";

    @NonNull
    String HEADER_AUTHORIZATION = "Authorization";

    @NonNull
    String ENDPOINT_AUTHORIZE = "authorize";

    @NonNull
    String ENDPOINT_TOKEN = "token";


    @NonNull
    String FIELD_CLIENT_ID = "client_id";

    @NonNull
    String FIELD_GRANT_TYPE = "grant_type";

    @NonNull
    String FIELD_REDIRECT_URI = "redirect_uri";

    @NonNull
    String FIELD_CODE = "code";

    @NonNull
    String FIELD_RESOURCE = "resource";

    @NonNull
    String FIELD_REFRESH_TOKEN = "refresh_token";

    @NonNull
    String FIELD_RESPONSE_TYPE = "response_type";

    @NonNull
    String FIELD_MAIL = "mail";


    @NonNull
    String GRANT_TYPE_REFRESH_TOKEN = "refresh_token";

    @NonNull
    String GRANT_TYPE_TRADE_TOKEN = "authorization_code";


    @NonNull
    String QUERY_PARAM_CODE = "code";

    @NonNull
    String QUERY_PARAM_ERROR = "error";

    @NonNull
    String QUERY_PARAM_ERROR_DESCRIPTION = "error_description";


    @FormUrlEncoded
    @POST(ENDPOINT_TOKEN)
    Call<AzureToken> tradeCode(
            @Field(FIELD_GRANT_TYPE) String grantType,
            @Field(FIELD_CLIENT_ID) String cliendID,
            @Field(FIELD_REDIRECT_URI) String redirectUri,
            @Field(FIELD_CODE) String code,
            @Field(FIELD_RESOURCE) String resourceString
    );

    @FormUrlEncoded
    @POST(ENDPOINT_TOKEN)
    Call<AzureToken> refreshToken(
            @Field(FIELD_GRANT_TYPE) String grantType,
            @Field(FIELD_CLIENT_ID) String cliendID,
            @Field(FIELD_REDIRECT_URI) String redirectUri,
            @Field(FIELD_REFRESH_TOKEN) String refreshToken,
            @Field(FIELD_RESOURCE) String resourceString
    );

    @GET
    Observable<JsonObject> me(
            @Header(HEADER_AUTHORIZATION) String bearer,
            @Url String url
    );
}
