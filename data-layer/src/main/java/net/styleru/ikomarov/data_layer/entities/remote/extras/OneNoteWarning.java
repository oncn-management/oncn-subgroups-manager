package net.styleru.ikomarov.data_layer.entities.remote.extras;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.remote.extras.WarningRemoteEntityMapping.*;

/**
 * Created by i_komarov on 21.02.17.
 */

public class OneNoteWarning {

    @SerializedName(FIELD_MESSAGE)
    private String message;
    @SerializedName(FIELD_URL)
    private String url;

    private OneNoteWarning() {

    }

    public String getMessage() {
        return message;
    }

    public String getUrl() {
        return url;
    }
}
