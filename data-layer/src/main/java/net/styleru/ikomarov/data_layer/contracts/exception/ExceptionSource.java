package net.styleru.ikomarov.data_layer.contracts.exception;

import android.util.SparseArray;

import net.styleru.ikomarov.data_layer.exceptions.internal.UnknownEnumValueException;

/**
 * Created by i_komarov on 07.03.17.
 */

public enum ExceptionSource {
    DATABASE     (0x000000),
    NETWORK      (0x000001),
    HTTP         (0x000002),
    ONE_NOTE     (0x000003),
    SESSION      (0x000004),
    INTERNAL     (0x000005);

    private static final SparseArray<ExceptionSource> sourceMap;

    static {
        sourceMap = new SparseArray<>(ExceptionSource.values().length);
        for(ExceptionSource val : ExceptionSource.values()) {
            sourceMap.put(val.code, val);
        }
    }

    private final int code;

    public static ExceptionSource forValue(int val) {
        ExceptionSource value = sourceMap.get(val);
        if(value != null) {
            return value;
        } else {
            throw new UnknownEnumValueException(String.valueOf(val), "ExceptionSource", "forValue");
        }
    }

    ExceptionSource(int code) {
        this.code = code;
    }

    public int code() {
        return this.code;
    }
}
