package net.styleru.ikomarov.presentation_layer.presenter.section_groups_list;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.events.IEventsLogger;
import net.styleru.ikomarov.domain_layer.analytics.IScreenAnalyticsManager;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.use_cases.section_groups.SectionGroupGetUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.section_groups.SectionGroupsFetchUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.section_groups.SectionGroupsGetUseCase;
import net.styleru.ikomarov.presentation_layer.mapping.section_groups.SectionGroupMapper;
import net.styleru.ikomarov.presentation_layer.mapping.section_groups.SectionGroupsMapper;
import net.styleru.ikomarov.presentation_layer.presenter.base.Presenter;
import net.styleru.ikomarov.domain_layer.contracts.errors.IErrorCodeTranslator;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.section_groups_list.ISectionGroupsListView;
import net.styleru.ikomarov.presentation_layer.view.section_groups_list.SectionGroupViewObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsListPresenter extends Presenter<ISectionGroupsListView> implements SectionGroupsGetUseCase.Callbacks<SectionGroupViewObject>, SectionGroupGetUseCase.Callbacks<SectionGroupViewObject>, SectionGroupsFetchUseCase.Callbacks<SectionGroupViewObject> {

    @NonNull
    private final SectionGroupsGetUseCase sectionGroupsGet;

    @NonNull
    private final SectionGroupGetUseCase sectionGroupGet;

    @NonNull
    private final SectionGroupsFetchUseCase sectionGroupsFetch;

    @NonNull
    private final IErrorCodeTranslator oneNoteCodeTranslator;

    @NonNull
    private final IScreenAnalyticsManager screenAnalytics;

    @NonNull
    private final IEventsLogger loggingManager;

    @NonNull
    private final Function<SectionGroupDTO, SectionGroupViewObject> singleMapper;

    @NonNull
    private final Function<List<SectionGroupDTO>, List<SectionGroupViewObject>> listMapper;

    @NonNull
    private final String notebookId;

    private SectionGroupsListPresenter(@NonNull SectionGroupsGetUseCase sectionGroupsGet,
                                       @NonNull SectionGroupGetUseCase sectionGroupGet,
                                       @NonNull SectionGroupsFetchUseCase sectionGroupsFetch,
                                       @NonNull IErrorCodeTranslator oneNoteCodeTranslator,
                                       @NonNull IScreenAnalyticsManager screenAnalytics,
                                       @NonNull IEventsLogger loggingManager,
                                       @NonNull String notebookId) {

        this.sectionGroupsGet = sectionGroupsGet;
        this.sectionGroupGet = sectionGroupGet;
        this.sectionGroupsFetch = sectionGroupsFetch;
        this.oneNoteCodeTranslator = oneNoteCodeTranslator;
        this.screenAnalytics = screenAnalytics;
        this.loggingManager = loggingManager;
        this.notebookId = notebookId;
        this.singleMapper = new SectionGroupMapper();
        this.listMapper = new SectionGroupsMapper();
    }

    @Override
    protected void onViewAttached() {
        screenAnalytics.onScreenEvent("List of SectionGroups", "office365:///class_notebooks/" + notebookId);
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {
        sectionGroupsGet.dispose();
        sectionGroupsFetch.dispose();
        sectionGroupGet.dispose();
    }

    public void loadSectionGroups(int offset, int limit) {
        sectionGroupsGet.execute(listMapper, this, notebookId, offset, limit);
    }

    public void loadSectionGroups(int offset, int limit, String nameSubstring) {
        sectionGroupsGet.execute(listMapper, this, notebookId, offset, limit, nameSubstring);
    }

    public void loadSectionGroup(String sectionGroupId) {
        sectionGroupGet.execute(singleMapper, this, sectionGroupId);
    }

    public void fetch(Date ifModifiedSince) {
        if(ifModifiedSince != null) {
            sectionGroupsFetch.execute(listMapper, this, notebookId, ifModifiedSince);
        }
    }

    @Override
    public void onSectionGroupsFetched(List<SectionGroupViewObject> sectionGroups) {
        postAction((view) -> {
            view.addSectionGroups(sectionGroups);
        });
    }

    @Override
    public void onSectionGroupsFetchFailed(ExceptionBundle error) {
        handleException(
                error,
                SectionGroupsListPresenter.class.getSimpleName(),
                "onSectionGroupsFetchFailed",
                ExceptionBundle.Reason.ONE_NOTE_ERROR,
                ExceptionBundle.Reason.NETWORK_UNAVAILABLE,
                ExceptionBundle.Reason.NETWORK_SSL_HANDSHAKE_FAILED,
                ExceptionBundle.Reason.NETWORK_UNKNOWN
        );
    }

    @Override
    public void onSectionGroupsLoaded(List<SectionGroupViewObject> sectionGroups) {
        postAction((view) -> {
            view.addSectionGroups(sectionGroups);
        });
    }

    @Override
    public void onSectionGroupsLoadingFailed(ExceptionBundle error) {
        handleException(
                error,
                SectionGroupsListPresenter.class.getSimpleName(),
                "onSectionGroupsLoadingFailed",
                ExceptionBundle.Reason.ONE_NOTE_ERROR,
                ExceptionBundle.Reason.NETWORK_UNAVAILABLE,
                ExceptionBundle.Reason.NETWORK_SSL_HANDSHAKE_FAILED,
                ExceptionBundle.Reason.NETWORK_UNKNOWN
        );
    }

    @Override
    public void onSectionGroupLoaded(SectionGroupViewObject sectionGroup) {
        postAction((view) -> {
            view.addSectionGroups(Collections.singletonList(sectionGroup));
        });
    }

    @Override
    public void onSectionGroupLoadingFailed(ExceptionBundle error) {
        handleException(
                error,
                SectionGroupsListPresenter.class.getSimpleName(),
                "onSectionGroupLoadingFailed",
                ExceptionBundle.Reason.ONE_NOTE_ERROR,
                ExceptionBundle.Reason.NETWORK_UNAVAILABLE,
                ExceptionBundle.Reason.NETWORK_SSL_HANDSHAKE_FAILED,
                ExceptionBundle.Reason.NETWORK_UNKNOWN
        );
    }

    private void handleException(ExceptionBundle error, String clazz, String method, ExceptionBundle.Reason... ignoreReasons) {
        if(-1 == Arrays.binarySearch(ignoreReasons, error.getReason())) {
            return;
        }

        postAction((view) -> {
            switch (error.getReason()) {
                case NO_CACHED_RESOURCES: {
                    view.showNoCachedDataPlug();
                    break;
                }
                case NO_RESOURCES: {
                    view.showNoDataPlug();
                    break;
                }
                case NO_SESSION: {
                    view.showAuthorizationDialog();
                    break;
                }
                case ONE_NOTE_ERROR: {
                    final int code = error.getIntExtra(ExceptionBundle.OneNoteErrorContract.KEY_CODE);
                    final String message = error.getStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_MESSAGE);
                    view.showError(oneNoteCodeTranslator.getLocalizedMessage(code, message));
                    break;
                }
                case NETWORK_UNAVAILABLE: {
                    view.showConnectionFailedIndicator();
                    break;
                }
                default: {
                    if(error.getReason().isInternalError()) {
                        loggingManager.logEvent(clazz, method, error.getReason(), error.getThrowable(ExceptionBundle.InternalErrorContract.KEY_THROWABLE));
                    } else {
                        loggingManager.logEvent(clazz, method, error.getReason());
                    }
                }
            }
        });
    }

    public static final class Factory implements RetainContainer.Factory<SectionGroupsListPresenter> {

        @NonNull
        private final SectionGroupsGetUseCase sectionGroupsGet;

        @NonNull
        private final SectionGroupGetUseCase sectionGroupGet;

        @NonNull
        private final SectionGroupsFetchUseCase sectionGroupsFetch;

        @NonNull
        private final IErrorCodeTranslator translator;

        @NonNull
        private final IScreenAnalyticsManager screenAnalytics;

        @NonNull
        private final IEventsLogger logger;

        @NonNull
        private final String notebookId;

        public Factory(@NonNull SectionGroupsGetUseCase sectionGroupsGet,
                       @NonNull SectionGroupGetUseCase sectionGroupGet,
                       @NonNull SectionGroupsFetchUseCase sectionGroupsFetch,
                       @NonNull IErrorCodeTranslator translator,
                       @NonNull IScreenAnalyticsManager screenAnalytics,
                       @NonNull IEventsLogger logger,
                       @NonNull String notebookId) {

            this.sectionGroupsGet = sectionGroupsGet;
            this.sectionGroupGet = sectionGroupGet;
            this.sectionGroupsFetch = sectionGroupsFetch;
            this.translator = translator;
            this.screenAnalytics = screenAnalytics;
            this.logger = logger;
            this.notebookId = notebookId;
        }

        @Override
        public SectionGroupsListPresenter create() {
            return new SectionGroupsListPresenter(sectionGroupsGet, sectionGroupGet, sectionGroupsFetch, translator, screenAnalytics, logger, notebookId);
        }
    }
}
