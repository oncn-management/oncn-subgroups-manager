package net.styleru.ikomarov.data_layer.services.utils;

import java.util.Date;

/**
 * Created by i_komarov on 17.03.17.
 */

public interface IClaster<T> {

    T get(String id);

    Date lastModified(String id);
}
