package net.styleru.ikomarov.domain_layer.di.endpoints.section_groups;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsEnvironmentComponent implements ISectionGroupsEnvironmentComponent {

    @NonNull
    private final ICacheComponent cacheComponent;

    @NonNull
    private final SectionGroupsCacheModule cache;

    @NonNull
    private final SectionGroupsLocalServiceModule localService;

    @NonNull
    private final SectionGroupsRemoteServiceModule remoteService;

    @NonNull
    private final SectionGroupsMappingModule mapping;

    public SectionGroupsEnvironmentComponent(@NonNull IEnvironmentComponent environment, @NonNull IClientComponent client, @NonNull ICacheComponent cacheComponent) {
        this.cacheComponent = cacheComponent;
        this.cache = new SectionGroupsCacheModule(this);
        this.localService = new SectionGroupsLocalServiceModule(environment);
        this.remoteService = new SectionGroupsRemoteServiceModule(client);
        this.mapping = new SectionGroupsMappingModule();
    }

    @NonNull
    @Override
    public ICacheComponent cacheComponent() {
        return cacheComponent;
    }

    @NonNull
    @Override
    public SectionGroupsCacheModule cache() {
        return cache;
    }

    @NonNull
    @Override
    public SectionGroupsLocalServiceModule localService() {
        return localService;
    }

    @NonNull
    @Override
    public SectionGroupsRemoteServiceModule remoteService() {
        return remoteService;
    }

    @NonNull
    @Override
    public SectionGroupsMappingModule mapping() {
        return mapping;
    }
}
