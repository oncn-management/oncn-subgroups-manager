package net.styleru.ikomarov.domain_layer.mapping.permissions;

import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 22.03.17.
 */

public class RemotePermissionMapper implements Function<PermissionRemoteEntity, PermissionDTO> {
    @Override
    public PermissionDTO apply(PermissionRemoteEntity entity) throws Exception {
        return PermissionDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getUserId(),
                entity.getUserRole()
        );
    }
}
