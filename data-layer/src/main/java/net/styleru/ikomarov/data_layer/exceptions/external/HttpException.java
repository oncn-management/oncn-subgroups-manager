package net.styleru.ikomarov.data_layer.exceptions.external;

import net.styleru.ikomarov.data_layer.contracts.exception.ExceptionSource;
import net.styleru.ikomarov.data_layer.exceptions.ExternalException;

/**
 * Created by i_komarov on 08.03.17.
 */

public class HttpException extends ExternalException {

    private int code;
    private String message;

    public HttpException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public ExceptionSource getSource() {
        return ExceptionSource.HTTP;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
