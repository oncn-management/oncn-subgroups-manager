package net.styleru.ikomarov.data_layer.mapping.remote.section_groups;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.remote.section_groups.SectionGroupRemoteEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 14.02.17.
 */

public class SectionGroupsRemoteEntityMapping implements IRemoteMapping<SectionGroupRemoteEntity> {

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_SELF = "self";

    @NonNull
    public static final String FIELD_CREATED_BY = "createdBy";

    @NonNull
    public static final String FIELD_CREATED_TIME = "createdTime";

    @NonNull
    public static final String FIELD_LAST_MODIFIED_BY = "lastModifiedBy";

    @NonNull
    public static final String FIELD_LAST_MODIFIED_TIME = "lastModifiedTime";

    @NonNull
    public static final String FIELD_NAME = "name";

    @NonNull
    public static final String FIELD_SECTION_GROUPS_URL = "sectionGroupsUrl";

    @NonNull
    public static final String FIELD_SECTIONS_URl = "sectionsUrl";

    @NonNull
    public static final String FIELD_PARENT_NOTEBOOK = "parentNotebook";

    @NonNull
    public static final String FIELD_PARENT_SECTION_GROUP = "parentSectionGroup";

    private final Gson gson;

    public SectionGroupsRemoteEntityMapping() {
        this.gson = new Gson();
    }

    @Override
    public SectionGroupRemoteEntity fromJson(JsonObject json) {
        return gson.fromJson(json, SectionGroupRemoteEntity.class);
    }

    @Override
    public List<SectionGroupRemoteEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<SectionGroupRemoteEntity>>(){}.getType());
    }

    @Override
    public String toJson(SectionGroupRemoteEntity entity) {
        return gson.toJson(entity, SectionGroupRemoteEntity.class);
    }

    @Override
    public String toJsonArray(List<SectionGroupRemoteEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<SectionGroupRemoteEntity>>(){}.getType());
    }
}
