package net.styleru.ikomarov.data_layer.exceptions.internal;

/**
 * Created by i_komarov on 21.02.17.
 */

public final class MalformedJsonException extends IllegalStateException {

    private static final String MESSAGE = "Entity to parse was neither of JsonObject type, nor of JsonArray";

    public MalformedJsonException() {
        super(MESSAGE);
    }
}
