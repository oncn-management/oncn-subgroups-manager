package net.styleru.ikomarov.data_layer.services.utils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by i_komarov on 17.03.17.
 */

public abstract class AbstractCacheClaster implements IClaster<IMemCache> {

    private final Object lock = new Object();

    private Map<String, IMemCache> nodes;

    protected AbstractCacheClaster() {
        this.nodes = new HashMap<>();
    }

    @Override
    public IMemCache get(String id) {
        IMemCache node = nodes.get(id);
        if(node == null) {
            synchronized (lock) {
                node = nodes.get(id);
                if(node == null) {
                    node = create(id);
                    nodes.put(id, node);
                }
            }
        }

        return node;
    }

    @Override
    public Date lastModified(String id) {
        synchronized (lock) {
            for(Map.Entry<String, IMemCache> node : nodes.entrySet()) {
                Date lastModified;
                if(null != (lastModified = node.getValue().get(id))) {
                    return lastModified;
                }
            }
        }

        return null;
    }

    protected abstract IMemCache create(String id);
}
