package net.styleru.ikomarov.presentation_layer.application;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;

import net.styleru.ikomarov.domain_layer.analytics.IScreenAnalyticsManager;
import net.styleru.ikomarov.domain_layer.di.endpoints.jobs.IJobEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.jobs.JobEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.jobs.JobEnvironmentModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.notebooks.INotebooksComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.notebooks.INotebooksEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.notebooks.NotebooksComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.notebooks.NotebooksEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.notebooks.NotebooksRepositoryModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.permissions.ISGPermissionsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.permissions.ISGPermissionsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.permissions.SGPermissionsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.permissions.SGPermissionsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.permissions.SGPermissionsRepositoryModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.reports.INotebookReportRepositoryComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.reports.IReportRepositoryFacadeComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.reports.IReportsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.reports.NotebooksReportRepositoryComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.reports.ReportRepositoryFacadeComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.reports.ReportRepositoryFacadeModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.reports.ReportsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.section_groups.ISectionGroupsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.section_groups.ISectionGroupsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.section_groups.SectionGroupsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.section_groups.SectionGroupsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.section_groups.SectionGroupsRepositoryModule;
import net.styleru.ikomarov.domain_layer.di.endpoints.students.IStudentsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.students.IStudentsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.students.StudentsComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.students.StudentsEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.endpoints.students.StudentsRepositoryModule;
import net.styleru.ikomarov.domain_layer.di.level_1.AppComponent;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.ErrorTranslationModule;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.LoggingModule;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.NetworkModule;
import net.styleru.ikomarov.domain_layer.di.level_3.analytics.AnalyticsModule;
import net.styleru.ikomarov.domain_layer.di.level_3.analytics.IAnalyticsComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.ISessionComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.session.SessionModule;
import net.styleru.ikomarov.domain_layer.di.level_4.session_consumer.IClientComponent;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by i_komarov on 09.03.17.
 */

public class App extends Application implements ISingletonProvider {

    //THE ONE WOULD SUFFER
    //WHO DISRESPECT DI FRAMEWORKS
    //ENJOY THE HORROR

    private IAppComponent app;

    private IEnvironmentComponent environment;

    private ICacheComponent cache;

    private ISessionComponent session;

    private IClientComponent client;

    private INotebooksEnvironmentComponent notebooksEnvironmentComponent;

    private INotebooksComponent notebooksComponent;

    private ISectionGroupsEnvironmentComponent sectionGroupsEnvironmentComponent;

    private ISectionGroupsComponent sectionGroupsComponent;

    private IStudentsEnvironmentComponent studentsEnvironmentComponent;

    private IStudentsComponent studentsComponent;

    private ISGPermissionsEnvironmentComponent sgPermissionsEnvironmentComponent;

    private ISGPermissionsComponent sgPermissionsComponent;

    private IReportsEnvironmentComponent reportsEnvironmentComponent;

    private INotebookReportRepositoryComponent notebookReportRepositoryComponent;

    private IReportRepositoryFacadeComponent reportFacadesComponent;

    private IJobEnvironmentComponent jobEnvironmentComponent;

    private IAnalyticsComponent analytics;

    private Scheduler ioScheduler = Schedulers.io();

    private Scheduler mainThreadScheduler = AndroidSchedulers.mainThread();

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        buildDependencyGraph();
    }

    @NonNull
    @Override
    public NetworkModule network() {
        return environment.network();
    }

    @NonNull
    @Override
    public LoggingModule logging() {
        return environment.logging();
    }

    @NonNull
    @Override
    public SessionModule session() {
        return session.session();
    }

    @NonNull
    @Override
    public NotebooksRepositoryModule notebooksRepository() {
        return notebooksComponent.repository();
    }

    @NonNull
    @Override
    public SectionGroupsRepositoryModule sectionGroupsRepository() {
        return sectionGroupsComponent.repository();
    }

    @NonNull
    @Override
    public StudentsRepositoryModule studentsRepository() {
        return studentsComponent.repository();
    }

    @NonNull
    @Override
    public SGPermissionsRepositoryModule sgPermissionsRepository() {
        return sgPermissionsComponent.repository();
    }

    @NonNull
    @Override
    public ReportRepositoryFacadeModule reportReporitoryFacade() {
        return reportFacadesComponent.facade();
    }

    @NonNull
    @Override
    public JobEnvironmentModule jobEnvironment() {
        return jobEnvironmentComponent.environment();
    }

    @NonNull
    @Override
    public ErrorTranslationModule errorTranslation() {
        return environment.errorTranslation();
    }

    @NonNull
    @Override
    public AnalyticsModule analytics() {
        return analytics.analytics();
    }

    @NonNull
    @Override
    public Scheduler ioScheduler() {
        return ioScheduler;
    }

    @NonNull
    @Override
    public Scheduler mainThreadScheduler() {
        return mainThreadScheduler;
    }

    private void buildDependencyGraph() {
        //top-level
        app = AppComponent.fromAppProcess(this);
        //second-level
        environment = app.plusEnvironmentComponent();
        //third-level
        cache = environment.plusCacheComponent();
        session = environment.plusSessionComponent();
        analytics = environment.plusAnalyticsComponent();
        //forth-level
        client = session.plusClientComponent();

        //custom dependency graph endpoints

        //per notebooks
        notebooksEnvironmentComponent = new NotebooksEnvironmentComponent(environment, client, cache);
        notebooksComponent = new NotebooksComponent(notebooksEnvironmentComponent);
        //per section groups
        sectionGroupsEnvironmentComponent = new SectionGroupsEnvironmentComponent(environment, client, cache);
        sectionGroupsComponent = new SectionGroupsComponent(sectionGroupsEnvironmentComponent);
        //per students
        studentsEnvironmentComponent = new StudentsEnvironmentComponent(environment, client, cache);
        studentsComponent = new StudentsComponent(studentsEnvironmentComponent);
        //per section group permissions
        sgPermissionsEnvironmentComponent = new SGPermissionsEnvironmentComponent(environment, client, cache);
        sgPermissionsComponent = new SGPermissionsComponent(sgPermissionsEnvironmentComponent);
        //per reports
        reportsEnvironmentComponent = new ReportsEnvironmentComponent(app);
        notebookReportRepositoryComponent = new NotebooksReportRepositoryComponent(reportsEnvironmentComponent);
        reportFacadesComponent = new ReportRepositoryFacadeComponent(notebookReportRepositoryComponent, notebooksComponent, sectionGroupsComponent, sgPermissionsComponent);
        //per jobs
        jobEnvironmentComponent = new JobEnvironmentComponent(app);
    }
}
