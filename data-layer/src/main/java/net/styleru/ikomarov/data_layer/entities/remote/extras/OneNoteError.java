package net.styleru.ikomarov.data_layer.entities.remote.extras;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

import static net.styleru.ikomarov.data_layer.mapping.remote.extras.ErrorRemoteEntityMapping.*;

/**
 * Created by i_komarov on 20.02.17.
 */

public class OneNoteError {

    @SerializedName(FIELD_CODE)
    private int code;
    @SerializedName(FIELD_MESSAGE)
    private String message;
    @SerializedName(FIELD_API_URL)
    private String apiUrl;
    @SerializedName(FIELD_WARNINGS)
    private List<OneNoteWarning> warnings;

    private OneNoteError() {

    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getApiUrl() {
        return apiUrl;
    }

    public List<OneNoteWarning> getWarnings() {
        return warnings;
    }



    public static class Deserializer implements JsonDeserializer<OneNoteError> {

        @Override
        public OneNoteError deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject root = json.getAsJsonObject();

            OneNoteError entity = new OneNoteError();

            entity.code = root.get(FIELD_CODE).getAsInt();
            entity.message = root.get(FIELD_MESSAGE).getAsString();
            entity.apiUrl = root.get(FIELD_API_URL).getAsString();

            if(root.has(FIELD_WARNINGS)) {
                JsonElement warnings = root.get(FIELD_WARNINGS);
                if(warnings.isJsonArray()) {
                    entity.warnings = new Gson().fromJson(warnings, new TypeToken<List<OneNoteWarning>>(){}.getType());
                } else if(warnings.isJsonObject()) {
                    entity.warnings = Collections.singletonList(new Gson().fromJson(warnings, OneNoteWarning.class));
                }
            }

            return entity;
        }
    }
}
