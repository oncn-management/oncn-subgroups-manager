package net.styleru.ikomarov.domain_layer.mapping.student_sections;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.StudentSectionLocalEntity;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class LocalStudentSectionMapper implements Function<StudentSectionLocalEntity, String> {

    @Override
    public String apply(StudentSectionLocalEntity entity) throws Exception {
        return entity.getId();
    }
}
