package net.styleru.ikomarov.domain_layer.mapping.sections;

import net.styleru.ikomarov.data_layer.entities.remote.sections.SectionRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.SectionDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class RemoteSectionMapper implements Function<SectionRemoteEntity, SectionDTO> {
    @Override
    public SectionDTO apply(SectionRemoteEntity entity) throws Exception {
        return SectionDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getCreatedBy(),
                entity.getCreatedTime(),
                entity.getLastModifiedBy(),
                entity.getLastModifiedTime()
        );
    }
}
