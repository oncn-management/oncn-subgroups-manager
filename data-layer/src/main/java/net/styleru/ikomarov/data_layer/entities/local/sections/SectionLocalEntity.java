package net.styleru.ikomarov.data_layer.entities.local.sections;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static net.styleru.ikomarov.data_layer.mapping.local.sections.SectionsLocalEntityMapping.*;

/**
 * Created by i_komarov on 26.02.17.
 */

@StorIOSQLiteType(table = TABLE)
public class SectionLocalEntity {

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ID, key = true)
    String id;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_NOTEBOOK_ID)
    String notebookId;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_NAME, ignoreNull = true)
    String name;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_SELF)
    String self;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_CREATED_BY)
    String createdBy;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_CREATED_TIME)
    String createdTime;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_LAST_MODIFIED_BY)
    String lastModifiedBy;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_LAST_MODIFIED_TIME)
    String lastModifiedTime;

    public static SectionLocalEntity newInstance(String id, String notebookId, String name, String self, String createdBy, Date createdTime, String lastModifiedBy, Date lastModifiedTime) {
        SectionLocalEntity entity = new SectionLocalEntity();

        entity.id = id;
        entity.notebookId = notebookId;
        entity.name = name;
        entity.self = self;
        entity.createdBy = createdBy;
        entity.createdTime = new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).format(createdTime);
        entity.lastModifiedBy = lastModifiedBy;
        entity.lastModifiedTime = new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).format(lastModifiedTime);;

        return entity;
    }

    SectionLocalEntity() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getNotebookId() {
        return notebookId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getSelf() {
        return self;
    }

    @NonNull
    public String getCreatedBy() {
        return createdBy;
    }

    @NonNull
    public Date getCreatedTime() throws ParseException {
        return new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).parse(createdTime);
    }

    @NonNull
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    @NonNull
    public Date getLastModifiedTime() throws ParseException {
        return new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).parse(lastModifiedTime);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SectionLocalEntity that = (SectionLocalEntity) o;

        if (!id.equals(that.id)) return false;
        if (!notebookId.equals(that.notebookId)) return false;
        if (!name.equals(that.name)) return false;
        if (!self.equals(that.self)) return false;
        if (!createdBy.equals(that.createdBy)) return false;
        if (!createdTime.equals(that.createdTime)) return false;
        if (!lastModifiedBy.equals(that.lastModifiedBy)) return false;
        return lastModifiedTime.equals(that.lastModifiedTime);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + notebookId.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + self.hashCode();
        result = 31 * result + createdBy.hashCode();
        result = 31 * result + createdTime.hashCode();
        result = 31 * result + lastModifiedBy.hashCode();
        result = 31 * result + lastModifiedTime.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SectionLocalEntity{" +
                "id='" + id + '\'' +
                ", notebookId='" + notebookId + '\'' +
                ", name='" + name + '\'' +
                ", self='" + self + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdTime='" + createdTime + '\'' +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", lastModifiedTime='" + lastModifiedTime + '\'' +
                '}';
    }
}
