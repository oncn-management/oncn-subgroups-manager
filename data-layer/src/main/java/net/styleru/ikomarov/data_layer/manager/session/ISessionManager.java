package net.styleru.ikomarov.data_layer.manager.session;

import net.styleru.ikomarov.data_layer.contracts.session.SessionStatus;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 26.02.17.
 */

public interface ISessionManager {

    String RESOURCE_GRAPH = "https://graph.microsoft.com";

    String RESOURCE_ONENOTE = "https://onenote.com/";

    Observable<SessionStatus> getCurrentSessionStatus();

    Observable<String> getSessionToken(final String resourceType);

    String blockingGetSessionToken(final String resourceType) throws ExceptionBundle;

    Observable<String> refreshAndGetSessionToken(final String resourceType);

    String blockingRefreshAndGetSessionToken(final String resourceType) throws ExceptionBundle;

    void registerCallback(String key, SessionManager.IAuthenticationCallbacks callbacks);

    void unregisterCallback(String key);

    void deauthenticate();

    void authenticate(String code) throws ExceptionBundle;
}
