package net.styleru.ikomarov.presentation_layer.view.students_list;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import net.styleru.ikomarov.domain_layer.use_cases.students.StudentDeleteUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.students.StudentsGetUseCase;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.adapter.students.StudentsAdapter;
import net.styleru.ikomarov.presentation_layer.application.ISingletonProvider;
import net.styleru.ikomarov.presentation_layer.presenter.students_list.StudentsListPresenter;
import net.styleru.ikomarov.presentation_layer.utils.pagination.PaginationListener;
import net.styleru.ikomarov.presentation_layer.utils.pagination.State;
import net.styleru.ikomarov.presentation_layer.view.base.PresenterFragment;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.drawer.IDrawerView;
import net.styleru.ikomarov.presentation_layer.view_holder.students_list.StudentViewHolder;

import java.util.List;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsListFragment extends PresenterFragment<IStudentsListView, StudentsListPresenter> implements IStudentsListView, StudentViewHolder.InteractionCallbacks, PaginationListener.Callback {

    private RecyclerView studentsList;

    private StudentsAdapter adapter;

    private PaginationListener paginationListener;

    private FragmentState state;

    public static Fragment newInstance(String notebookId, String notebookName) {
        Fragment fragment = new StudentsListFragment();
        Bundle arguments = new Bundle();
        arguments.putString(FragmentState.KEY_NOTEBOOK_ID, notebookId);
        arguments.putString(FragmentState.KEY_NOTEBOOK_NAME, notebookName);
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.state = new FragmentState(getArguments(), savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onListStateChanged(State state) {
        getPresenter().loadStudents(state.getOffset(), state.getLimit());
    }

    @Override
    public void addStudents(List<StudentViewObject> items) {
        adapter.add(items);
    }

    @Override
    public void deleteStudent(String studentId) {
        adapter.remove(studentId);
    }

    @Override
    public void showError(@StringRes int error) {
        Snackbar errorSnack = Snackbar.make(getView(), getResources().getString(error), Snackbar.LENGTH_INDEFINITE);
        errorSnack.setAction(R.string.action_ok, (view) -> errorSnack.dismiss());
        errorSnack.setActionTextColor(getResources().getColor(R.color.colorAccent));
        errorSnack.show();
    }

    @Override
    public void onStudentClicked(int position, StudentViewObject item) {

    }

    @Override
    public void onStudentAction(int position, StudentViewObject item, MenuItem menuItem) {
        switch(menuItem.getItemId()) {
            case R.id.action_delete: {
                getPresenter().deleteStudent(item.getId());
                break;
            }
            case R.id.action_edit_permissions: {

                break;
            }
            case R.id.action_open_in_browser: {

                break;
            }
        }
    }

    @Override
    public void showConnectionFailedIndicator() {
        showError(R.string.error_connection_lost);
    }

    @Override
    public void showOperationDelayedDialog() {
        showError(R.string.notification_operation_delayed);
    }

    @Override
    public void showNoCachedDataPlug() {
        showError(R.string.notification_no_cached_data);
    }

    @Override
    public void showNoDataPlug() {
        showError(R.string.notification_no_data);
    }

    @Override
    public void showNoPermissionsDialog() {
        showError(R.string.notification_no_permissions);
    }

    @Override
    public void showAuthorizationDialog() {
        ((IDrawerView) getActivity()).showAuthenticationActivity();
    }

    @Override
    protected void bindUserInterface(View view) {
        paginationListener = new PaginationListener(this);
        studentsList = (RecyclerView) view.findViewById(R.id.fragment_students_list_recycler_view);
        studentsList.setLayoutManager(new LinearLayoutManager(getActivity()));

        //checking for previously saved adapter state
        Bundle savedInstanceState;
        if(null != (savedInstanceState = state.getSavedInstanceState())) {
            adapter = new StudentsAdapter(savedInstanceState);
        } else {
            adapter = new StudentsAdapter();
        }

        adapter.registerCallbacks(this);
        studentsList.setAdapter(adapter);

        //checking for previously saved list state
        Parcelable listState;
        if(null != (listState = state.getListState())) {
            studentsList.getLayoutManager().onRestoreInstanceState(listState);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        state.saveState(outState, studentsList);
        adapter.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(state.getNotebookName());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(R.string.title_manage_students);
        studentsList.setOnScrollListener(paginationListener);
        if(adapter.getItemCount() == 0) {
            getPresenter().loadStudents(0, paginationListener.getLimit());
        }
    }

    @Override
    public void onPause() {
        studentsList.setOnScrollListener(null);
        super.onPause();
    }

    @Override
    protected RetainContainer.Factory<StudentsListPresenter> providePresenterFactory() {
        ISingletonProvider provider = (ISingletonProvider) getActivity().getApplication();
        return new StudentsListPresenter.Factory(
                new StudentsGetUseCase(
                        provider.studentsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ),
                new StudentDeleteUseCase(
                        provider.studentsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ), provider.errorTranslation().provideOneNoteErrorCodeTranslator(),
                provider.logging().provideLoggingManager(),
                state.getNotebookId()
        );
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_students_list;
    }

    @Override
    public IStudentsListView getViewInterface() {
        return this;
    }

    private static class FragmentState {

        private static final String PREFIX = FragmentState.class.getCanonicalName() + ".";

        private static final String KEY_NOTEBOOK_ID = PREFIX + "NOTEBOOK_ID";
        private static final String KEY_NOTEBOOK_NAME = PREFIX + "NOTEBOOK_NAME";
        private static final String KEY_LIST_STATE = PREFIX + "LIST_STATE";

        private Bundle savedInstanceState;

        private String notebookId;
        private String notebookName;
        private Parcelable listState;

        public FragmentState(Bundle arguments, Bundle savedInstanceState) {
            this.savedInstanceState = savedInstanceState;
            if(arguments != null && arguments.containsKey(KEY_NOTEBOOK_ID) && arguments.containsKey(KEY_NOTEBOOK_NAME)) {
                this.notebookId = arguments.getString(KEY_NOTEBOOK_ID);
                this.notebookName = arguments.getString(KEY_NOTEBOOK_NAME);
            } else if(savedInstanceState != null) {
                if(savedInstanceState.containsKey(KEY_NOTEBOOK_ID)) {
                    this.notebookId = savedInstanceState.getString(KEY_NOTEBOOK_ID);
                }
                if(savedInstanceState.containsKey(KEY_NOTEBOOK_NAME)) {
                    this.notebookName = savedInstanceState.getString(KEY_NOTEBOOK_NAME);
                }
                if(savedInstanceState.containsKey(KEY_LIST_STATE)) {
                    this.listState = savedInstanceState.getParcelable(KEY_LIST_STATE);
                }
            }
        }

        public void saveState(Bundle outState, RecyclerView list) {
            outState.putString(KEY_NOTEBOOK_ID, notebookId);
            outState.putString(KEY_NOTEBOOK_NAME, notebookName);
            outState.putParcelable(KEY_LIST_STATE, list.getLayoutManager().onSaveInstanceState());
        }

        public Bundle getSavedInstanceState() {
            return this.savedInstanceState;
        }

        public String getNotebookId() {
            return this.notebookId;
        }

        public String getNotebookName() {
            return this.notebookName;
        }

        public Parcelable getListState() {
            return this.listState;
        }
    }
}
