package net.styleru.ikomarov.data_layer.contracts.response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.contracts.model.DataModel;
import net.styleru.ikomarov.data_layer.entities.remote.extras.OneNoteError;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static net.styleru.ikomarov.data_layer.contracts.response.DataResponseMapping.*;

/**
 * Created by i_komarov on 21.02.17.
 */

public class DataResponse<T> extends DataModel<T> {

    @SerializedName(FIELD_BODY)
    private JsonElement bodyRaw;

    private T bodyActual;

    @SerializedName(FIELD_ERROR)
    private ExceptionBundle error;

    public static <T> DataResponse<T> unsuccessfulRequestResult(ExceptionBundle error) {
        DataResponse<T> response = new DataResponse<>();
        response.error = error;
        return response;
    }

    public static <T> DataResponse<T> successfulRequestResult(T data) {
        DataResponse<T> response = new DataResponse<>();
        response.bodyActual = data;
        return response;
    }

    public static <T> DataResponse<T> emptyRequestResult() {
        return new DataResponse<>();
    }

    private DataResponse() {

    }

    @Override
    public boolean containsError() {
        return error != null;
    }

    @Override
    public boolean containsData() {
        return bodyRaw != null || bodyActual != null;
    }

    @Override
    public String toString() {
        return "DataResponse{" +
                "bodyRaw=" + bodyRaw +
                ", bodyActual=" + bodyActual +
                ", error=" + error +
                '}';
    }

    @Override
    public List<T> getBody(IRemoteMapping<T> mapping) {
        if(bodyRaw != null) {
            if (bodyRaw.isJsonArray()) {
                return mapping.fromJsonArray(bodyRaw.getAsJsonArray());
            } else if (bodyRaw.isJsonObject()) {
                return Collections.singletonList(mapping.fromJson(bodyRaw.getAsJsonObject()));
            } else {
                return new ArrayList<>();
            }
        } else if(bodyActual != null) {
            return Collections.singletonList(bodyActual);
        } else {
            return new ArrayList<>(0);
        }
    }

    @Override
    public ExceptionBundle getError() {
        return error;
    }

    public static class Deserializer implements JsonDeserializer<DataResponse> {

        @Override
        public DataResponse deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            Gson gson = new GsonBuilder()
                    .registerTypeAdapter(OneNoteError.class, new OneNoteError.Deserializer())
                    .create();

            JsonObject root = json.getAsJsonObject();

            DataResponse entity = new DataResponse();

            if(root.has(FIELD_BODY)) {
                entity.bodyRaw = root.get(FIELD_BODY);
            } else {
                Map<String, JsonElement> extras = gson.fromJson(root, new TypeToken<Map<String, JsonElement>>(){}.getType());

                if(extras.containsKey(FIELD_WARNINGS)) {
                    extras.remove(FIELD_WARNINGS);
                }
                if(extras.containsKey(FIELD_ERROR)) {
                    extras.remove(FIELD_ERROR);
                }

                JsonObject body = new JsonObject();

                for(Map.Entry<String, JsonElement> extra : extras.entrySet()) {
                    body.add(extra.getKey(), extra.getValue());
                }

                entity.bodyRaw = body;
            }

            if(root.has(FIELD_ERROR)) {
                OneNoteError apiError = gson.fromJson(root.get(FIELD_ERROR), OneNoteError.class);

                entity.error = new ExceptionBundle(ExceptionBundle.Reason.ONE_NOTE_ERROR);

                entity.error.addStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_MESSAGE, apiError.getMessage());
                entity.error.addStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_API_URL, apiError.getApiUrl());
                entity.error.addIntExtra(ExceptionBundle.OneNoteErrorContract.KEY_CODE, apiError.getCode());
            }

            return entity;
        }
    }
}
