package net.styleru.ikomarov.domain_layer.di.endpoints.students;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;

/**
 * Created by i_komarov on 19.03.17.
 */

public interface IStudentsEnvironmentComponent {

    @NonNull
    ICacheComponent cacheComponent();

    @NonNull
    StudentsLocalServiceModule localService();

    @NonNull
    StudentsRemoteServiceModule remoteService();

    @NonNull
    StudentsMappingModule mapping();
}
