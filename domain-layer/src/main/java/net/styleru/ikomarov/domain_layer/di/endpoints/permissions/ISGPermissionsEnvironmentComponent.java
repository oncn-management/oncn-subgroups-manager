package net.styleru.ikomarov.domain_layer.di.endpoints.permissions;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;

/**
 * Created by i_komarov on 22.03.17.
 */

public interface ISGPermissionsEnvironmentComponent {

    @NonNull
    ICacheComponent cacheComponent();

    @NonNull
    SGPermissionsLocalServiceModule localService();

    @NonNull
    SGPermissionsRemoteServiceModule remoteService();

    @NonNull
    SGPermissionsMappingModule mapping();
}
