package net.styleru.ikomarov.data_layer.exceptions;

import net.styleru.ikomarov.data_layer.contracts.exception.ExceptionSource;

/**
 * Created by i_komarov on 07.03.17.
 */

public abstract class ExternalException extends Exception {

    public abstract ExceptionSource getSource();
}
