package net.styleru.ikomarov.domain_layer.facades.repository;

import android.support.annotation.NonNull;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 18.04.17.
 */

public interface IReportRepositoryFacade {

    Observable<String> createReport(@NonNull String notebookId);
}
