package net.styleru.ikomarov.presentation_layer.view.students_list;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentViewObject implements Parcelable {

    private String id;
    private String name;
    private String email;

    private StudentViewObject(Parcel in) {
        id = in.readString();
        name = in.readString();
        email = in.readString();
    }

    public StudentViewObject(String id, String name, String email) {
        this.id = id;
        this.name = name;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(email);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<StudentViewObject> CREATOR = new Creator<StudentViewObject>() {
        @Override
        public StudentViewObject createFromParcel(Parcel in) {
            return new StudentViewObject(in);
        }

        @Override
        public StudentViewObject[] newArray(int size) {
            return new StudentViewObject[size];
        }
    };
}
