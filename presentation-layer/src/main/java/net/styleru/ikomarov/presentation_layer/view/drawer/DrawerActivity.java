package net.styleru.ikomarov.presentation_layer.view.drawer;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import net.styleru.ikomarov.authentication.login.LoginActivity;

import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.application.ISingletonProvider;
import net.styleru.ikomarov.presentation_layer.presenter.drawer.DrawerPresenter;
import net.styleru.ikomarov.presentation_layer.view.base.PresenterActivity;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.notebooks_list.NotebooksListFragment;

public class DrawerActivity extends PresenterActivity<IDrawerView, DrawerPresenter> implements IDrawerView, FragmentManager.OnBackStackChangedListener {

    private static final int CODE_AUTHORIZATION = 9901;

    private Toolbar toolbar;

    private CoordinatorLayout rootView;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle drawerToggle;

    //TODO: implement an independent service-side authentication and other data related stuff that's needed in background

    /*
    private ISessionCallbacks.Stub callbacks = new ISessionCallbacks.Stub() {
        @Override
        public void onAuthenticated() throws RemoteException {
            Log.d("DrawerActivity#SessionService", "onAuthenticated");
        }

        @Override
        public void onDeauthenticated() throws RemoteException {
            Log.d("DrawerActivity#SessionService", "onDeauthenticated");
        }

        @Override
        public void onAuthenticationFailure(ServiceException exception) throws RemoteException {
            Log.d("DrawerActivity#SessionService", "onAuthenticationFailure");
        }
    };

    private ISessionControl sessionControl;

    private ServiceConnection sessionServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d("DrawerActivity", "service connected, name: " + name);
            sessionControl = ISessionControl.Stub.asInterface(service);
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            sessionControl = null;
        }
    };
    */

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        /*
        bindService(new Intent(this, SessionService.class), sessionServiceConnection, BIND_AUTO_CREATE);
         */
    }

    @Override
    public void onStart() {
        super.onStart();
        getFragmentManager().addOnBackStackChangedListener(this);
    }

    @Override
    public void onStop() {
        getFragmentManager().removeOnBackStackChangedListener(this);
        super.onStop();
    }

    @Override
    public void showAuthenticationActivity() {
        startActivityForResult(new Intent(this, LoginActivity.class), CODE_AUTHORIZATION);
    }

    @Override
    public void setupUI() {
        rootView       = (CoordinatorLayout) findViewById(R.id.activity_drawer_root);
        drawerLayout   = (DrawerLayout) findViewById(R.id.activity_drawer_drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.activity_drawer_navigation_view);
        toolbar        = (Toolbar) findViewById(R.id.activity_drawer_toolbar);

        setupToolbar(toolbar);
        setupDrawer();

        if(getFragmentManager().findFragmentById(R.id.activity_drawer_fragment_container) == null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.activity_drawer_fragment_container, NotebooksListFragment.newInstance())
                    .commit();
        }
    }

    @Override
    protected void handleActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CODE_AUTHORIZATION) {
            if(resultCode == RESULT_OK) {
                getPresenter().authenticate(data.getStringExtra(LoginActivity.KEY_OAUTH_CODE));
            } else {
                showAuthenticationActivity();
            }
        }
    }

    @Override
    public void showError(@StringRes int error) {
        Snackbar errorSnack = Snackbar.make(rootView, getResources().getString(error), Snackbar.LENGTH_INDEFINITE);
        errorSnack.setAction(R.string.action_ok, (view) -> errorSnack.dismiss());
        errorSnack.setActionTextColor(getResources().getColor(R.color.colorAccent));
        errorSnack.show();
    }

    @Override
    public void showConnectionFailedIndicator() {
        showError(R.string.error_connection_lost);
    }

    @Override
    public void onBackStackChanged() {
        drawerToggle.setDrawerIndicatorEnabled(getFragmentManager().getBackStackEntryCount() == 0);
    }

    @Override
    protected RetainContainer.Factory<DrawerPresenter> providePresenterFactory() {
        ISingletonProvider provider = ((ISingletonProvider) getApplication());
        return new DrawerPresenter.Factory(
                provider.logging().provideLoggingManager(),
                provider.session().provideSessionManager(),
                provider.network().provideNetworkManager()
        );
    }

    @Override
    protected IDrawerView getView() {
        return this;
    }

    private void setupToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setupDrawer() {
        navigationView.setNavigationItemSelectedListener(this::onNavigationViewItemSelected);

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_opened, R.string.drawer_closed);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();

        drawerLayout.addDrawerListener(drawerToggle);

        drawerToggle.setDrawerIndicatorEnabled(getFragmentManager().getBackStackEntryCount() == 0);

        toolbar.setNavigationOnClickListener((view) -> onNavigationClicked());
    }

    private boolean onNavigationViewItemSelected(MenuItem item) {
        Fragment fragment = null;
        final int id = item.getItemId();
        switch(id) {

        }

        if(fragment != null) {
            final int backStackDepth = getFragmentManager().getBackStackEntryCount();

            for(int i = 0; i < backStackDepth; i++) {
                getFragmentManager().popBackStack();
            }

            getFragmentManager().beginTransaction().replace(R.id.activity_drawer_fragment_container, fragment).commit();
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            drawerLayout.closeDrawer(GravityCompat.START);
            return false;
        }

        return true;
    }

    private void onNavigationClicked() {
        if(drawerToggle.isDrawerIndicatorEnabled()) {
            drawerLayout.openDrawer(GravityCompat.START);
        } else {
            onBackPressed();
        }
    }

    @Override
    public void onDestroy() {
        /*
        unbindService(sessionServiceConnection);
        */
        super.onDestroy();
    }
}
