package net.styleru.ikomarov.data_layer.entities.report.base;

import android.support.annotation.NonNull;

import java.util.List;

/**
 * Created by i_komarov on 17.04.17.
 */

public class ReportGroupEntity<T extends ReportSubgroupEntity> extends ReportSubgroupEntity {

    @NonNull
    private final String id;

    @NonNull
    private final List<T> subgroups;

    public ReportGroupEntity(@NonNull String id, @NonNull String name, @NonNull List<T> subgroups) {
        super(name);
        this.id = id;
        this.subgroups = subgroups;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public List<T> getSubgroups() {
        return subgroups;
    }

    @Override
    public String toString() {
        return "ReportGroupEntity{" +
                "id='" + id + '\'' +
                ", subgroups=" + subgroups +
                '}';
    }
}
