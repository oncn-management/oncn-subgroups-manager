package net.styleru.ikomarov.presentation_layer.view_holder.notebooks_list;

import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.view.notebooks_list.NotebookViewObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by i_komarov on 14.03.17.
 */

public class NotebookViewHolder extends RecyclerView.ViewHolder implements PopupMenu.OnMenuItemClickListener, View.OnClickListener {

    private AppCompatTextView subgroupsCountHolder;
    private AppCompatTextView lastUpdateHolder;
    private AppCompatTextView titleHolder;
    private AppCompatImageButton actionOptionsHolder;

    private PopupMenu menu;

    private SimpleDateFormat contractDateTimeFormat = new SimpleDateFormat(DateTimeContract.VIEW_DATE_FORMAT, Locale.getDefault());

    private int position;
    private NotebookViewObject item;
    private InteractionCallbacks callbacks;

    public NotebookViewHolder(View itemView) {
        super(itemView);

        subgroupsCountHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_notebook_subgroups_count);
        lastUpdateHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_notebook_last_update);
        titleHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_notebook_title);
        actionOptionsHolder = (AppCompatImageButton) itemView.findViewById(R.id.list_item_notebook_options_button);

        menu = new PopupMenu(itemView.getContext(), actionOptionsHolder);
        menu.getMenuInflater().inflate(R.menu.menu_list_item_notebook, menu.getMenu());

        actionOptionsHolder.setOnClickListener((view) -> {
            prepareOptionsMenu(menu.getMenu());
            menu.show();
        });

        menu.setOnMenuItemClickListener(this);
        itemView.setOnClickListener(this);
    }

    public void bind(int position, NotebookViewObject item, InteractionCallbacks callbacks) {
        this.position = position;
        this.item = item;
        this.callbacks = callbacks;

        lastUpdateHolder.setText(parseDate(item.getLastModified()));
        titleHolder.setText(item.getName());
    }

    @Override
    public void onClick(View v) {
        postClick(position, item);
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        final int id = menuItem.getItemId();
        if(id == R.id.action_extended_content || id == R.id.action_report_create || id == R.id.action_extended_content || id == R.id.action_unlock || id == R.id.action_enable_teachers_only_section_group) {
            postAction(position, item, menuItem);
            return true;
        } else {
            return false;
        }
    }

    private String parseDate(Date date) {
        return contractDateTimeFormat.format(date);
    }

    private void postClick(int position, NotebookViewObject item) {
        if(callbacks != null) {
            callbacks.onNotebookClicked(position, item);
        }
    }

    private void postAction(int position, NotebookViewObject item, MenuItem menuItem) {
        if(callbacks != null) {
            callbacks.onNotebookAction(position, item, menuItem);
        }
    }

    private void prepareOptionsMenu(Menu menu) {
        boolean isLocked = item.isCollaborationSpaceLocked();
        boolean isTeacherOnlyDisabled = !item.isHasTeacherOnlySectionGroup();

        menu.findItem(R.id.action_extended_content).setVisible(true);
        menu.findItem(R.id.action_report_create).setVisible(true);
        menu.findItem(R.id.action_lock).setVisible(!isLocked);
        menu.findItem(R.id.action_unlock).setVisible(isLocked);
        menu.findItem(R.id.action_enable_teachers_only_section_group).setVisible(isTeacherOnlyDisabled);
    }

    public interface InteractionCallbacks {

        void onNotebookClicked(int position, NotebookViewObject item);

        void onNotebookAction(int position, NotebookViewObject item, MenuItem menuItem);
    }
}
