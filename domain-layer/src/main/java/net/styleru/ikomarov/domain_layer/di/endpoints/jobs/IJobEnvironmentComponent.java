package net.styleru.ikomarov.domain_layer.di.endpoints.jobs;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 20.04.17.
 */

public interface IJobEnvironmentComponent {

    @NonNull
    JobEnvironmentModule environment();
}
