package net.styleru.ikomarov.data_layer.exceptions.external;

import net.styleru.ikomarov.data_layer.contracts.exception.ExceptionSource;
import net.styleru.ikomarov.data_layer.exceptions.ExternalException;

/**
 * Created by i_komarov on 07.03.17.
 */

public class NetworkException extends ExternalException {

    private Reason reason;
    private String message;
    private Throwable origin;

    public NetworkException(Reason reason, String message, Throwable origin) {
        this.reason = reason;
        this.message = message;
        this.origin = origin;
    }

    public Reason getReason() {
        return this.reason;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getOrigin() {
        return origin;
    }

    @Override
    public ExceptionSource getSource() {
        return ExceptionSource.NETWORK;
    }

    public enum Reason {
        INTERNET_LOSS,
        SSL_HANDSHAKE_FAIL
    }
}
