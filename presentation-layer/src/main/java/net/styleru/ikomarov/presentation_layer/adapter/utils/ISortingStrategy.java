package net.styleru.ikomarov.presentation_layer.adapter.utils;

import net.styleru.ikomarov.presentation_layer.adapter.utils.SortingStrategyType;

/**
 * Created by i_komarov on 14.03.17.
 */

public interface ISortingStrategy {

    SortingStrategyType current();
}
