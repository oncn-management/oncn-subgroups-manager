package net.styleru.ikomarov.data_layer.services.permissions.section_groups;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by i_komarov on 21.03.17.
 */

public interface ISGPermissionsRemoteStorageService {

    Observable<Response<DataResponse<PermissionRemoteEntity>>> create(String userSpacePath, String sectionGroupId, JsonObject permissionJson);

    //read permissions for the specific section group
    Observable<Response<DataResponse<PermissionRemoteEntity>>> read(String userSpacePath, String sectionGroupId);

    //read permission for the specific section group and user
    Observable<Response<DataResponse<PermissionRemoteEntity>>> read(String userSpacePath, String sectionGroupId, String userId);

    Observable<Response<DataResponse<Object>>> delete(String userSpacePath, String sectionGroupId, String permissionId);

}
