package net.styleru.ikomarov.domain_layer.repository.utils;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.mapping.utils.QueryBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by i_komarov on 08.03.17.
 */

public class ODataUtils {

    private static final String PARAM_LAST_MODIFIED = "lastModifiedTime";
    private static final String PARAM_NAME = "name";

    private static final String COMPARISON_GREATER_THAN = "gt";
    private static final String COMPARISON_GREATER_THAN_OR_EQUALS = "ge";
    private static final String COMPARISON_LESS_THAN = "lt";
    private static final String COMPARISON_LESS_THAN_OR_EQUALS = "le";

    private static final String ORDER_DESC = "desc";
    private static final String ORDER_ASC = "asc";

    public static String modifiedSince(Date timeStamp) {
        return PARAM_LAST_MODIFIED + " " + COMPARISON_GREATER_THAN + " " + new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).format(timeStamp);
    }

    public static String orderByLastModified() {
        return PARAM_LAST_MODIFIED + " " + ORDER_DESC;
    }

    public static String orderByName() {
        return PARAM_NAME + " " + ORDER_ASC;
    }

    public static String byNameSubstring(String filterQuery) {
        return "contains(tolower(" + PARAM_NAME + "),'" + filterQuery + "')";
    }

    public static String and(String c1, String c2) {
        return c1 + " and " + c2;
    }
}
