package net.styleru.ikomarov.domain_layer.use_cases.section_groups;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.repository.section_groups.ISectionGroupsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SectionGroupGetUseCase extends UseCase {

    @NonNull
    private ISectionGroupsRepository repository;

    @NonNull
    private Scheduler executeScheduler;

    @NonNull
    private Scheduler postScheduler;

    public SectionGroupGetUseCase(@NonNull ISectionGroupsRepository repository,
                                  @NonNull Scheduler executeScheduler,
                                  @NonNull Scheduler postScheduler) {
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<SectionGroupDTO, T> mapper, Callbacks<T> callbacks, String sectionGroupId) {
        execute(repository.get(sectionGroupId), mapper, callbacks);
    }

    protected <T> void execute(Observable<SectionGroupDTO> upstream, Function<SectionGroupDTO, T> mapper, Callbacks<T> callbacks) {
        upstream.subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createSectionGroupObserver(callbacks));
    }

    private <T> DisposableObserver<T> createSectionGroupObserver(final Callbacks<T> callbacks) {
        DisposableObserver<T> notebooksObserver = new DisposableObserver<T>() {
            @Override
            public void onNext(T value) {
                callbacks.onSectionGroupLoaded(value);
            }

            @Override
            public void onError(Throwable e) {
                if(e instanceof ExceptionBundle) {
                    callbacks.onSectionGroupLoadingFailed((ExceptionBundle) e);
                } else {
                    callbacks.onSectionGroupLoadingFailed(newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(notebooksObserver);

        return notebooksObserver;
    }

    public interface Callbacks<T> {

        void onSectionGroupLoaded(T sectionGroup);

        void onSectionGroupLoadingFailed(ExceptionBundle error);
    }
}
