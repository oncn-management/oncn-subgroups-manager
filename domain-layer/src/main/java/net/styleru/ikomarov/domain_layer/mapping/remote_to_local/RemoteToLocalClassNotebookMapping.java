package net.styleru.ikomarov.domain_layer.mapping.remote_to_local;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.entities.local.notebooks.ClassNotebookLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ClassNotebookRemoteEntity;

import java.text.SimpleDateFormat;
import java.util.Locale;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class RemoteToLocalClassNotebookMapping implements Function<ClassNotebookRemoteEntity, ClassNotebookLocalEntity> {

    private SimpleDateFormat contractDateFormat;

    public RemoteToLocalClassNotebookMapping() {
        this.contractDateFormat = new SimpleDateFormat(DateTimeContract.SQL_COMPARE_DATE_FORMAT, Locale.getDefault());
    }

    @Override
    public ClassNotebookLocalEntity apply(ClassNotebookRemoteEntity entity) throws Exception {
        return ClassNotebookLocalEntity.newInstance(
                entity.getId(),
                entity.getName(),
                entity.isDefault(),
                entity.isShared(),
                entity.getCreatedBy(),
                contractDateFormat.format(entity.getCreatedTime()),
                entity.getLastModifiedBy(),
                contractDateFormat.format(entity.getLastModifiedTime()),
                entity.getLinks().getWebUrl().getHref(),
                entity.getLinks().getClientUrl().getHref(),
                entity.getSectionsUrl(),
                entity.getSectionGroupsUrl(),
                entity.getSelf(),
                entity.getRole().value(),
                entity.hasTeacherOnlySectionGroup(),
                entity.getCollaborationSpaceLocked()
        );
    }
}
