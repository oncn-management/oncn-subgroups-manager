package net.styleru.ikomarov.data_layer.exceptions.external;

import net.styleru.ikomarov.data_layer.contracts.exception.ExceptionSource;
import net.styleru.ikomarov.data_layer.exceptions.ExternalException;

/**
 * Created by i_komarov on 07.03.17.
 */

public class DatabaseException extends ExternalException {

    private String message;
    private int code;

    public DatabaseException(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public int getCode() {
        return code;
    }

    @Override
    public ExceptionSource getSource() {
        return ExceptionSource.DATABASE;
    }
}
