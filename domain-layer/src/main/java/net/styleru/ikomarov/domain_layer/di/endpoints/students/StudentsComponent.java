package net.styleru.ikomarov.domain_layer.di.endpoints.students;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsComponent implements IStudentsComponent {

    @NonNull
    private final StudentsRepositoryModule repository;

    public StudentsComponent(@NonNull IStudentsEnvironmentComponent environment) {
        this.repository = new StudentsRepositoryModule(environment);
    }

    @NonNull
    @Override
    public StudentsRepositoryModule repository() {
        return repository;
    }
}
