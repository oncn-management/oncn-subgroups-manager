package net.styleru.ikomarov.domain_layer.mapping.remote_to_local;

import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.section_groups.SectionGroupRemoteEntity;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 15.03.17.
 */

public class RemoteToLocalSectionGroupMapper implements Function<SectionGroupRemoteEntity, SectionGroupLocalEntity> {

    @Override
    public SectionGroupLocalEntity apply(SectionGroupRemoteEntity entity) throws Exception {
        String notebookId = null;
        String sectionGroupId = null;

        if(entity.getParentSectionGroup() != null) {
            sectionGroupId = entity.getParentSectionGroup().getId();
        } else if(entity.getParentNotebook() != null) {
            notebookId = entity.getParentNotebook().getId();
        }

        return SectionGroupLocalEntity.newInstance(
                entity.getId(),
                notebookId,
                sectionGroupId,
                entity.getName(),
                entity.getSelf(),
                entity.getSectionGroupsUrl(),
                entity.getSectionsUrl(),
                entity.getCreatedBy(),
                entity.getCreatedTime(),
                entity.getLastModifiedBy(),
                entity.getLastModifiedTime()
        );
    }
}
