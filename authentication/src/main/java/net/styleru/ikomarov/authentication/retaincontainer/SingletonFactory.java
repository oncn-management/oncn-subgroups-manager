package net.styleru.ikomarov.authentication.retaincontainer;

/**
 * Created by i_komarov on 18.02.17.
 */

public final class SingletonFactory<T> implements Factory<T> {

    private final Object lock = new Object();

    private final Factory<T> factory;

    private T content;

    public SingletonFactory(Factory<T> factory) {
        this.factory = factory;
    }

    public final T create() {
        T localContent = content;
        if(localContent == null) {
            synchronized (lock) {
                localContent = content;
                if(localContent == null) {
                    localContent = content = factory.create();
                }
            }
        }

        return localContent;
    }
}
