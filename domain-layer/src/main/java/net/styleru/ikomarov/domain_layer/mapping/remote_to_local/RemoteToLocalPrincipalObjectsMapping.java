package net.styleru.ikomarov.domain_layer.mapping.remote_to_local;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 19.03.17.
 */

public class RemoteToLocalPrincipalObjectsMapping implements Function<List<PrincipalObjectRemoteEntity>, List<PrincipalObjectLocalEntity>> {
    @Override
    public List<PrincipalObjectLocalEntity> apply(List<PrincipalObjectRemoteEntity> principalObjectRemoteEntities) throws Exception {
        return Observable.fromIterable(principalObjectRemoteEntities)
                .map(new RemoteToLocalPrincipalObjectMapper())
                .toList()
                .blockingGet();
    }
}
