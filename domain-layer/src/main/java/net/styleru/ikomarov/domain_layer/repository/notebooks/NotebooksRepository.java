package net.styleru.ikomarov.domain_layer.repository.notebooks;

import android.util.Pair;

import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ClassNotebookRemoteEntity;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.services.class_notebooks.IClassNotebooksLocalStorageService;
import net.styleru.ikomarov.data_layer.services.class_notebooks.IClassNotebooksRemoteStorageService;
import net.styleru.ikomarov.data_layer.services.utils.IMemCache;

import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.contracts.policies.CachePolicy;
import net.styleru.ikomarov.domain_layer.cache.ICacheStrategy;
import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;
import net.styleru.ikomarov.domain_layer.mapping.class_notebooks.LocalClassNotebookMapper;
import net.styleru.ikomarov.domain_layer.mapping.class_notebooks.LocalClassNotebooksMapper;
import net.styleru.ikomarov.domain_layer.mapping.class_notebooks.RemoteClassNotebooksMapper;
import net.styleru.ikomarov.domain_layer.mapping.remote_to_local.RemoteToLocalClassNotebookMapping;
import net.styleru.ikomarov.domain_layer.repository.base.AbstractRepository;
import net.styleru.ikomarov.domain_layer.repository.utils.ODataUtils;
import net.styleru.ikomarov.domain_layer.utils.RxUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_CREATED;
import static java.net.HttpURLConnection.HTTP_NO_CONTENT;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_PARTIAL;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

/**
 * Created by i_komarov on 09.03.17.
 */

public class NotebooksRepository extends AbstractRepository implements INotebooksRepository {

    private static final String SELF = "me";

    private IClassNotebooksLocalStorageService localService;
    private IClassNotebooksRemoteStorageService remoteService;
    private IMemCache recordsCache;
    private ICacheStrategy strategy;
    private IRemoteMapping<ClassNotebookRemoteEntity> mapping;

    private NotebooksRepository(IClassNotebooksLocalStorageService localService,
                                IClassNotebooksRemoteStorageService remoteService,
                                IMemCache recordsCache,
                                ICacheStrategy strategy,
                                IRemoteMapping<ClassNotebookRemoteEntity> mapping) {
        super();
        this.localService = localService;
        this.remoteService = remoteService;
        this.recordsCache = recordsCache;
        this.strategy = strategy;
        this.mapping = mapping;
    }

    @Override
    public Observable<OperationStatus> create(UUID operationId, ClassNotebookDTO.Builder builder) {
        CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            //send an operation to the delayed operations stack
            //TODO: implement the operations stack
            return Observable.just(OperationStatus.DELAYED);
        }

        return performCache(remoteService.create(SELF, builder.build()), HTTP_CREATED)
                .map(data -> OperationStatus.SUCCESS);
    }

    @Override
    public Observable<ClassNotebookDTO> get(String id) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy == CachePolicy.CACHE_ONLY ? tryReadFromCache(id, true) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(policy, id) : Observable.empty()
        );
    }

    @Override
    public Observable<List<ClassNotebookDTO>> getList(int offset, int limit) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(offset, limit, shouldThrowOnNoCachedResourcesFound(policy)) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(policy, offset, limit) : Observable.empty()
        );
    }

    @Override
    public Observable<List<ClassNotebookDTO>> getList(int offset, int limit, String nameSubstring) {
        CachePolicy policy = strategy.currentPolicy();
        return Observable.merge(
                policy != CachePolicy.FORCE_NETWORK ? tryReadFromCache(offset, limit, nameSubstring, shouldThrowOnNoCachedResourcesFound(policy)) : Observable.empty(),
                policy != CachePolicy.CACHE_ONLY ? readUpdatesFromNetwork(policy, offset, limit, nameSubstring) : Observable.empty()
        );
    }

    @Override
    public Observable<OperationStatus> delete(UUID operationId, final String id) {
        CachePolicy policy = strategy.currentPolicy();
        if(policy == CachePolicy.CACHE_ONLY) {
            //send an operation to the delayed operations stack
            //TODO: implement the operations stack
            return Observable.just(OperationStatus.DELAYED);
        }

        return remoteService.delete(SELF, id)
                .flatMap(verifyResponse(Object.class, HTTP_NO_CONTENT))
                .doOnNext(objectDataResponse -> removeFromCache(id))
                .map(objectDataResponse -> OperationStatus.SUCCESS);
    }

    @Override
    public void release() {

    }

    private Observable<ClassNotebookDTO> tryReadFromCache(String id, boolean shouldThrowOnNoResourcesFound) {
        return entityStamp(id).flatMap(
                (wrapper) -> wrapper.isNull() ?
                        raiseNoResourcesException(shouldThrowOnNoResourcesFound) :
                        localService.read(id).map(new LocalClassNotebookMapper())
        );
    }

    private Observable<List<ClassNotebookDTO>> tryReadFromCache(int offset, int limit, boolean shouldThrowOnNoResourcesFound) {
        return entitiesCount().flatMap(
                (size) -> size == 0 ?
                        raiseNoResourcesException(shouldThrowOnNoResourcesFound) :
                        localService.read(offset, limit).map(new LocalClassNotebooksMapper())
        );
    }

    private Observable<List<ClassNotebookDTO>> tryReadFromCache(int offset, int limit, String nameSubstring, boolean shouldThrowOnNoResourcesFound) {
        return entitiesCount().flatMap(
                (size) -> size == 0 ?
                        raiseNoResourcesException(shouldThrowOnNoResourcesFound) :
                        localService.read(offset, limit, nameSubstring).map(new LocalClassNotebooksMapper())
        );
    }

    private Observable<ClassNotebookDTO> readUpdatesFromNetwork(CachePolicy policy, String id) {
        Date lastModified = recordsCache.get(id);
        return performCache(lastModified != null && policy != CachePolicy.FORCE_NETWORK ?
                remoteService.read(SELF, id) :
                remoteService.read(SELF, id),
                HTTP_OK
        )
        .flatMap(Observable::fromIterable);
    }

    private Observable<List<ClassNotebookDTO>> readUpdatesFromNetwork(CachePolicy policy, int offset, int limit) {
        Date lastModified = recordsCache.lastModified();
        return performCache(lastModified != null && policy != CachePolicy.FORCE_NETWORK ?
                remoteService.read(SELF, offset, limit, ODataUtils.orderByLastModified(), ODataUtils.modifiedSince(lastModified)) :
                remoteService.read(SELF, offset, limit, ODataUtils.orderByLastModified()),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<ClassNotebookDTO>> readUpdatesFromNetwork(CachePolicy policy, int offset, int limit, String nameSubstring) {
        Date lastModified = recordsCache.lastModified();
        return performCache(lastModified != null && policy != CachePolicy.FORCE_NETWORK ?
                remoteService.read(SELF, offset, limit, ODataUtils.orderByName(), ODataUtils.and(ODataUtils.byNameSubstring(nameSubstring), ODataUtils.modifiedSince(lastModified))) :
                remoteService.read(SELF, offset, limit, ODataUtils.orderByName(), ODataUtils.byNameSubstring(nameSubstring)),
                HTTP_OK, HTTP_PARTIAL
        );
    }

    private Observable<List<ClassNotebookDTO>> performCache(Observable<Response<DataResponse<ClassNotebookRemoteEntity>>> fromNetwork, Integer... successCodes) {
        return fromNetwork.flatMap(verifyResponse(ClassNotebookRemoteEntity.class, successCodes))
                .doOnNext(response -> addToCache(response, mapping))
                .flatMap(response -> Observable.just(response.getBody(mapping))
                        .map(new RemoteClassNotebooksMapper()));
    }

    private void addToCache(DataResponse<ClassNotebookRemoteEntity> data, IRemoteMapping<ClassNotebookRemoteEntity> mapping) {
        Observable.fromIterable(data.getBody(mapping))
                .map(new RemoteToLocalClassNotebookMapping())
                .flatMap(local -> localService.create(local).map(putResult -> new Pair<>(local.getId(), local.getLastModifiedTime())))
                .subscribe(timestamp -> recordsCache.onModified(timestamp.first, timestamp.second));
    }

    private void removeFromCache(final String id) {
        localService.deleteNotebook(id)
                .subscribe(deleteResult -> recordsCache.onDeleted(id));
    }

    private Observable<RxUtils.NullSafe<Date>> entityStamp(String id) {
        return Observable.just(new RxUtils.NullSafe<>(recordsCache.get(id)))
                .flatMap((wrapper) -> wrapper.isNull() ?
                        Observable.defer(() -> Observable.just(0L).delay(50L, TimeUnit.MILLISECONDS).flatMap(val -> Observable.just(new RxUtils.NullSafe<>(recordsCache.get(id))))) :
                        Observable.just(wrapper)
                );
    }

    private Observable<Long> entitiesCount() {
        return Observable.just(recordsCache.size())
                .delay(20, TimeUnit.MILLISECONDS)
                .repeatUntil(() -> !recordsCache.isCold())
                .flatMap(nodeSize -> Observable.just(recordsCache.size()));
    }

    public static final class Factory {

        private Factory() {

        }

        public static INotebooksRepository create(IClassNotebooksLocalStorageService localService,
                                                  IClassNotebooksRemoteStorageService remoteService,
                                                  IMemCache recordsCache,
                                                  ICacheStrategy strategy,
                                                  IRemoteMapping<ClassNotebookRemoteEntity> mapping) {
            return new NotebooksRepository(localService, remoteService, recordsCache, strategy, mapping);
        }
    }
}
