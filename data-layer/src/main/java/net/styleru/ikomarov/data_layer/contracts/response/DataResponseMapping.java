package net.styleru.ikomarov.data_layer.contracts.response;

import android.support.annotation.NonNull;

import com.google.gson.GsonBuilder;

/**
 * Created by i_komarov on 21.02.17.
 */

public class DataResponseMapping {

    @NonNull
    public static final String FIELD_BODY = "value";

    @NonNull
    public static final String FIELD_ERROR = "error";

    @NonNull
    public static final String FIELD_WARNINGS = "@api.diagnostics";

    private DataResponseMapping() {
        throw new IllegalStateException("No instances please");
    }

    public static GsonBuilder modifyParser(GsonBuilder origin) {
        return origin.registerTypeAdapter(DataResponseMapping.class, new DataResponse.Deserializer());
    }
}
