package net.styleru.ikomarov.domain_layer.mapping.principal_objects;

import net.styleru.ikomarov.data_layer.contracts.principal_object.PrincipalObjectEntityType;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 09.03.17.
 */

public class LocalPrincipalObjectMapper implements Function<PrincipalObjectLocalEntity, PrincipalObjectDTO> {

    @Override
    public PrincipalObjectDTO apply(PrincipalObjectLocalEntity entity) throws Exception {
        return PrincipalObjectDTO.newInstance(
                entity.getId(),
                entity.getName(),
                entity.getEmail(),
                entity.getDepartment(),
                PrincipalObjectEntityType.forValue(entity.getType())
        );
    }
}
