package net.styleru.ikomarov.data_layer.exceptions.internal;

/**
 * Created by i_komarov on 25.02.17.
 */

public class EmptyCursorException extends Exception {

    private static final String MESSAGE = "Cursor contained 0 rows";

    public EmptyCursorException() {
        super(MESSAGE);
    }
}
