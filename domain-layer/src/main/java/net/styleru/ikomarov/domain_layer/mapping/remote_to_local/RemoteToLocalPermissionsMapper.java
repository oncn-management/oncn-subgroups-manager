package net.styleru.ikomarov.domain_layer.mapping.remote_to_local;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.permissions.PermissionCategory;
import net.styleru.ikomarov.data_layer.entities.local.permissions.PermissionLocalEntity;
import net.styleru.ikomarov.data_layer.entities.remote.permissions.PermissionRemoteEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 22.03.17.
 */

public class RemoteToLocalPermissionsMapper implements Function<List<PermissionRemoteEntity>, List<PermissionLocalEntity>> {

    @NonNull
    private final PermissionCategory hostType;

    @NonNull
    private final String hostId;

    public RemoteToLocalPermissionsMapper(@NonNull PermissionCategory hostType, @NonNull String hostId) {
        this.hostType = hostType;
        this.hostId = hostId;
    }

    @Override
    public List<PermissionLocalEntity> apply(List<PermissionRemoteEntity> permissionRemoteEntities) throws Exception {
        return Observable.fromIterable(permissionRemoteEntities)
                .map(new RemoteToLocalPermissionMapper(hostType, hostId))
                .toList()
                .blockingGet();
    }
}
