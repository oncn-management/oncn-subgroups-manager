package net.styleru.ikomarov.domain_layer.dto;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.contracts.principal_object.PrincipalObjectEntityType;

import static net.styleru.ikomarov.data_layer.mapping.remote.notebooks.PrincipalObjectsRemoteEntityMapping.FIELD_ID;
import static net.styleru.ikomarov.data_layer.mapping.remote.notebooks.PrincipalObjectsRemoteEntityMapping.FIELD_PRINCIPAL_TYPE;

/**
 * Created by i_komarov on 20.02.17.
 */

public class PrincipalObjectDTO {

    private String id;
    private String name;
    private String email;
    private String department;
    private PrincipalObjectEntityType type;

    public static PrincipalObjectDTO newInstance(String id, PrincipalObjectEntityType type) {
        PrincipalObjectDTO object = new PrincipalObjectDTO();

        object.id = id;
        object.type = type;

        return object;
    }

    public static PrincipalObjectDTO newInstance(String id, String name, String email, String department, PrincipalObjectEntityType type) {
        PrincipalObjectDTO object = new PrincipalObjectDTO();

        object.id = id;
        object.name = name;
        object.email = email;
        object.department = department;
        object.type = type;

        return object;
    }

    private PrincipalObjectDTO() {

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getDepartment() {
        return department;
    }

    public PrincipalObjectEntityType getType() {
        return type;
    }

    public static final class SimpleBuilder {

        private JsonObject json;

        public SimpleBuilder(String id, PrincipalObjectEntityType type) {
            this.json = new JsonObject();
            this.json.addProperty(FIELD_ID, id);
            this.json.addProperty(FIELD_PRINCIPAL_TYPE, type.value());
        }

        public JsonObject build() {
            return this.json;
        }
    }
}
