package net.styleru.ikomarov.domain_layer.use_cases.section_groups;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.JobManager;

import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.jobs.base.IBroadcastFactory;
import net.styleru.ikomarov.domain_layer.jobs.section_groups.SectionGroupCreateJob;
import net.styleru.ikomarov.domain_layer.repository.section_groups.ISectionGroupsRepository;

import java.util.UUID;

import io.reactivex.Scheduler;

/**
 * Created by i_komarov on 07.04.17.
 */

public class NestedSectionGroupCreateUseCase extends SectionGroupCreateUseCase {

    public NestedSectionGroupCreateUseCase(@NonNull IBroadcastFactory<SectionGroupCreateJob.JobParams> broadcastFactory,
                                           @NonNull JobManager jobManager,
                                           @NonNull ISectionGroupsRepository repository,
                                           @NonNull Scheduler executeScheduler,
                                           @NonNull Scheduler postScheduler) {

        super(broadcastFactory, jobManager, repository, executeScheduler, postScheduler);
    }

    @Override
    public UUID execute(Callbacks callbacks, String notebookId, SectionGroupDTO.Builder builder) {
        UUID operationId = UUID.randomUUID();
        execute(operationId, repository.createNested(operationId, notebookId, builder), callbacks, new SectionGroupCreateJob.JobParams(operationId, notebookId, builder, true));
        return operationId;
    }
}
