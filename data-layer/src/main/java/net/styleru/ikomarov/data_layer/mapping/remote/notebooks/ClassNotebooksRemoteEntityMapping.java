package net.styleru.ikomarov.data_layer.mapping.remote.notebooks;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.ClassNotebookRemoteEntity;

import java.util.List;

/**
 * Created by i_komarov on 14.02.17.
 */

public class ClassNotebooksRemoteEntityMapping implements IRemoteMapping<ClassNotebookRemoteEntity> {

    @NonNull
    public static final String FIELD_CREATED_BY = "createdBy";

    @NonNull
    public static final String FIELD_CREATED_TIME = "createdTime";

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_IS_DEFAULT = "isDefault";

    @NonNull
    public static final String FIELD_IS_SHARED = "isShared";

    @NonNull
    public static final String FIELD_LAST_MODIFIED_BY = "lastModifiedBy";

    @NonNull
    public static final String FIELD_LAST_MODIFIED_TIME = "lastModifiedTime";

    @NonNull
    public static final String FIELD_LINKS = "links";

    @NonNull
    public static final String FIELD_NAME = "name";

    @NonNull
    public static final String FIELD_SECTION_GROUPS = "sectionGroups";

    @NonNull
    public static final String FIELD_SECTION_GROUPS_URL = "sectionGroupsUrl";

    @NonNull
    public static final String FIELD_SECTIONS = "sections";

    @NonNull
    public static final String FIELD_SECTIONS_URL = "sectionsUrl";

    @NonNull
    public static final String FIELD_SELF = "self";

    @NonNull
    public static final String FIELD_USER_ROLE = "userRole";

    @NonNull
    public static final String FIELD_STUDENT_SECTIONS = "studentSections";

    @NonNull
    public static final String FIELD_TEACHERS = "teachers";

    @NonNull
    public static final String FIELD_STUDENTS = "students";

    @NonNull
    public static final String FIELD_HAS_TEACHER_ONLY_SECTION_GROUP = "hasTeacherOnlySectionGroup";

    @NonNull
    public static final String FIELD_IS_COLLABORATION_SPACE_LOCKED = "isCollaborationSpaceLocked";

    @NonNull
    public static final String FIELD_SECTION_IDS = "sectionIds";


    private final Gson gson;

    public ClassNotebooksRemoteEntityMapping() {
        this.gson = new GsonBuilder()
                .registerTypeAdapter(ClassNotebookRemoteEntity.class, new ClassNotebookRemoteEntity.Deserializer())
                .registerTypeAdapter(ClassNotebookRemoteEntity.class, new ClassNotebookRemoteEntity.Serializer())
                .create();
    }

    @Override
    public ClassNotebookRemoteEntity fromJson(JsonObject json) {
        return this.gson.fromJson(json, ClassNotebookRemoteEntity.class);
    }

    @Override
    public List<ClassNotebookRemoteEntity> fromJsonArray(JsonArray json) {
        return this.gson.fromJson(json, new TypeToken<List<ClassNotebookRemoteEntity>>(){}.getType());
    }

    @Override
    public String toJson(ClassNotebookRemoteEntity entity) {
        return this.gson.toJson(entity, ClassNotebookRemoteEntity.class);
    }

    @Override
    public String toJsonArray(List<ClassNotebookRemoteEntity> entities) {
        return this.gson.toJson(entities, new TypeToken<List<ClassNotebookRemoteEntity>>(){}.getType());
    }
}
