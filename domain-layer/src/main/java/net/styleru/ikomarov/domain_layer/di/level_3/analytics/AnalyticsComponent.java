package net.styleru.ikomarov.domain_layer.di.level_3.analytics;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;

/**
 * Created by i_komarov on 11.05.17.
 */

public class AnalyticsComponent implements IAnalyticsComponent {

    @NonNull
    private final AnalyticsModule analytics;

    public AnalyticsComponent(@NonNull IEnvironmentComponent component) {
        this.analytics = new AnalyticsModule(component);
    }

    @NonNull
    @Override
    public AnalyticsModule analytics() {
        return analytics;
    }
}
