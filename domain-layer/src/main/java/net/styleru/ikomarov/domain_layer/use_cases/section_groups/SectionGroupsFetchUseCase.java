package net.styleru.ikomarov.domain_layer.use_cases.section_groups;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.repository.section_groups.ISectionGroupsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.Date;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 21.03.17.
 */

public class SectionGroupsFetchUseCase extends UseCase {

    @NonNull
    protected final ISectionGroupsRepository repository;

    @NonNull
    private Scheduler executeScheduler;

    @NonNull
    private Scheduler postScheduler;

    public SectionGroupsFetchUseCase(@NonNull ISectionGroupsRepository repository,
                                     @NonNull Scheduler executeScheduler,
                                     @NonNull Scheduler postScheduler) {

        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Function<List<SectionGroupDTO>, List<T>> mapper, Callbacks<T> callbacks, String notebookId, Date ifModifiedSince) {
        execute(repository.fetch(notebookId, ifModifiedSince), mapper, callbacks);
    }

    protected final <T> void execute(Observable<List<SectionGroupDTO>> upstream, Function<List<SectionGroupDTO>, List<T>> mapper, Callbacks<T> callbacks) {
        upstream.subscribeOn(executeScheduler)
                .map(mapper)
                .observeOn(postScheduler)
                .subscribe(createNotebooksObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createNotebooksObserver(final Callbacks<T> callbacks) {
        DisposableObserver<List<T>> notebooksObserver = new DisposableObserver<List<T>>() {
            @Override
            public void onNext(List<T> value) {
                callbacks.onSectionGroupsFetched(value);
            }

            @Override
            public void onError(Throwable e) {
                if(e instanceof ExceptionBundle) {
                    callbacks.onSectionGroupsFetchFailed((ExceptionBundle) e);
                } else {
                    callbacks.onSectionGroupsFetchFailed(newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(notebooksObserver);

        return notebooksObserver;
    }

    public interface Callbacks<T> {

        void onSectionGroupsFetched(List<T> sectionGroups);

        void onSectionGroupsFetchFailed(ExceptionBundle error);
    }
}
