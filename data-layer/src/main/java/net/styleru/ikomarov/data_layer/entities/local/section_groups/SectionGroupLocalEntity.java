package net.styleru.ikomarov.data_layer.entities.local.section_groups;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static net.styleru.ikomarov.data_layer.mapping.local.section_groups.SectionGroupsLocalEntityMapping.*;

/**
 * Created by i_komarov on 26.02.17.
 */

@StorIOSQLiteType(table = TABLE)
public class SectionGroupLocalEntity {

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ID, key = true)
    String id;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_PARENT_NOTEBOOK_ID, ignoreNull = true)
    String notebookId;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_PARENT_SECTION_GROUP_ID, ignoreNull = true)
    String sectionGroupId;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_NAME, ignoreNull = true)
    String name;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_SELF)
    String self;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_SECTION_GROUPS_URL, ignoreNull = true)
    String sectionGroupsUrl;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_SECTIONS_URL)
    String sectionsUrl;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_CREATED_BY)
    String createdBy;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_CREATED_TIME)
    String createdTime;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_LAST_MODIFIED_BY)
    String lastModifiedBy;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_LAST_MODIFIED_TIME)
    String lastModifiedTime;

    public static SectionGroupLocalEntity newInstance(String id, String notebookId, String sectionGroupId, String name, String self, String sectionGroupsUrl, String sectionsUrl, String createdBy, Date createdTime, String lastModifiedBy, Date lastModifiedTime) {
        SectionGroupLocalEntity entity = new SectionGroupLocalEntity();

        entity.id = id;

        if(entity.sectionGroupId != null) {
            entity.notebookId = null;
            entity.sectionGroupId = sectionGroupId;
        } else {
            entity.notebookId = notebookId;
            entity.sectionGroupId = null;
        }
        entity.name = name;
        entity.self = self;
        entity.sectionGroupsUrl = sectionGroupsUrl;
        entity.sectionsUrl = sectionsUrl;
        entity.createdBy = createdBy;
        entity.createdTime = new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).format(createdTime);
        entity.lastModifiedBy = lastModifiedBy;
        entity.lastModifiedTime = new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).format(lastModifiedTime);

        return entity;
    }

    SectionGroupLocalEntity() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    @Nullable
    public String getNotebookId() {
        return notebookId;
    }

    @Nullable
    public String getSectionGroupId() {
        return sectionGroupId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getSelf() {
        return self;
    }

    @NonNull
    public String getSectionGroupsUrl() {
        return sectionGroupsUrl;
    }

    @NonNull
    public String getSectionsUrl() {
        return sectionsUrl;
    }

    @NonNull
    public String getCreatedBy() {
        return createdBy;
    }

    @NonNull
    public Date getCreatedTime() throws ParseException {
        return new SimpleDateFormat(DateTimeContract.API_DATE_FORMAT, Locale.getDefault()).parse(createdTime);
    }

    @NonNull
    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    @NonNull
    public String getLastModifiedTime() {
        return lastModifiedTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SectionGroupLocalEntity that = (SectionGroupLocalEntity) o;

        if (!id.equals(that.id)) return false;
        if (notebookId != null ? !notebookId.equals(that.notebookId) : that.notebookId != null)
            return false;
        if (sectionGroupId != null ? !sectionGroupId.equals(that.sectionGroupId) : that.sectionGroupId != null)
            return false;
        if (!name.equals(that.name)) return false;
        if (!self.equals(that.self)) return false;
        if (!sectionGroupsUrl.equals(that.sectionGroupsUrl)) return false;
        if (!sectionsUrl.equals(that.sectionsUrl)) return false;
        if (!createdBy.equals(that.createdBy)) return false;
        if (!createdTime.equals(that.createdTime)) return false;
        if (!lastModifiedBy.equals(that.lastModifiedBy)) return false;
        return lastModifiedTime.equals(that.lastModifiedTime);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (notebookId != null ? notebookId.hashCode() : 0);
        result = 31 * result + (sectionGroupId != null ? sectionGroupId.hashCode() : 0);
        result = 31 * result + name.hashCode();
        result = 31 * result + self.hashCode();
        result = 31 * result + sectionGroupsUrl.hashCode();
        result = 31 * result + sectionsUrl.hashCode();
        result = 31 * result + createdBy.hashCode();
        result = 31 * result + createdTime.hashCode();
        result = 31 * result + lastModifiedBy.hashCode();
        result = 31 * result + lastModifiedTime.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "SectionGroupLocalEntity{" +
                "id='" + id + '\'' +
                ", notebookId='" + notebookId + '\'' +
                ", sectionGroupId='" + sectionGroupId + '\'' +
                ", name='" + name + '\'' +
                ", self='" + self + '\'' +
                ", sectionGroupsUrl='" + sectionGroupsUrl + '\'' +
                ", sectionsUrl='" + sectionsUrl + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdTime='" + createdTime + '\'' +
                ", lastModifiedBy='" + lastModifiedBy + '\'' +
                ", lastModifiedTime='" + lastModifiedTime + '\'' +
                '}';
    }
}
