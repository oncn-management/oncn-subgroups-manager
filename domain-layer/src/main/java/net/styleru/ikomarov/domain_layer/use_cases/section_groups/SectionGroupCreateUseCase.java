package net.styleru.ikomarov.domain_layer.use_cases.section_groups;

import android.support.annotation.NonNull;

import com.birbit.android.jobqueue.JobManager;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.jobs.base.IBroadcastFactory;
import net.styleru.ikomarov.domain_layer.jobs.section_groups.SectionGroupCreateJob;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.repository.section_groups.ISectionGroupsRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 21.03.17.
 */

public class SectionGroupCreateUseCase extends UseCase {

    @NonNull
    private final JobManager jobManager;

    @NonNull
    protected final ISectionGroupsRepository repository;

    @NonNull
    private Scheduler executeScheduler;

    @NonNull
    private Scheduler postScheduler;

    @NonNull
    private final SectionGroupCreateJob.Factory jobFactory;

    private Map<UUID, CreateOperationResult> results;

    public SectionGroupCreateUseCase(@NonNull IBroadcastFactory<SectionGroupCreateJob.JobParams> broadcastFactory,
                                     @NonNull JobManager jobManager,
                                     @NonNull ISectionGroupsRepository repository,
                                     @NonNull Scheduler executeScheduler,
                                     @NonNull Scheduler postScheduler) {
        super();
        this.jobManager = jobManager;
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
        this.jobFactory = new SectionGroupCreateJob.Factory(broadcastFactory, repository);
        this.results = new HashMap<>();
    }

    public UUID execute(Callbacks callbacks, String notebookId, SectionGroupDTO.Builder builder) {
        UUID operationId = UUID.randomUUID();
        execute(operationId, repository.create(operationId, notebookId, builder), callbacks, new SectionGroupCreateJob.JobParams(operationId, notebookId, builder, false));
        return operationId;
    }

    public CreateOperationResult retrieveResult(UUID id) {
        final CreateOperationResult result = results.get(id);
        results.remove(id);
        return result;
    }

    protected final void execute(UUID operationId, Observable<CreateOperationResult> upstream, Callbacks callbacks, SectionGroupCreateJob.JobParams params) {
        upstream.subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createCreateSectionGroupOperationStatusObserver(operationId, callbacks, params));
    }

    private DisposableObserver<CreateOperationResult> createCreateSectionGroupOperationStatusObserver(UUID operationId, Callbacks callbacks, SectionGroupCreateJob.JobParams params) {
        DisposableObserver<CreateOperationResult> createSectionGroupOperationStatusObserver = new DisposableObserver<CreateOperationResult>() {
            @Override
            public void onNext(CreateOperationResult value) {
                if(!isDisposed()) {
                    if (value.getStatus() == OperationStatus.SUCCESS) {
                        results.put(value.getOperationId(), value);
                        callbacks.onSectionGroupCreationSucceeded(operationId, value.getResourceId());
                    } else if (value.getStatus() == OperationStatus.DELAYED) {
                        callbacks.onSectionGroupCreationDelayed(operationId);

                    }
                }

                if(value.getStatus() == OperationStatus.DELAYED) {
                    jobManager.addJobInBackground(jobFactory.create(params));
                }
            }

            @Override
            public void onError(Throwable e) {
                if(!isDisposed()) {
                    if (e instanceof ExceptionBundle) {
                        callbacks.onSectionGroupCreationFailed(operationId, (ExceptionBundle) e);
                    } else {
                        callbacks.onSectionGroupCreationFailed(operationId, newUseCaseError(e));
                    }
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(createSectionGroupOperationStatusObserver);

        return createSectionGroupOperationStatusObserver;
    }

    public interface Callbacks {

        void onSectionGroupCreationSucceeded(UUID operationId, String realResourceId);

        void onSectionGroupCreationFailed(UUID operationId, ExceptionBundle error);

        void onSectionGroupCreationDelayed(UUID operationId);
    }
}
