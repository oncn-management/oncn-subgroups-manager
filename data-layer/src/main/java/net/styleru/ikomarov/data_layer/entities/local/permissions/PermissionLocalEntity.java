package net.styleru.ikomarov.data_layer.entities.local.permissions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import net.styleru.ikomarov.data_layer.contracts.permissions.PermissionCategory;
import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;

import java.util.UUID;

import static net.styleru.ikomarov.data_layer.mapping.local.permissions.PermissionsLocalEntitiesMapping.*;

/**
 * Created by i_komarov on 26.02.17.
 */

@StorIOSQLiteType(table = TABLE)
public class PermissionLocalEntity {

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_PK, ignoreNull = true, key = true)
    Long pk;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ID)
    String id;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_HOST_ID)
    String parentId;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_HOST_TYPE)
    int categoryId;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_NAME)
    String name;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_SELF)
    String self;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_USER_ID)
    String userId;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_USER_ROLE)
    String userRole;

    public static PermissionLocalEntity newInstance(String id, String name, String self, String userId, String userRole) {
        PermissionLocalEntity entity = new PermissionLocalEntity();

        entity.id = id;
        entity.name = name;
        entity.self = self;
        entity.userId = userId;
        entity.userRole = userRole;

        return entity;
    }

    public static PermissionLocalEntity newInstance(String id, String hostId, int hostType, String name, String self, String userId, String userRole) {
        PermissionLocalEntity entity = new PermissionLocalEntity();

        entity.id = id;
        entity.parentId = hostId;
        entity.categoryId = hostType;
        entity.name = name;
        entity.self = self;
        entity.userId = userId;
        entity.userRole = userRole;

        return entity;
    }

    PermissionLocalEntity() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getParentId() {
        return parentId;
    }

    @NonNull
    public PermissionCategory getCategory() {
        return PermissionCategory.forValue(categoryId);
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getSelf() {
        return self;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @NonNull
    public UserRole getUserRole() {
        return UserRole.forValue(userRole);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PermissionLocalEntity that = (PermissionLocalEntity) o;

        if (categoryId != that.categoryId) return false;
        if (!id.equals(that.id)) return false;
        if (!parentId.equals(that.parentId)) return false;
        if (!name.equals(that.name)) return false;
        if (!self.equals(that.self)) return false;
        if (!userId.equals(that.userId)) return false;
        return userRole.equals(that.userRole);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + parentId.hashCode();
        result = 31 * result + categoryId;
        result = 31 * result + name.hashCode();
        result = 31 * result + self.hashCode();
        result = 31 * result + userId.hashCode();
        result = 31 * result + userRole.hashCode();
        return result;
    }
}
