package net.styleru.ikomarov.authentication.account;

import android.accounts.Account;
import android.os.Parcel;

/**
 * Created by i_komarov on 18.02.17.
 */

public class OncnAccount extends Account {

    public static final String RESOURCE_GRAPH = "https://graph.microsoft.com";

    public static final String RESOURCE_ONENOTE = "https://onenote.com/";

    public static final String TYPE = "net.styleru.ikomarov.oncnsubgroupsmanager";

    public static final String TOKEN_REFRESH = "net.styleru.ikomarov.oncnsubgroupsmanager.REFRESH_TOKEN";

    public static final String TOKEN_GRAPH = "net.styleru.ikomarov.oncnsubgroupsmanager.ACCESS_TOKEN_GRAPH";

    public static final String TOKEN_ONE_NOTE = "net.styleru.ikomarov.oncnsubgroupsmanager.ACCESS_TOKEN_ONE_NOTE";

    public static final String KEY_PASSWORD = "net.styleru.ikomarov.oncnsubgroupsmanager.KEY_PASSWORD";

    public static final String KEY_RESOURCE = "net.styleru.ikomarov.oncnsubgroupsmanager.KEY_RESOURCE";

    public OncnAccount(Parcel in) {
        super(in);
    }

    public OncnAccount(String name, String type) {
        super(name, type);
    }
}
