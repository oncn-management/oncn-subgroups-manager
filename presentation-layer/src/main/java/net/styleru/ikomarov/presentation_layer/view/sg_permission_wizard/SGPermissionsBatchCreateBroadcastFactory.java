package net.styleru.ikomarov.presentation_layer.view.sg_permission_wizard;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.jobs.base.IBroadcastFactory;
import net.styleru.ikomarov.domain_layer.jobs.permissions.SGPermissionsBatchCreateJob;
import net.styleru.ikomarov.domain_layer.jobs.section_groups.SectionGroupCreateJob;
import net.styleru.ikomarov.presentation_layer.BuildConfig;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.notifications.LocalNotificationsReceiver;

/**
 * Created by i_komarov on 21.04.17.
 */

public class SGPermissionsBatchCreateBroadcastFactory implements IBroadcastFactory<SGPermissionsBatchCreateJob.JobParams> {

    @NonNull
    private final Context context;

    @NonNull
    private final String sectionGroupName;

    @NonNull
    private final String sectionGroupCreatedPlaceholder;

    public SGPermissionsBatchCreateBroadcastFactory(@NonNull Context context, @NonNull String sectionGroupName) {
        this.context = context;
        this.sectionGroupName = sectionGroupName;
        this.sectionGroupCreatedPlaceholder = context.getResources().getString(R.string.notification_permissions_created);
    }

    @Override
    public Intent createBroadcast(SGPermissionsBatchCreateJob.JobParams params) {
        Intent intent = new Intent();
        intent.setAction(BuildConfig.LOCAL_NOTIFICATIONS_ACTION);

        Bundle extras = new Bundle();
        extras.putInt(LocalNotificationsReceiver.KEY_NOTIFICATION_TYPE, LocalNotificationsReceiver.NOTIFICATION_PERMISSIONS_SECTION_GROUP);
        extras.putString(LocalNotificationsReceiver.KEY_NOTIFICATION_TEXT, String.format(sectionGroupCreatedPlaceholder, sectionGroupName));
        extras.putString(LocalNotificationsReceiver.KEY_NOTIFICATION_UUID, params.getOperationId().toString());
        extras.putString(LocalNotificationsReceiver.KEY_RESOURCE_ID, params.getSectionGroupId());

        intent.putExtras(extras);

        return intent;
    }
}
