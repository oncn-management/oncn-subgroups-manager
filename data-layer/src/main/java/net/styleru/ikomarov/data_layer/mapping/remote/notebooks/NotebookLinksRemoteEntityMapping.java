package net.styleru.ikomarov.data_layer.mapping.remote.notebooks;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.NotebookLinksRemoteEntity;

import java.util.List;

/**
 * Created by i_komarov on 14.02.17.
 */

public class NotebookLinksRemoteEntityMapping implements IRemoteMapping<NotebookLinksRemoteEntity> {

    @NonNull
    public static final String FIELD_ONENOTE_CLIENT_URL = "oneNoteClientUrl";

    @NonNull
    public static final String FIELD_ONENOTE_WEB_URL = "oneNoteWebUrl";

    private final Gson gson;

    public NotebookLinksRemoteEntityMapping() {
        this.gson = new Gson();
    }

    @Override
    public NotebookLinksRemoteEntity fromJson(JsonObject json) {
        return gson.fromJson(json, NotebookLinksRemoteEntity.class);
    }

    @Override
    public List<NotebookLinksRemoteEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<NotebookLinksRemoteEntity>>(){}.getType());
    }

    @Override
    public String toJson(NotebookLinksRemoteEntity entity) {
        return gson.toJson(entity);
    }

    @Override
    public String toJsonArray(List<NotebookLinksRemoteEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<NotebookLinksRemoteEntity>>(){}.getType());
    }
}
