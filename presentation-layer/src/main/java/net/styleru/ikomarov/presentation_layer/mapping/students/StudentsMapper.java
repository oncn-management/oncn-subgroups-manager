package net.styleru.ikomarov.presentation_layer.mapping.students;

import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsMapper implements Function<List<PrincipalObjectDTO>, List<StudentViewObject>> {

    @Override
    public List<StudentViewObject> apply(List<PrincipalObjectDTO> principalObjectDTOs) throws Exception {
        return Observable.fromIterable(principalObjectDTOs)
                .map(new StudentMapper())
                .toList()
                .blockingGet();
    }
}
