package net.styleru.ikomarov.presentation_layer.presenter.drawer;

import android.util.Log;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.network.INetworkManager;
import net.styleru.ikomarov.data_layer.manager.session.ISessionManager;
import net.styleru.ikomarov.data_layer.manager.session.SessionManager;
import net.styleru.ikomarov.data_layer.manager.events.IEventsLogger;
import net.styleru.ikomarov.presentation_layer.presenter.base.Presenter;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.drawer.IDrawerView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by i_komarov on 04.03.17.
 */

public class DrawerPresenter extends Presenter<IDrawerView> implements SessionManager.IAuthenticationCallbacks {

    private static final String TAG = DrawerPresenter.class.getCanonicalName();

    private IEventsLogger logger;

    private ISessionManager sessionManager;
    private INetworkManager networkManager;

    private DrawerPresenter(IEventsLogger logger, ISessionManager sessionManager, INetworkManager networkManager) {
        this.logger = logger;
        this.sessionManager = sessionManager;
        this.networkManager = networkManager;
    }

    @Override
    protected void onViewAttached() {
        Log.d("DrawerPresenter", "onViewAttached");

    }

    @Override
    public void onResume() {
        Log.d("DrawerPresenter", "onResume");
        sessionManager.registerCallback(TAG, this);

        sessionManager.getCurrentSessionStatus()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((status) -> postAction((view) -> {
                    switch (status) {
                        case ACTIVE: {
                            view.setupUI();
                            Log.d("DrawerPresenter", "session is active");
                            break;
                        }
                        case INACTIVE: {
                            view.showAuthenticationActivity();
                            Log.d("DrawerPresenter", "session is inactive, requesting authentication");
                            break;
                        }
                    }
                }));
    }

    @Override
    public void onPause() {
        Log.d("DrawerPresenter", "onPause");
        sessionManager.unregisterCallback(TAG);
    }

    @Override
    protected void onViewDetached() {
        Log.d("DrawerPresenter", "onViewDetached");
    }

    public void authenticate(String code) {
        try {
            sessionManager.authenticate(code);
        } catch (ExceptionBundle e) {
            if(e.getReason() == ExceptionBundle.Reason.NETWORK_UNAVAILABLE) {
                postAction(IDrawerView::showConnectionFailedIndicator);
            }
        }
    }

    @Override
    public void onAuthenticated() {
        Log.d("DrawerPresenter", "onAuthenticated");
        postAction(IDrawerView::setupUI);
    }

    @Override
    public void onDeauthenticated() {
        postAction(IDrawerView::showAuthenticationActivity);
    }

    @Override
    public void onAuthenticationFailed(ExceptionBundle error) {
        String message = error.getReason().isInternalError() ?
                error.getStringExtra(ExceptionBundle.InternalErrorContract.KEY_MESSAGE) :
                error.getMessage();

        logger.logEvent(getClass().getSimpleName(), message, error.getReason());
    }

    public static final class Factory implements RetainContainer.Factory<DrawerPresenter> {

        private final IEventsLogger eventsLogger;
        private final ISessionManager sessionManager;
        private final INetworkManager networkManager;

        public Factory(IEventsLogger eventsLogger, ISessionManager sessionManager, INetworkManager networkManager) {
            this.eventsLogger = eventsLogger;
            this.sessionManager = sessionManager;
            this.networkManager = networkManager;
        }

        @Override
        public DrawerPresenter create() {
            return new DrawerPresenter(eventsLogger, sessionManager, networkManager);
        }
    }
}
