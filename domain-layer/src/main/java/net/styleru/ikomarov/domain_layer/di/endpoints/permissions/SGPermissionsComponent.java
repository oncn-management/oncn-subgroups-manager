package net.styleru.ikomarov.domain_layer.di.endpoints.permissions;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 22.03.17.
 */

public class SGPermissionsComponent implements ISGPermissionsComponent {

    @NonNull
    private final SGPermissionsRepositoryModule repository;

    public SGPermissionsComponent(@NonNull ISGPermissionsEnvironmentComponent environmentComponent) {
        this.repository = new SGPermissionsRepositoryModule(environmentComponent);
    }

    @NonNull
    @Override
    public SGPermissionsRepositoryModule repository() {
        return repository;
    }
}
