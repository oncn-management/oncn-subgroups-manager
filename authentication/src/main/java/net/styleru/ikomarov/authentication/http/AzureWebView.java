package net.styleru.ikomarov.authentication.http;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import net.styleru.ikomarov.authentication.web_interfaces.OAuth2;

/**
 * Created by i_komarov on 18.02.17.
 */

public class AzureWebView extends WebViewClient {

    private Callback callback;

    public AzureWebView(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);

        Uri uri = Uri.parse(url);

        String code = uri.getQueryParameter(OAuth2.QUERY_PARAM_CODE);

        if(code != null) {
            callback.onSuccess(uri.getQueryParameter(OAuth2.QUERY_PARAM_CODE));
        } else {
            String error = uri.getQueryParameter(OAuth2.QUERY_PARAM_ERROR);
            if(error != null) {
                String errorDescription = uri.getQueryParameter(OAuth2.QUERY_PARAM_ERROR_DESCRIPTION);
                callback.onFailure(OAuthError.parse(error), errorDescription);
            }
        }
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);

        if(url.contains(OAuth2.BASE_URL)) {
            callback.onPageLoaded();
        }
    }

    public interface Callback {

        void onPageLoaded();

        void onSuccess(String code);

        void onFailure(OAuthError error, String description);
    }
}
