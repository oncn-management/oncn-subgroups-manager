package net.styleru.ikomarov.domain_layer.dto;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.mapping.local.section_groups.SectionGroupsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.remote.section_groups.SectionGroupsRemoteEntityMapping;

import java.util.Date;

/**
 * Created by i_komarov on 20.02.17.
 */

public class SectionGroupDTO {

    private String id;
    private String name;
    private String createdBy;
    private Date createdTime;
    private String lastModifiedBy;
    private Date lastModifiedTime;
    private String sectionGroupsUrl;
    private String sectionsUrl;

    public static SectionGroupDTO newInstance(String id, String name, String createdBy, Date createdTime, String lastModifiedBy, Date lastModifiedTime, String sectionGroupsUrl, String sectionsUrl) {
        SectionGroupDTO object = new SectionGroupDTO();

        object.id = id;
        object.name = name;
        object.createdBy = createdBy;
        object.createdTime = createdTime;
        object.lastModifiedBy = lastModifiedBy;
        object.lastModifiedTime = lastModifiedTime;
        object.sectionGroupsUrl = sectionGroupsUrl;
        object.sectionsUrl = sectionsUrl;

        return object;
    }

    private SectionGroupDTO() {

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public Date getLastModifiedTime() {
        return lastModifiedTime;
    }

    public String getSectionGroupsUrl() {
        return sectionGroupsUrl;
    }

    public String getSectionsUrl() {
        return sectionsUrl;
    }

    public static final class Builder {

        private JsonObject json;

        private String name;

        public Builder(String name) {
            this.json = new JsonObject();
            this.name = name;
        }

        public String getName() {
            return this.name;
        }

        public JsonObject build() {
            this.json.addProperty(SectionGroupsRemoteEntityMapping.FIELD_NAME, name);
            return this.json;
        }
    }
}
