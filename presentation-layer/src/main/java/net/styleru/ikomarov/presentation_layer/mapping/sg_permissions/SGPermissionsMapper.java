package net.styleru.ikomarov.presentation_layer.mapping.sg_permissions;

import net.styleru.ikomarov.domain_layer.dto.PermissionDTO;
import net.styleru.ikomarov.presentation_layer.view.sg_permissions_list.SGPermissionViewObject;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 02.04.17.
 */

public class SGPermissionsMapper implements Function<List<PermissionDTO>, List<SGPermissionViewObject>> {

    @Override
    public List<SGPermissionViewObject> apply(List<PermissionDTO> permissionDTOs) {
        return Observable.fromIterable(permissionDTOs)
                .map(new SGPermissionMapper())
                .toList()
                .blockingGet();
    }
}
