package net.styleru.ikomarov.data_layer.services.section_groups;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.data_layer.entities.local.section_groups.SectionGroupLocalEntity;
import net.styleru.ikomarov.data_layer.mapping.local.section_groups.SectionGroupsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.utils.QueryBuilder;
import net.styleru.ikomarov.data_layer.services.base.DatabaseService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsLocalStorageService extends DatabaseService implements ISectionGroupsLocalStorageService {

    private SectionGroupsLocalStorageService(StorIOSQLite sqlite) {
        super(sqlite);
    }

    @Override
    public Observable<PutResult> create(SectionGroupLocalEntity entity) {
        return wrapTransactionSynchronousOff(() -> {
            return getSQLite().put().object(entity)
                    .prepare()
                    .executeAsBlocking();
        });
    }

    @Override
    public Observable<PutResults<SectionGroupLocalEntity>> create(List<SectionGroupLocalEntity> entities) {
        return wrapTransactionSynchronousOff(() -> {
            return getSQLite().put().objects(entities)
                    .prepare()
                    .executeAsBlocking();
        });
    }

    @Override
    public Observable<SectionGroupLocalEntity> read(String id) {
        return wrap(() -> {
            return new SectionGroupsLocalEntityMapping().fromCursor(
                    getSQLite().get()
                            .cursor()
                            .withQuery(
                                    RawQuery.builder().query(QueryBuilder.selectQuery(SectionGroupsLocalEntityMapping.TABLE)
                                            .where(SectionGroupsLocalEntityMapping.COLUMN_ID + " = " + "\"" + id + "\"")
                                            .build()
                                    ).build()
                            )
                            .prepare()
                            .executeAsBlocking()
            );
        });
    }

    @Override
    public Observable<List<SectionGroupLocalEntity>> read(String notebookId, Date ifModifiedSince) {
        return wrap(() -> {
            return new SectionGroupsLocalEntityMapping().listFromCursor(
                    getSQLite().get()
                            .cursor()
                            .withQuery(RawQuery.builder().query(
                                    QueryBuilder.selectQuery(SectionGroupsLocalEntityMapping.TABLE)
                                    .where(SectionGroupsLocalEntityMapping.COLUMN_PARENT_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"")
                                    .where(SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME + " > \'" + new SimpleDateFormat(DateTimeContract.SQL_COMPARE_DATE_FORMAT, Locale.getDefault()).format(ifModifiedSince) + "\'").build()
                            ).build())
                            .prepare()
                            .executeAsBlocking()
            );
        });
    }

    @Override
    public Observable<List<SectionGroupLocalEntity>> readAll(final String notebookId) {
        return wrap(() -> {
            return new SectionGroupsLocalEntityMapping().listFromCursor(
                    getSQLite().get()
                            .cursor()
                            .withQuery(Query.builder().table(SectionGroupsLocalEntityMapping.TABLE).where(SectionGroupsLocalEntityMapping.COLUMN_PARENT_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"").build())
                            .prepare()
                            .executeAsBlocking()
            );
        });
    }

    @Override
    public Observable<List<SectionGroupLocalEntity>> read(String notebookId, int offset, int limit) {
        return wrap(() -> {
            return new SectionGroupsLocalEntityMapping().listFromCursor(
                    getSQLite().get()
                            .cursor()
                            .withQuery(
                                    RawQuery.builder().query(QueryBuilder.selectQuery(SectionGroupsLocalEntityMapping.TABLE)
                                            .limit(offset, limit)
                                            .where(SectionGroupsLocalEntityMapping.COLUMN_PARENT_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"")
                                            .orderBy(SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME, QueryBuilder.Order.DESC)
                                            .build()
                                    ).build()
                            )
                            .prepare()
                            .executeAsBlocking()
            );
        });
    }

    @Override
    public Observable<List<SectionGroupLocalEntity>> read(String notebookId, int offset, int limit, String nameSubstring) {
        return wrap(() -> {
            return new SectionGroupsLocalEntityMapping().listFromCursor(
                    getSQLite().get()
                            .cursor()
                            .withQuery(
                                    RawQuery.builder().query(QueryBuilder.selectQuery(SectionGroupsLocalEntityMapping.TABLE)
                                            .limit(offset, limit)
                                            .orderBy(SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME, QueryBuilder.Order.DESC)
                                            .where(SectionGroupsLocalEntityMapping.COLUMN_PARENT_NOTEBOOK_ID + " = " + "\"" + notebookId + "\"")
                                            .where(
                                                    SectionGroupsLocalEntityMapping.COLUMN_NAME + " LIKE " + "\"" + nameSubstring + "%\"" +
                                                    " OR " +
                                                    SectionGroupsLocalEntityMapping.COLUMN_NAME + " = " + "\"" + nameSubstring + "\"")
                                            .build()
                                    ).build()
                            )
                            .prepare()
                            .executeAsBlocking()
            );
        });
    }

    @Override
    public Observable<List<SectionGroupLocalEntity>> readNested(String sectionGroupId, Date ifModifiedSince) {
        return wrap(() -> {
            return new SectionGroupsLocalEntityMapping().listFromCursor(
                    getSQLite().get()
                            .cursor()
                            .withQuery(RawQuery.builder().query(
                                    QueryBuilder.selectQuery(SectionGroupsLocalEntityMapping.TABLE)
                                            .where(SectionGroupsLocalEntityMapping.COLUMN_PARENT_SECTION_GROUP_ID + " = " + "\"" + sectionGroupId + "\"")
                                            .where(SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME + " > \'" + new SimpleDateFormat(DateTimeContract.SQL_COMPARE_DATE_FORMAT, Locale.getDefault()).format(ifModifiedSince) + "\'").build()
                            ).build())
                            .prepare()
                            .executeAsBlocking()
            );
        });
    }

    @Override
    public Observable<List<SectionGroupLocalEntity>> readAllNested(String sectionGroupId) {
        return wrap(() -> {
            return new SectionGroupsLocalEntityMapping().listFromCursor(
                    getSQLite().get()
                            .cursor()
                            .withQuery(Query.builder().table(SectionGroupsLocalEntityMapping.TABLE).where(SectionGroupsLocalEntityMapping.COLUMN_PARENT_SECTION_GROUP_ID + " = " + "\"" + sectionGroupId + "\"").build())
                            .prepare()
                            .executeAsBlocking()
            );
        });
    }

    @Override
    public Observable<List<SectionGroupLocalEntity>> readNested(String sectionGroupId, int offset, int limit) {
        return wrap(() -> {
            return new SectionGroupsLocalEntityMapping().listFromCursor(
                    getSQLite().get()
                            .cursor()
                            .withQuery(
                                    RawQuery.builder().query(QueryBuilder.selectQuery(SectionGroupsLocalEntityMapping.TABLE)
                                            .limit(offset, limit)
                                            .where(SectionGroupsLocalEntityMapping.COLUMN_PARENT_SECTION_GROUP_ID + " = " + "\"" + sectionGroupId + "\"")
                                            .orderBy(SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME, QueryBuilder.Order.DESC)
                                            .build()
                                    ).build()
                            )
                            .prepare()
                            .executeAsBlocking()
            );
        });
    }

    @Override
    public Observable<List<SectionGroupLocalEntity>> readNested(String sectionGroupId, int offset, int limit, String nameSubstring) {
        return wrap(() -> {
            return new SectionGroupsLocalEntityMapping().listFromCursor(
                    getSQLite().get()
                            .cursor()
                            .withQuery(
                                    RawQuery.builder().query(QueryBuilder.selectQuery(SectionGroupsLocalEntityMapping.TABLE)
                                            .limit(offset, limit)
                                            .orderBy(SectionGroupsLocalEntityMapping.COLUMN_LAST_MODIFIED_TIME, QueryBuilder.Order.DESC)
                                            .where(SectionGroupsLocalEntityMapping.COLUMN_PARENT_SECTION_GROUP_ID + " = " + "\"" + sectionGroupId + "\"")
                                            .where(
                                                    SectionGroupsLocalEntityMapping.COLUMN_NAME + " LIKE " + "\"" + nameSubstring + "%\"" +
                                                            " OR " +
                                                            SectionGroupsLocalEntityMapping.COLUMN_NAME + " = " + "\"" + nameSubstring + "\"")
                                            .build()
                                    ).build()
                            )
                            .prepare()
                            .executeAsBlocking()
            );
        });
    }

    public static final class Factory {

        private Factory() {

        }

        public static ISectionGroupsLocalStorageService create(StorIOSQLite sqlite) {
            return new SectionGroupsLocalStorageService(sqlite);
        }
    }
}
