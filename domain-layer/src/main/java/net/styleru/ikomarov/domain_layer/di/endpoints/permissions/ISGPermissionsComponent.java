package net.styleru.ikomarov.domain_layer.di.endpoints.permissions;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 22.03.17.
 */

public interface ISGPermissionsComponent {

    @NonNull
    SGPermissionsRepositoryModule repository();
}
