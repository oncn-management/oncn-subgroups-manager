package net.styleru.ikomarov.presentation_layer.view.section_groups_list;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;

import net.styleru.ikomarov.domain_layer.use_cases.section_groups.NestedSectionGroupsFetchUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.section_groups.NestedSectionGroupsGetUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.section_groups.SectionGroupGetUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.section_groups.SectionGroupsFetchUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.section_groups.SectionGroupsGetUseCase;
import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.adapter.section_groups_list.SectionGroupsAdapter;
import net.styleru.ikomarov.presentation_layer.adapter.utils.SortingStrategyType;
import net.styleru.ikomarov.presentation_layer.application.ISingletonProvider;
import net.styleru.ikomarov.presentation_layer.presenter.section_groups_list.SectionGroupsListPresenter;
import net.styleru.ikomarov.presentation_layer.utils.pagination.PaginationListener;
import net.styleru.ikomarov.presentation_layer.utils.pagination.State;
import net.styleru.ikomarov.presentation_layer.view.base.PresenterFragment;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.drawer.IDrawerView;
import net.styleru.ikomarov.presentation_layer.view.section_group_create.SectionGroupCreateActivity;
import net.styleru.ikomarov.presentation_layer.view.sg_permissions_list.SGPermissionsListFragment;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentsListFragment;
import net.styleru.ikomarov.presentation_layer.view_holder.section_groups_list.SectionGroupViewHolder;

import java.util.List;

/**
 * Created by i_komarov on 15.03.17.
 */

public class SectionGroupsListFragment extends PresenterFragment<ISectionGroupsListView, SectionGroupsListPresenter> implements ISectionGroupsListView, PaginationListener.Callback, SectionGroupViewHolder.InteractionCallbacks {

    private final int REQUEST_CODE_CREATE_SECTION_GROUP = 1197;

    private RecyclerView sectionGroupsList;

    private AppBarLayout appBarLayout;

    private AppCompatButton manageStudentsButton;

    private FloatingActionButton createSectionGroupButton;

    private SectionGroupsAdapter adapter;

    private PaginationListener paginationListener;

    private FragmentState state;

    public static Fragment newInstance(String notebookId, String notebookName, ContentState contentState) {
        Fragment fragment = new SectionGroupsListFragment();
        Bundle arguments = new Bundle();
        arguments.putString(FragmentState.KEY_ID, notebookId);
        arguments.putString(FragmentState.KEY_NAME, notebookName);
        arguments.putString(FragmentState.KEY_CONTENT_STATE, contentState.name());
        fragment.setArguments(arguments);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.state = new FragmentState(getArguments(), savedInstanceState);
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onListStateChanged(State state) {
        getPresenter().loadSectionGroups(state.getOffset(), state.getLimit());
    }

    @Override
    public void addSectionGroups(List<SectionGroupViewObject> items) {
        adapter.add(items);
    }

    @Override
    public void showError(@StringRes int error) {
        Snackbar errorSnack = Snackbar.make(getView(), getResources().getString(error), Snackbar.LENGTH_INDEFINITE);
        errorSnack.setAction(R.string.action_ok, (view) -> errorSnack.dismiss());
        errorSnack.setActionTextColor(getResources().getColor(R.color.colorAccent));
        errorSnack.show();
    }

    @Override
    public void onSectionGroupClicked(int position, SectionGroupViewObject item) {
        getActivity().getFragmentManager().beginTransaction()
                .replace(R.id.activity_drawer_fragment_container, SectionGroupsListFragment.newInstance(item.getId(), item.getName(), ContentState.NEST))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onSectionGroupAction(int position, SectionGroupViewObject item, MenuItem menuItem) {
        if(menuItem.getItemId() == R.id.action_edit_permissions) {
            getActivity().getFragmentManager().beginTransaction()
                    .replace(R.id.activity_drawer_fragment_container, SGPermissionsListFragment.newInstance(state.getId(), item.getId(), item.getName()))
                    .addToBackStack(null)
                    .commit();
        }
    }

    @Override
    public void showConnectionFailedIndicator() {
        showError(R.string.error_connection_lost);
    }

    @Override
    public void showOperationDelayedDialog() {
        showError(R.string.notification_operation_delayed);
    }

    @Override
    public void showNoCachedDataPlug() {
        showError(R.string.notification_no_cached_data);
    }

    @Override
    public void showNoDataPlug() {
        showError(R.string.notification_no_data);
    }

    @Override
    public void showNoPermissionsDialog() {
        showError(R.string.notification_no_permissions);
    }

    @Override
    public void showAuthorizationDialog() {
        ((IDrawerView) getActivity()).showAuthenticationActivity();
    }

    @Override
    protected void bindUserInterface(View view) {
        paginationListener = new PaginationListener(this);
        sectionGroupsList = (RecyclerView) view.findViewById(R.id.fragment_subgroups_list_recycler_view);
        appBarLayout = (AppBarLayout) view.findViewById(R.id.fragment_subgroups_list_appbar_layout);
        manageStudentsButton = (AppCompatButton) view.findViewById(R.id.fragment_subgroups_list_manage_subgroups_button);
        createSectionGroupButton = (FloatingActionButton) view.findViewById(R.id.fragment_subgroups_list_create_button);
        sectionGroupsList.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new SectionGroupsAdapter(SortingStrategyType.LAST_MODIFIED_DESCENDANCE);
        adapter.registerCallbacks(this);
        sectionGroupsList.setAdapter(adapter);
        manageStudentsButton.setOnClickListener(this::onManageStudentsButtonClicked);
        createSectionGroupButton.setOnClickListener(this::onCreateSectionGroupButtonClicked);

        if(state.getContentState() == ContentState.NEST) {
            manageStudentsButton.setVisibility(View.GONE);
            appBarLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        state.saveState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(state.getName());
        ((AppCompatActivity) getActivity()).getSupportActionBar().setSubtitle(R.string.title_manage_subgroups);
        sectionGroupsList.setOnScrollListener(paginationListener);
        if(adapter.getItemCount() == 0) {
            getPresenter().loadSectionGroups(0, paginationListener.getLimit());
        } else {
            getPresenter().fetch(adapter.lastModified());
        }
    }

    @Override
    public void onPause() {
        sectionGroupsList.setOnScrollListener(null);
        super.onPause();
    }

    @Override
    public ISectionGroupsListView getViewInterface() {
        return this;
    }

    @Override
    protected RetainContainer.Factory<SectionGroupsListPresenter> providePresenterFactory() {
        ISingletonProvider provider = (ISingletonProvider) getActivity().getApplication();
        SectionGroupsGetUseCase sectionGroupsGet = state.getContentState() == ContentState.ROOT ?
                new SectionGroupsGetUseCase(
                        provider.sectionGroupsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ) : state.getContentState() == ContentState.NEST ?
                new NestedSectionGroupsGetUseCase(
                        provider.sectionGroupsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ) : null;
        SectionGroupsFetchUseCase sectionGroupsFetch = state.getContentState() == ContentState.ROOT ?
                new SectionGroupsFetchUseCase(
                        provider.sectionGroupsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ) : state.getContentState() == ContentState.NEST ?
                new NestedSectionGroupsFetchUseCase(
                        provider.sectionGroupsRepository().provideRepository(),
                        provider.ioScheduler(),
                        provider.mainThreadScheduler()
                ) : null;
        SectionGroupGetUseCase sectionGroupGet = new SectionGroupGetUseCase(
                provider.sectionGroupsRepository().provideRepository(),
                provider.ioScheduler(),
                provider.mainThreadScheduler()
        );

        return new SectionGroupsListPresenter.Factory(
                sectionGroupsGet,
                sectionGroupGet,
                sectionGroupsFetch,
                provider.errorTranslation().provideOneNoteErrorCodeTranslator(),
                provider.analytics().provideScreenAnalytics(),
                provider.logging().provideLoggingManager(),
                state.getId()
        );
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_section_groups_list;
    }

    @Override
    public void handleActivityResult(int requestCode, int resultCode, Intent data) {
        super.handleActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE_CREATE_SECTION_GROUP && resultCode == Activity.RESULT_OK) {
            getPresenter().loadSectionGroup(data.getStringExtra(SectionGroupCreateActivity.KEY_RESULT_IDENTIFIER));
        }
    }

    private void onManageStudentsButtonClicked(View view) {
        getFragmentManager().beginTransaction()
                .replace(R.id.activity_drawer_fragment_container, StudentsListFragment.newInstance(state.getId(), state.getName()))
                .addToBackStack(null)
                .commit();
    }

    private void onCreateSectionGroupButtonClicked(View view) {
        Intent intent = new Intent(getActivity(), SectionGroupCreateActivity.class);

        //checking whether to create a SectionGroup inside a ClassNotebook or in parent SectionGroup
        if(state.getContentState() == ContentState.ROOT) {
            intent.putExtra(SectionGroupCreateActivity.KEY_NOTEBOOK_ID, state.getId());
            intent.putExtra(SectionGroupCreateActivity.KEY_NOTEBOOK_NAME, state.getName());
        } else if(state.getContentState() == ContentState.NEST) {
            intent.putExtra(SectionGroupCreateActivity.KEY_SECTION_GROUP_ID, state.getId());
            intent.putExtra(SectionGroupCreateActivity.KEY_SECTION_GROUP_NAME, state.getName());
        }

        if(intent.getExtras() != null) {
            startActivityForResult(
                    intent,
                    REQUEST_CODE_CREATE_SECTION_GROUP
            );
        }
    }

    public enum ContentState {
        ROOT,
        NEST
    }

    private static class FragmentState {

        private static final String PREFIX = FragmentState.class.getCanonicalName() + ".";

        private static final String KEY_ID = PREFIX + "NOTEBOOK_ID";
        private static final String KEY_NAME = PREFIX + "NOTEBOOK_NAME";
        private static final String KEY_CONTENT_STATE = PREFIX + "CONTENT_STATE";
        private static final String KEY_LIST_STATE = PREFIX + "LIST_STATE";
        private static final String KEY_LIST_ITEMS = PREFIX + "LIST_ITEMS";

        private String notebookId;
        private String notebookName;
        private ContentState contentState;

        public FragmentState(Bundle arguments, Bundle savedInstanceState) {
            if(arguments != null && arguments.containsKey(KEY_ID) && arguments.containsKey(KEY_NAME) && arguments.containsKey(KEY_CONTENT_STATE)) {
                this.notebookId = arguments.getString(KEY_ID);
                this.notebookName = arguments.getString(KEY_NAME);
                this.contentState = ContentState.valueOf(arguments.getString(KEY_CONTENT_STATE));
            } else if(savedInstanceState != null && savedInstanceState.containsKey(KEY_ID) && savedInstanceState.containsKey(KEY_NAME) && savedInstanceState.containsKey(KEY_CONTENT_STATE)) {
                this.notebookId = savedInstanceState.getString(KEY_ID);
                this.notebookName = savedInstanceState.getString(KEY_NAME);
                this.contentState = ContentState.valueOf(savedInstanceState.getString(KEY_CONTENT_STATE));
            }
        }

        public void saveState(Bundle outState) {
            outState.putString(KEY_ID, notebookId);
            outState.putString(KEY_NAME, notebookName);
            outState.putString(KEY_CONTENT_STATE, contentState.name());
        }

        public String getId() {
            return this.notebookId;
        }

        public String getName() {
            return this.notebookName;
        }

        public ContentState getContentState() {
            return contentState;
        }
    }
}
