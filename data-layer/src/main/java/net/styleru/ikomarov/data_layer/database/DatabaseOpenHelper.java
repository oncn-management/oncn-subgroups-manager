package net.styleru.ikomarov.data_layer.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import net.styleru.ikomarov.data_layer.mapping.local.notebooks.ClassNotebooksLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.PrincipalObjectsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.section_groups.SectionGroupsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.sections.SectionsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.notebooks.StudentSectionsLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.permissions.PermissionsLocalEntitiesMapping;
import net.styleru.ikomarov.data_layer.mapping.local.users.UsersBusinessPhonesLocalEntityMapping;
import net.styleru.ikomarov.data_layer.mapping.local.users.UsersLocalEntityMapping;

/**
 * Created by i_komarov on 25.02.17.
 */

public class DatabaseOpenHelper extends SQLiteOpenHelper {

    public DatabaseOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        for(String query : UsersBusinessPhonesLocalEntityMapping.getInitializationQueries()) {
            db.execSQL(query);
        }

        for(String query : UsersLocalEntityMapping.getInitializationQueries()) {
            db.execSQL(query);
        }

        for(String query : PermissionsLocalEntitiesMapping.getInitializationQueries()) {
            db.execSQL(query);
        }

        for(String query : ClassNotebooksLocalEntityMapping.getInitializationQueries()) {
            db.execSQL(query);
        }

        for(String query : SectionsLocalEntityMapping.getInitializationQueries()) {
            db.execSQL(query);
        }

        for(String query : SectionGroupsLocalEntityMapping.getInitializationQueries()) {
            db.execSQL(query);
        }

        for(String query : StudentSectionsLocalEntityMapping.getInitializationQueries()) {
            db.execSQL(query);
        }

        for(String query : PrincipalObjectsLocalEntityMapping.getInitializationQueries()) {
            db.execSQL(query);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
