package net.styleru.ikomarov.domain_layer.facades.repository;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.date_time.DateTimeContract;
import net.styleru.ikomarov.domain_layer.dto.PermissionReportSubgroupDTO;
import net.styleru.ikomarov.domain_layer.dto.base.ReportDTO;
import net.styleru.ikomarov.domain_layer.dto.base.ReportGroupDTO;
import net.styleru.ikomarov.domain_layer.mapping.reports.PermissionsGroupReportMapper;
import net.styleru.ikomarov.domain_layer.repository.notebooks.INotebooksRepository;
import net.styleru.ikomarov.domain_layer.repository.permissions.ISGPermissionsRepository;
import net.styleru.ikomarov.domain_layer.repository.reports.INotebookReportsRepository;
import net.styleru.ikomarov.domain_layer.repository.section_groups.ISectionGroupsRepository;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 18.04.17.
 */

public class ReportRepositoryFacade implements IReportRepositoryFacade {

    @NonNull
    private final INotebooksRepository notebooks;

    @NonNull
    private final ISectionGroupsRepository sectionGroups;

    @NonNull
    private final ISGPermissionsRepository permissions;

    @NonNull
    private final INotebookReportsRepository reports;

    public ReportRepositoryFacade(@NonNull INotebooksRepository notebooks,
                                  @NonNull ISectionGroupsRepository sectionGroups,
                                  @NonNull ISGPermissionsRepository permissions,
                                  @NonNull INotebookReportsRepository reports) {

        this.notebooks = notebooks;
        this.sectionGroups = sectionGroups;
        this.permissions = permissions;
        this.reports = reports;
    }

    @Override
    public Observable<String> createReport(@NonNull String notebookId) {
        return notebooks.get(notebookId)
                //get the notebook needed and retrieve groups from it
                .flatMap(notebook -> sectionGroups.getList(notebook.getId())
                        //iterate through groups and request permissions for each
                        .flatMap(Observable::fromIterable)
                        .onExceptionResumeNext(Observable.empty())
                        .flatMap(group -> permissions.read(group.getId(), false).onExceptionResumeNext(Observable.empty()).flatMap(permissions -> Observable.just(permissions).map(new PermissionsGroupReportMapper(group.getId(), group.getName()))))
                        //accumulate groups into a report and add a postfix with current date
                        .reduce(new ReportDTO.Builder<PermissionReportSubgroupDTO, ReportGroupDTO<PermissionReportSubgroupDTO>>(notebook.getName()), ReportDTO.Builder::addGroup)
                        .map(builder -> builder.addPostfix(new SimpleDateFormat(DateTimeContract.VIEW_DATE_FORMAT, Locale.getDefault()).format(new Date(System.currentTimeMillis()))).build())
                        .toObservable())
                //create the report finally
                .flatMap(report -> reports.createReportPDF(UUID.randomUUID(), report));
    }
}
