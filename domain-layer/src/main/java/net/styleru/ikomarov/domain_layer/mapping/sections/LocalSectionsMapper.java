package net.styleru.ikomarov.domain_layer.mapping.sections;

import net.styleru.ikomarov.data_layer.entities.local.sections.SectionLocalEntity;
import net.styleru.ikomarov.domain_layer.dto.SectionDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 05.03.17.
 */

public class LocalSectionsMapper implements Function<List<SectionLocalEntity>, List<SectionDTO>> {

    @Override
    public List<SectionDTO> apply(List<SectionLocalEntity> sectionLocalEntities) throws Exception {
        return Observable.fromIterable(sectionLocalEntities)
                .map(new LocalSectionMapper())
                .toList()
                .blockingGet();
    }
}
