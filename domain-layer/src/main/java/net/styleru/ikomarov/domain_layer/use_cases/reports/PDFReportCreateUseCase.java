package net.styleru.ikomarov.domain_layer.use_cases.reports;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.facades.repository.IReportRepositoryFacade;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.UUID;

import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 18.04.17.
 */

public class PDFReportCreateUseCase extends UseCase {

    @NonNull
    private final IReportRepositoryFacade repository;

    @NonNull
    private final Scheduler executedScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public PDFReportCreateUseCase(@NonNull IReportRepositoryFacade repository, @NonNull Scheduler executedScheduler, @NonNull Scheduler postScheduler) {
        this.repository = repository;
        this.executedScheduler = executedScheduler;
        this.postScheduler = postScheduler;
    }

    public void execute(UUID operationId, Callbacks callbacks, String notebookId) {
        repository.createReport(notebookId)
                .subscribeOn(executedScheduler)
                .observeOn(postScheduler)
                .subscribe(createReportCreationEventObserver(operationId, callbacks));
    }

    private DisposableObserver<String> createReportCreationEventObserver(UUID operationId, Callbacks callbacks) {
        DisposableObserver<String> reportCreationEventObserver = new DisposableObserver<String>() {
            @Override
            public void onNext(String value) {
                if(!isDisposed()) {
                    callbacks.onReportCreated(operationId, value);
                }
            }

            @Override
            public void onError(Throwable e) {
                if(!isDisposed()) {
                    callbacks.onReportCreationFailed(operationId, e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(reportCreationEventObserver);

        return reportCreationEventObserver;
    }

    public interface Callbacks {

        void onReportCreated(UUID operationId, String path);

        void onReportCreationFailed(UUID operationId, ExceptionBundle error);
    }
}
