package net.styleru.ikomarov.data_layer.entities.remote.section_groups;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.remote.section_groups.SectionGroupContainersRemoteEntityMapping.*;

public class ContainerRemoteEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_NAME)
    private String name;
    @SerializedName(FIELD_SELF)
    private String self;

    private ContainerRemoteEntity() {

    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSelf() {
        return self;
    }
}
