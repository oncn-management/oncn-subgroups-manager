package net.styleru.ikomarov.domain_layer.cache;

import net.styleru.ikomarov.data_layer.contracts.network.NetworkStatus;
import net.styleru.ikomarov.data_layer.manager.network.INetworkManager;
import net.styleru.ikomarov.domain_layer.contracts.policies.CachePolicy;

/**
 * Created by i_komarov on 09.03.17.
 */

public class CacheStrategy implements ICacheStrategy {

    private INetworkManager networkManager;

    public CacheStrategy(INetworkManager networkManager) {
        this.networkManager = networkManager;
    }

    @Override
    public CachePolicy currentPolicy() {
        NetworkStatus status = networkManager.getCurrentNetworkStatus();
        if(status == NetworkStatus.DISCONNECTED) {
            return CachePolicy.CACHE_ONLY;
        } else if(status == NetworkStatus.CONNECTED_MOBILE) {
            //TODO: add battery save mode without making requests to network
            return CachePolicy.CHECK_UPDATES;
        } else {
            return CachePolicy.FORCE_NETWORK;
        }
    }
}
