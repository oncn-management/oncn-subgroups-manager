package net.styleru.ikomarov.presentation_layer.presenter.base;

import net.styleru.ikomarov.presentation_layer.view.base.IView;

/**
 * Created by i_komarov on 04.03.17.
 */

public interface IPresenter<V extends IView> {

    void attachView(V view);

    void onResume();

    void onPause();

    void detachView();
}
