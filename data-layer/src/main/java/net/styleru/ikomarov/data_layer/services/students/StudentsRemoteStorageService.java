package net.styleru.ikomarov.data_layer.services.students;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.contracts.response.DataResponse;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;
import net.styleru.ikomarov.data_layer.services.base.NetworkService;
import net.styleru.ikomarov.data_layer.services.section_groups.SectionGroupsRemoteStorageService;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentsRemoteStorageService extends NetworkService<IStudentsRemoteStorageService> implements IStudentsRemoteStorageService {

    private StudentsRemoteStorageService(OkHttpClient client) {
        super(Endpoints.ONE_NOTE_BASE_URL, client);
    }

    @Override
    public Observable<Response<DataResponse<PrincipalObjectRemoteEntity>>> create(String userId, String notebookId, JsonObject studentJson) {
        return getService().create(userId, notebookId, studentJson)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<PrincipalObjectRemoteEntity>>> read(String userId, String notebookId, int offset, int limit) {
        return getService().read(userId, notebookId, offset, limit)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<PrincipalObjectRemoteEntity>>> read(String userId, String notebookId, int offset, int limit, String orderBy) {
        return getService().read(userId, notebookId, offset, limit, orderBy)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<PrincipalObjectRemoteEntity>>> read(String userId, String notebookId, int offset, int limit, String orderBy, String filter) {
        return getService().read(userId, notebookId, offset, limit, orderBy, filter)
                .compose(handleError());
    }

    @Override
    public Observable<Response<DataResponse<Object>>> delete(String userId, String notebookId, String studentId) {
        return getService().delete(userId, notebookId, studentId)
                .compose(handleError());
    }

    @Override
    protected IStudentsRemoteStorageService createService(Retrofit retrofit) {
        return retrofit.create(IStudentsRemoteStorageService.class);
    }

    @Override
    protected Gson getGson() {
        return new GsonBuilder()
                .registerTypeAdapter(DataResponse.class, new DataResponse.Deserializer())
                .registerTypeAdapter(PrincipalObjectRemoteEntity.class, new PrincipalObjectRemoteEntity.Deserializer())
                .registerTypeAdapter(PrincipalObjectRemoteEntity.class, new PrincipalObjectRemoteEntity.Serializer())
                .create();
    }

    public static final class Factory {

        private Factory() {

        }

        public static IStudentsRemoteStorageService create(OkHttpClient client) {
            return new StudentsRemoteStorageService(client);
        }
    }
}
