package net.styleru.ikomarov.domain_layer.repository.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.contracts.operations.OperationStatus;

import java.util.UUID;

/**
 * Created by i_komarov on 21.03.17.
 */

public class CreateOperationResult {

    @NonNull
    private UUID operationId;

    @NonNull
    private OperationStatus status;

    @Nullable
    private String resourceId;

    public static CreateOperationResult successfulResult(UUID operationId, String resourceId) {
        CreateOperationResult result = new CreateOperationResult(operationId, OperationStatus.SUCCESS);
        result.resourceId = resourceId;
        return result;
    }

    public static CreateOperationResult delayedResult(UUID operationId) {
        return new CreateOperationResult(operationId, OperationStatus.DELAYED);
    }

    public static CreateOperationResult failedResult(UUID operationId) {
        return new CreateOperationResult(operationId, OperationStatus.FAILURE);
    }

    private CreateOperationResult(@NonNull UUID operationId, @NonNull OperationStatus status) {
        this.operationId = operationId;
        this.status = status;
    }

    @NonNull
    public UUID getOperationId() {
        return operationId;
    }

    @NonNull
    public OperationStatus getStatus() {
        return status;
    }

    @Nullable
    public String getResourceId() {
        return resourceId;
    }

    @Override
    public String toString() {
        return "CreateOperationResult{" +
                "operationId=" + operationId +
                ", status=" + status +
                ", resourceId='" + resourceId + '\'' +
                '}';
    }
}
