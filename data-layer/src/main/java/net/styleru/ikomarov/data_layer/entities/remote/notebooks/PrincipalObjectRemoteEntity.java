package net.styleru.ikomarov.data_layer.entities.remote.notebooks;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;

import net.styleru.ikomarov.data_layer.contracts.principal_object.PrincipalObjectEntityType;

import java.lang.reflect.Type;

import static net.styleru.ikomarov.data_layer.mapping.remote.notebooks.PrincipalObjectsRemoteEntityMapping.*;

/**
 * Created by i_komarov on 14.02.17.
 */

public class PrincipalObjectRemoteEntity {

    @SerializedName(FIELD_ID)
    private String id;
    @SerializedName(FIELD_NAME)
    private String name;
    @SerializedName(FIELD_EMAIL)
    private String email;
    @SerializedName(FIELD_DEPARTMENT)
    private String department;
    @SerializedName(FIELD_PRINCIPAL_TYPE)
    private PrincipalObjectEntityType principalType;

    private PrincipalObjectRemoteEntity() {

    }

    public static PrincipalObjectRemoteEntity newInstance(String id, PrincipalObjectEntityType principalType) {
        PrincipalObjectRemoteEntity entity = new PrincipalObjectRemoteEntity();

        entity.id = id;
        entity.principalType = principalType;

        return entity;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getDepartment() {
        return department;
    }

    public PrincipalObjectEntityType getPrincipalType() {
        return principalType;
    }

    @Override
    public String toString() {
        return "PrincipalObjectRemoteEntity{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", department='" + department + '\'' +
                ", principalType=" + principalType +
                '}';
    }

    public static class Serializer implements JsonSerializer<PrincipalObjectRemoteEntity> {

        @Override
        public JsonElement serialize(PrincipalObjectRemoteEntity src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject json = new JsonObject();

            json.addProperty(FIELD_ID, src.id);
            json.addProperty(FIELD_PRINCIPAL_TYPE, src.principalType.value());

            return json;
        }
    }

    public static class Deserializer implements JsonDeserializer<PrincipalObjectRemoteEntity> {


        @Override
        public PrincipalObjectRemoteEntity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            JsonObject root = json.getAsJsonObject();

            PrincipalObjectRemoteEntity entity = new PrincipalObjectRemoteEntity();

            entity.id = root.get(FIELD_ID).getAsString();
            JsonElement name = root.get(FIELD_NAME);
            if(!name.isJsonNull()) {
                entity.name = name.getAsString();
            }
            JsonElement email = root.get(FIELD_EMAIL);
            if(!email.isJsonNull()) {
                entity.email = email.getAsString();
            }
            JsonElement department = root.get(FIELD_DEPARTMENT);
            if(!department.isJsonNull()) {
                entity.department = department.getAsString();
            }
            entity.principalType = PrincipalObjectEntityType.forValue(root.get(FIELD_PRINCIPAL_TYPE).getAsString());

            return entity;
        }
    }
}
