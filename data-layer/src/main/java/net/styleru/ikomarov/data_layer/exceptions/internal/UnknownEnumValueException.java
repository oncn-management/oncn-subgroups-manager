package net.styleru.ikomarov.data_layer.exceptions.internal;

/**
 * Created by i_komarov on 24.02.17.
 */

public class UnknownEnumValueException extends IllegalStateException {

    private static final String MESSAGE_PATTERN = "Unknown enum value %1$s was passed to %2$s#%3$s";

    public UnknownEnumValueException(String value, String className, String methodName) {
        super(String.format(MESSAGE_PATTERN, value, className, methodName));
    }
}
