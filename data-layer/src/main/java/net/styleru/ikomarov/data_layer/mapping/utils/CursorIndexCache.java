package net.styleru.ikomarov.data_layer.mapping.utils;

import android.database.Cursor;
import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by i_komarov on 06.03.17.
 */

public class CursorIndexCache {

    private Map<String, Integer> columnMap;

    public CursorIndexCache() {
        this.columnMap = new HashMap<>();
    }

    public void add(@NonNull String columnName, @NonNull Integer columnIndex) {
        this.columnMap.put(columnName, columnIndex);
    }

    public Integer indexFor(@NonNull Cursor cursor, String columnName) {
        Integer columnIndex;
        if(null == (columnIndex = columnMap.get(columnName))) {
            columnMap.put(columnName, columnIndex = cursor.getColumnIndex(columnName));
        }

        return columnIndex;
    }
}
