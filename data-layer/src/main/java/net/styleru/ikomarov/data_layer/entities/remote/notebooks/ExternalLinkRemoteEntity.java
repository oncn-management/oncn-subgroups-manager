package net.styleru.ikomarov.data_layer.entities.remote.notebooks;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.remote.notebooks.ExternalLinkRemoteEntityMapping.*;

/**
 * Created by i_komarov on 14.02.17.
 */

public class ExternalLinkRemoteEntity {

    @SerializedName(FIELD_HREF)
    private String href;

    private ExternalLinkRemoteEntity(String href) {
        this.href = href;
    }

    public String getHref() {
        return href;
    }

    @Override
    public String toString() {
        return "ExternalLinkRemoteEntity{" +
                "href='" + href + '\'' +
                '}';
    }
}
