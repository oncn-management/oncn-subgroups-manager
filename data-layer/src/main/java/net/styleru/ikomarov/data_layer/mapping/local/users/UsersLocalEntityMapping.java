package net.styleru.ikomarov.data_layer.mapping.local.users;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.queries.Query;

import net.styleru.ikomarov.data_layer.entities.local.users.UserLocalEntity;
import net.styleru.ikomarov.data_layer.exceptions.internal.EmptyCursorException;
import net.styleru.ikomarov.data_layer.mapping.utils.CursorIndexCache;
import net.styleru.ikomarov.data_layer.mapping.base.ILocalMapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by i_komarov on 25.02.17.
 */

public class UsersLocalEntityMapping implements ILocalMapping<UserLocalEntity> {

    @NonNull
    public static final String TABLE = "users";

    @NonNull
    public static final String COLUMN_ID = "_id";

    @NonNull
    public static final String COLUMN_DISPLAY_NAME = "DISPLAY_NAME";

    @NonNull
    public static final String COLUMN_GIVEN_NAME = "GIVEN_NAME";

    @NonNull
    public static final String COLUMN_JOB_TITLE = "JOB_TITLE";

    @NonNull
    public static final String COLUMN_MAIL = "MAIL";

    @NonNull
    public static final String COLUMN_MOBILE_PHONE = "MOBILE_PHONE";

    @NonNull
    public static final String COLUMN_OFFICE_LOCATION = "OFFICE_LOCATION";

    @NonNull
    public static final String COLUMN_PREFERRED_LANGUAGE = "PREFERRED_LANGUAGE";

    @NonNull
    public static final String COLUMN_LAST_NAME = "LAST_NAME";

    @NonNull
    public static final String COLUMN_PRINCIPAL_NAME = "PRINCIPAL_NAME";


    public static final String COLUMN_ID_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_ID;
    public static final String COLUMN_DISPLAY_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_DISPLAY_NAME;
    public static final String COLUMN_GIVEN_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_GIVEN_NAME;
    public static final String COLUMN_JOB_TITLE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_JOB_TITLE;
    public static final String COLUMN_MAIL_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_MAIL;
    public static final String COLUMN_MOBILE_PHONE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_MOBILE_PHONE;
    public static final String COLUMN_OFFICE_LOCATION_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_OFFICE_LOCATION;
    public static final String COLUMN_PREFERRED_LANGUAGE_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_PREFERRED_LANGUAGE;
    public static final String COLUMN_LAST_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_LAST_NAME;
    public static final String COLUMN_PRINCIPAL_NAME_WITH_TABLE_PREFIX = TABLE + "." + COLUMN_PRINCIPAL_NAME;


    @NonNull
    public static Query QUERY_ALL = Query.builder()
            .table(TABLE)
            .build();


    public UsersLocalEntityMapping() {

    }

    public static List<String> getInitializationQueries() {
        return Collections.singletonList(getCreateTableQuery());
    }

    private static String getCreateTableQuery() {
        return "CREATE TABLE IF NOT EXISTS " + TABLE + "(" +
                    COLUMN_ID + " TEXT PRIMARY KEY NOT NULL," +
                    COLUMN_DISPLAY_NAME + " TEXT," +
                    COLUMN_GIVEN_NAME + " TEXT," +
                    COLUMN_JOB_TITLE + " TEXT," +
                    COLUMN_MAIL + " TEXT NOT NULL," +
                    COLUMN_MOBILE_PHONE + " TEXT," +
                    COLUMN_OFFICE_LOCATION + " TEXT," +
                    COLUMN_PREFERRED_LANGUAGE + " TEXT," +
                    COLUMN_LAST_NAME + " TEXT," +
                    COLUMN_PRINCIPAL_NAME + " TEXT NOT NULL" +
                ");";
    }

    @Override
    public UserLocalEntity fromCursor(Cursor cursor) throws EmptyCursorException {
        if(cursor.moveToFirst()) {
            return UserLocalEntity.newInstance(
                    cursor.getString(cursor.getColumnIndex(COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_DISPLAY_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_GIVEN_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_JOB_TITLE)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_MAIL)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_MOBILE_PHONE)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_OFFICE_LOCATION)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PREFERRED_LANGUAGE)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_LAST_NAME)),
                    cursor.getString(cursor.getColumnIndex(COLUMN_PRINCIPAL_NAME))
            );
        } else {
            throw new EmptyCursorException();
        }
    }

    @Override
    public List<UserLocalEntity> listFromCursor(Cursor cursor) {
        List<UserLocalEntity> entities = new ArrayList<>(cursor.getCount());

        if(cursor.moveToFirst()) {
            CursorIndexCache indexCache = new CursorIndexCache();

            do {
                entities.add(
                        UserLocalEntity.newInstance(
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_ID)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_DISPLAY_NAME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_GIVEN_NAME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_JOB_TITLE)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_MAIL)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_MOBILE_PHONE)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_OFFICE_LOCATION)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_PREFERRED_LANGUAGE)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_LAST_NAME)),
                                cursor.getString(indexCache.indexFor(cursor, COLUMN_PRINCIPAL_NAME))
                        )
                );
            } while(cursor.moveToNext());
        }

        return entities;
    }
}
