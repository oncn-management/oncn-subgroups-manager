package net.styleru.ikomarov.data_layer.services.students;

import com.pushtorefresh.storio.sqlite.operations.delete.DeleteResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.operations.put.PutResults;

import net.styleru.ikomarov.data_layer.entities.local.notebooks.PrincipalObjectLocalEntity;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 19.03.17.
 */

public interface IStudentsLocalStorageService {

    Observable<PutResult> create(final String notebookId, final PrincipalObjectLocalEntity entity);

    Observable<PutResults<PrincipalObjectLocalEntity>> create(final String notebookId, final List<PrincipalObjectLocalEntity> entities);

    Observable<List<PrincipalObjectLocalEntity>> read(final String notebookId, int offset, int limit);

    Observable<List<PrincipalObjectLocalEntity>> read(final String notebookId, int offset, int limit, String nameSubstring);

    Observable<DeleteResult> delete(final String notebookId, final String studentId);
}
