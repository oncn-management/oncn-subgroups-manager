package net.styleru.ikomarov.domain_layer.di.level_1;

/**
 * Created by i_komarov on 09.03.17.
 */

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;

/**
 * does not depend on any other component
 * */
public interface IAppComponent {

    @NonNull
    AppModule app();

    @NonNull
    IEnvironmentComponent plusEnvironmentComponent();
}
