package net.styleru.ikomarov.domain_layer.repository.section_groups;

import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.repository.base.IRepository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 15.03.17.
 */

public interface ISectionGroupsRepository extends IRepository {

    Observable<SectionGroupDTO> get(String id);

    Observable<CreateOperationResult> create(UUID operationId, String notebookId, SectionGroupDTO.Builder builder);

    Observable<List<SectionGroupDTO>> fetch(String notebookId, Date ifModifiedSince);

    Observable<List<SectionGroupDTO>> getList(String notebookId);

    Observable<List<SectionGroupDTO>> getList(String notebookId, int offset, int limit);

    Observable<List<SectionGroupDTO>> getList(String notebookId, int offset, int limit, String nameSubstring);

    Observable<CreateOperationResult> createNested(UUID operationId, String sectionGroupId, SectionGroupDTO.Builder builder);

    Observable<List<SectionGroupDTO>> fetchNested(String sectionGroupId, Date ifModifiedSince);

    Observable<List<SectionGroupDTO>> getListNested(String sectionGroupId, int offset, int limit);

    Observable<List<SectionGroupDTO>> getListNested(String sectionGroupId, int offset, int limit, String nameSubstring);
}
