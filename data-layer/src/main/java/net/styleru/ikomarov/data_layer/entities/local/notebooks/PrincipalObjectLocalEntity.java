package net.styleru.ikomarov.data_layer.entities.local.notebooks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;

import static net.styleru.ikomarov.data_layer.mapping.local.notebooks.PrincipalObjectsLocalEntityMapping.*;

/**
 * Created by i_komarov on 28.02.17.
 */

@StorIOSQLiteType(table = TABLE)
public class PrincipalObjectLocalEntity {

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_ID, key = true)
    String id;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_NAME)
    String name;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_EMAIL)
    String email;

    @Nullable
    @StorIOSQLiteColumn(name = COLUMN_DEPARTMENT)
    String department;

    @NonNull
    @StorIOSQLiteColumn(name = COLUMN_TYPE)
    String type;

    public static PrincipalObjectLocalEntity newInstance(String id, String type) {
        PrincipalObjectLocalEntity entity = new PrincipalObjectLocalEntity();

        entity.id = id;
        entity.type = type;

        return entity;
    }

    public static PrincipalObjectLocalEntity newInstance(String id, String name, String email, String department, String type) {
        PrincipalObjectLocalEntity entity = new PrincipalObjectLocalEntity();

        entity.id = id;
        entity.name = name;
        entity.email = email;
        entity.department = department;
        entity.type = type;

        return entity;
    }

    PrincipalObjectLocalEntity() {

    }

    @NonNull
    public String getId() {
        return id;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public String getEmail() {
        return email;
    }

    @Nullable
    public String getDepartment() {
        return department;
    }

    @NonNull
    public String getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PrincipalObjectLocalEntity that = (PrincipalObjectLocalEntity) o;

        if (!id.equals(that.id)) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (department != null ? !department.equals(that.department) : that.department != null)
            return false;
        return type.equals(that.type);

    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (department != null ? department.hashCode() : 0);
        result = 31 * result + type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "PrincipalObjectLocalEntity{" +
                "id='" + id + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
