package net.styleru.ikomarov.data_layer.entities.remote.extras;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.remote.extras.OneNoteObjectRemoteEntityMapping.*;

/**
 * Created by i_komarov on 14.02.17.
 */

public class OneNoteObjectRemoteEntity {

    @SerializedName(FIELD_ODATA_CONTEXT)
    protected String oDataContext;

    protected OneNoteObjectRemoteEntity() {

    }

    public String getODataContext() {
        return oDataContext;
    }

    @Override
    public String toString() {
        return "OneNoteObjectRemoteEntity{" +
                "oDataContext='" + oDataContext + '\'' +
                '}';
    }
}
