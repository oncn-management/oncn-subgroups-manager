package net.styleru.ikomarov.presentation_layer.adapter.notebooks_list;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.util.SortedList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.util.SortedListAdapterCallback;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.styleru.ikomarov.presentation_layer.R;
import net.styleru.ikomarov.presentation_layer.adapter.utils.SortingStrategyType;
import net.styleru.ikomarov.presentation_layer.view.notebooks_list.NotebookViewObject;
import net.styleru.ikomarov.presentation_layer.view_holder.notebooks_list.NotebookViewHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by i_komarov on 14.03.17.
 */

public class NotebooksAdapter extends RecyclerView.Adapter<NotebookViewHolder> {

    private NotebookViewHolder.InteractionCallbacks callbacks;

    private SortedList<NotebookViewObject> items;

    private Map<String, NotebookViewObject> itemMap;

    private SortingStrategyType sortingStrategyType;

    public NotebooksAdapter(Bundle savedInstanceState) {
        AdapterState state = new AdapterState(savedInstanceState);
        this.itemMap = state.restoreItemMap();
        this.sortingStrategyType = state.getSortingStrategyType();
        initialize(sortingStrategyType, itemMap);
    }

    public NotebooksAdapter(SortingStrategyType type) {
        this.itemMap = new HashMap<>();
        initialize(type, itemMap);
    }

    public NotebooksAdapter(SortingStrategyType type, NotebooksAdapter previousInstance) {
        this.itemMap = previousInstance.itemMap;
        initialize(type, itemMap);
    }

    private void initialize(SortingStrategyType type, Map<String, NotebookViewObject> itemMap) {
        this.sortingStrategyType = type;
        this.items = new SortedList<>(NotebookViewObject.class, new SortedListAdapterCallback<NotebookViewObject>(this) {
            @Override
            public int compare(NotebookViewObject o1, NotebookViewObject o2) {
                switch (type) {
                    case NAME_ASCENDANCE: {
                        return o1.getName().compareTo(o2.getName());
                    }
                    case LAST_MODIFIED_DESCENDANCE: {
                        return o2.getLastModified().compareTo(o1.getLastModified());
                    }
                    default: {
                        return 0;
                    }
                }
            }

            @Override
            public boolean areContentsTheSame(NotebookViewObject o1, NotebookViewObject o2) {
                return  o1.getName().equals(o2.getName()) &&
                        o1.getLastModified().equals(o2.getLastModified()) &&
                        o1.isSynchronized() == o2.isSynchronized();
            }

            @Override
            public boolean areItemsTheSame(NotebookViewObject o1, NotebookViewObject o2) {
                return o1.getId() != null && o2.getId() != null && o1.getId().equals(o2.getId());

            }
        });

        this.items.addAll(itemMap.values());
    }

    @Override
    public NotebookViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NotebookViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notebook, parent, false));
    }

    @Override
    public void onBindViewHolder(NotebookViewHolder holder, int position) {
        holder.bind(position, items.get(position), callbacks);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void onSaveInstanceState(Bundle outState) {
        AdapterState state = new AdapterState(itemMap, sortingStrategyType);
        state.onSaveInstanceState(outState);
    }

    public void registerCallbacks(NotebookViewHolder.InteractionCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    public void unregisterCallbacks() {
        this.callbacks = null;
    }

    public void add(NotebookViewObject newItem) {
        NotebookViewObject oldItem = itemMap.get(newItem.getId());
        if(oldItem == null) {
            this.items.add(newItem);
        } else {
            this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
        }

        itemMap.put(newItem.getId(), newItem);
    }

    public void add(List<NotebookViewObject> items) {
        boolean allExists = true;

        for(int i = 0; i < items.size(); i++) {
            NotebookViewObject newItem = items.get(i);
            NotebookViewObject oldItem = itemMap.get(newItem.getId());

            if(oldItem == null) {
                allExists = false;
                break;
            }
        }

        if(allExists) {
            performBatchUpdateWithoutNewItems(items);
        } else {
            performBatchUpdateWithNewItems(items);
        }
    }

    public void remove(String id) {
        if(this.itemMap.containsKey(id)) {
            this.items.remove(this.itemMap.get(id));
        }
    }

    public void remove(final int position) {
        String uri = this.items.get(position).getId();
        this.items.removeItemAt(position);
        this.itemMap.remove(uri);
    }

    private void performBatchUpdateWithoutNewItems(List<NotebookViewObject> items) {
        for(int i = 0; i < items.size(); i++) {
            NotebookViewObject newItem = items.get(i);
            NotebookViewObject oldItem = itemMap.get(newItem.getId());

            this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
            this.itemMap.put(newItem.getId(), newItem);
        }
    }

    private void performBatchUpdateWithNewItems(List<NotebookViewObject> items) {
        try {
            this.items.beginBatchedUpdates();
            for (int i = 0; i < items.size(); i++) {
                NotebookViewObject newItem = items.get(i);
                NotebookViewObject oldItem = itemMap.get(newItem.getId());

                if (oldItem != null) {
                    this.items.updateItemAt(this.items.indexOf(oldItem), newItem);
                } else {
                    this.items.add(newItem);
                }

                this.itemMap.put(newItem.getId(), newItem);
            }

        } finally {
            this.items.endBatchedUpdates();
        }
    }

    private static final class AdapterState {

        private static final String PREFIX = AdapterState.class.getCanonicalName() + ".";

        private static final String KEY_CONTAINER = PREFIX + "CONTAINER";

        private static final String KEY_ITEMS = PREFIX + "ITEMS";

        private static final String KEY_SORTING_STRATEGY_TYPE = "SORTING_STRATEGY_TYPE";

        private Bundle state;

        public AdapterState(Bundle savedInstanceState) {
            this.state = savedInstanceState.containsKey(KEY_CONTAINER) ? savedInstanceState.getBundle(KEY_CONTAINER) : null;
        }

        public AdapterState(Map<String, NotebookViewObject> itemMap, SortingStrategyType sortingStrategyType) {
            this.state = new Bundle();
            this.state.putParcelableArrayList(KEY_ITEMS, new ArrayList<>(itemMap.values()));
            this.state.putInt(KEY_SORTING_STRATEGY_TYPE, sortingStrategyType.getCode());
        }

        public void onSaveInstanceState(Bundle outState) {
            outState.putBundle(KEY_CONTAINER, state);
        }

        public Map<String, NotebookViewObject> restoreItemMap() {
            ArrayList<Parcelable> itemList = state != null && state.containsKey(KEY_ITEMS) ? state.getParcelableArrayList(KEY_ITEMS) : new ArrayList<>();
            Map<String, NotebookViewObject> itemMap = new HashMap<>(itemList.size());
            for(Parcelable item : itemList) {
                NotebookViewObject itemActual = (NotebookViewObject) item;
                itemMap.put(itemActual.getId(), itemActual);
            }

            return itemMap;
        }

        public SortingStrategyType getSortingStrategyType() {
            return state != null && state.containsKey(KEY_SORTING_STRATEGY_TYPE) ? SortingStrategyType.forValue(state.getInt(KEY_SORTING_STRATEGY_TYPE)) : SortingStrategyType.LAST_MODIFIED_DESCENDANCE;
        }
    }
}
