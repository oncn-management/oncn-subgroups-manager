package net.styleru.ikomarov.authentication.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by i_komarov on 19.02.17.
 */

public class OncnAuthenticationService extends Service {

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        OncnAccountAuthenticator authenticator = new OncnAccountAuthenticator(this);
        return authenticator.getIBinder();
    }
}
