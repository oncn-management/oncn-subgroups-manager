package net.styleru.ikomarov.presentation_layer.mapping.students;

import net.styleru.ikomarov.domain_layer.dto.PrincipalObjectDTO;
import net.styleru.ikomarov.presentation_layer.view.students_list.StudentViewObject;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 19.03.17.
 */

public class StudentMapper implements Function<PrincipalObjectDTO, StudentViewObject> {

    @Override
    public StudentViewObject apply(PrincipalObjectDTO dto) throws Exception {
        return new StudentViewObject(
                dto.getId(),
                dto.getName(),
                dto.getEmail()
        );
    }
}
