package net.styleru.ikomarov.data_layer.entities.report.base;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 17.04.17.
 */

public class ReportSubgroupEntity {

    @NonNull
    private final String name;

    public ReportSubgroupEntity(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "ReportSubgroupEntity{" +
                "name='" + name + '\'' +
                '}';
    }
}
