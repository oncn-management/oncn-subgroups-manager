package net.styleru.ikomarov.presentation_layer.view.notebooks_list;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;

import net.styleru.ikomarov.presentation_layer.view.base.IView;

import java.util.List;

/**
 * Created by i_komarov on 08.03.17.
 */

public interface INotebooksListView extends IView {

    void onReportCreated(String path);

    void addNotebooks(List<NotebookViewObject> items);

    void showError(@StringRes int error);

    void showConnectionFailedIndicator();

    void showOperationDelayedDialog();

    void showNoCachedDataPlug();

    void showNoDataPlug();

    void showNoPermissionsDialog();

    void showAuthorizationDialog();
}
