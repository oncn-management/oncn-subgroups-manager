package net.styleru.ikomarov.authentication.retaincontainer;

import android.app.Fragment;
import android.os.Bundle;

/**
 * Created by i_komarov on 18.02.17.
 */

public class RetainFragmentContainer<T> extends Fragment {

    private Factory<T> factory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public void setContentProducer(Factory<T> factory) {
        this.factory = factory;
    }

    public T getContent() {
        return factory.create();
    }
}
