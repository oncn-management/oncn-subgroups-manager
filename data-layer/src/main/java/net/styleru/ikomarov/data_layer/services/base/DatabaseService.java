package net.styleru.ikomarov.data_layer.services.base;

import android.util.Log;

import com.pushtorefresh.storio.StorIOException;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.RawQuery;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.exceptions.ExternalException;
import net.styleru.ikomarov.data_layer.exceptions.external.DatabaseException;
import net.styleru.ikomarov.data_layer.exceptions.external.InternalException;
import net.styleru.ikomarov.data_layer.services.class_notebooks.ClassNotebooksLocalStorageService;

import java.sql.SQLException;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 25.02.17.
 */

public abstract class DatabaseService {

    private StorIOSQLite sqlite;

    public DatabaseService(StorIOSQLite sqlite) {
        this.sqlite = sqlite;
    }

    protected StorIOSQLite getSQLite() {
        return sqlite;
    }

    protected void releaseSQLiteReference() {
        this.sqlite = null;
    }

    protected final ExceptionBundle parseException(Throwable thr) {
        Log.e("DatabaseService", "parseException was invoked", thr);

        ExceptionBundle error;

        if(thr instanceof StorIOException) {
            Throwable cause = thr.getCause();
            if(cause instanceof SQLException) {
                error = new ExceptionBundle(ExceptionBundle.Reason.DATABASE_CONSTRAINT);

                error.addIntExtra(ExceptionBundle.DatabaseErrorContract.KEY_CODE, ((SQLException) cause).getErrorCode());
                error.addStringExtra(ExceptionBundle.DatabaseErrorContract.KEY_MESSAGE, ((SQLException) cause).getMessage());
                error.addStringExtra(ExceptionBundle.DatabaseErrorContract.KEY_SQL_STATE, ((SQLException) cause).getSQLState());
            } else {


                error = new ExceptionBundle(ExceptionBundle.Reason.DATABASE_UNKNOWN);
            }
        } else {
            error = new ExceptionBundle(ExceptionBundle.Reason.INTERNAL_DATA);
        }

        return error;
    }

    protected final <T> Observable<T> wrap(final Producer<T> producer) {
        return Observable.create(e -> {
            if(!e.isDisposed()) {
                try {
                    T data = producer.produce();
                    e.onNext(data);
                    e.onComplete();
                } catch(Exception thr) {
                    e.onError(parseException(thr));
                }
            }
        });
    }

    protected final <T> Observable<T> wrapTransactionSynchronousOff(final Producer<T> producer) {
        return Observable.create(
                e -> {
                    if(!e.isDisposed()) {
                        try {
                            getSQLite().lowLevel().executeSQL(RawQuery.builder().query("PRAGMA synchronous=OFF").build());
                            getSQLite().lowLevel().beginTransaction();

                            T data = producer.produce();

                            getSQLite().lowLevel().setTransactionSuccessful();
                            getSQLite().lowLevel().endTransaction();
                            e.onNext(data);
                            e.onComplete();
                        } catch(Exception thr) {
                            getSQLite().lowLevel().endTransaction();
                            e.onError(parseException(thr));
                        } finally {
                            getSQLite().lowLevel().executeSQL(RawQuery.builder().query("PRAGMA synchronous=NORMAL").build());
                        }
                    }
                }
        );
    }

    protected interface Producer<T> {

        T produce() throws Exception;
    }
}
