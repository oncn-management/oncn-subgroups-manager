package net.styleru.ikomarov.domain_layer.di.level_3.analytics;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 11.05.17.
 */

public interface IAnalyticsComponent {

    @NonNull
    AnalyticsModule analytics();
}
