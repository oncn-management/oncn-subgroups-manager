package net.styleru.ikomarov.domain_layer.di.endpoints.notebooks;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.cache.ICacheComponent;

/**
 * Created by i_komarov on 14.03.17.
 */

public class NotebooksComponent implements INotebooksComponent {

    @NonNull
    private final NotebooksRepositoryModule repository;

    public NotebooksComponent(@NonNull INotebooksEnvironmentComponent environment) {
        this.repository = new NotebooksRepositoryModule(environment);
    }

    @NonNull
    @Override
    public NotebooksRepositoryModule repository() {
        return repository;
    }
}
