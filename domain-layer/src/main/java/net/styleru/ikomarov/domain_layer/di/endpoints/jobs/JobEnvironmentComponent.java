package net.styleru.ikomarov.domain_layer.di.endpoints.jobs;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;

/**
 * Created by i_komarov on 20.04.17.
 */

public class JobEnvironmentComponent implements IJobEnvironmentComponent {

    @NonNull
    private final JobEnvironmentModule environment;

    public JobEnvironmentComponent(@NonNull IAppComponent component) {
        this.environment = new JobEnvironmentModule(component);
    }

    @NonNull
    @Override
    public JobEnvironmentModule environment() {
        return environment;
    }
}
