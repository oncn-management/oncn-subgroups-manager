package net.styleru.ikomarov.data_layer.contracts.user_role;

import net.styleru.ikomarov.data_layer.exceptions.internal.UnknownEnumValueException;

/**
 * Created by i_komarov on 14.02.17.
 */

public enum UserRole {

    OWNER {
        @Override
        public String value() {
            return UserRoleContract.ROLE_OWNER;
        }
    },
    CONTRIBUTOR {
        @Override
        public String value() {
            return UserRoleContract.ROLE_CONTRIBUTOR;
        }
    },
    READER {
        @Override
        public String value() {
            return UserRoleContract.ROLE_READER;
        }
    },
    NONE {
        @Override
        public String value() {
            return UserRoleContract.ROLE_NONE;
        }
    };

    public abstract String value();

    public static UserRole forValue(String value) {
        if(value.equals(UserRoleContract.ROLE_OWNER)) {
            return OWNER;
        } else if(value.equals(UserRoleContract.ROLE_CONTRIBUTOR)) {
            return CONTRIBUTOR;
        } else if(value.equals(UserRoleContract.ROLE_READER)) {
            return READER;
        } else if(value.equals(UserRoleContract.ROLE_NONE)) {
            return NONE;
        } else {
            throw new UnknownEnumValueException(value, UserRole.class.getSimpleName(), "forValue");
        }
    }
}
