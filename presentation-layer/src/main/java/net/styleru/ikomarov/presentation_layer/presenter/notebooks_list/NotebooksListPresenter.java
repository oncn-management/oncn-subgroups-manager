package net.styleru.ikomarov.presentation_layer.presenter.notebooks_list;

import android.support.annotation.NonNull;
import android.util.Log;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.exceptions.external.NetworkException;
import net.styleru.ikomarov.domain_layer.analytics.IScreenAnalyticsManager;
import net.styleru.ikomarov.domain_layer.dto.ClassNotebookDTO;
import net.styleru.ikomarov.data_layer.manager.events.IEventsLogger;
import net.styleru.ikomarov.domain_layer.use_cases.notebooks.NotebooksGetUseCase;
import net.styleru.ikomarov.domain_layer.use_cases.reports.PDFReportCreateUseCase;
import net.styleru.ikomarov.presentation_layer.mapping.notebooks.ClassNotebookMapper;
import net.styleru.ikomarov.presentation_layer.mapping.notebooks.ClassNotebooksMapper;
import net.styleru.ikomarov.presentation_layer.presenter.base.Presenter;
import net.styleru.ikomarov.domain_layer.contracts.errors.IErrorCodeTranslator;
import net.styleru.ikomarov.presentation_layer.view.base.RetainContainer;
import net.styleru.ikomarov.presentation_layer.view.notebooks_list.INotebooksListView;
import net.styleru.ikomarov.presentation_layer.view.notebooks_list.NotebookViewObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 08.03.17.
 */

public class NotebooksListPresenter extends Presenter<INotebooksListView> implements NotebooksGetUseCase.Callbacks<NotebookViewObject>, PDFReportCreateUseCase.Callbacks {

    @NonNull
    private final NotebooksGetUseCase notebooksGet;

    @NonNull
    private final PDFReportCreateUseCase reportCreate;

    @NonNull
    private final IErrorCodeTranslator oneNoteCodeTranslator;

    @NonNull
    private final IScreenAnalyticsManager screenAnalytics;

    @NonNull
    private final IEventsLogger loggingManager;

    @NonNull
    private final Function<ClassNotebookDTO, NotebookViewObject> singleMapper;

    @NonNull
    private final Function<List<ClassNotebookDTO>, List<NotebookViewObject>> listMapper;

    @NonNull
    private final Map<String, UUID> reportQueue;

    public NotebooksListPresenter(@NonNull NotebooksGetUseCase notebooksGet,
                                  @NonNull PDFReportCreateUseCase reportCreate,
                                  @NonNull IErrorCodeTranslator oneNoteCodeTranslator,
                                  @NonNull IScreenAnalyticsManager screenAnalytics,
                                  @NonNull IEventsLogger loggingManager) {
        this.notebooksGet = notebooksGet;
        this.reportCreate = reportCreate;
        this.oneNoteCodeTranslator = oneNoteCodeTranslator;
        this.screenAnalytics = screenAnalytics;
        this.loggingManager = loggingManager;
        this.singleMapper = new ClassNotebookMapper();
        this.listMapper = new ClassNotebooksMapper(singleMapper);
        this.reportQueue = new HashMap<>();
    }

    @Override
    protected void onViewAttached() {
        screenAnalytics.onScreenEvent("List of ClassNotebooks");
    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {
        notebooksGet.dispose();
    }

    public void loadNotebooks(int offset, int limit) {
        notebooksGet.execute(listMapper, this, offset, limit);
    }

    public void loadNotebooks(int offset, int limit, String filterQuery) {
        notebooksGet.execute(listMapper, this, offset, limit, filterQuery);
    }

    public void createReport(String notebookId, boolean delay) {
        if (delay) {
            reportQueue.put(notebookId, UUID.randomUUID());
        } else {
            reportCreate.execute(UUID.randomUUID(), this, notebookId);
        }
    }

    public void onExternalStoragePermissionsGranted() {
        if(reportQueue.size() != 0) {
            for(Map.Entry<String, UUID> entry : reportQueue.entrySet()) {
                reportCreate.execute(entry.getValue(), this, entry.getKey());
            }

            reportQueue.clear();
        }
    }

    public void cancelCurrentUpdates() {
        notebooksGet.dispose();
    }

    @Override
    public void onReportCreated(UUID operationId, String path) {
        postAction((view) -> view.onReportCreated(path));
    }

    @Override
    public void onReportCreationFailed(UUID operationId, ExceptionBundle error) {
        handleException(
                error,
                NotebooksListPresenter.class.getSimpleName(),
                "onReportCreationFailed"
        );
    }

    @Override
    public void onNotebooksLoaded(List<NotebookViewObject> notebooks) {
        if(getView() != null) {
            getView().addNotebooks(notebooks);
        }
    }

    @Override
    public void onNotebooksLoadingFailed(ExceptionBundle error) {
        handleException(
                error,
                NotebooksListPresenter.class.getSimpleName(),
                "onNotebooksLoadingFailed",
                ExceptionBundle.Reason.NETWORK_UNKNOWN,
                ExceptionBundle.Reason.NETWORK_UNAVAILABLE,
                ExceptionBundle.Reason.ONE_NOTE_ERROR
        );
    }

    private void handleException(ExceptionBundle error, String clazz, String method, ExceptionBundle.Reason... ignoreReasons) {
        if(-1 == Arrays.binarySearch(ignoreReasons, error.getReason())) {
            return;
        }

        postAction((view) -> {
            switch (error.getReason()) {
                case NO_CACHED_RESOURCES: {
                    view.showNoCachedDataPlug();
                    break;
                }
                case NO_RESOURCES: {
                    view.showNoDataPlug();
                    break;
                }
                case NO_SESSION: {
                    view.showAuthorizationDialog();
                    break;
                }
                case ONE_NOTE_ERROR: {
                    final int code = error.getIntExtra(ExceptionBundle.OneNoteErrorContract.KEY_CODE);
                    final String message = error.getStringExtra(ExceptionBundle.OneNoteErrorContract.KEY_MESSAGE);
                    view.showError(oneNoteCodeTranslator.getLocalizedMessage(code, message));
                    break;
                }
                case NETWORK_UNAVAILABLE: {
                    view.showConnectionFailedIndicator();
                    break;
                }
                default: {
                    if(error.getReason().isInternalError()) {
                        loggingManager.logEvent(clazz, method, error.getReason(), error.getThrowable(ExceptionBundle.InternalErrorContract.KEY_THROWABLE));
                    } else {
                        loggingManager.logEvent(clazz, method, error.getReason());
                    }
                }
            }
        });
    }

    public static final class Factory implements RetainContainer.Factory<NotebooksListPresenter> {

        @NonNull
        private final NotebooksGetUseCase notebooksGet;

        @NonNull
        private final PDFReportCreateUseCase reportCreate;

        @NonNull
        private final IErrorCodeTranslator translator;

        @NonNull
        private final IScreenAnalyticsManager screenAnalytics;

        @NonNull
        private final IEventsLogger logger;

        public Factory(@NonNull NotebooksGetUseCase notebooksGet,
                       @NonNull PDFReportCreateUseCase reportCreate,
                       @NonNull IErrorCodeTranslator translator,
                       @NonNull IScreenAnalyticsManager screenAnalytics,
                       @NonNull IEventsLogger logger) {

            this.notebooksGet = notebooksGet;
            this.reportCreate = reportCreate;
            this.translator = translator;
            this.screenAnalytics = screenAnalytics;
            this.logger = logger;
        }

        @Override
        public NotebooksListPresenter create() {
            return new NotebooksListPresenter(notebooksGet, reportCreate, translator, screenAnalytics, logger);
        }
    }
}
