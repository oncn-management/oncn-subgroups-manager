package net.styleru.ikomarov.domain_layer.di.endpoints.notebooks;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.services.class_notebooks.ClassNotebooksLocalStorageService;
import net.styleru.ikomarov.data_layer.services.class_notebooks.IClassNotebooksLocalStorageService;
import net.styleru.ikomarov.domain_layer.di.level_2.environment.IEnvironmentComponent;

/**
 * Created by i_komarov on 14.03.17.
 */

public class NotebooksLocalServiceModule {

    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent environment;

    @Nullable
    private volatile IClassNotebooksLocalStorageService service;

    public NotebooksLocalServiceModule(@NonNull IEnvironmentComponent environment) {
        this.environment = environment;
    }

    @NonNull
    public IClassNotebooksLocalStorageService provideService() {
        IClassNotebooksLocalStorageService localInstance = service;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = service;
                if(localInstance == null) {
                    localInstance = service = ClassNotebooksLocalStorageService.Factory.create(environment.database().provideDatabase());
                }
            }
        }

        return localInstance;
    }
}
