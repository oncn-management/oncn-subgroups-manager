package net.styleru.ikomarov.data_layer.mapping.remote.notebooks;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;
import net.styleru.ikomarov.data_layer.entities.remote.notebooks.PrincipalObjectRemoteEntity;

import java.util.List;

/**
 * Created by i_komarov on 14.02.17.
 */

public class PrincipalObjectsRemoteEntityMapping implements IRemoteMapping<PrincipalObjectRemoteEntity> {

    @NonNull
    public static final String FIELD_ID = "id";
    @NonNull
    public static final String FIELD_PRINCIPAL_TYPE = "principalType";
    @NonNull
    public static final String FIELD_NAME = "name";
    @NonNull
    public static final String FIELD_EMAIL = "email";
    @NonNull
    public static final String FIELD_DEPARTMENT = "department";

    private final Gson gson;

    public PrincipalObjectsRemoteEntityMapping() {
        this.gson = new GsonBuilder()
                .registerTypeAdapter(PrincipalObjectRemoteEntity.class, new PrincipalObjectRemoteEntity.Deserializer())
                .registerTypeAdapter(PrincipalObjectRemoteEntity.class, new PrincipalObjectRemoteEntity.Serializer())
                .create();
    }

    @Override
    public PrincipalObjectRemoteEntity fromJson(JsonObject json) {
        return gson.fromJson(json, PrincipalObjectRemoteEntity.class);
    }

    @Override
    public List<PrincipalObjectRemoteEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<PrincipalObjectRemoteEntity>>(){}.getType());
    }

    @Override
    public String toJson(PrincipalObjectRemoteEntity entity) {
        return gson.toJson(entity, PrincipalObjectRemoteEntity.class);
    }

    @Override
    public String toJsonArray(List<PrincipalObjectRemoteEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<PrincipalObjectRemoteEntity>>(){}.getType());
    }
}
