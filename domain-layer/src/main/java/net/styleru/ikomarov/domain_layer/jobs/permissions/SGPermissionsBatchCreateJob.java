package net.styleru.ikomarov.domain_layer.jobs.permissions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import net.styleru.ikomarov.data_layer.contracts.user_role.UserRole;
import net.styleru.ikomarov.domain_layer.jobs.base.IBroadcastFactory;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.repository.permissions.ISGPermissionsRepository;

import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 21.04.17.
 */

public class SGPermissionsBatchCreateJob extends Job {

    private static final int PRIORITY = 1;

    private static final int INITIAL_BACKOFF = 1 * 1000;

    @NonNull
    private final IBroadcastFactory<JobParams> broadcastFactory;

    @NonNull
    private final ISGPermissionsRepository repository;

    @NonNull
    private final JobParams params;

    private SGPermissionsBatchCreateJob(@NonNull IBroadcastFactory<JobParams> broadcastFactory,
                                          @NonNull ISGPermissionsRepository repository,
                                          @NonNull JobParams params) {
        super(new Params(PRIORITY).setRequiresNetwork(true).setPersistent(false));
        this.broadcastFactory = broadcastFactory;
        this.repository = repository;
        this.params = params;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        List<CreateOperationResult> value = Observable.fromIterable(params.getParticipantIds())
                .flatMap(id -> repository.create(UUID.randomUUID(), params.getSectionGroupId(), id, UserRole.CONTRIBUTOR))
                .toList()
                .toObservable()
                .blockingFirst();

        int resultStatus = 0;
        for(CreateOperationResult result : value) {
            resultStatus += result.getStatus().code();
        }

        if(resultStatus == 0) {
            getApplicationContext().sendOrderedBroadcast(broadcastFactory.createBroadcast(params), null);
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.createExponentialBackoff(runCount, INITIAL_BACKOFF);
    }

    public static final class Factory {

        @NonNull
        private final IBroadcastFactory<JobParams> broadcastFactory;

        @NonNull
        private final ISGPermissionsRepository repository;

        public Factory(@NonNull IBroadcastFactory<JobParams> broadcastFactory, @NonNull ISGPermissionsRepository repository) {
            this.broadcastFactory = broadcastFactory;
            this.repository = repository;
        }

        public Job create(JobParams params) {
            return new SGPermissionsBatchCreateJob(broadcastFactory, repository, params);
        }
    }

    public static final class JobParams {

        @NonNull
        private final UUID operationId;

        @NonNull
        private final String sectionGroupId;

        @NonNull
        private final List<String> participantIds;

        public JobParams(@NonNull UUID operationId, @NonNull String sectionGroupId, @NonNull List<String> participantIds) {
            this.operationId = operationId;
            this.sectionGroupId = sectionGroupId;
            this.participantIds = participantIds;
        }

        @NonNull
        public UUID getOperationId() {
            return operationId;
        }

        @NonNull
        public String getSectionGroupId() {
            return sectionGroupId;
        }

        @NonNull
        public List<String> getParticipantIds() {
            return participantIds;
        }
    }
}
