package net.styleru.ikomarov.domain_layer.jobs.section_groups;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.birbit.android.jobqueue.Job;
import com.birbit.android.jobqueue.Params;
import com.birbit.android.jobqueue.RetryConstraint;

import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;
import net.styleru.ikomarov.domain_layer.jobs.base.IBroadcastFactory;
import net.styleru.ikomarov.domain_layer.repository.base.CreateOperationResult;
import net.styleru.ikomarov.domain_layer.repository.section_groups.ISectionGroupsRepository;

import java.util.UUID;

/**
 * Created by i_komarov on 20.04.17.
 */

public class SectionGroupCreateJob extends Job {

    private static final int PRIORITY = 1;

    private static final int INITIAL_BACKOFF = 1 * 1000;

    @NonNull
    private final IBroadcastFactory<JobParams> broadcastFactory;

    @NonNull
    private final ISectionGroupsRepository repository;

    @NonNull
    private final JobParams params;

    private SectionGroupCreateJob(@NonNull IBroadcastFactory<JobParams> broadcastFactory,
                                  @NonNull ISectionGroupsRepository repository,
                                  @NonNull JobParams params) {

        super(new Params(PRIORITY).setRequiresNetwork(true).setPersistent(false));
        this.broadcastFactory = broadcastFactory;
        this.repository = repository;
        this.params = params;
    }

    @Override
    public void onAdded() {

    }

    @Override
    public void onRun() throws Throwable {
        CreateOperationResult result = (params.isNested() ? repository.createNested(params.getId(), params.getNotebookId(), params.getBuilder()) : repository.create(params.getId(), params.getNotebookId(), params.getBuilder())).blockingFirst();
        switch(result.getStatus()) {
            case SUCCESS: {
                params.setResourceId(result.getResourceId());
                getApplicationContext().sendOrderedBroadcast(broadcastFactory.createBroadcast(params), null);
                break;
            }
            case FAILURE: {

                break;
            }
            case DELAYED: {

                break;
            }
        }
    }

    @Override
    protected void onCancel(int cancelReason, @Nullable Throwable throwable) {

    }

    @Override
    protected RetryConstraint shouldReRunOnThrowable(@NonNull Throwable throwable, int runCount, int maxRunCount) {
        return RetryConstraint.createExponentialBackoff(runCount, INITIAL_BACKOFF);
    }

    public static final class Factory {

        @NonNull
        private final ISectionGroupsRepository repository;

        @NonNull
        private final IBroadcastFactory<JobParams> broadcastFactory;

        public Factory(@NonNull IBroadcastFactory<JobParams> broadcastFactory,
                       @NonNull ISectionGroupsRepository repository) {
            this.broadcastFactory = broadcastFactory;
            this.repository = repository;
        }

        public Job create(JobParams params) {
            return new SectionGroupCreateJob(broadcastFactory, repository, params);
        }
    }

    public static final class JobParams {

        @NonNull
        private final UUID id;

        @NonNull
        private final String notebookId;

        @NonNull
        private final SectionGroupDTO.Builder builder;

        @NonNull
        private final boolean isNested;

        @Nullable
        private String resourceId;

        public JobParams(@NonNull UUID id,
                         @NonNull String notebookId,
                         @NonNull SectionGroupDTO.Builder builder,
                         @NonNull Boolean isNested) {

            this.id = id;
            this.notebookId = notebookId;
            this.builder = builder;
            this.isNested = isNested;
        }

        @NonNull
        public UUID getId() {
            return id;
        }

        @NonNull
        public String getNotebookId() {
            return notebookId;
        }

        @NonNull
        public SectionGroupDTO.Builder getBuilder() {
            return builder;
        }

        @NonNull
        public boolean isNested() {
            return isNested;
        }

        @Nullable
        public String getResourceId() {
            return resourceId;
        }

        public void setResourceId(@Nullable String resourceId) {
            this.resourceId = resourceId;
        }
    }
}
