package net.styleru.ikomarov.data_layer.exceptions.external;

import net.styleru.ikomarov.data_layer.contracts.exception.ExceptionSource;
import net.styleru.ikomarov.data_layer.exceptions.ExternalException;

/**
 * Created by i_komarov on 07.03.17.
 */

public class InternalException extends ExternalException {

    private String message;
    private Throwable origin;

    public InternalException(Throwable origin, String message) {
        this.origin = origin;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public Throwable getOrigin() {
        return origin;
    }

    @Override
    public ExceptionSource getSource() {
        return ExceptionSource.INTERNAL;
    }
}
