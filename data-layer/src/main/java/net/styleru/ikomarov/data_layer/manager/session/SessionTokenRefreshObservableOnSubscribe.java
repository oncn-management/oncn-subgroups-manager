package net.styleru.ikomarov.data_layer.manager.session;

import net.styleru.ikomarov.data_layer.entities.session.AzureToken;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.services.session.ISessionService;

import java.net.UnknownHostException;

import javax.net.ssl.SSLHandshakeException;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by i_komarov on 13.03.17.
 */

class SessionTokenRefreshObservableOnSubscribe implements ObservableOnSubscribe<AzureToken> {

    private final ISessionService service;
    private final String apiKey;
    private final String redirectUri;
    private final String refreshToken;
    private final String resourceType;

    SessionTokenRefreshObservableOnSubscribe(ISessionService service, String apiKey, String redirectUri, String refreshToken, String resourceType) {
        this.service = service;
        this.apiKey = apiKey;
        this.redirectUri = redirectUri;
        this.refreshToken = refreshToken;
        this.resourceType = resourceType;
    }

    @Override
    public void subscribe(final ObservableEmitter<AzureToken> emitter) throws Exception {
        service.refreshToken(ISessionService.GRANT_TYPE_REFRESH_TOKEN, apiKey, redirectUri, refreshToken, resourceType)
                .enqueue(new Callback<AzureToken>() {
                    @Override
                    public void onResponse(Call<AzureToken> call, Response<AzureToken> response) {
                        final AzureToken token;

                        if(response.isSuccessful() && (token = response.body()) != null) {
                            if(!emitter.isDisposed()) {
                                emitter.onNext(token);
                            }
                        } else {
                            ExceptionBundle error = new ExceptionBundle(ExceptionBundle.Reason.HTTP_BAD_CODE);
                            error.addIntExtra(ExceptionBundle.HttpErrorContract.KEY_CODE, response.code());
                            error.addStringExtra(ExceptionBundle.HttpErrorContract.KEY_MESSAGE, response.message());

                            if(!emitter.isDisposed()) {
                                emitter.onError(error);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<AzureToken> call, Throwable t) {
                        if(!emitter.isDisposed()) {
                            if (t instanceof UnknownHostException) {
                                emitter.onError(new ExceptionBundle(ExceptionBundle.Reason.NETWORK_UNAVAILABLE));
                            } else if (t instanceof SSLHandshakeException) {
                                emitter.onError(new ExceptionBundle(ExceptionBundle.Reason.NETWORK_SSL_HANDSHAKE_FAILED));
                            } else {
                                ExceptionBundle error = new ExceptionBundle(ExceptionBundle.Reason.INTERNAL_DATA);
                                error.addThrowable(ExceptionBundle.InternalErrorContract.KEY_THROWABLE, t);
                                emitter.onError(error);
                            }
                        }
                    }
                });
    }
}
