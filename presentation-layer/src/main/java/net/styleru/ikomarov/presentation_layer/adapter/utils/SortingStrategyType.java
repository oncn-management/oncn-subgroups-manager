package net.styleru.ikomarov.presentation_layer.adapter.utils;

import android.util.SparseArray;

/**
 * Created by i_komarov on 14.03.17.
 */

public enum SortingStrategyType {
    LAST_MODIFIED_DESCENDANCE(0),
    NAME_ASCENDANCE(1);

    private static final SparseArray<SortingStrategyType> typeMap;

    static {
        typeMap = new SparseArray<>(SortingStrategyType.values().length);
        for(SortingStrategyType type : SortingStrategyType.values()) {
            typeMap.put(type.code, type);
        }
    }

    public static SortingStrategyType forValue(int code) {
        return typeMap.get(code);
    }

    private final int code;

    SortingStrategyType(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
