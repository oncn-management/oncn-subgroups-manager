package net.styleru.ikomarov.domain_layer.mapping.section_groups;

import net.styleru.ikomarov.data_layer.entities.remote.section_groups.SectionGroupRemoteEntity;
import net.styleru.ikomarov.domain_layer.dto.SectionGroupDTO;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 06.03.17.
 */

public class RemoteSectionGroupsMapper implements Function<List<SectionGroupRemoteEntity>, List<SectionGroupDTO>> {

    @Override
    public List<SectionGroupDTO> apply(List<SectionGroupRemoteEntity> sectionGroupRemoteEntities) throws Exception {
        return Observable.fromIterable(sectionGroupRemoteEntities)
                .map(new RemoteSectionGroupMapper())
                .toList()
                .blockingGet();
    }
}
