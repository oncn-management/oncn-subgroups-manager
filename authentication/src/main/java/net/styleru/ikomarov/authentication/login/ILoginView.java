package net.styleru.ikomarov.authentication.login;

/**
 * Created by i_komarov on 18.02.17.
 */

public interface ILoginView {

    void onAccessTokenLoaded(String displayName, String accessToken, String refreshToken);
}
